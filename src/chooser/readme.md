# About

isr.IO.OL.DeviceChooser is a .Net library supporting open layers data acquisition device chooser.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

isr.IO.OL.DeviceChooser is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Open Layers Repository].

[Open Layers Repository]: https://bitbucket.org/davidhary/dn.openlayers

