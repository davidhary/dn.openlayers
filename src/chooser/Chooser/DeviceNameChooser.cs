using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;

namespace isr.IO.OL.DeviceChooser
{
    /// <summary>
    /// Selects a device name.
    /// </summary>
    /// <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para></remarks>
    [Description( "Open Layers Device Name Chooser Control" )]
    [DefaultEvent( "DeviceSelected" )]
    [ToolboxBitmap( "DeviceNameChooser.bmp" )]
    public partial class DeviceNameChooser : System.Windows.Forms.UserControl
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        public DeviceNameChooser() : base()
        {
            this.InitializeComponent();
        }

        /// <summary>   UserControl overrides dispose to clean up the component list. </summary>
        /// <remarks>   David, 2022-03-03. </remarks>
        /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
        ///                             resources; <see langword="false" /> to release only unmanaged
        ///                             resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( disposing )
                {
                    if ( DevicesLocated is object )
                    {
                        foreach ( Delegate d in DevicesLocated.GetInvocationList() )
                            DevicesLocated -= ( EventHandler<EventArgs> ) d;
                    }

                    if ( DeviceSelected is object )
                    {
                        foreach ( Delegate d in DeviceSelected.GetInvocationList() )
                            DeviceSelected -= ( EventHandler<EventArgs> ) d;
                    }

                    this.components?.Dispose();
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " METHODS, PROPERTIES, EVENTS "

        /// <summary>
        /// Returns the selected device name.
        /// </summary>
        /// <value>The name of the device.</value>
        public string DeviceName => this._NamesComboBox.Text;

        /// <summary>
        /// Updates the device name list and raises the device located event if devices where found.
        /// </summary>
        /// <remarks>Call this method from the form load method to set the user interface.</remarks>
        public void RefreshDeviceNamesList()
        {
            if ( OpenLayers.Base.DeviceMgr.Get().HardwareAvailable() )
            {
                this._NamesComboBox.DataSource = OpenLayers.Base.DeviceMgr.Get().GetDeviceNames();
                this.OnDevicesLocated( new EventArgs() );
            }
            else
            {
                this._NamesComboBox.DataSource = Array.Empty<string>();
            }
        }

        #endregion

        #region " EVENTS "

        /// <summary>
        /// Notifies of the location of device.
        /// </summary>
        public event EventHandler<EventArgs> DevicesLocated;

        /// <summary>
        /// Raises the device located event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void OnDevicesLocated( EventArgs e )
        {
            DevicesLocated?.Invoke( this, e );
        }

        /// <summary>
        /// Notifies of the selected of a device.
        /// </summary>
        public event EventHandler<EventArgs> DeviceSelected;

        /// <summary>
        /// Raises the Device Selected event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected void OnDeviceNameSelected( EventArgs e )
        {
            DeviceSelected?.Invoke( this, e );
        }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary>
        /// Handles the SelectedIndexChanged event of the availableDeviceNamesComboBox control.
        /// Selects a device.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
        private void AvailableDeviceNamesComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( this._NamesComboBox.SelectedItem is object )
            {
                this._ToolTip.SetToolTip( this._NamesComboBox, this._NamesComboBox.SelectedItem.ToString() );
                this.OnDeviceNameSelected( new EventArgs() );
            }
        }

        /// <summary>
        /// Handles the Click event of the findButton control.
        /// Looks for devices.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
        private void FindButton_Click( object sender, EventArgs e )
        {
            this.RefreshDeviceNamesList();
        }


        #endregion

    }
}
