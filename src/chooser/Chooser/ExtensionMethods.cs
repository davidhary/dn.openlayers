using System;
using System.Linq;

namespace isr.IO.OL.DeviceChooser
{
    /// <summary>
    /// Extends the Data Translation Open Layers <see cref="OpenLayers.Base.SubsystemBase">subsystem base</see>,
    /// <see cref="OpenLayers.Base.Device">Device</see>,
    /// <see cref="OpenLayers.Base.ChannelList">Channel List</see>, and 
    /// <see cref="OpenLayers.Signals.MemorySignalList">Signal List</see> functionality.
    /// </summary>
    /// <remarks> (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para></remarks>
    public static class ExtensionMethods
    {

        #region " DEVICE MANAGER "

        /// <summary> Returns true if the device exists.  This permits detecting if the device failed to
        /// connect such as the case after waking up from system sleep. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="manager">    The device manager. </param>
        /// <param name="deviceName"> Specifies the device name. </param>
        /// <returns> <c>True</c> if a device with the specified name exists; otherwise, <c>False</c>. </returns>
        public static bool Exists( this OpenLayers.Base.DeviceMgr manager, string deviceName )
        {
            return manager is not object
                ? throw new ArgumentNullException( nameof( manager ) )
                : string.IsNullOrWhiteSpace( deviceName )
                ? throw new ArgumentNullException( nameof( deviceName ) )
                : manager.HardwareAvailable() && manager.GetDeviceNames().Contains( deviceName, StringComparer.CurrentCultureIgnoreCase );
        }

        /// <summary> Selects and returns an open layers device.  If the system has multiple board, this
        /// would allow the operator to select a board from a list. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="manager">    The device manager. </param>
        /// <param name="deviceName"> Specifies the device name. </param>
        /// <returns> OpenLayers.Base.Device. </returns>
        public static OpenLayers.Base.Device SelectDevice( this OpenLayers.Base.DeviceMgr manager, string deviceName )
        {
            return manager is not object
                ? throw new ArgumentNullException( nameof( manager ) )
                : string.IsNullOrWhiteSpace( deviceName )
                ? throw new ArgumentNullException( nameof( deviceName ) )
                : manager.Exists( deviceName ) ? manager.GetDevice( deviceName ) : null;
        }

        /// <summary> Selects and returns an open layers device.  If the system has multiple board, this
        /// would allow the operator to select a board from a list. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="manager"> The device manager. </param>
        /// <returns> OpenLayers.Base.Device or nothing if no hardware or no such device. </returns>
        public static OpenLayers.Base.Device SelectDevice( this OpenLayers.Base.DeviceMgr manager )
        {
            return manager is not object
                ? throw new ArgumentNullException( nameof( manager ) )
                : manager.HardwareAvailable() ? manager.SelectDevice( DeviceChooser.SelectDeviceName() ) : null;
        }

        #endregion

    }
}
