using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace isr.IO.OL.DeviceChooser
{
    public partial class DeviceNameChooser
    {

        // Required by the Windows Form Designer
        private IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new Container();
            var resources = new ComponentResourceManager(typeof(DeviceNameChooser));
            _FindButton = new Button();
            _NamesComboBox = new ComboBox();
            _ToolTip = new ToolTip(components);
            SuspendLayout();
            // 
            // _FindButton
            // 
            _FindButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _FindButton.Image = (Image)resources.GetObject("_FindButton.Image");
            _FindButton.Location = new Point(207, 1);
            _FindButton.Name = "_FindButton";
            _FindButton.Size = new Size(29, 25);
            _FindButton.TabIndex = 3;
            _FindButton.Click += new EventHandler( FindButton_Click );
            // 
            // _NamesComboBox
            // 
            _NamesComboBox.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            _NamesComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            _NamesComboBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold);
            _NamesComboBox.Location = new Point(1, 1);
            _NamesComboBox.Name = "_NamesComboBox";
            _NamesComboBox.Size = new Size(205, 25);
            _NamesComboBox.TabIndex = 2;
            _NamesComboBox.SelectedIndexChanged += new EventHandler( AvailableDeviceNamesComboBox_SelectedIndexChanged );
            // 
            // DeviceNameChooser
            // 
            Controls.Add(_FindButton);
            Controls.Add(_NamesComboBox);
            Name = "DeviceNameChooser";
            Size = new Size(235, 28);
            ResumeLayout(false);
        }

        private Button _FindButton;
        private ComboBox _NamesComboBox;
        private ToolTip _ToolTip;
    }
}
