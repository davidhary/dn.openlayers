using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace isr.IO.OL.SingleIO
{

    /// <summary> Form for viewing the spectrum panels. </summary>
    /// <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 5/6/2019 </para></remarks>
    public class SingleInputOutputForm : Form
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        public SingleInputOutputForm() : base()
        {
        }

        #endregion

        #region " MAIN "

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private static void Main()
        {
            try
            {
                Application.Run( new SingleInputOutputForm() );
            }
            catch ( Exception ex )
            {
                _ = MessageBox.Show( ex.Message.ToString(), "Error" );
            }
        }

        #endregion

        #region " FORM EVENTS "

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.Closing" /> event. Releases all publishers.
        /// </summary>
        /// <remarks>   David, 2022-01-11. </remarks>
        /// <param name="e">    A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains
        ///                     the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnClosing( System.ComponentModel.CancelEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = "closing";
                this.Cursor = Cursors.WaitCursor;
            }
            catch ( Exception ex )
            {
                _ = MessageBox.Show( ex.Message.ToString(), $"Exception {activity}" );
            }
            finally
            {
                this.Cursor = Cursors.Default;
                base.OnClosing( e );
            }
        }

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
        /// </summary>
        /// <remarks>   David, 2022-01-11. </remarks>
        /// <param name="e">    An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnLoad( EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Name} adding single I/O view";
                var view = new WinViews.SingleInputOutputView();
                this.Controls.Add( view );
                view.Dock = DockStyle.Fill;
            }
            catch ( Exception ex )
            {
                _ = MessageBox.Show( ex.Message.ToString(), $"Exception {activity}" );
            }
            finally
            {
                base.OnLoad( e );
            }
        }

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Shown" /> event.
        /// </summary>
        /// <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnShown( EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                activity = $"{this.Name} showing dialog";
                base.OnShown( e );
            }
            catch ( Exception ex )
            {
                _ = MessageBox.Show( ex.Message.ToString(), $"Exception {activity}" );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion
    }
}
