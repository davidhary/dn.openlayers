using System.Drawing;
using System.Security.Permissions;
using System.Windows.Forms;

namespace isr.IO.OL.Scope.Demo
{

    /// <summary> Form with drop shadow. </summary>
    /// <remarks> (c) 2007 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 08/13/2007" by="Nicholas Seward" revision="1.0.2781.x"> http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx. 
    /// David, 08/13/2007, 1.0.2781.x">      Convert from C#. </para></remarks>
    public partial class FormBase : Form
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Specialized default constructor for use only by derived classes. </summary>
        protected FormBase() : base()
        {
            this.InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            this.AutoScaleDimensions = new SizeF( 7.0f, 17.0f );
            this.AutoScaleMode = AutoScaleMode.Font;
            this.ClientSize = new Size( 331, 341 );
            this.Font = new Font( SystemFonts.MessageBoxFont.FontFamily, 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0 );
            this.Name = "FormBase";
            this.ResumeLayout( false );
        }

        #region " DROP SHADOW "

        /// <summary>
        /// Defines the Drop Shadow constant.
        /// </summary>
        private const int _CS_DROPSHADOW = 131072;

        /// <summary> Adds a drop shadow parameter. </summary>
        /// <remarks> From Code Project: http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx. </remarks>
        /// <value> Options that control the create. </value>
        protected override CreateParams CreateParams
        {
            [SecurityPermission( SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode )]
            get {
                var cp = base.CreateParams;
                cp.ClassStyle |= _CS_DROPSHADOW;
                return cp;
            }
        }

        #endregion

        #endregion

    }
}
