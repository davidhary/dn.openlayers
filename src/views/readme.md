# About

isr.IO.OL.WinViews is a .Net library providing custom Windows Control views for open layers data acquisition subsystems.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

isr.IO.OL.WinViews is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Open Layers Repository].

[Open Layers Repository]: https://bitbucket.org/davidhary/dn.openlayers

