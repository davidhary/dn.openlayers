using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace isr.IO.OL.WinViews
{
    public partial class SignalView
    {

        // Required by the Windows Form Designer
        private IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new Container();
            _ColorDialog = new ColorDialog();
            _Display = new OpenLayers.Controls.Display();
            _RefreshTimeLabel = new Label();
            _RampCheckBox = new CheckBox();
            _PrintButton = new Button();
            _TimeLabel = new Label();
            _YMax2Label = new Label();
            _YMin2Label = new Label();
            _AutoScaleButton = new Button();
            _ColorGridsButton = new Button();
            _Seperator2 = new Panel();
            _Seperator1 = new Panel();
            _ColorAxesButton = new Button();
            _ColorDataButton = new Button();
            _SineWaveCheckBox = new CheckBox();
            _SquareWaveCheckBox = new CheckBox();
            _XMinTrackBar = new TrackBar();
            _XMaxLabel = new Label();
            _XMinLabel = new Label();
            _YAxisLabel = new Label();
            _MultipleRadioButton = new RadioButton();
            _SingleRadioButton = new RadioButton();
            _XAxisLabel = new Label();
            _BandModeGroupBox = new GroupBox();
            _YMinTrackBar = new TrackBar();
            _YMax1Label = new Label();
            _YMin1Label = new Label();
            _SignalListComboBox = new ComboBox();
            _YMaxTrackBar = new TrackBar();
            _XMaxTrackBar = new TrackBar();
            _CreateDataTimer = new Timer(components);
            ((ISupportInitialize)_Display).BeginInit();
            ((ISupportInitialize)_XMinTrackBar).BeginInit();
            _BandModeGroupBox.SuspendLayout();
            ((ISupportInitialize)_YMinTrackBar).BeginInit();
            ((ISupportInitialize)_YMaxTrackBar).BeginInit();
            ((ISupportInitialize)_XMaxTrackBar).BeginInit();
            SuspendLayout();
            // 
            // _Display
            // 
            _Display.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;

            _Display.AutoScale = false;
            _Display.AxesColor = Color.Blue;
            _Display.BackGradientAngle = 90.0f;
            _Display.BackGradientColor = Color.Chartreuse;
            _Display.BackgroundStyle = OpenLayers.Chart.Layers.BackgroundStyle.Gradient;
            _Display.BandMode = OpenLayers.Controls.BandMode.SingleBand;
            _Display.Footer = string.Empty;
            _Display.FooterFont = new Font(Font.FontFamily, 8.0f);
            _Display.GridColor = Color.Blue;
            _Display.Location = new Point(141, 13);
            _Display.Name = "_Display";
            _Display.SignalBufferLength = 0;
            _Display.Size = new Size(604, 362);
            _Display.TabIndex = 76;
            _Display.Title = "Chart";
            _Display.TitleFont = new Font(Font.FontFamily, 10.0f, FontStyle.Bold | FontStyle.Underline);
            _Display.XDataCurrentRangeMax = 1000.0d;
            _Display.XDataCurrentRangeMin = 0.0d;
            _Display.XDataName = "Time";
            _Display.XDataRangeMax = 1000.0d;
            _Display.XDataRangeMin = 0.0d;
            _Display.XDataUnit = "sec";
            // 
            // _RefreshTimeLabel
            // 
            _RefreshTimeLabel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            _RefreshTimeLabel.AutoSize = true;
            _RefreshTimeLabel.Location = new Point(514, 378);
            _RefreshTimeLabel.Name = "_RefreshTimeLabel";
            _RefreshTimeLabel.Size = new Size(132, 17);
            _RefreshTimeLabel.TabIndex = 75;
            _RefreshTimeLabel.Text = "Signals Refresh Time:";
            _RefreshTimeLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _RampCheckBox
            // 
            _RampCheckBox.AutoSize = true;
            _RampCheckBox.Location = new Point(16, 348);
            _RampCheckBox.Name = "_RampCheckBox";
            _RampCheckBox.Size = new Size(97, 21);
            _RampCheckBox.TabIndex = 74;
            _RampCheckBox.Text = "Ramp Wave";
            _RampCheckBox.CheckedChanged += new EventHandler( RampCheckBox_CheckedChanged );
            // 
            // _PrintButton
            // 
            _PrintButton.Location = new Point(24, 261);
            _PrintButton.Name = "_PrintButton";
            _PrintButton.Size = new Size(72, 32);
            _PrintButton.TabIndex = 73;
            _PrintButton.Text = "Print";
            _PrintButton.Click += new EventHandler( PrintButton_Click );
            // 
            // _TimeLabel
            // 
            _TimeLabel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            _TimeLabel.Location = new Point(652, 378);
            _TimeLabel.Name = "_TimeLabel";
            _TimeLabel.Size = new Size(93, 16);
            _TimeLabel.TabIndex = 72;
            // 
            // _YMax2Label
            // 
            _YMax2Label.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _YMax2Label.AutoSize = true;
            _YMax2Label.Cursor = Cursors.Default;
            _YMax2Label.Location = new Point(434, 501);
            _YMax2Label.Name = "_YMax2Label";
            _YMax2Label.Size = new Size(60, 17);
            _YMax2Label.TabIndex = 71;
            _YMax2Label.Text = "(0...1500)";
            // 
            // _YMin2Label
            // 
            _YMin2Label.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _YMin2Label.AutoSize = true;
            _YMin2Label.Location = new Point(153, 501);
            _YMin2Label.Name = "_YMin2Label";
            _YMin2Label.Size = new Size(79, 17);
            _YMin2Label.TabIndex = 70;
            _YMin2Label.Text = "(-500...1000)";
            // 
            // _AutoScaleButton
            // 
            _AutoScaleButton.Location = new Point(24, 213);
            _AutoScaleButton.Name = "_AutoScaleButton";
            _AutoScaleButton.Size = new Size(72, 32);
            _AutoScaleButton.TabIndex = 60;
            _AutoScaleButton.Text = "Auto Scale";
            _AutoScaleButton.Click += new EventHandler( AutoScaleButton_Click );
            // 
            // _ColorGridsButton
            // 
            _ColorGridsButton.Location = new Point(24, 165);
            _ColorGridsButton.Name = "_ColorGridsButton";
            _ColorGridsButton.Size = new Size(72, 32);
            _ColorGridsButton.TabIndex = 59;
            _ColorGridsButton.Text = "Grids color";
            _ColorGridsButton.Click += new EventHandler( ColorGridsButton_Click );
            // 
            // _Seperator2
            // 
            _Seperator2.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            _Seperator2.BackColor = SystemColors.Control;
            _Seperator2.BorderStyle = BorderStyle.FixedSingle;
            _Seperator2.ForeColor = SystemColors.ControlText;
            _Seperator2.Location = new Point(0, 411);
            _Seperator2.Name = "_Seperator2";
            _Seperator2.Size = new Size(758, 1);
            _Seperator2.TabIndex = 69;
            // 
            // _Seperator1
            // 
            _Seperator1.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            _Seperator1.BackColor = SystemColors.Control;
            _Seperator1.BorderStyle = BorderStyle.FixedSingle;
            _Seperator1.ForeColor = SystemColors.ControlText;
            _Seperator1.Location = new Point(11, 469);
            _Seperator1.Name = "_Seperator1";
            _Seperator1.Size = new Size(758, 1);
            _Seperator1.TabIndex = 68;
            // 
            // _ColorAxesButton
            // 
            _ColorAxesButton.Location = new Point(24, 117);
            _ColorAxesButton.Name = "_ColorAxesButton";
            _ColorAxesButton.Size = new Size(72, 32);
            _ColorAxesButton.TabIndex = 58;
            _ColorAxesButton.Text = "Axes color";
            _ColorAxesButton.Click += new EventHandler( ColorAxesButton_Click );
            // 
            // _ColorDataButton
            // 
            _ColorDataButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _ColorDataButton.Location = new Point(673, 485);
            _ColorDataButton.Name = "_ColorDataButton";
            _ColorDataButton.Size = new Size(72, 31);
            _ColorDataButton.TabIndex = 57;
            _ColorDataButton.Text = "Signal color";
            _ColorDataButton.Click += new EventHandler( ColorDataButton_Click );
            // 
            // _SineWaveCheckBox
            // 
            _SineWaveCheckBox.AutoSize = true;
            _SineWaveCheckBox.Location = new Point(16, 315);
            _SineWaveCheckBox.Name = "_SineWaveCheckBox";
            _SineWaveCheckBox.Size = new Size(87, 21);
            _SineWaveCheckBox.TabIndex = 50;
            _SineWaveCheckBox.Text = "Sine Wave";
            _SineWaveCheckBox.CheckedChanged += new EventHandler( SineWaveCheckBox_CheckedChanged );
            _SquareWaveCheckBox.CheckedChanged += new EventHandler( SquareWaveCheckBox_CheckedChanged );
            // 
            // _SquareWaveCheckBox
            // 
            _SquareWaveCheckBox.AutoSize = true;
            _SquareWaveCheckBox.Location = new Point(16, 381);
            _SquareWaveCheckBox.Name = "_SquareWaveCheckBox";
            _SquareWaveCheckBox.Size = new Size(104, 21);
            _SquareWaveCheckBox.TabIndex = 51;
            _SquareWaveCheckBox.Text = "Square Wave";
            // 
            // _XMinTrackBar
            // 
            _XMinTrackBar.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _XMinTrackBar.LargeChange = 100;
            _XMinTrackBar.Location = new Point(236, 416);
            _XMinTrackBar.Maximum = 990;
            _XMinTrackBar.Name = "_XMinTrackBar";
            _XMinTrackBar.Size = new Size(160, 45);
            _XMinTrackBar.SmallChange = 10;
            _XMinTrackBar.TabIndex = 52;
            _XMinTrackBar.TickFrequency = 100;
            _XMinTrackBar.Scroll += new EventHandler( XMinTrackBar_Scroll );
            // 
            // _XMaxLabel
            // 
            _XMaxLabel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _XMaxLabel.AutoSize = true;
            _XMaxLabel.Location = new Point(406, 430);
            _XMaxLabel.Name = "_XMaxLabel";
            _XMaxLabel.Size = new Size(88, 17);
            _XMaxLabel.TabIndex = 65;
            _XMaxLabel.Text = "max (0...1000)";
            // 
            // _XMinLabel
            // 
            _XMinLabel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _XMinLabel.AutoSize = true;
            _XMinLabel.Location = new Point(147, 430);
            _XMinLabel.Name = "_XMinLabel";
            _XMinLabel.Size = new Size(85, 17);
            _XMinLabel.TabIndex = 64;
            _XMinLabel.Text = "min (0...1000)";
            // 
            // _YAxisLabel
            // 
            _YAxisLabel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _YAxisLabel.AutoSize = true;
            _YAxisLabel.Location = new Point(8, 492);
            _YAxisLabel.Name = "_YAxisLabel";
            _YAxisLabel.Size = new Size(46, 17);
            _YAxisLabel.TabIndex = 63;
            _YAxisLabel.Text = "Y-Axis:";
            // 
            // _MultipleRadioButton
            // 
            _MultipleRadioButton.AutoSize = true;
            _MultipleRadioButton.Location = new Point(16, 56);
            _MultipleRadioButton.Name = "_MultipleRadioButton";
            _MultipleRadioButton.Size = new Size(73, 21);
            _MultipleRadioButton.TabIndex = 1;
            _MultipleRadioButton.Text = "Multiple";
            // 
            // _SingleRadioButton
            // 
            _SingleRadioButton.AutoSize = true;
            _SingleRadioButton.Checked = true;
            _SingleRadioButton.Location = new Point(16, 24);
            _SingleRadioButton.Name = "_SingleRadioButton";
            _SingleRadioButton.Size = new Size(61, 21);
            _SingleRadioButton.TabIndex = 0;
            _SingleRadioButton.TabStop = true;
            _SingleRadioButton.Text = "Single";
            _SingleRadioButton.CheckedChanged += new EventHandler( SingleRadioButton_CheckedChanged );
            // 
            // _XAxisLabel
            // 
            _XAxisLabel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _XAxisLabel.AutoSize = true;
            _XAxisLabel.Location = new Point(8, 430);
            _XAxisLabel.Name = "_XAxisLabel";
            _XAxisLabel.Size = new Size(47, 17);
            _XAxisLabel.TabIndex = 62;
            _XAxisLabel.Text = "X-Axis:";
            // 
            // _BandModeGroupBox
            // 
            _BandModeGroupBox.Controls.Add(_MultipleRadioButton);
            _BandModeGroupBox.Controls.Add(_SingleRadioButton);
            _BandModeGroupBox.Location = new Point(16, 5);
            _BandModeGroupBox.Name = "_BandModeGroupBox";
            _BandModeGroupBox.Size = new Size(104, 92);
            _BandModeGroupBox.TabIndex = 61;
            _BandModeGroupBox.TabStop = false;
            _BandModeGroupBox.Text = "Band mode";
            // 
            // _YMinTrackBar
            // 
            _YMinTrackBar.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _YMinTrackBar.LargeChange = 100;
            _YMinTrackBar.Location = new Point(236, 477);
            _YMinTrackBar.Maximum = 1490;
            _YMinTrackBar.Minimum = -500;
            _YMinTrackBar.Name = "_YMinTrackBar";
            _YMinTrackBar.Size = new Size(160, 45);
            _YMinTrackBar.SmallChange = 10;
            _YMinTrackBar.TabIndex = 55;
            _YMinTrackBar.TickFrequency = 100;
            _YMinTrackBar.Scroll += new EventHandler( YMinTrackBar_Scroll );
            // 
            // _YMax1Label
            // 
            _YMax1Label.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _YMax1Label.AutoSize = true;
            _YMax1Label.Location = new Point(462, 478);
            _YMax1Label.Name = "_YMax1Label";
            _YMax1Label.Size = new Size(32, 17);
            _YMax1Label.TabIndex = 67;
            _YMax1Label.Text = "max";
            // 
            // _YMin1Label
            // 
            _YMin1Label.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _YMin1Label.AutoSize = true;
            _YMin1Label.Location = new Point(203, 478);
            _YMin1Label.Name = "_YMin1Label";
            _YMin1Label.Size = new Size(29, 17);
            _YMin1Label.TabIndex = 66;
            _YMin1Label.Text = "min";
            // 
            // _SignalListComboBox
            // 
            _SignalListComboBox.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _SignalListComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            _SignalListComboBox.Location = new Point(61, 488);
            _SignalListComboBox.Name = "_SignalListComboBox";
            _SignalListComboBox.Size = new Size(80, 25);
            _SignalListComboBox.TabIndex = 54;
            _SignalListComboBox.SelectedValueChanged += new EventHandler( SignalListComboBox_SelectedValueChanged );
            // 
            // _YMaxTrackBar
            // 
            _YMaxTrackBar.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _YMaxTrackBar.LargeChange = 100;
            _YMaxTrackBar.Location = new Point(500, 477);
            _YMaxTrackBar.Maximum = 1500;
            _YMaxTrackBar.Minimum = 10;
            _YMaxTrackBar.Name = "_YMaxTrackBar";
            _YMaxTrackBar.Size = new Size(160, 45);
            _YMaxTrackBar.SmallChange = 10;
            _YMaxTrackBar.TabIndex = 56;
            _YMaxTrackBar.TickFrequency = 100;
            _YMaxTrackBar.Value = 1000;
            _YMaxTrackBar.Scroll += new EventHandler( YMaxTrackBar_Scroll );
            // 
            // _XMaxTrackBar
            // 
            _XMaxTrackBar.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _XMaxTrackBar.LargeChange = 100;
            _XMaxTrackBar.Location = new Point(500, 416);
            _XMaxTrackBar.Maximum = 1000;
            _XMaxTrackBar.Minimum = 10;
            _XMaxTrackBar.Name = "_XMaxTrackBar";
            _XMaxTrackBar.Size = new Size(160, 45);
            _XMaxTrackBar.SmallChange = 10;
            _XMaxTrackBar.TabIndex = 53;
            _XMaxTrackBar.TickFrequency = 100;
            _XMaxTrackBar.Value = 1000;
            _XMaxTrackBar.Scroll += new EventHandler( XMaxTrackBar_Scroll );
            // 
            // _CreateDataTimer
            // 
            _CreateDataTimer.Interval = 1000;
            _CreateDataTimer.Tick += new EventHandler( CreateDataTimer_Tick );
            // 
            // SignalDisplay
            // 
            ClientSize = new Size(758, 531);
            Controls.Add(_Display);
            Controls.Add(_RefreshTimeLabel);
            Controls.Add(_RampCheckBox);
            Controls.Add(_PrintButton);
            Controls.Add(_TimeLabel);
            Controls.Add(_YMax2Label);
            Controls.Add(_YMin2Label);
            Controls.Add(_AutoScaleButton);
            Controls.Add(_ColorGridsButton);
            Controls.Add(_Seperator2);
            Controls.Add(_Seperator1);
            Controls.Add(_ColorAxesButton);
            Controls.Add(_ColorDataButton);
            Controls.Add(_SineWaveCheckBox);
            Controls.Add(_SquareWaveCheckBox);
            Controls.Add(_XMinTrackBar);
            Controls.Add(_XMaxLabel);
            Controls.Add(_XMinLabel);
            Controls.Add(_YAxisLabel);
            Controls.Add(_XAxisLabel);
            Controls.Add(_BandModeGroupBox);
            Controls.Add(_YMinTrackBar);
            Controls.Add(_YMax1Label);
            Controls.Add(_YMin1Label);
            Controls.Add(_SignalListComboBox);
            Controls.Add(_YMaxTrackBar);
            Controls.Add(_XMaxTrackBar);
            Name = "SignalDisplay";
            Text = "Signal Display";
            ((ISupportInitialize)_Display).EndInit();
            ((ISupportInitialize)_XMinTrackBar).EndInit();
            _BandModeGroupBox.ResumeLayout(false);
            _BandModeGroupBox.PerformLayout();
            ((ISupportInitialize)_YMinTrackBar).EndInit();
            ((ISupportInitialize)_YMaxTrackBar).EndInit();
            ((ISupportInitialize)_XMaxTrackBar).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        private ColorDialog _ColorDialog;
        private OpenLayers.Controls.Display _Display;
        private Label _RefreshTimeLabel;
        private CheckBox _RampCheckBox;
        private Button _PrintButton;
        private Label _TimeLabel;
        private Label _YMax2Label;
        private Label _YMin2Label;
        private Button _AutoScaleButton;
        private Button _ColorGridsButton;
        private Panel _Seperator2;
        private Panel _Seperator1;
        private Button _ColorAxesButton;
        private Button _ColorDataButton;
        private CheckBox _SineWaveCheckBox;
        private CheckBox _SquareWaveCheckBox;
        private TrackBar _XMinTrackBar;
        private Label _XMaxLabel;
        private Label _XMinLabel;
        private Label _YAxisLabel;
        private RadioButton _MultipleRadioButton;
        private RadioButton _SingleRadioButton;
        private Label _XAxisLabel;
        private GroupBox _BandModeGroupBox;
        private TrackBar _YMinTrackBar;
        private Label _YMax1Label;
        private Label _YMin1Label;
        private ComboBox _SignalListComboBox;
        private TrackBar _YMaxTrackBar;
        private TrackBar _XMaxTrackBar;
        private Timer _CreateDataTimer;
    }
}
