using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using isr.IO.OL.Display;

namespace isr.IO.OL.WinViews
{
    /// <summary> A control panel for single I/O. </summary>
    /// <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para></remarks>
    public partial class SingleInputOutputView : UserControl, INotifyPropertyChanged
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        public SingleInputOutputView() : base()
        {
            // This method is required by the Windows Form Designer.
            this.InitializeComponent();
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        ///                          release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( disposing )
                {

                    // disable the timer
                    this._UpdateIOTimer.Enabled = false;
                    Application.DoEvents();
                    if ( this.DigitalInInternal is object )
                    {
                        this.DigitalInInternal.Dispose();
                        this.DigitalInInternal = null;
                    }

                    if ( this.DigitalOutInternal is object )
                    {
                        this.DigitalOutInternal.Dispose();
                        this.DigitalOutInternal = null;
                    }

                    if ( this.AnalogInputInternal is object )
                    {
                        this.AnalogInputInternal.Dispose();
                        this.AnalogInputInternal = null;
                    }

                    if ( this.AnalogOutputInternal is object )
                    {
                        this.AnalogOutputInternal.Dispose();
                        this.AnalogOutputInternal = null;
                    }

                    this.components?.Dispose();                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " I NOTIFY PROPERTY CHAGNED "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Synchronously notify property changed described by propertyName. </summary>
        /// <remarks>   David, 2021-02-25. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        #endregion


        #region " METHODS "

        /// <summary> Gets or sets a value indicating whether this instance is open. </summary>
        /// <value> <c>True</c> if this instance is open; otherwise, <c>False</c>. </value>
        public bool IsOpen => this.Device is object;

        /// <summary> Gets or sets the default device. </summary>
        /// <value> The device. </value>
        private OpenLayers.Base.Device Device { get; set; }

        private string _BoardName;
        /// <summary> Gets or sets the default board name. </summary>
        /// <value> The name of the board. </value>
        public string BoardName
        {
            get => this._BoardName;

            set {
                this._BoardName = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> Gets or sets the caption. </summary>
        /// <value> The caption. </value>
        public string Caption
        {
            get => this.Text;

            set {
                if ( !string.Equals( value, this.Caption ) )
                {
                    this.Text = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> opens access to the open layers device. </summary>
        /// <remarks> Use this method to open the driver in real or demo modes. </remarks>
        /// <param name="deviceName"> Name of the device. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void OpenIO( string deviceName )
        {

            // indicate that the device is not open.
            if ( this.Device is object )
            {
                this.Device.Dispose();
                this.Device = null;
            }

            if ( string.IsNullOrWhiteSpace( deviceName ) )
            {
                this.BoardMessage = "Board not connected.";
                return;
            }

            // clear the board name.
            this.BoardName = string.Empty;
            try
            {

                // get a board 
                this.ApplicationMessage = "Opening I/O Board " + deviceName + "...";
                this.BoardMessage = "Opening I/O Board " + deviceName + "...";
                this.Device = OpenLayers.Base.DeviceMgr.Get().SelectDevice( deviceName );
                this._UpdateIOButton.Enabled = this.Device is object;
                this._AutoUpdateCheckBox.Enabled = this.Device is object;
                if ( this.Device is object )
                {
                    this.BoardName = this.Device.DeviceName;
                    this.ApplicationMessage = "Opened I/O Board " + this.BoardName;
                    this.BoardMessage = "Opened I/O Board " + this.BoardName;
                    this.Text = System.Reflection.Assembly.GetExecutingAssembly().BuildProductTimeCaption(3, null, null)
                                + $": {this.BoardName}: SINGLE I/O ";

                    // open the sub systems
                    this._DigitalInputCheckBox.Enabled = false;
                    if ( this.DigitalInInternal is object )
                    {
                        this.DigitalInInternal.Dispose();
                    }

                    if ( this.Device.GetNumSubsystemElements( OpenLayers.Base.SubsystemType.DigitalInput ) > 0 )
                    {
                        this.ApplicationMessage = "Opening digital input...";
                        this.DigitalInInternal = new DigitalInputSubsystem( this.Device, 0 );
                        this.ApplicationMessage = "Digital input opened.";
                        if ( this.DigitalInInternal is object )
                        {
                            this.ApplicationMessage = "Configuring digital input...";
                            this.DigitalInInternal.Configure( OpenLayers.Base.DataFlow.SingleValue );
                            this._DigitalIoGroupBox.Enabled = true;
                            this._DigitalInputCheckBox.Enabled = true;
                            this.ApplicationMessage = "Digital input configured.";
                        }
                        else
                        {
                            this._DigitalIoGroupBox.Enabled = false;
                            this.ApplicationMessage = "Failed configuring digital input.";
                        }
                    }

                    this._DigitalOutputCheckBox.Enabled = false;
                    if ( this.DigitalOutInternal is object )
                    {
                        this.DigitalOutInternal.Dispose();
                    }

                    if ( this.Device.GetNumSubsystemElements( OpenLayers.Base.SubsystemType.DigitalOutput ) > 0 )
                    {
                        this.ApplicationMessage = "Opening digital output...";
                        this.DigitalOutInternal = new DigitalOutputSubsystem( this.Device, 0 );
                        this.ApplicationMessage = "Digital output opened.";
                        if ( this.DigitalOutInternal is object )
                        {
                            this.ApplicationMessage = "Configuring digital output...";
                            this.DigitalOutInternal.Configure( OpenLayers.Base.DataFlow.SingleValue );
                            this._DigitalIoGroupBox.Enabled = true;
                            this._DigitalOutputCheckBox.Enabled = true;
                            this.ApplicationMessage = "Digital output configured.";
                        }
                        else
                        {
                            this.ApplicationMessage = "Failed configuring digital output.";
                        }
                    }

                    this._AnalogInputGroupBox.Enabled = false;
                    if ( this.AnalogInputInternal is object )
                    {
                        this.AnalogInputInternal.Dispose();
                    }

                    if ( this.Device.GetNumSubsystemElements( OpenLayers.Base.SubsystemType.AnalogInput ) > 0 )
                    {
                        // open analog to digital sub systems
                        this.ApplicationMessage = "Opening analog input...";
                        this.AnalogInputInternal = new AnalogInputSubsystem( this.Device, 0 );
                        this.ApplicationMessage = "analog input opened.";
                        if ( this.AnalogInputInternal is object )
                        {
                            this.ApplicationMessage = "Configuring analog input...";
                            this._AnalogInputGroupBox.Enabled = true;
                            this.AnalogInputInternal.DataFlow = OpenLayers.Base.DataFlow.SingleValue;
                            this.AnalogInputInternal.Config();
                            this._InputOneChannelCombo.Enabled = true;
                            this._InputOneChannelCombo.DataSource = this.AnalogInputInternal.AvailableChannels( "{0}" );
                            this._InputOneChannelCombo.SelectedIndex = Math.Min( 0, this._InputOneChannelCombo.Items.Count - 1 );
                            this._InputOneRangeCombo.Enabled = true;
                            this._InputOneRangeCombo.DataSource = this.AnalogInputInternal.AvailableRanges( "{0}/{1}" );
                            this._InputOneRangeCombo.SelectedIndex = 0;
                            this._InputTwoChannelCombo.Enabled = true;
                            this._InputTwoChannelCombo.DataSource = this.AnalogInputInternal.AvailableChannels( "{0}" );
                            this._InputTwoChannelCombo.SelectedIndex = Math.Min( 1, this._InputOneChannelCombo.Items.Count - 1 );
                            this._InputTwoRangeCombo.Enabled = true;
                            this._InputTwoRangeCombo.DataSource = this.AnalogInputInternal.AvailableRanges( "{0}/{1}" );
                            this._InputTwoRangeCombo.SelectedIndex = 0;
                            this._InputThreeChannelCombo.Enabled = true;
                            this._InputThreeChannelCombo.DataSource = this.AnalogInputInternal.AvailableChannels( "{0}" );
                            this._InputThreeChannelCombo.SelectedIndex = Math.Min( 2, this._InputOneChannelCombo.Items.Count - 1 );
                            this._InputThreeRangeCombo.Enabled = true;
                            this._InputThreeRangeCombo.DataSource = this.AnalogInputInternal.AvailableRanges( "{0}/{1}" );
                            this._InputThreeRangeCombo.SelectedIndex = 0;
                            this._InputFourChannelCombo.Enabled = true;
                            this._InputFourChannelCombo.DataSource = this.AnalogInputInternal.AvailableChannels( "{0}" );
                            this._InputFourChannelCombo.SelectedIndex = Math.Min( 3, this._InputOneChannelCombo.Items.Count - 1 );
                            this._InputFourRangeCombo.Enabled = true;
                            this._InputFourRangeCombo.DataSource = this.AnalogInputInternal.AvailableRanges( "{0}/{1}" );
                            this._InputFourRangeCombo.SelectedIndex = 0;
                            this.ApplicationMessage = "Analog input configured.";
                        }
                        else
                        {
                            this.ApplicationMessage = "Failed configuring analog input.";
                            this._InputOneChannelCombo.Enabled = false;
                            this._InputOneRangeCombo.Enabled = false;
                            this._InputTwoChannelCombo.Enabled = false;
                            this._InputTwoRangeCombo.Enabled = false;
                            this._InputTwoChannelCombo.Enabled = false;
                            this._InputTwoRangeCombo.Enabled = false;
                            this._InputFourChannelCombo.Enabled = false;
                            this._InputFourRangeCombo.Enabled = false;
                        }
                    }

                    // open digital to analog sub systems
                    this._AnalogOutputGroupBox.Enabled = false;
                    if ( this.AnalogOutputInternal is object )
                    {
                        this.AnalogOutputInternal.Dispose();
                    }

                    // check if device has analog output capabilities
                    if ( this.Device.GetNumSubsystemElements( OpenLayers.Base.SubsystemType.AnalogOutput ) > 0 )
                    {
                        this.ApplicationMessage = "Opening analog output...";
                        this.AnalogOutputInternal = new AnalogOutputSubsystem( this.Device, 1 );
                        this.ApplicationMessage = "analog output opened.";
                        if ( this.AnalogOutputInternal is object )
                        {
                            this.ApplicationMessage = "Configuring analog output...";
                            this._AnalogOutputGroupBox.Enabled = true;
                            this.AnalogOutputInternal.DataFlow = OpenLayers.Base.DataFlow.SingleValue;
                            this.AnalogOutputInternal.Config();
                            this.ApplicationMessage = "Analog output configured.";
                        }
                        else
                        {
                            this.ApplicationMessage = "Failed configuring analog output.";
                        }
                    }

                    // open counter subsystems
                    this._CounterGroupBox.Enabled = false;

                    // enable the timer
                    this._UpdateIOTimer.Enabled = true;
                    this.ApplicationMessage = this.BoardName + " configured.";
                    this.BoardMessage = this.BoardName + " opened.";
                }
                else
                {
                    this.Device = null;
                    this.BoardMessage = "board not found.";

                    // disable the timer
                    this._UpdateIOTimer.Enabled = false;
                    this.Text = System.Reflection.Assembly.GetExecutingAssembly().BuildProductTimeCaption( 3, null, null )
                                + $". NO DEVICE: SINGLE I/O ";
                }

                this.NotifyPropertyChanged( nameof( this.IsOpen ) );
            }


            // enable controls
            catch ( Exception ex )
            {
                if ( string.IsNullOrWhiteSpace( deviceName ) )
                {
                    deviceName = "unknown";
                }

                ex.Data.Add( "@isr", "Exception opening IO." );
                ex.Data.Add( "@isr.DeviceName", deviceName );
                this.ShowException( ex );
                _ = MessageBox.Show( "Exception opening IO.", ex.ToString() );
            }
        }

        /// <summary> Closes and releases the data acquisition sub systems. </summary>
        /// <remarks> Use this method to close and release the driver. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void CloseIO()
        {
            try
            {

                // disable the timer
                this._UpdateIOTimer.Enabled = false;
                Application.DoEvents();
                try
                {
                    if ( this.DigitalInInternal is null )
                    {
                        this.ApplicationMessage = "Warning. Digital input not defined.";
                    }
                    else
                    {
                        this.DigitalInInternal.Dispose();
                    }
                }
                catch ( Exception ex )
                {
                    this.ShowException( ex );
                }

                try
                {
                    if ( this.DigitalOutInternal is null )
                    {
                        this.ApplicationMessage = "Warning. Digital output not defined.";
                    }
                    else
                    {
                        this.DigitalOutInternal.Dispose();
                    }
                }
                catch ( Exception ex )
                {
                    this.ShowException( ex );
                }

                try
                {
                    if ( this.AnalogInputInternal is null )
                    {
                        this.ApplicationMessage = "Warning. Analog input not defined.";
                    }
                    else
                    {
                        this.AnalogInputInternal.Dispose();
                    }
                }
                catch ( Exception ex )
                {
                    this.ShowException( ex );
                }

                try
                {
                    if ( this.AnalogOutputInternal is object )
                    {
                        this.AnalogOutputInternal.Dispose();
                    }
                }
                catch ( Exception ex )
                {
                    this.ShowException( ex );
                }

                if ( this.Device is object )
                {
                    this.Device.Dispose();
                    this.Device = null;
                    this.ApplicationMessage = this.BoardName + " closed.";
                    this.BoardMessage = this.BoardName + " close.";
                }
            }
            catch ( Exception ex )
            {
                this.ShowException( ex );
            }
            finally
            {

                // disable all group boxes
                this._AnalogInputGroupBox.Enabled = false;
                this._AnalogOutputGroupBox.Enabled = false;
                this._CounterGroupBox.Enabled = false;
                this._DigitalIoGroupBox.Enabled = false;
            }
        }



        /// <summary> Terminates and disposes of class-level objects. </summary>
        /// <remarks> Called from the form Closing method. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private void TerminateObjects()
        {

            // disable all group boxes
            this._AnalogInputGroupBox.Enabled = false;
            this._AnalogOutputGroupBox.Enabled = false;
            this._CounterGroupBox.Enabled = false;
            this._DigitalIoGroupBox.Enabled = false;

            // disable the timer
            this._UpdateIOTimer.Enabled = false;
            Application.DoEvents();
        }

        /// <summary> Updates all outputs and inputs. </summary>
        private void UpdateIO()
        {
            if ( this.AnalogInputInternal is object )
            {
                double channelGain;
                double voltage;
                if ( this.AnalogOutputInternal is object )
                {

                    // output analog voltages first so that we can use the analog output.
                    _ = this.AnalogOutputInternal.OutputSingleVoltage( 1, ( double ) this._AnalogOutputZeroVoltageNumeric.Value );
                    _ = this.AnalogOutputInternal.OutputSingleVoltage( 0, ( double ) this._AnalogOutputOneVoltageNumeric.Value );
                }

                if ( this._DigitalOutputCheckBox.Checked && this.DigitalOutInternal is object )
                {
                    this.DigitalOutInternal.SingleReading = Convert.ToInt32( this._DigitalOutputNumericUpDown.Value, System.Globalization.CultureInfo.CurrentCulture );
                }

                if ( this.AnalogInputInternal is object )
                {
                    short channelNumber;
                    if ( short.TryParse( this._InputOneChannelCombo.Text, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.CurrentCulture, out _ ) )
                    {
                        channelNumber = short.Parse( this._InputOneChannelCombo.Text, System.Globalization.CultureInfo.CurrentCulture );
                        channelGain = this.AnalogInputInternal.SupportedGains[Convert.ToInt16( this._InputOneRangeCombo.SelectedIndex )];
                        voltage = this.AnalogInputInternal.SingleVoltage( channelNumber, channelGain );
                        this._InputOneVoltageTextBox.Text = voltage.ToString( "F4", System.Globalization.CultureInfo.CurrentCulture );
                    }

                    if ( short.TryParse( this._InputTwoChannelCombo.Text, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.CurrentCulture, out channelNumber ) )
                    {
                        channelGain = this.AnalogInputInternal.SupportedGains[Convert.ToInt16( this._InputTwoRangeCombo.SelectedIndex )];
                        voltage = this.AnalogInputInternal.SingleVoltage( channelNumber, channelGain );
                        this._InputTwoVoltageTextBox.Text = voltage.ToString( "F4", System.Globalization.CultureInfo.CurrentCulture );
                    }

                    if ( short.TryParse( this._InputThreeChannelCombo.Text, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.CurrentCulture, out channelNumber ) )
                    {
                        channelGain = this.AnalogInputInternal.SupportedGains[Convert.ToInt16( this._InputThreeRangeCombo.SelectedIndex )];
                        voltage = this.AnalogInputInternal.SingleVoltage( channelNumber, channelGain );
                        this._InputThreeVoltageTextBox.Text = voltage.ToString( "F4", System.Globalization.CultureInfo.CurrentCulture );
                    }

                    if ( short.TryParse( this._InputFourChannelCombo.Text, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.CurrentCulture, out channelNumber ) )
                    {
                        channelGain = this.AnalogInputInternal.SupportedGains[Convert.ToInt16( this._InputFourRangeCombo.SelectedIndex )];
                        voltage = this.AnalogInputInternal.SingleVoltage( channelNumber, channelGain );
                        this._InputFourVoltageTextBox.Text = voltage.ToString( "F4", System.Globalization.CultureInfo.CurrentCulture );
                    }
                }
            }

            if ( this._DigitalInputCheckBox.Checked && this.DigitalInInternal is object )
            {
                this._DigitalInputNumericUpDown.Value = Convert.ToDecimal( this.DigitalInInternal.SingleReading, System.Globalization.CultureInfo.CurrentCulture );
            }
        }

        #endregion

        #region " PROPERTIES "

        /// <summary>Gets or sets reference to the digital input subsystem.</summary>
        private DigitalInputSubsystem DigitalInInternal { [MethodImpl( MethodImplOptions.Synchronized )] get; [MethodImpl( MethodImplOptions.Synchronized )] set; }

        /// <summary>   Gets or sets the digital out internal. </summary>
        /// <value> The digital out internal. </value>
        private DigitalOutputSubsystem DigitalOutInternal { [MethodImpl( MethodImplOptions.Synchronized )] get; [MethodImpl( MethodImplOptions.Synchronized )] set; }

        /// <summary>   Gets or sets the analog input internal. </summary>
        /// <value> The analog input internal. </value>
        private AnalogInputSubsystem AnalogInputInternal { [MethodImpl( MethodImplOptions.Synchronized )] get; [MethodImpl( MethodImplOptions.Synchronized )] set; }

        /// <summary>   Gets or sets the analog output internal. </summary>
        /// <value> The analog output internal. </value>
        private AnalogOutputSubsystem AnalogOutputInternal { [MethodImpl( MethodImplOptions.Synchronized )] get; [MethodImpl( MethodImplOptions.Synchronized )] set; }

        /// <summary> Gets or sets the status message. </summary>
        /// <value> The status message. </value>
        private string ApplicationMessage
        {
            get => this._ApplicationMessagesComboBox.Text;

            set {
                if ( !string.IsNullOrWhiteSpace( value ) )
                {
                    if ( this._ApplicationMessagesComboBox.Items.Count > 50 )
                    {
                        while ( this._ApplicationMessagesComboBox.Items.Count >= 25 )
                            this._ApplicationMessagesComboBox.Items.RemoveAt( this._ApplicationMessagesComboBox.Items.Count - 1 );
                    }

                    this._ApplicationMessagesComboBox.Items.Insert( 0, value );
                    if ( this._ApplicationMessagesComboBox.SelectedIndex != 0 )
                    {
                        this._ApplicationMessagesComboBox.SelectedIndex = 0;
                    }

                    this._ApplicationMessagesComboBox.Invalidate();
                    this._ToolTip.SetToolTip( this._ApplicationMessagesComboBox, value );
                }
            }
        }

        /// <summary> Shows the exception. </summary>
        /// <param name="ex"> The ex. </param>
        private void ShowException( Exception ex )
        {
            this.ApplicationMessage = ex.Message;
            this.BoardMessage = ex.ToString();
            this._ToolTip.SetToolTip( this._BoardMessagesComboBox, ex.ToString() );
        }


        /// <summary> Gets or sets the board error message. </summary>
        /// <value> The board error message. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0052:Remove unread private members", Justification = "<Pending>" )]
        private string BoardMessage
        {
            get => this._BoardMessagesComboBox.Text;

            set {
                if ( !string.IsNullOrWhiteSpace( value ) )
                {
                    if ( this._BoardMessagesComboBox.Items.Count > 50 )
                    {
                        while ( this._BoardMessagesComboBox.Items.Count >= 25 )
                            this._BoardMessagesComboBox.Items.RemoveAt( this._BoardMessagesComboBox.Items.Count - 1 );
                    }

                    this._BoardMessagesComboBox.Items.Insert( 0, value );
                    this._ToolTip.SetToolTip( this._BoardMessagesComboBox, value );
                    if ( this._BoardMessagesComboBox.SelectedIndex != 0 )
                    {
                        this._BoardMessagesComboBox.SelectedIndex = 0;
                    }

                    this._BoardMessagesComboBox.Invalidate();
                }
            }
        }

        /// <summary>
        /// Gets or sets the sync locker.
        /// </summary>
        private static readonly object SyncLocker = new();

        /// <summary>
        /// The shared instance.
        /// </summary>
        private static SingleInputOutputView _Instance;

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static SingleInputOutputView Get()
        {
            if ( !Instantiated )
            {
                lock ( SyncLocker )
                    _Instance = new SingleInputOutputView();
            }

            return _Instance;
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> The instantiated. </value>
        internal static bool Instantiated
        {
            get {
                lock ( SyncLocker )
                    return !(_Instance is null || _Instance.IsDisposed);
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Raises the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLoad( EventArgs e )
        {
            base.OnLoad( e );
            this._OpenDeviceCheckBox.Enabled = true;
        }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Event handler. Called by channelSelect for key press events. </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Key press event information. </param>
        private void ChannelSelect_KeyPress( object sender, KeyPressEventArgs e )
        {
            _ = (( ComboBox ) sender).SearchAndSelect( e );
        }

        /// <summary> Handles the CheckedChanged event of the openDeviceCheckBox control. Opens or closes
        /// the device. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs"/> instance containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void OpenDeviceCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            if ( !this._OpenDeviceCheckBox.Enabled )
                return;
            bool isOpening = this._OpenDeviceCheckBox.Checked;
            try
            {
                this._OpenDeviceCheckBox.Enabled = false;
                if ( isOpening )
                {
                    // open the board
                    this.OpenIO( DeviceChooser.DeviceChooser.SelectDeviceName() );
                }
                else
                {
                    // close the board
                    this.CloseIO();
                }

                this._OpenDeviceCheckBox.Checked = this.IsOpen;
                if ( this._OpenDeviceCheckBox.Checked )
                {
                    // set the caption to close
                    this._OpenDeviceCheckBox.Text = "Cl&ose Device";
                }
                else
                {
                    // set the caption to open
                    this._OpenDeviceCheckBox.Text = "&Open Device";
                }
            }
            catch ( Exception ex )
            {
                if ( string.IsNullOrWhiteSpace( this.BoardName ) )
                {
                    try
                    {
                        this.BoardName = DeviceChooser.DeviceChooser.SelectDeviceName();
                    }
                    catch
                    {
                    }
                }

                if ( string.IsNullOrWhiteSpace( this.BoardName ) )
                {
                    this.BoardName = "N/A";
                }

                this.ApplicationMessage = isOpening ? "Failed opening " + this.BoardName + " board" : "Failed closing " + this.BoardName + " board";
                ex.Data.Add( "@isr", this.ApplicationMessage );
                ex.Data.Add( "@isr.BoardName", this.BoardName );
                this.ShowException( ex );
                _  = MessageBox.Show( this.ApplicationMessage, ex.ToString() );
            }
            finally
            {
                this._OpenDeviceCheckBox.Enabled = true;
            }
        }

        /// <summary> Occurs upon timer events. </summary>
        /// <remarks> Use this method to execute all timer actions. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void UpdateIOTimer_Tick( object sender, EventArgs e )
        {
            if ( this._AutoUpdateCheckBox.Checked )
            {
                try
                {
                    this._UpdateIOTimer.Enabled = false;
                    this.UpdateIO();
                    this._UpdateIOTimer.Enabled = true;
                }
                catch ( Exception ex )
                {
                    this.ApplicationMessage = "Failed monitoring. Timer is stopped. Close and reopen the board.";
                    this.ShowException( ex );
                }
            }
        }

        /// <summary> Handles the CheckedChanged event of the autoUpdateCheckBox control. Toggles the
        /// visibility of the manual update box. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs"/> instance containing the event data. </param>
        private void AutoUpdateCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this._UpdateIOButton.Visible = !this._AutoUpdateCheckBox.Checked;
        }

        /// <summary> Event handler. Called by UpdateIOButton for click events. </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void UpdateIOButton_Click( object sender, EventArgs e )
        {
            try
            {
                this.UpdateIO();
            }
            catch ( Exception ex )
            {
                this.ShowException( ex );
            }
        }

        #endregion

    }
}
