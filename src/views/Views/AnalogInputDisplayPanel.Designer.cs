using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace isr.IO.OL.WinViews
{
    public partial class AnalogInputDisplayPanel
    {

        // Required by the Windows Form Designer
        private IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new Container();
            _AnalogInputDisplay = new Display.AnalogInputDisplay();
            _AnalogInputDisplay.AcquisitionStarted += new EventHandler<EventArgs>(AnalogInputDisplay_AcquisitionStarted);
            _AnalogInputDisplay.BoardsLocated += new EventHandler<EventArgs>(AnalogInputDisplay_BoardsLocated);
            _AnalogInputDisplay.BufferDone += new EventHandler<EventArgs>(AnalogInputDisplay_BufferDone);
            SuspendLayout();
            // 
            // _AnalogInputDisplay
            // 
            _AnalogInputDisplay.CalculatePeriodCount = 0;
            _AnalogInputDisplay.ChartMode = ChartModeId.Scope;
            _AnalogInputDisplay.ConfigurationPanelVisible = true;
            _AnalogInputDisplay.Dock = DockStyle.Fill;
            _AnalogInputDisplay.Location = new Point(3, 3);
            _AnalogInputDisplay.MaximumMovingAverageLength = 1000;
            _AnalogInputDisplay.MaximumRefreshRate = new decimal(new int[] { 100, 0, 0, 0 });
            _AnalogInputDisplay.MaximumSampleSize = 1000000;
            _AnalogInputDisplay.MaximumSamplingRate = new decimal(new int[] { 100000000, 0, 0, 0 });
            _AnalogInputDisplay.MaximumSignalBufferSize = 1000000;
            _AnalogInputDisplay.MinimumMovingAverageLength = 2;
            _AnalogInputDisplay.MinimumRefreshRate = new decimal(new int[] { 1, 0, 0, 0 });
            _AnalogInputDisplay.MinimumSamplingRate = new decimal(new int[] { 0, 0, 0, 0 });
            _AnalogInputDisplay.MinimumSignalMemoryLength = 0;
            _AnalogInputDisplay.Name = "_AnalogInputDisplay";
            _AnalogInputDisplay.SampleSize = 1000;
            _AnalogInputDisplay.SamplingPeriod = 0.0d;
            _AnalogInputDisplay.SignalBufferSize = 1000;
            _AnalogInputDisplay.SignalMemoryLength = 10000;
            _AnalogInputDisplay.Size = new Size(871, 510);
            _AnalogInputDisplay.TabIndex = 0;
            _AnalogInputDisplay.UpdatePeriodCount = 10;
            // 
            // AnalogInputDisplayPanel
            // 
            ClientSize = new Size(885, 542);
            Controls.Add(_AnalogInputDisplay);
            Name = "AnalogInputDisplayView";
            Text = "Analog Input Display View";
            ResumeLayout(false);
        }

        private Display.AnalogInputDisplay _AnalogInputDisplay;
    }
}
