using System;
using System.Diagnostics;
using System.Windows.Forms;


namespace isr.IO.OL.WinViews
{
    /// <summary> Displays acquired signals. </summary>
    /// <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 6/8/2019 </para></remarks>
    public partial class SignalView : UserControl
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        public SignalView() : base()
        {
            this.InitializeComponent();
            this.OnInstantiate();
            this._CreateDataTimer.Interval = 200;
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks>   David, 2022-03-03. </remarks>
        /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
        ///                             resources; <see langword="false" /> to release only unmanaged
        ///                             resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( disposing )
                {
                    this.components?.Dispose();
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        /// <summary> Initializes additional components. </summary>
        /// <remarks> Call this method from the class constructor if you would like to add other controls
        /// or change anything set by the InitializeComponent method. </remarks>
        private void OnInstantiate()
        {
            this._Display.ChartBeginInit();
            this._Display.XDataCurrentRangeMin = _XYMin;
            this._Display.XDataCurrentRangeMax = _XYMax;
            this._Display.XDataRangeMin = _XYMin;
            this._Display.XDataRangeMax = _XYMax;
            this._Display.XDataName = "Time";
            this._Display.XDataUnit = "sec";
            this._Display.SignalBufferLength = _DefaultBufferLength;
            this._XMinTrackBar.Value = Convert.ToInt32( this._Display.XDataCurrentRangeMin );
            this._XMaxTrackBar.Value = Convert.ToInt32( this._Display.XDataCurrentRangeMax );
            this._SineWaveSignal = new OpenLayers.Signals.MemorySignal() {
                Name = "SineWave",
                RangeMax = _XYMax,
                RangeMin = _XYMin,
                CurrentRangeMax = _XYMax,
                CurrentRangeMin = _XYMin,
                Unit = "SineWave Unit"
            };
            this._RampSignal = new OpenLayers.Signals.MemorySignal() {
                Name = "Ramp",
                RangeMax = _XYMax,
                RangeMin = _XYMin,
                CurrentRangeMax = _XYMax,
                CurrentRangeMin = _XYMin,
                Unit = "Ramp Unit"
            };
            this._SquareWaveSignal = new OpenLayers.Signals.MemorySignal() {
                Name = "SquareWave",
                RangeMax = _XYMax,
                RangeMin = _XYMin,
                CurrentRangeMax = _XYMax,
                CurrentRangeMin = _XYMin,
                Unit = "SquareWave Unit"
            };
            this._Display.ChartEndInit();
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> The default buffer length. </summary>
        private const long _DefaultBufferLength = 100000L;

        /// <summary> The x/y minimum. </summary>
        private const long _XYMin = 0L;

        /// <summary> The x/y maximum. </summary>
        private const long _XYMax = 1000L;

        /// <summary> The sine wave signal. </summary>
        private OpenLayers.Signals.MemorySignal _SineWaveSignal;

        /// <summary> The ramp signal. </summary>
        private OpenLayers.Signals.MemorySignal _RampSignal;

        /// <summary> The square wave signal. </summary>
        private OpenLayers.Signals.MemorySignal _SquareWaveSignal;

        #endregion

        #region " METHODS "

        /// <summary> Creates signal data. </summary>
        /// <param name="memorySignal"> The memory signal. </param>
        private void CreateSignalData( OpenLayers.Signals.MemorySignal memorySignal )
        {
            var rnd = new Random( DateTimeOffset.Now.Millisecond );
            int xOffset = rnd.Next( -20, 20 );
            switch ( memorySignal.Name ?? "" )
            {
                case "SineWave":
                    {
                        int i = 0;
                        while ( i < this._Display.SignalBufferLength )
                        {
                            memorySignal.Data[i] = Math.Sin( (i + xOffset) * (Math.PI / 180d) * 0.01d ) * 380d + 400d + rnd.Next( 20 );
                            _ = Math.Min( System.Threading.Interlocked.Increment( ref i ), i - 1 );
                        }

                        break;
                    }
                // break 
                case "Ramp":
                    {
                        int i = 0;
                        while ( i < this._Display.SignalBufferLength )
                        {
                            memorySignal.Data[i] = (i + xOffset) % 100 * 2 + rnd.Next( 20 );
                            _ = Math.Min( System.Threading.Interlocked.Increment( ref i ), i - 1 );
                        }

                        break;
                    }
                // break 
                case "SquareWave":
                    {
                        int offset = rnd.Next( 20 );
                        int i = 0;
                        while ( i < this._Display.SignalBufferLength )
                        {
                            memorySignal.Data[i] = (i + xOffset) % 100 > 30 + 3 * 10 + offset ? 300 + rnd.Next( 10 ) : 10 + rnd.Next( 10 );
                            _ = Math.Min( System.Threading.Interlocked.Increment( ref i ), i - 1 );
                            // break 
                        }

                        break;
                    }

                default:
                    {
                        int i = 0;
                        while ( i < this._Display.SignalBufferLength )
                        {
                            memorySignal.Data[i] = i;
                            _ = Math.Min( System.Threading.Interlocked.Increment( ref i ), i - 1 );
                        }

                        break;
                    }
                    // break 
            }
        }

        /// <summary> Event handler. Called by _SingleRadioButton for checked changed events. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void SingleRadioButton_CheckedChanged( object sender, EventArgs e )
        {
            _ = this._Display.DisableRendering();
            this._Display.BandMode = this._SingleRadioButton.Checked ? OpenLayers.Controls.BandMode.SingleBand : OpenLayers.Controls.BandMode.MultiBand;
            _ = this._Display.EnableRendering();
            this._Display.SignalUpdate();
        }

        /// <summary> Sets track bar positions dependent on current range. </summary>
        private void SetTrackbarPositionsDependentOnCurrentRange()
        {
            if ( this._Display.Signals.Count > 0 )
            {
                if ( this._Display.Signals[this._SignalListComboBox.SelectedIndex].CurrentRangeMax > Convert.ToDouble( this._YMaxTrackBar.Maximum ) )
                {
                    this._Display.Signals[this._SignalListComboBox.SelectedIndex].CurrentRangeMax = Convert.ToDouble( this._YMaxTrackBar.Maximum );
                }

                if ( this._Display.Signals[this._SignalListComboBox.SelectedIndex].CurrentRangeMin < Convert.ToDouble( this._YMinTrackBar.Minimum ) )
                {
                    this._Display.Signals[this._SignalListComboBox.SelectedIndex].CurrentRangeMin = Convert.ToDouble( this._YMinTrackBar.Minimum );
                }

                this._YMaxTrackBar.Value = this._Display.Signals[this._SignalListComboBox.SelectedIndex].CurrentRangeMax > this._YMaxTrackBar.Maximum ? this._YMaxTrackBar.Maximum : this._Display.Signals[this._SignalListComboBox.SelectedIndex].CurrentRangeMax < this._YMaxTrackBar.Minimum ? this._YMaxTrackBar.Minimum : Convert.ToInt32( this._Display.Signals[this._SignalListComboBox.SelectedIndex].CurrentRangeMax );
                this._YMinTrackBar.Value = this._Display.Signals[this._SignalListComboBox.SelectedIndex].CurrentRangeMin < this._YMaxTrackBar.Minimum ? this._YMinTrackBar.Minimum : this._Display.Signals[this._SignalListComboBox.SelectedIndex].CurrentRangeMin > this._YMaxTrackBar.Maximum ? this._YMinTrackBar.Maximum : Convert.ToInt32( this._Display.Signals[this._SignalListComboBox.SelectedIndex].CurrentRangeMin );
            }
        }

        #endregion

        #region " CONTROL EVENTS "

        /// <summary> Event handler. Called by _createDataTimer for tick events. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void CreateDataTimer_Tick( object sender, EventArgs e )
        {
            _ = this._Display.DisableRendering();
            int indexSignal = 0;
            while ( indexSignal < this._Display.Signals.Count )
            {
                this.CreateSignalData( this._Display.Signals[indexSignal] );
                _ = Math.Min( System.Threading.Interlocked.Increment( ref indexSignal ), indexSignal - 1 );
            }

            var sw = Stopwatch.StartNew();
            _ = this._Display.EnableRendering();
            this._Display.SignalUpdate();
            this._Display.Refresh();
            sw.Stop();
            this._TimeLabel.Text = $"{sw.Elapsed.TotalMilliseconds} [ms]";
        }

        /// <summary> Event handler. Called by _XMinTrackBar for scroll events. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void XMinTrackBar_Scroll( object sender, EventArgs e )
        {
            if ( this._Display.Signals.Count > 0 )
            {
                if ( this._XMinTrackBar.Value >= this._XMaxTrackBar.Value )
                {
                    this._XMinTrackBar.Value = this._XMaxTrackBar.Value - 10;
                }

                this._Display.XDataCurrentRangeMin = this._XMinTrackBar.Value;
                this._Display.SignalUpdate();
            }
        }

        /// <summary> Event handler. Called by _XMaxTrackBar for scroll events. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void XMaxTrackBar_Scroll( object sender, EventArgs e )
        {
            if ( this._Display.Signals.Count > 0 )
            {
                if ( this._XMinTrackBar.Value >= this._XMaxTrackBar.Value )
                {
                    this._XMaxTrackBar.Value = this._XMinTrackBar.Value + 10;
                }

                this._Display.XDataCurrentRangeMax = Convert.ToDouble( this._XMaxTrackBar.Value );
                this._Display.SignalUpdate();
            }
        }

        /// <summary> Event handler. Called by _YMaxTrackBar for scroll events. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void YMaxTrackBar_Scroll( object sender, EventArgs e )
        {
            if ( this._Display.Signals.Count > 0 )
            {
                if ( this._YMinTrackBar.Value >= this._YMaxTrackBar.Value )
                {
                    this._YMaxTrackBar.Value = this._YMinTrackBar.Value + 10;
                }

                this._Display.Signals[this._SignalListComboBox.SelectedIndex].CurrentRangeMax = Convert.ToDouble( this._YMaxTrackBar.Value );
                this._Display.SignalUpdate();
            }
        }

        /// <summary> Event handler. Called by _YMinTrackBar for scroll events. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void YMinTrackBar_Scroll( object sender, EventArgs e )
        {
            if ( this._Display.Signals.Count > 0 )
            {
                if ( this._YMinTrackBar.Value >= this._YMaxTrackBar.Value )
                {
                    this._YMinTrackBar.Value = this._YMaxTrackBar.Value - 10;
                }

                this._Display.Signals[this._SignalListComboBox.SelectedIndex].CurrentRangeMin = Convert.ToDouble( this._YMinTrackBar.Value );
                this._Display.SignalUpdate();
            }
        }

        /// <summary> Event handler. Called by _SignalListComboBox for selected value changed events. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void SignalListComboBox_SelectedValueChanged( object sender, EventArgs e )
        {
            this.SetTrackbarPositionsDependentOnCurrentRange();
        }

        /// <summary> Event handler. Called by _ColorDataButton for click events. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ColorDataButton_Click( object sender, EventArgs e )
        {
            if ( this._Display.Signals.Count > 0 )
            {
                this._ColorDialog.Color = this._Display.GetCurveColor( this._SignalListComboBox.SelectedIndex );
                _ = this._ColorDialog.ShowDialog();
                this._Display.SetCurveColor( this._SignalListComboBox.SelectedIndex, this._ColorDialog.Color );
            }
        }

        /// <summary> Event handler. Called by _ColorAxesButton for click events. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ColorAxesButton_Click( object sender, EventArgs e )
        {
            this._ColorDialog.Color = this._Display.AxesColor;
            _ = this._ColorDialog.ShowDialog();
            this._Display.AxesColor = this._ColorDialog.Color;
        }

        /// <summary> Event handler. Called by _ColorGridsButton for click events. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ColorGridsButton_Click( object sender, EventArgs e )
        {
            this._ColorDialog.Color = this._Display.GridColor;
            _ = this._ColorDialog.ShowDialog();
            this._Display.GridColor = this._ColorDialog.Color;
        }

        /// <summary> Event handler. Called by _AutoScaleButton for click events. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void AutoScaleButton_Click( object sender, EventArgs e )
        {
            _ = this._Display.DisableRendering();
            this._Display.AutoScale = true;
            this.SetTrackbarPositionsDependentOnCurrentRange();
            _ = this._Display.EnableRendering();
            this._Display.SignalUpdate();
        }

        /// <summary> Event handler. Called by _PrintButton for click events. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void PrintButton_Click( object sender, EventArgs e )
        {
            this._Display.Print();
        }

        /// <summary> Event handler. Called by )_SineWaveCheckBox for checked changed events. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void SineWaveCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            CheckBox checkBox1;
            checkBox1 = ( CheckBox ) sender;
            if ( checkBox1.Checked )
            {
                this._CreateDataTimer.Enabled = false;
                _ = this._Display.DisableRendering();
                _ = this._Display.Signals.Add( this._SineWaveSignal );
                int indexSignal = this._SignalListComboBox.Items.Add( this._SineWaveSignal.Name );
                this._SignalListComboBox.SelectedIndex = indexSignal;
                _ = this._Display.EnableRendering();
                this._CreateDataTimer.Enabled = true;
            }
            else if ( this._Display.Signals.Contains( this._SineWaveSignal ) )
            {
                _ = this._Display.DisableRendering();
                int signalIndex = this._Display.Signals.IndexOf( this._SineWaveSignal );
                this._Display.Signals.Remove( this._SineWaveSignal );
                this._SignalListComboBox.Items.RemoveAt( signalIndex );
                if ( !(this._SignalListComboBox.Items.Count == 0) )
                {
                    this._SignalListComboBox.SelectedIndex = 0;
                }

                _ = this._Display.EnableRendering();
                this._Display.SignalUpdate();
            }
        }

        /// <summary> Event handler. Called by _RampCheckBox for checked changed events. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void RampCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            CheckBox checkBox1;
            checkBox1 = ( CheckBox ) sender;
            if ( checkBox1.Checked )
            {
                this._CreateDataTimer.Enabled = false;
                _ = this._Display.DisableRendering();
                _ = this._Display.Signals.Add( this._RampSignal );
                int indexSignal = this._SignalListComboBox.Items.Add( this._RampSignal.Name );
                this._SignalListComboBox.SelectedIndex = indexSignal;
                _ = this._Display.EnableRendering();
                this._CreateDataTimer.Enabled = true;
            }
            else if ( this._Display.Signals.Contains( this._RampSignal ) )
            {
                _ = this._Display.DisableRendering();
                int signalIndex = this._Display.Signals.IndexOf( this._RampSignal );
                this._Display.Signals.Remove( this._RampSignal );
                this._SignalListComboBox.Items.RemoveAt( signalIndex );
                if ( !(this._SignalListComboBox.Items.Count == 0) )
                {
                    this._SignalListComboBox.SelectedIndex = 0;
                }

                _ = this._Display.EnableRendering();
                this._Display.SignalUpdate();
            }
        }

        /// <summary> Event handler. Called by _SquareWaveCheckBox for checked changed events. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void SquareWaveCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            CheckBox checkBox1;
            checkBox1 = ( CheckBox ) sender;
            if ( checkBox1.Checked )
            {
                this._CreateDataTimer.Enabled = false;
                _ = this._Display.DisableRendering();
                _ = this._Display.Signals.Add( this._SquareWaveSignal );
                int indexSignal = this._SignalListComboBox.Items.Add( this._SquareWaveSignal.Name );
                this._SignalListComboBox.SelectedIndex = indexSignal;
                _ = this._Display.EnableRendering();
                this._CreateDataTimer.Enabled = true;
            }
            else if ( this._Display.Signals.Contains( this._SquareWaveSignal ) )
            {
                _ = this._Display.DisableRendering();
                int signalIndex = this._Display.Signals.IndexOf( this._SquareWaveSignal );
                this._Display.Signals.Remove( this._SquareWaveSignal );
                this._SignalListComboBox.Items.RemoveAt( signalIndex );
                if ( !(this._SignalListComboBox.Items.Count == 0) )
                {
                    this._SignalListComboBox.SelectedIndex = 0;
                }

                _ = this._Display.EnableRendering();
                this._Display.SignalUpdate();
            }
        }

        #endregion

    }
}
