using System;

namespace isr.IO.OL
{
    /// <summary>
    /// Adds functionality to the Data Translation Open Layers
    /// <see cref="OpenLayers.Base.DigitalInputSubsystem">digital input subsystem</see>.</summary>
    /// <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para></remarks>
    public class DigitalInputSubsystem : OpenLayers.Base.DigitalInputSubsystem
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>Constructs this class.</summary>
        /// <param name="device">A reference to 
        ///     <see cref="OpenLayers.Base.Device">an open layers device</see>.</param>
        /// <param name="elementNumber">Specifies the subsystem logical element number.</param>
        public DigitalInputSubsystem( OpenLayers.Base.Device device, int elementNumber ) : base( device, elementNumber )
        {
        }

        /// <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        /// <param name="disposing">True if this method releases both managed and unmanaged 
        ///   resources; <c>False</c> if this method releases only unmanaged resources.</param>
        /// <remarks>Executes in two distinct scenarios as determined by
        ///   its disposing parameter.  If True, the method has been called directly or 
        ///   indirectly by a user's code--managed and unmanaged resources can be disposed.
        ///   If disposing equals False, the method has been called by the 
        ///   runtime from inside the finalizer and you should not reference 
        ///   other objects--only unmanaged resources can be disposed.</remarks>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.disposed )
                {
                    if ( disposing )
                    {
                        // remove handlers
                        this.HandlesBufferDoneEvents = false;
                    }
                    // Free shared unmanaged resources
                }
            }
            finally
            {
                // Invoke the base class dispose method
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " SUBSYSTEM "

        /// <summary> Configures a single point digital input sub-system. </summary>
        /// <param name="dataFlow"> The data flow. </param>
        public void Configure( OpenLayers.Base.DataFlow dataFlow )
        {
            this.DataFlow = dataFlow;
            base.Config();
        }

        /// <summary> Gets a single value from the sub system. </summary>
        /// <value> The single reading. </value>
        public int SingleReading
        {
            get {
                this.LastSingleReading = this.GetSingleValue();
                return this.LastSingleReading;
            }
        }

        #endregion

        #region " CHANNELS "

        /// <summary>
        /// Add a set of channels.
        /// </summary>
        /// <param name="firstPhysicalChannel">The first physical channel.</param>
        /// <param name="lastPhysicalChannel">The last physical channel.</param>
        /// <returns>OpenLayers.Base.ChannelList.</returns>
        public OpenLayers.Base.ChannelList AddChannels( int firstPhysicalChannel, int lastPhysicalChannel )
        {
            this.ChannelList.Clear();

            // set the channel numbers in the channel list
            for ( int phyicalChannelNumber = firstPhysicalChannel, loopTo = lastPhysicalChannel; phyicalChannelNumber <= loopTo; phyicalChannelNumber++ )

                // add the channel to the list
                _ = this.ChannelList.Add( phyicalChannelNumber );
            return this.ChannelList;
        }

        /// <summary> Adds a set of channels. </summary>
        /// <param name="firstPhysicalChannel"> The first physical channel. </param>
        /// <param name="lastPhysicalChannel">  The last physical channel. </param>
        /// <param name="gain">                 The gain. </param>
        /// <returns> The channel list. </returns>
        public OpenLayers.Base.ChannelList AddChannels( int firstPhysicalChannel, int lastPhysicalChannel, double gain )
        {
            this.ChannelList.Clear();

            // set the channel numbers in the channel list
            for ( int phyicalChannelNumber = firstPhysicalChannel, loopTo = lastPhysicalChannel; phyicalChannelNumber <= loopTo; phyicalChannelNumber++ )

                // add the channel to the list
                this.ChannelList.Add( phyicalChannelNumber ).Gain = gain;
            return this.ChannelList;
        }

        /// <summary>Gets or sets reference to the selected channel for this subsystem.
        /// </summary>
        public OpenLayers.Base.ChannelListEntry SelectedChannel { get; private set; }

        /// <summary>
        /// Gets or sets the selected channel gain
        /// </summary>
        /// <value>The selected channel gain.</value>
        /// <exception cref="System.InvalidOperationException">No channel selected.</exception>
        public double SelectedChannelGain
        {
            get {
                if ( this.SelectedChannel is not object )
                {
                    // return zero if no channel selected - no exception allowed in Get methods.
                    return 0d;
                }
                else
                {
                    return this.SelectedChannel.Gain;
                }
            }

            set => this.SelectedChannel.Gain = this.SelectedChannel is not object ? throw new InvalidOperationException( "No channel selected." ) : value;
        }

        /// <summary>
        /// Gets the logical number (index) of the selected channel.
        /// </summary>
        public int SelectedChannelLogicalNumber { get; private set; }

        /// <summary>
        /// Gets the selected channel physical number.
        /// </summary>
        public int SelectedChannelPhysicalNumber => this.SelectedChannel.PhysicalChannelNumber;

        /// <summary>
        /// Selects a channel.
        /// </summary>
        /// <param name="logicalChannelNumber">The logical channel number
        /// (the channel index in the <see cref="OpenLayers.Base.ChannelList">channel list</see>.)</param>
        /// <returns>OpenLayers.Base.ChannelListEntry.</returns>
        public OpenLayers.Base.ChannelListEntry SelectLogicalChannel( int logicalChannelNumber )
        {
            this.SelectedChannel = this.ChannelList.SelectLogicalChannel( logicalChannelNumber );
            return this.SelectedChannel;
        }

        /// <summary>Selects a channel from the subsystem channel list by its physical channel number.</summary>
        /// <param name="physicalChannelNumber">Specifies the physical channel number.</param>
        public OpenLayers.Base.ChannelListEntry SelectPhysicalChannel( int physicalChannelNumber )
        {
            if ( this.PhysicalChannelExists( physicalChannelNumber ) )
            {
                this.SelectedChannelLogicalNumber = this.ChannelList.LogicalChannelNumber( physicalChannelNumber );
                this.SelectedChannel = this.ChannelList[this.SelectedChannelLogicalNumber];
            }

            return this.SelectedChannel;
        }

        /// <summary>
        /// Returns true if the specified channel exists.
        /// </summary>
        /// <param name="physicalChannelNumber">Specifies the physical channel number.</param>
        public bool PhysicalChannelExists( int physicalChannelNumber )
        {
            return this.ChannelList.Contains( physicalChannelNumber );
        }

        #endregion

        #region " METHODS "

        /// <summary>Aborts the subsystem operations.</summary>
        public override void Abort()
        {
            base.Abort();
            if ( this.BufferQueue is object )
            {
                this.BufferQueue.FreeAllQueuedBuffers();
            }
        }

        /// <summary>Configures the digital input for single channel input.</summary>
        public void ConfigureSingleInput()
        {
            this.SelectedChannel = this.ChannelList.Count == 0 ? this.ChannelList.Add( 0 ) : this.ChannelList[0];
            this.SelectedChannel.Gain = 1d;
        }

        /// <summary>Starts the subsystem operations.</summary>
        public override void Start()
        {
            base.Start();
        }

        /// <summary>Stops the subsystem operations.</summary>
        public override void Stop()
        {
            base.Stop();
        }

        #endregion

        #region " PROPERTIES "

        /// <summary>
        /// Returns true if the subsystem has channels defined.
        /// </summary>
        public bool HasChannels => this.ChannelList.Count > 0;

        /// <summary>Returns the last input reading set using <see cref="SingleReading"/>.</summary>
        public int LastSingleReading { get; private set; }

        private bool _HandlesBufferDoneEvents;
        /// <summary>
        /// Gets or Sets the condition as True have the sub system process buffer done events.
        /// </summary>
        public bool HandlesBufferDoneEvents
        {
            get => this._HandlesBufferDoneEvents;

            set {
                if ( this._HandlesBufferDoneEvents != value )
                {
                    if ( value )
                    {
                        BufferDoneEvent += this.BufferDoneHandler;
                    }
                    else
                    {
                        BufferDoneEvent -= this.BufferDoneHandler;
                    }

                    this._HandlesBufferDoneEvents = value;
                }
            }
        }

        private bool _RetrievesReadings;
        /// <summary>Gets or Sets the condition as True have the system retrieve readings.</summary>
        public bool RetrievesReadings
        {
            get => this._RetrievesReadings;

            set {
                if ( value )
                {
                    this._HandlesBufferDoneEvents = true;
                }

                this._RetrievesReadings = value;
            }
        }

        /// <summary>Gets or sets the status message.</summary>
        /// <value>A <see cref="System.String">String</see>.</value>
        /// <remarks>Use this property to get the status message generated by the object.</remarks>
        public string StatusMessage { get; private set; } = string.Empty;

        #endregion

        #region " EVENT HANDLERS "

        /// <summary>
        /// Handles buffer done events.
        /// </summary>
        /// <param name="sender">Specifies reference to the 
        /// <see cref="OpenLayers.Base.SubsystemBase">subsystem</see></param>
        /// <param name="e">Specifies the 
        /// <see cref="OpenLayers.Base.BufferDoneEventArgs">event arguments</see>.</param>
        private void BufferDoneHandler( object sender, OpenLayers.Base.BufferDoneEventArgs e )
        {
            this.OnBufferDone( e );
        }

        /// <summary>
        /// Handle buffer done events.
        /// </summary>
        protected virtual void OnBufferDone( OpenLayers.Base.BufferDoneEventArgs e )
        {
        }

        #endregion

    }
}
