using System.ComponentModel;

namespace isr.IO.OL
{

    /// <summary> Enumerates the device families.  This was created to address device features not
    /// covered in the API such as the individual gain settings for the channel list. </summary>
    public enum DeviceFamily
    {
        /// <summary>   An enum constant representing the none option. </summary>
        [Description( "None" )]
        None,
        /// <summary>   An enum constant representing the dt 9800 option. </summary>
        [Description( "DT9800 Series" )]
        DT9800 = 9800
    }

    /// <summary> Enumerates the chart modes. </summary>
    public enum ChartModeId
    {
        /// <summary>   An enum constant representing the none option. </summary>
        None,
        /// <summary>   An enum constant representing the scope option. </summary>
        Scope,
        /// <summary>   An enum constant representing the strip chart option. </summary>
        StripChart
    }
}
