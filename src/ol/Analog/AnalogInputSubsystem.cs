using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;


namespace isr.IO.OL
{
    /// <summary> Adds functionality to the Data Translation Open Layers
    /// <see cref="OpenLayers.Base.AnalogInputSubsystem">analog input subsystem</see>. </summary>
    /// <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 8/7/2013 </para></remarks>
    public class AnalogInputSubsystem : OpenLayers.Base.AnalogInputSubsystem
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs this class. </summary>
        /// <param name="device">        A reference to
        /// <see cref="OpenLayers.Base.Device">an open layers device</see>. </param>
        /// <param name="elementNumber"> Specifies the subsystem logical element number. </param>
        public AnalogInputSubsystem( OpenLayers.Base.Device device, int elementNumber ) : base( device, elementNumber )
        {
            this._LastSingleReadingsChannel = -1;
            this._LastSingleVoltagesChannel = -1;
            this._LastSingleVoltages = new System.Collections.Generic.Dictionary<int, double>();
            this._LastSingleReadings = new System.Collections.Generic.Dictionary<int, int>();
        }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks> Executes in two distinct scenarios as determined by its disposing parameter.  If True,
        /// the method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed. </remarks>
        /// <param name="disposing"> <c>True</c> if this method releases both managed and unmanaged resources;
        /// False if this method releases only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.disposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called
                        this.HandlesBufferDoneEvents = false;
                        this.BufferQueue?.FreeAllQueuedBuffers();
                        if ( this._Buffers.Any() )
                        {
                            foreach ( Buffer buffer in this._Buffers )
                                buffer.Dispose();
                            this._Buffers = null;
                        }
                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // Invoke the base class dispose method
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " CONFIGURATION "

        /// <summary> Lists the available Analog Input physical channel numbers. </summary>
        /// <param name="format"> Specifies a format string for listing the channels. The format string
        /// should specify one element for receiving the number, e.g., '{0}'. </param>
        /// <returns> An array of <see cref="T:System.Sting"/> .</returns>
        public string[] AvailableChannels( string format )
        {
            var channels = new string[this.NumberOfChannels];
            for ( int i = 0, loopTo = this.NumberOfChannels - 1; i <= loopTo; i++ )
            {
                var channelInfo = this.SupportedChannels[i];
                channels[i] = string.Format( System.Globalization.CultureInfo.CurrentCulture, format, channelInfo.PhysicalChannelNumber );
            }

            return channels;
        }

        /// <summary> Lists the available Analog Input Ranges for the board based. </summary>
        /// <param name="format"> Specifies a format string for listing the range. The format string should
        /// specify two elements for receiving the range, e.g., '{0/1}'. </param>
        /// <returns> An array of <see cref="T:System.Sting"/> .</returns>
        public string[] AvailableBoardRanges( string format )
        {
            var ranges = new string[this.NumberOfSupportedGains];
            for ( int i = 0, loopTo = this.SupportedVoltageRanges.Length - 1; i <= loopTo; i++ )
            {
                var supportedRange = this.SupportedVoltageRanges[i];
                ranges[i] = string.Format( System.Globalization.CultureInfo.CurrentCulture, format, supportedRange.Low, supportedRange.High );
            }

            return ranges;
        }

        /// <summary> Lists the available Analog Input Ranges based on the current range and list of gains. </summary>
        /// <param name="format"> Specifies a format string for listing the range. The format string should
        /// specify two elements for receiving the range, e.g., '{0/1}'. </param>
        /// <returns> An array of <see cref="T:System.Sting"/> .</returns>
        public string[] AvailableRanges( string format )
        {
            var ranges = new string[this.NumberOfSupportedGains];
            for ( int i = 0, loopTo = this.NumberOfSupportedGains - 1; i <= loopTo; i++ )
            {
                double gain = this.SupportedGains[i];
                ranges[i] = string.Format( System.Globalization.CultureInfo.CurrentCulture, format, this.VoltageRange.Low / gain, this.VoltageRange.High / gain );
            }

            return ranges;
        }

        /// <summary> <c>True</c> if has bipolar range. </summary>
        /// <value> <c>True</c> if bipolar range; otherwise, <c>False</c>. </value>
        public bool BipolarRange => this.VoltageRange.Low < 0d;

        /// <summary> Get the index for the gain list item that best fits the voltage range to best fit the
        /// specified voltage. </summary>
        /// <param name="voltage"> Specifies the maximum voltage expected at the input. </param>
        /// <returns> The index into the list of
        /// <see cref="OpenLayers.Base.AnalogSubsystem.SupportedVoltageRanges">ranges</see>
        /// for selecting the range or gain.  Returns -1 if the voltage is too high for the maximum
        /// range. </returns>
        public int GainIndex( double voltage )
        {
            if ( this.SupportsProgrammableGain )
            {
                int i = 0;
                // Look for the highest gain
                while ( i < this.NumberOfSupportedGains && voltage <= this.VoltageRange.High / this.SupportedGains[i] )
                    i += 1;
                return i - 1;
            }
            else
            {
                return 0;
            }
        }

        /// <summary> Gets the number of channels allocated for sampling. </summary>
        /// <value> The channels count. </value>
        public int ChannelsCount => this.ChannelList.Count;

        /// <summary> Returns true if the subsystem has channels defined. </summary>
        /// <value> <c>True</c> if this instance has channels; otherwise, <c>False</c>. </value>
        public bool HasChannels => this.ChannelList.Count > 0;

        /// <summary> true to handles buffer done events. </summary>
        private bool _HandlesBufferDoneEvents;

        /// <summary> Gets or sets or Sets the condition as True have the sub system process buffer done
        /// events. </summary>
        /// <value> <c>True</c> if [handles buffer done events]; otherwise, <c>False</c>. </value>
        public bool HandlesBufferDoneEvents
        {
            get => this._HandlesBufferDoneEvents;

            set {
                if ( this._HandlesBufferDoneEvents != value )
                {
                    if ( value )
                    {
                        BufferDoneEvent += this.HandleBufferDoneEvent;
                    }
                    else
                    {
                        BufferDoneEvent -= this.HandleBufferDoneEvent;
                    }

                    this._HandlesBufferDoneEvents = value;
                }
            }
        }

        /// <summary> Returns true of the subsystem supports differential inputs. </summary>
        /// <value> <c>True</c> if [supports differential input]; otherwise, <c>False</c>. </value>
        public bool SupportsDifferentialInput => this.MaxDifferentialChannels > 0;

        /// <summary> Returns true of the subsystem supports single ended inputs. </summary>
        /// <value> <c>True</c> if [supports single ended input]; otherwise, <c>False</c>. </value>
        public bool SupportsSingleEndedInput => this.MaxSingleEndedChannels > 0;

        /// <summary> Returns true of the subsystem supports different gain settings for channels on the
        /// channel list.  The DT9800 series boards support multiple gains but all gains on the channel
        /// list must be identical. </summary>
        /// <value> <c>True</c> if [supports individual gains]; otherwise, <c>False</c>. </value>
        public bool SupportsIndividualGains => this.SupportedGains.Length > 1 && this.Device.DeviceFamily() != DeviceFamily.DT9800;

        /// <summary> Returns the analog input voltage resolution. </summary>
        /// <returns> The voltage resolution. </returns>
        public double VoltageResolution()
        {
            return this.VoltageResolution( this.SelectedChannel );
        }

        /// <summary> Returns the analog input voltage resolution. </summary>
        /// <param name="channel"> Specifies a reference to a
        ///                        <see cref="OpenLayers.Base.ChannelListEntry">channel</see>. </param>
        /// <returns> The voltage resolution. </returns>
        public double VoltageResolution( OpenLayers.Base.ChannelListEntry channel )
        {
            return channel is not object
                ? throw new ArgumentNullException( nameof( channel ) )
                : (this.MaxVoltage( channel ) - this.MinVoltage( channel )) / this.Resolution;
        }

        #endregion

        #region " CONFIG "

        /// <summary> Configures a single point analog input sub-system.  The input is configured for
        /// differential input unless the sub system allows only single-ended. </summary>
        /// <param name="physicalChannelNumber"> Specifies the physical channel number. </param>
        /// <param name="gain">                  Specifies the channel gain. </param>
        public void Configure( int physicalChannelNumber, double gain )
        {
            OpenLayers.Base.ChannelListEntry channel;
#pragma warning disable CS0162 // Unreachable code detected
            if ( true )
            {
                this.ChannelList.Clear();
                channel = this.ChannelList.Add( physicalChannelNumber );
                channel.Gain = gain;
            }
            // replace with this code
            else if ( this.ChannelList is object && this.ChannelList.Count > 1 )
            {
                this.ChannelList.Clear();
                channel = this.ChannelList.Add( physicalChannelNumber );
                channel.Gain = gain;
                _ = this.ChannelList.Add( physicalChannelNumber );
                // Else
                // channel = MyBase.ChannelList(???)
            }
#pragma warning restore CS0162 // Unreachable code detected

            this.DataFlow = OpenLayers.Base.DataFlow.SingleValue;
            base.Config();
        }

        /// <summary> Configures continuous analog input sub-system for contiguous channels starting at the
        /// specified channel. </summary>
        /// <param name="bufferCount">                The buffer count. </param>
        /// <param name="sampleCount">                The sample count. </param>
        /// <param name="firstPhysicalChannelNumber"> Specifies the physical channel number of the first
        /// channel. </param>
        /// <param name="channelCount">               The channel count. </param>
        /// <param name="samplingRate">               The sampling rate. </param>
        /// <param name="maxVoltage">                 The max voltage. </param>
        public void Configure( int bufferCount, int sampleCount, int firstPhysicalChannelNumber, int channelCount, double samplingRate, double maxVoltage )
        {

            // set the channel type
            this.ChannelType = this.ChannelType == OpenLayers.Base.ChannelType.SingleEnded ? this.SupportsSingleEnded ? OpenLayers.Base.ChannelType.SingleEnded : OpenLayers.Base.ChannelType.Differential : this.SupportsDifferential ? OpenLayers.Base.ChannelType.Differential : OpenLayers.Base.ChannelType.SingleEnded;

            // Set the board range
            if ( this.NumberOfRanges > 0 )
            {
                if ( this.BipolarRange )
                {
                    this.VoltageRange = this.SupportedVoltageRanges[0];
                }
                else if ( this.NumberOfRanges > 1 )
                {
                    this.VoltageRange = this.SupportedVoltageRanges[1];
                }
            }

            // set up for continuous operation
            this.DataFlow = OpenLayers.Base.DataFlow.Continuous;

            // DateTimeOffset.Now set the rest of the parameters

            // check if the board supports triggered scan
            if ( this.SupportsTriggeredScan )
            {

                // if we can re-trigger the scan

                // turn on the re-trigger mode
                var ts = this.TriggeredScan;
                ts.Enabled = true;

                // set the number of scans to 1
                ts.MultiScanCount = 1;

                // settings for internal trigger

                // set the scan mode to internal
                ts.RetriggerSource = OpenLayers.Base.TriggerType.Software;

                // set the re-trigger frequency to the
                // required sampling rage
                ts.RetriggerFrequency = samplingRate;

                // set the time between channels to 100 times the sampling rate
                this.Clock.Frequency = 100d * samplingRate;
            }
            else
            {

                // if we cannot re-trigger the scan, turn off the
                // re-trigger mode
                var ts = this.TriggeredScan;
                ts.Enabled = false;

                // Is A/D subsystem a Simultaneous Sample & Hold ??
                if ( this.SupportsSimultaneousSampleHold )
                {

                    // set the clock frequency
                    this.Clock.Frequency = samplingRate;
                }
                else
                {

                    // set the clock frequency times the channel count
                    this.Clock.Frequency = samplingRate * channelCount;
                }
            }

            // set up channel list
            _ = this.AddChannels( firstPhysicalChannelNumber, firstPhysicalChannelNumber + channelCount - 1, this.BestGain( maxVoltage ) );

            // allocate buffers.
            this.AllocateBuffers( bufferCount, channelCount * sampleCount );

            // apply the configuration.
            base.Config();
        }

        /// <summary>
        /// Configures triggered sampling. The input is configured for differential input unless the sub system allows only single-ended.
        /// </summary>
        /// <param name="channelCount"> The channel count. </param>
        /// <param name="samplingRate"> The sampling rate. </param>
        public void ConfigureContinuousSampling( int channelCount, double samplingRate )
        {

            // set the channel type
            this.ChannelType = this.ChannelType == OpenLayers.Base.ChannelType.SingleEnded ? this.SupportsSingleEnded ? OpenLayers.Base.ChannelType.SingleEnded : OpenLayers.Base.ChannelType.Differential : this.SupportsDifferential ? OpenLayers.Base.ChannelType.Differential : OpenLayers.Base.ChannelType.SingleEnded;

            // Set the board range
            if ( this.NumberOfRanges > 0 )
            {
                if ( this.BipolarRange )
                {
                    this.VoltageRange = this.SupportedVoltageRanges[0];
                }
                // MyBase.VoltageRange.High = MyBase.SupportedVoltageRanges(0).High
                // MyBase.VoltageRange.Low = MyBase.SupportedVoltageRanges(0).Low
                else if ( this.NumberOfRanges > 1 )
                {
                    this.VoltageRange = this.SupportedVoltageRanges[1];
                    // MyBase.VoltageRange.High = MyBase.SupportedVoltageRanges(1).High
                    // MyBase.VoltageRange.Low = MyBase.SupportedVoltageRanges(1).Low
                }
            }

            // set up for continuous operation
            this.DataFlow = OpenLayers.Base.DataFlow.Continuous;

            // DateTimeOffset.Now set the rest of the parameters

            // check if the board supports triggered scan
            if ( this.SupportsTriggeredScan )
            {

                // if we can re-trigger the scan

                // turn on the re-trigger mode
                var ts = this.TriggeredScan;
                ts.Enabled = true;

                // set the number of scans to 1
                ts.MultiScanCount = 1;

                // settings for internal trigger

                // set the scan mode to internal
                ts.RetriggerSource = OpenLayers.Base.TriggerType.Software;

                // set the re-trigger frequency to the
                // required sampling rage
                ts.RetriggerFrequency = samplingRate;

                // set the time between channels to 100 times the sampling rate
                this.Clock.Frequency = 100d * samplingRate;
            }
            else
            {

                // if we cannot re-trigger the scan, turn off the
                // re-trigger mode
                var ts = this.TriggeredScan;
                ts.Enabled = false;

                // Is A/D subsystem a Simultaneous Sample & Hold ??
                if ( this.SupportsSimultaneousSampleHold )
                {

                    // set the clock frequency
                    this.Clock.Frequency = samplingRate;
                }
                else
                {

                    // set the clock frequency times the channel count
                    this.Clock.Frequency = samplingRate * channelCount;
                }
            }
        }

        #endregion

        #region " CONTINUOUS OPERATION "

        /// <summary> Stops a continuous operation on the subsystem immediately without waiting for the
        /// current buffer to be filled. </summary>
        public override void Abort()
        {
            base.Abort();
        }

        /// <summary> Stops a continuous operation on the subsystem immediately without waiting for the
        /// current buffer to be filled. </summary>
        /// <exception cref="TimeoutException"> Thrown when a Timeout error condition occurs. </exception>
        /// <param name="timeout"> The timeout waiting for operations to stop. </param>
        public void Abort( TimeSpan timeout )
        {
            if ( States.Stopping == this.State || States.Aborting == this.State )
            {
                if ( States.Stopping == this.State )
                {
                    this.Abort();
                }

                // wait for the device to stop 
                var sw = Stopwatch.StartNew();
                while ( sw.Elapsed <= timeout && (States.Stopping == this.State || States.Aborting == this.State) )
                {
                    System.Threading.Thread.Sleep( 100 );
                    DeviceInfo.DoEventsAction?.Invoke();
                }

                if ( sw.Elapsed > timeout && (States.Stopping == this.State || States.Aborting == this.State) )
                {
                    throw new TimeoutException( $"Abort timeout--system still at the '{this.State}' state after abort." );
                }
            }
            else if ( this.ContinuousOperationActive )
            {
                this.Abort();

                // wait for the device to stop 
                var sw = Stopwatch.StartNew();
                while ( sw.Elapsed <= timeout && this.ContinuousOperationActive )
                {
                    System.Threading.Thread.Sleep( 100 );
                    DeviceInfo.DoEventsAction?.Invoke();
                }

                if ( sw.Elapsed > timeout && this.ContinuousOperationActive )
                {
                    throw new TimeoutException( $"Abort timeout--system still at the '{this.State}' state after abort" );
                }
            }
        }

        /// <summary> Returns true if the analog input is in continuous operation running state. </summary>
        /// <value> The continuous operation running. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public bool ContinuousOperationRunning => States.Running == this.State;

        /// <summary> Returns true if the analog input is in continuous operation. </summary>
        /// <value> The continuous operation active. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public bool ContinuousOperationActive => States.Running == this.State || States.IoComplete == this.State || States.PreStarted == this.State;

        /// <summary> Starts the subsystem operations. </summary>
        public override void Start()
        {

            // queue allocated buffers
            _ = this.QueueAllBuffers();

            // get started.
            base.Start();
        }

        /// <summary> Stops a continuous operation on the subsystem after the current buffer is filled. </summary>
        /// <exception cref="TimeoutException"> Thrown when a Timeout error condition occurs--continuous
        /// operation still
        /// <see cref="ContinuousOperationActive">active</see> after stop. </exception>
        /// <param name="timeout"> The timeout waiting for operations to stop. </param>
        public void Stop( TimeSpan timeout )
        {
            if ( States.Stopping == this.State || States.Aborting == this.State )
            {

                // wait for the device to stop 
                var sw = Stopwatch.StartNew();
                while ( sw.Elapsed <= timeout && (States.Stopping == this.State || States.Aborting == this.State) )
                {
                    var startTime = sw.Elapsed;
                    while ( sw.Elapsed <= startTime.Add( TimeSpan.FromMilliseconds( 100 ) ) ) { }
                    DeviceInfo.DoEventsAction?.Invoke();
                }

                if ( sw.Elapsed > timeout && (States.Stopping == this.State || States.Aborting == this.State) )
                {
                    throw new TimeoutException( $"Stop timeout--system still at the '{this.State}' state after stop." );
                }
            }
            else if ( this.ContinuousOperationActive )
            {
                base.Stop();

                // wait for the device to stop 
                var sw = Stopwatch.StartNew();
                while ( sw.Elapsed <= timeout && this.ContinuousOperationActive )
                {
                    var startTime = sw.Elapsed;
                    while ( sw.Elapsed <= startTime.Add( TimeSpan.FromMilliseconds( 100 ) ) ) { }
                    DeviceInfo.DoEventsAction?.Invoke();
                }

                if ( sw.Elapsed > timeout && this.ContinuousOperationActive )
                {
                    throw new TimeoutException( $"Stop timeout--system still at the '{this.State}' state after stop." );
                }
            }
        }

        #endregion

        #region " DATA "

        /// <summary> Get the maximum gain that would still fit in the specified voltage.. </summary>
        /// <param name="voltage"> Specifies the maximum voltage expected at the input. </param>
        /// <returns> System.Double. </returns>
        public double BestGain( double voltage )
        {
            if ( this.SupportsProgrammableGain )
            {
                int i = this.GainIndex( voltage );
                return i < 0 ? this.SupportedGains[0] : this.SupportedGains[i];
            }
            else
            {
                return this.SupportedGains[0];
            }
        }

        /// <summary> Clears the means and standard deviations. </summary>
        public void ClearMeanAndSigma()
        {
            this._Means = new double[this.ChannelList.Count];
            this._StandardDeviations = new double[this.ChannelList.Count];
        }

        private double[] _Means;
        /// <summary> Returns the means array. </summary>
        /// <returns> An array of <see cref="T:System.Double"/> .</returns>
        public double[] Means()
        {
            return this._Means;
        }

        private double[] _StandardDeviations;
        /// <summary> Returns the standard deviation of the channel item number. </summary>
        /// <returns> An array of <see cref="T:System.Double"/> .</returns>
        public double[] StandardDeviations()
        {
            return this._StandardDeviations;
        }

        /// <summary> true to calculates mean sigma. </summary>
        private bool _CalculatesMeanSigma;

        /// <summary> Gets or sets or Sets the condition as True have the buffer processed to calculate
        /// mean and standard deviation. </summary>
        /// <value> <c>True</c> if [calculates mean sigma]; otherwise, <c>False</c>. </value>
        public bool CalculatesMeanSigma
        {
            get => this._CalculatesMeanSigma;

            set {
                this._CalculatesMeanSigma = value;
                if ( value )
                {
                    this.RetrievesVoltages = true;
                }
            }
        }

        /// <summary> The last single voltages channel. </summary>
        private int _LastSingleVoltagesChannel;
        /// <summary> The last single voltages. </summary>
        private readonly System.Collections.Generic.Dictionary<int, double> _LastSingleVoltages;

        /// <summary> Returns the last voltage read using <see cref="isr.IO.OL.AnalogInputSubsystem.SingleVoltage()" />. </summary>
        /// <value> The last single voltage. </value>
        public double LastSingleVoltage
        {
            get => this._LastSingleVoltages[this._LastSingleVoltagesChannel];

            private set {
                if ( this._LastSingleVoltages.ContainsKey( this._LastSingleVoltagesChannel ) )
                {
                    _ = this._LastSingleVoltages.Remove( this._LastSingleVoltagesChannel );
                }

                this._LastSingleVoltages.Add( this._LastSingleVoltagesChannel, value );
            }
        }

        /// <summary> The last single readings channel. </summary>
        private int _LastSingleReadingsChannel;
        /// <summary> The last single readings. </summary>
        private readonly System.Collections.Generic.Dictionary<int, int> _LastSingleReadings;

        /// <summary> Returns the last input reading read using <see cref="isr.IO.OL.AnalogInputSubsystem.SingleVoltage()" />. </summary>
        /// <value> The last single reading. </value>
        public int LastSingleReading
        {
            get => this._LastSingleReadings[this._LastSingleReadingsChannel];

            private set {
                if ( this._LastSingleReadings.ContainsKey( this._LastSingleReadingsChannel ) )
                {
                    _ = this._LastSingleReadings.Remove( this._LastSingleReadingsChannel );
                }

                this._LastSingleVoltages.Add( this._LastSingleReadingsChannel, value );
            }
        }

        /// <summary> Returns the subsystem maximum voltage. </summary>
        /// <returns> System.Double. </returns>
        public double MaxVoltage()
        {
            return this.MaxVoltage( this.SelectedChannel );
        }

        /// <summary> Returns the subsystem maximum voltage. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="channel"> Specifies a reference to a
        /// <see cref="OpenLayers.Base.ChannelListEntry">channel</see>. </param>
        /// <returns> System.Double. </returns>
        public double MaxVoltage( OpenLayers.Base.ChannelListEntry channel )
        {
            return channel is not object
                ? throw new ArgumentNullException( nameof( channel ) )
                : channel.Gain == 0d ? 0d : this.VoltageRange.High / channel.Gain;
        }

        /// <summary> Returns the subsystem minimum voltage. </summary>
        /// <returns> System.Double. </returns>
        public double MinVoltage()
        {
            return this.MinVoltage( this.SelectedChannel );
        }

        /// <summary> Returns the subsystem minimum voltage. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="channel"> Specifies a reference to a
        /// <see cref="OpenLayers.Base.ChannelListEntry">channel</see>. </param>
        /// <returns> System.Double. </returns>
        public double MinVoltage( OpenLayers.Base.ChannelListEntry channel )
        {
            return channel is not object
                ? throw new ArgumentNullException( nameof( channel ) )
                : channel.Gain == 0d ? 0d : this.VoltageRange.Low / channel.Gain;
        }

        /// <summary> Gets or Sets the condition as True have the system retrieve readings as long unwrapped values. </summary>
        /// <value> <c>True</c> if retrieves unwrapped readings; otherwise, <c>False</c>. </value>
        public bool RetrievesUnwrappedReadings { get; set; }

        /// <summary> Gets or Sets the condition as True have the system retrieve readings. </summary>
        /// <value> <c>True</c> if [retrieves readings]; otherwise, <c>False</c>. </value>
        public bool RetrievesReadings { get; set; }

        /// <summary> Gets or Sets the condition as True have the system retrieve Voltages. </summary>
        /// <value> <c>True</c> if [retrieves voltages]; otherwise, <c>False</c>. </value>
        public bool RetrievesVoltages { get; set; }

        /// <summary> Returns a single reading from the selected channel. </summary>
        /// <returns> The single reading. </returns>>
        public int SingleReading()
        {
            return this.SingleReading( this.SelectedChannel );
        }

        /// <summary> Returns a single reading from the specified channel. </summary>
        /// <param name="channel"> Specifies a reference to a
        ///                        <see cref="OpenLayers.Base.ChannelListEntry">channel</see>. </param>
        /// <returns> The single reading. </returns>
        public int SingleReading( OpenLayers.Base.ChannelListEntry channel )
        {
            return channel is not object
                ? throw new ArgumentNullException( nameof( channel ) )
                : this.SingleReading( channel.PhysicalChannelNumber, channel.Gain );
        }

        /// <summary> Returns a single value from the sub system. </summary>
        /// <param name="physicalChannelNumber"> Specifies the physical channel number. </param>
        /// <param name="gain">                  The gain. </param>
        /// <returns> The single reading. </returns>
        public int SingleReading( int physicalChannelNumber, double gain )
        {
            this._LastSingleReadingsChannel = physicalChannelNumber;
            int value = this.GetSingleValueAsRaw( physicalChannelNumber, gain );
            this.LastSingleReading = value;
            return value;
        }

        /// <summary> Returns a single voltage from the selected channel. </summary>
        /// <returns> The single voltage. </returns>
        public double SingleVoltage()
        {
            return this.SingleVoltage( this.SelectedChannel );
        }

        /// <summary> Returns a single voltage from the specified channel. </summary>
        /// <param name="channel"> Specifies a reference to a
        ///                        <see cref="OpenLayers.Base.ChannelListEntry">channel</see>. </param>
        /// <returns> The single voltage. </returns>
        public double SingleVoltage( OpenLayers.Base.ChannelListEntry channel )
        {
            if ( channel is not object )
            {
                throw new ArgumentNullException( nameof( channel ) );
            }

            this._LastSingleVoltagesChannel = channel.PhysicalChannelNumber;
            double value = this.GetSingleValueAsVolts( channel.PhysicalChannelNumber, channel.Gain );
            this.LastSingleVoltage = value;
            return value;
        }

        /// <summary> Returns a single voltage from the specified channel. </summary>
        /// <param name="physicalChannelNumber"> Specifies the physical channel number. </param>
        /// <param name="gain">                  Specifies the channel gain. </param>
        /// <returns> The single voltage. </returns>
        public double SingleVoltage( int physicalChannelNumber, double gain )
        {
            this._LastSingleVoltagesChannel = physicalChannelNumber;
            double value = this.GetSingleValueAsVolts( physicalChannelNumber, gain );
            this.LastSingleVoltage = value;
            return value;
        }

        #endregion

        #region " DATA EPOCH  "

        /// <summary> Validates the data epoch. </summary>
        /// <param name="dataArray">    Array of data. </param>
        /// <param name="currentIndex"> Specifies the last data point collected into the array. </param>
        /// <param name="pointCount">   Specifies the number of data points to return. </param>
        /// <param name="currentCount"> Specifies the total number of valid points in the array. </param>
        /// <param name="details">      [in,out] The details. </param>
        /// <returns> <c>True</c> if valid; otherwise, <c>False</c>. </returns>
        public static bool TryValidateDataEpoch( double[] dataArray, int currentIndex, int pointCount, long currentCount, ref string details )
        {
            if ( dataArray is not object || dataArray.Length == 0 )
            {
                details = "Data array is nothing or empty";
                return false;
            }
            else if ( pointCount < 1 || pointCount > dataArray.Length )
            {
                details = $"Point count {pointCount,0} must be positive up to {dataArray.Length}";
                return false;
            }
            else if ( currentIndex < dataArray.GetLowerBound( 0 ) || currentIndex > dataArray.GetUpperBound( 0 ) )
            {
                details = $"Current index {currentIndex} must be between {dataArray.GetLowerBound( 0 )} and {dataArray.GetUpperBound( 0 )}";
                return false;
            }
            else if ( currentIndex >= currentCount )
            {
                details = $"Current index {currentIndex} must be between lower than the current count of valid values {currentCount}";
                return false;
            }

            return true;
        }

        /// <summary> Validates the data epoch. </summary>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="dataArray">    Array of data. </param>
        /// <param name="currentIndex"> Specifies the last data point collected into the array. </param>
        /// <param name="pointCount">   Specifies the number of data points to return. </param>
        /// <param name="currentCount"> Specifies the total number of valid points in the array. </param>
        public static void ValidateDataEpoch( double[] dataArray, int currentIndex, int pointCount, long currentCount )
        {
            if ( dataArray is not object || dataArray.Length == 0 )
            {
                throw new ArgumentNullException( nameof( dataArray ) );
            }
            else if ( pointCount < 1 || pointCount > dataArray.Length )
            {
                throw new ArgumentOutOfRangeException( nameof( pointCount ), pointCount, $"Must be positive up to {dataArray.Length}" );
            }
            else if ( currentIndex < dataArray.GetLowerBound( 0 ) || currentIndex > dataArray.GetUpperBound( 0 ) )
            {
                throw new ArgumentOutOfRangeException( nameof( currentIndex ), currentIndex, $"Must be between {dataArray.GetLowerBound( 0 )} and {dataArray.GetUpperBound( 0 )}" );
            }
            else if ( currentIndex >= currentCount )
            {
                throw new ArgumentOutOfRangeException( nameof( currentIndex ), currentIndex, $"Must be between lower than the current count of valid values {currentCount}" );
            }
        }

        /// <summary> Return a voltages array from the specified voltages. </summary>
        /// <remarks> Use <see cref="ValidateDataEpoch">validate</see> or
        /// <see cref="TryValidateDataEpoch">try validate</see> to validate the argument. Validation is
        /// excluded to improve speed. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="dataArray">    Array of data. </param>
        /// <param name="currentIndex"> Specifies the last data point collected into the array. </param>
        /// <param name="pointCount">   Specifies the number of data points to return. </param>
        /// <param name="currentCount"> Specifies the total number of valid points in the array. </param>
        /// <returns> Voltages array from the specified voltages. </returns>
        public static double[] DataEpoch( double[] dataArray, int currentIndex, int pointCount, long currentCount )
        {
            if ( dataArray is not object )
            {
                throw new ArgumentNullException( nameof( dataArray ) );
            }

            var epoch = new double[pointCount];
            if ( currentIndex < 0 || currentCount <= 0L )
            {

                // if we have no data, return the zero-valued epoch
                return epoch;
            }

            int i = currentIndex;
            int k = pointCount;
            if ( k > currentCount )
            {
                k = ( int ) currentCount;
            }

            // fill data from the end of the data set to the end of the signal.
            int j = pointCount;
            do
            {
                j -= 1;
                epoch[j] = dataArray[i];
                i -= 1;
                if ( i < 0 )
                {
                    i = dataArray.Length - 1;
                }

                k -= 1;
            }
            while ( k > 0 );
            return epoch;
        }

        /// <summary> Returns the average. </summary>
        /// <param name="dataArray"> . </param>
        /// <returns> The average value. </returns>
        public static double Average( double[] dataArray )
        {
            if ( dataArray is not object || dataArray.Length == 0 )
            {
                return 0d;
            }

            var sum = default( double );
            foreach ( double value in dataArray )
                sum += value;
            return sum / dataArray.Length;
        }

        /// <summary> Returns the standard deviation. </summary>
        /// <param name="dataArray"> . </param>
        /// <param name="mean">      . </param>
        /// <returns> The standard deviation. </returns>
        public static double StandardDeviation( double[] dataArray, double mean )
        {
            if ( dataArray is not object || dataArray.Length == 0 )
            {
                return 0d;
            }
            else if ( dataArray.Length == 1 )
            {
                double value = dataArray[0] - mean;
                return value * value;
            }

            var sum = default( double );
            foreach ( double value in dataArray )
            {
                double v = value - mean;
                sum += v * v;
            }

            return sum / (dataArray.Length - 1);
        }

        #endregion

        #region " BUFFERS "

        /// <summary> The buffers. </summary>
        private Buffer[] _Buffers;

        /// <summary> Select buffer. </summary>
        /// <param name="bufferIndex"> Zero-based index of the buffer. </param>
        /// <returns> An OpenLayers.Base.OlBuffer. </returns>
        public OpenLayers.Base.OlBuffer SelectBuffer( int bufferIndex )
        {
            return this._Buffers[bufferIndex];
        }

        /// <summary> Fill buffers. </summary>
        /// <param name="sampleValues"> The sample values. </param>
        [CLSCompliant( false )]
        public void FillBuffers( uint[][] sampleValues )
        {
            Buffer.FillBuffers( sampleValues, this._Buffers );
        }

        /// <summary> Allocates a set of buffers for data collection. </summary>
        /// <param name="bufferCount">  The buffer count. </param>
        /// <param name="bufferLength"> Length of the buffer. </param>
        public void AllocateBuffers( int bufferCount, int bufferLength )
        {
            // save the buffer length
            this.BufferLength = bufferLength;

            // allocate the buffers array.  This is needed to buffer can be queued when restarting.
            this._Buffers = new Buffer[bufferCount];

            // allocate queue for data collection
            var i = default( int );
            while ( i < bufferCount )
            {
                this._Buffers[i] = new Buffer( this.BufferLength, this );
                i += 1;
            }
        }

        /// <summary> Gets the number of buffers that were allocated. </summary>
        /// <value> The buffers count. </value>
        public int BuffersCount => this._Buffers is not object ? 0 : this._Buffers.Length;

        /// <summary> Gets buffer size that was allocated. </summary>
        /// <value> The length of the buffer. </value>
        public int BufferLength { get; private set; }

        /// <summary> Queue all allocated buffers. </summary>
        /// <returns> An OpenLayers.Base.BufferQueue. </returns>
        public OpenLayers.Base.BufferQueue QueueAllBuffers()
        {
            this.BufferQueue.FreeAllQueuedBuffers();
            return this.QueueAllFreeBuffers();
        }

        /// <summary> Queue all allocated buffers which states are Idle or Completed. </summary>
        public OpenLayers.Base.BufferQueue QueueAllFreeBuffers()
        {
            foreach ( OpenLayers.Base.OlBuffer buffer in this._Buffers )
            {
                if ( buffer.State == OpenLayers.Base.OlBuffer.BufferState.Idle || buffer.State == OpenLayers.Base.OlBuffer.BufferState.Completed )
                {
                    this.BufferQueue.QueueBuffer( buffer );
                }
            }

            return this.BufferQueue;
        }

        /// <summary> The voltages. </summary>
        private double[][] _Voltages;

        /// <summary> Returns a read only reference to the voltage array. </summary>
        /// <returns> An array of <see cref="T:System.Double"/> .</returns>
        public double[][] Voltages()
        {
            return this._Voltages;
        }

        /// <summary> The readings. </summary>
        private int[][] _Readings;

        /// <summary> Returns a read only reference to the readings array. </summary>
        /// <returns> An array of <see cref="T:System.UInteger"/> .</returns>
        public int[][] Readings()
        {
            return this._Readings;
        }

        #endregion

        #region " CHANNELS "

        /// <summary> Adds a channel to the channel list. </summary>
        /// <param name="channelNumber"> The channel number. </param>
        /// <param name="gain">          The gain. </param>
        /// <param name="name">          The name. </param>
        /// <returns> OpenLayers.Base.ChannelListEntry. </returns>
        public OpenLayers.Base.ChannelListEntry AddChannel( int channelNumber, double gain, string name )
        {

            // add the new channel to the channel list.
            this.SelectedChannelLogicalNumber = this.ChannelList.Count;
            this.SelectedChannel = this.ChannelList.Add( channelNumber );

            // set the channel name or leave the default name.
            if ( !string.IsNullOrWhiteSpace( name ) && (this.SelectedChannelInfo.Name ?? "") != (name ?? "") )
            {
                this.SelectedChannelInfo.Name = name;
            }

            // set channel gain.
            if ( this.SupportsProgrammableGain )
            {
                int gainIndex = this.SupportedGainIndex( gain );
                if ( gainIndex >= 0 )
                {
                    if ( this.SupportsIndividualGains )
                    {
                        this.SelectedChannel.Gain = gain;
                    }
                    else
                    {
                        foreach ( OpenLayers.Base.ChannelListEntry channel in this.ChannelList )
                            channel.Gain = gain;
                    }
                }
            }

            return this.SelectedChannel;
        }

        /// <summary> Adds a channel to the channel list. </summary>
        /// <param name="channelNumber"> The channel number. </param>
        /// <param name="gainIndex">     Index of the gain. </param>
        /// <param name="name">          The name. </param>
        /// <returns> OpenLayers.Base.ChannelListEntry. </returns>
        public OpenLayers.Base.ChannelListEntry AddChannel( int channelNumber, int gainIndex, string name )
        {

            // add the new channel to the channel list.
            this.SelectedChannelLogicalNumber = this.ChannelList.Count;
            this.SelectedChannel = this.ChannelList.Add( channelNumber );

            // set the channel name or leave the default name.
            if ( !string.IsNullOrWhiteSpace( name ) && (this.SelectedChannelInfo.Name ?? "") != (name ?? "") )
            {
                this.SelectedChannelInfo.Name = name;
            }

            // set channel gain.
            if ( this.SupportsProgrammableGain )
            {
                if ( this.SupportsIndividualGains )
                {
                    this.SelectedChannel.Gain = this.SupportedGains[gainIndex];
                }
                else
                {
                    foreach ( OpenLayers.Base.ChannelListEntry channel in this.ChannelList )
                        channel.Gain = this.SupportedGains[gainIndex];
                }
            }

            return this.SelectedChannel;
        }

        /// <summary> The gain to find. </summary>
        private double _GainToFind;

        /// <summary> A predicate to call for finding the specified gain in the supported gain list. </summary>
        /// <param name="gain"> The gain. </param>
        /// <returns> <c>True</c> if gain was found, <c>False</c> otherwise. </returns>
        private bool FindGain( double gain )
        {
            double limen = 0.0001d;
            return Math.Abs( gain - this._GainToFind ) < limen;
        }

        /// <summary> Returns the index to the
        /// <see cref="OpenLayers.Base.AnalogSubsystem.SupportedGains">supported gains</see> for this <paramref name="gain" />. </summary>
        /// <param name="gain"> The gain. </param>
        /// <returns> System.Int32. </returns>
        public int SupportedGainIndex( double gain )
        {
            this._GainToFind = gain;
            return Array.FindIndex( this.SupportedGains, new Predicate<double>( this.FindGain ) );
        }

        /// <summary> Adds a channel to the channel list. </summary>
        /// <param name="channelNumber"> The channel number. </param>
        /// <param name="gainIndex">     Index of the gain. </param>
        /// <returns> OpenLayers.Base.ChannelListEntry. </returns>
        public OpenLayers.Base.ChannelListEntry AddChannel( int channelNumber, int gainIndex )
        {

            // add the new channel to the channel list.
            this.SelectedChannelLogicalNumber = this.ChannelList.Count;
            this.SelectedChannel = this.ChannelList.Add( channelNumber );

            // set channel gain.
            if ( this.SupportsProgrammableGain )
            {
                if ( this.SupportsIndividualGains )
                {
                    this.SelectedChannel.Gain = this.SupportedGains[gainIndex];
                }
                else
                {
                    foreach ( OpenLayers.Base.ChannelListEntry channel in this.ChannelList )
                        channel.Gain = this.SupportedGains[gainIndex];
                }
            }

            return this.SelectedChannel;
        }

        /// <summary> Add a set of channels. </summary>
        /// <param name="firstPhysicalChannel"> The first physical channel. </param>
        /// <param name="lastPhysicalChannel">  The last physical channel. </param>
        /// <returns> OpenLayers.Base.ChannelList. </returns>
        public OpenLayers.Base.ChannelList AddChannels( int firstPhysicalChannel, int lastPhysicalChannel )
        {
            this.ChannelList.Clear();

            // set the channel numbers in the channel list
            for ( int phyicalChannelNumber = firstPhysicalChannel, loopTo = lastPhysicalChannel; phyicalChannelNumber <= loopTo; phyicalChannelNumber++ )

                // add the channel to the list
                _ = this.ChannelList.Add( phyicalChannelNumber );
            return this.ChannelList;
        }

        /// <summary> Add a set of channels. </summary>
        /// <param name="firstPhysicalChannel"> The first physical channel. </param>
        /// <param name="lastPhysicalChannel">  The last physical channel. </param>
        /// <param name="gain">                 The gain. </param>
        /// <returns> OpenLayers.Base.ChannelList. </returns>
        public OpenLayers.Base.ChannelList AddChannels( int firstPhysicalChannel, int lastPhysicalChannel, double gain )
        {
            this.ChannelList.Clear();

            // set the channel numbers in the channel list
            for ( int phyicalChannelNumber = firstPhysicalChannel, loopTo = lastPhysicalChannel; phyicalChannelNumber <= loopTo; phyicalChannelNumber++ )

                // add the channel to the list
                this.ChannelList.Add( phyicalChannelNumber ).Gain = gain;
            return this.ChannelList;
        }

        /// <summary> Returns count of Analog Input Channels. </summary>
        /// <value> The available channels count. </value>
        public int AvailableChannelsCount => this.ChannelType == OpenLayers.Base.ChannelType.Differential ? this.MaxDifferentialChannels : this.MaxSingleEndedChannels;

        /// <summary> Gets or sets the selected channel gain. </summary>
        /// <value> The selected channel gain. </value>
        public double SelectedChannelGain
        {
            get {
                if ( this.SelectedChannel is not object )
                {
                    // return zero if no channel selected - no exception allowed in Get methods.
                    return 0d;
                }
                else
                {
                    return this.SelectedChannel.Gain;
                }
            }

            set => this.SelectedChannel.Gain = this.SelectedChannel is not object ? throw new InvalidOperationException( "No channel selected." ) : value;
        }

        /// <summary> Gets the logical number (index) of the selected channel. </summary>
        /// <value> The selected channel logical number. </value>
        public int SelectedChannelLogicalNumber { get; private set; }

        /// <summary> Returns the channel info for the selected channel.  If no channel was selected this
        /// returns the info for physical channel 0. </summary>
        /// <value> The selected channel info. </value>
        public OpenLayers.Base.SupportedChannelInfo SelectedChannelInfo => this.SelectedChannel is object ? this.SupportedChannels.GetChannelInfo( this.SelectedChannel.PhysicalChannelNumber ) : this.SupportedChannels.GetChannelInfo( 0 );

        /// <summary> Gets reference to the selected channel for this subsystem. </summary>
        /// <value> The selected channel. </value>
        public OpenLayers.Base.ChannelListEntry SelectedChannel { get; private set; }

        /// <summary> Gets the selected channel physical number. </summary>
        /// <value> The selected channel physical number. </value>
        public int SelectedChannelPhysicalNumber => this.SelectedChannel.PhysicalChannelNumber;

        /// <summary> Selects a channel. </summary>
        /// <param name="logicalChannelNumber"> The logical channel number (channel index in the
        /// <see cref="OpenLayers.Base.ChannelList">channel list</see>.) </param>
        /// <returns> OpenLayers.Base.ChannelListEntry. </returns>
        public OpenLayers.Base.ChannelListEntry SelectLogicalChannel( int logicalChannelNumber )
        {
            this.SelectedChannel = this.ChannelList.SelectLogicalChannel( logicalChannelNumber );
            return this.SelectedChannel;
        }

        /// <summary> Selects a channel from the subsystem channel list by its channel name. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="channelName"> Specifies the physical channel number. </param>
        /// <returns> OpenLayers.Base.ChannelListEntry. </returns>
        public OpenLayers.Base.ChannelListEntry SelectNamedChannel( string channelName )
        {
            if ( string.IsNullOrWhiteSpace( channelName ) )
            {
                throw new ArgumentNullException( nameof( channelName ), "Channel name must not be empty." );
            }
            else
            {
                this.SelectedChannel = this.ChannelList.SelectNamedChannel( channelName );
                return this.SelectedChannel;
            }
        }

        /// <summary> Queries if a given named channel exists. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="channelName"> Specifies the physical channel number. </param>
        /// <returns> <c>True</c> if it succeeds, false if it fails. </returns>
        public bool NamedChannelExists( string channelName )
        {
            return string.IsNullOrWhiteSpace( channelName )
                ? throw new ArgumentNullException( nameof( channelName ), "Channel name must not be empty." )
                : this.ChannelList.Contains( channelName );
        }

        /// <summary> Selects a channel from the subsystem channel list by its physical channel number. </summary>
        /// <param name="physicalChannelNumber"> Specifies the physical channel number. </param>
        /// <returns> OpenLayers.Base.ChannelListEntry. </returns>
        public OpenLayers.Base.ChannelListEntry SelectPhysicalChannel( int physicalChannelNumber )
        {
            if ( this.PhysicalChannelExists( physicalChannelNumber ) )
            {
                this.SelectedChannelLogicalNumber = this.ChannelList.LogicalChannelNumber( physicalChannelNumber );
                this.SelectedChannel = this.ChannelList[this.SelectedChannelLogicalNumber];
            }

            return this.SelectedChannel;
        }

        /// <summary> Queries if a given physical channel exists. </summary>
        /// <param name="physicalChannelNumber"> Specifies the physical channel number. </param>
        /// <returns> <c>True</c> if it succeeds, false if it fails. </returns>
        public bool PhysicalChannelExists( int physicalChannelNumber )
        {
            return this.ChannelList.Contains( physicalChannelNumber );
        }

        /// <summary> Updates a channel in the channel list. </summary>
        /// <param name="channelNumber"> The channel number. </param>
        /// <param name="gainIndex">     Index of the gain. </param>
        /// <param name="name">          The name. </param>
        /// <returns> OpenLayers.Base.ChannelListEntry. </returns>
        public OpenLayers.Base.ChannelListEntry UpdateChannel( int channelNumber, int gainIndex, string name )
        {

            // select the channel
            _ = this.SelectPhysicalChannel( channelNumber );

            // set the channel name or leave the default name.
            if ( !string.IsNullOrWhiteSpace( name ) && (this.SelectedChannelInfo.Name ?? "") != (name ?? "") )
            {
                this.SelectedChannelInfo.Name = name;
            }

            // set channel gain.
            if ( this.SupportsProgrammableGain )
            {
                if ( this.SupportsIndividualGains )
                {
                    this.SelectedChannel.Gain = this.SupportedGains[gainIndex];
                }
                else
                {
                    foreach ( OpenLayers.Base.ChannelListEntry channel in this.ChannelList )
                        channel.Gain = this.SupportedGains[gainIndex];
                }
            }

            return this.SelectedChannel;
        }

        #endregion

        #region " EVENT HANDLERS: BUFFER DONE "

        /// <summary> Handles buffer done events. </summary>
        /// <param name="sender"> Specifies reference to the
        /// <see cref="OpenLayers.Base.SubsystemBase">subsystem</see> </param>
        /// <param name="e">      Specifies the
        /// <see cref="OpenLayers.Base.BufferDoneEventArgs">event arguments</see>. </param>
        private void HandleBufferDoneEvent( object sender, OpenLayers.Base.BufferDoneEventArgs e )
        {
            this.OnBufferDone( e );
        }

        /// <summary> Handles buffer done events. </summary>
        /// <param name="e"> Specifies the
        /// <see cref="OpenLayers.Base.BufferDoneEventArgs">event arguments</see>. </param>
        protected virtual void OnBufferDone( OpenLayers.Base.BufferDoneEventArgs e )
        {
            if ( e is not object )
                return;
            Buffer activeBuffer = ( Buffer ) e.OlBuffer;

            // check if a buffer is ready to be processed.
            if ( activeBuffer.ValidSamples > 0 )
            {
                if ( this.RetrievesReadings )
                {
                    // retrieve the readings
                    this._Readings = activeBuffer.Readings( e.Subsystem.ChannelList );
                }

                if ( this.RetrievesVoltages )
                {
                    this._Voltages = activeBuffer.Voltages( e.Subsystem.ChannelList );
                    double voltage;
                    if ( this._CalculatesMeanSigma )
                    {
                        for ( int channelNumber = 0, loopTo = e.Subsystem.ChannelList.Count - 1; channelNumber <= loopTo; channelNumber++ )
                        {
                            double sum = 0d;
                            double ssq = 0d;
                            int sampleCount = this._Voltages.GetUpperBound( channelNumber ) + 1;
                            for ( int sampleNumber = 0, loopTo1 = sampleCount - 1; sampleNumber <= loopTo1; sampleNumber++ )
                            {
                                voltage = this._Voltages[channelNumber][sampleNumber];
                                sum += voltage;
                                ssq += voltage * voltage;
                            }

                            double average = sum / sampleCount;
                            this._Means[channelNumber] = average;
                            this._StandardDeviations[channelNumber] = Math.Sqrt( (ssq - average * average) / sampleCount );
                        }
                    }

                    // MyBase.OnBufferDone(e)

                }
            }
            else
            {
                if ( this.RetrievesReadings )
                {
                    if ( this._Readings is object )
                    {
                        // clear the readings
                        Array.Clear( this._Readings, 0, this._Readings.Length );
                    }
                }

                if ( this.RetrievesVoltages )
                {
                    if ( this._Voltages is object )
                    {
                        Array.Clear( this._Voltages, 0, this._Voltages.Length );
                    }

                    if ( this._Means is object )
                    {
                        Array.Clear( this.Means(), 0, this.Means().Length );
                    }

                    if ( this._StandardDeviations is object )
                    {
                        Array.Clear( this._StandardDeviations, 0, this._StandardDeviations.Length );
                    }
                }
            }

            // recycle the buffer
            e.Subsystem.BufferQueue.QueueBuffer( activeBuffer );
        }

        #endregion

    }
}
