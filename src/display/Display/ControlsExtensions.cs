using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace isr.IO.OL.Display
{

    /// <summary>   The controls extensions. </summary>
    /// <remarks>   David, 2022-01-12. </remarks>
    public static class ControlsExtensions
    {

        #region " CHECK BOX "

        /// <summary>
        /// Sets the <see cref="Control">check box</see> checked value to the
        /// <paramref name="value">value</paramref>.
        /// This setter disables the control before altering the checked state allowing the control code
        /// to use the enabled state for preventing the execution of the control checked change actions.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="control"> Check box control. </param>
        /// <param name="value">   The value. </param>
        /// <returns> value. </returns>
        public static CheckState SilentCheckStateSetter( this CheckBox control, CheckState value )
        {
            if ( control is object )
            {
                if ( control.ThreeState )
                {
                    bool wasEnabled = control.Enabled;
                    control.Enabled = false;
                    control.CheckState = value;
                    control.Enabled = wasEnabled;
                }
                else
                {
                    throw new InvalidOperationException( "Attempted to set a two-state control with a three-state value." );
                }
            }

            return value;
        }

        /// <summary>
        /// Sets the <see cref="Control">check box</see> check state value to the
        /// <paramref name="value">value</paramref>.
        /// This setter disables the control before altering the checked state allowing the control code
        /// to use the enabled state for preventing the execution of the control checked change actions.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> Check box control. </param>
        /// <param name="value">   The value. </param>
        /// <returns> value. </returns>
        public static bool SilentCheckedSetter( this CheckBox control, bool value )
        {
            if ( control is object )
            {
                bool wasEnabled = control.Enabled;
                control.Enabled = false;
                control.Checked = value;
                control.Enabled = wasEnabled;
            }

            return value;
        }

        /// <summary> Converts a value to a check state. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="value"> The value to convert to check state. </param>
        /// <returns>
        /// <see cref="CheckState.Indeterminate">Indeterminate</see> if nothing, otherwise, checked or
        /// unchecked.
        /// </returns>
        public static CheckState ToCheckState( this bool? value )
        {
            return !value.HasValue ? CheckState.Indeterminate : value.Value ? CheckState.Checked : CheckState.Unchecked;
        }

        /// <summary> Initializes this object from the given check state. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="value"> The value to set. </param>
        /// <returns> A Boolean? </returns>
        public static bool? FromCheckState( this CheckState value )
        {
            return value == CheckState.Checked || value == CheckState.Unchecked && new bool();
        }

        #endregion

        #region " COMBO BOX "

        #region " TEXT SETTER "

        /// <summary>
        /// Sets the <see cref="System.Windows.Forms.ComboBox">control</see> text to the
        /// <paramref name="value">value</paramref>.
        /// The control is disabled when set so that the handling of the changed event can be skipped.
        /// </summary>
        /// <remarks> The value is set to empty if null or empty. </remarks>
        /// <param name="control"> The combo box control. </param>
        /// <param name="value">   The value. </param>
        /// <returns> value. </returns>
        public static string SilentTextSetter( this ComboBox control, string value )
        {
            if ( control is object )
            {
                if ( string.IsNullOrWhiteSpace( value ) )
                {
                    value = string.Empty;
                }

                bool enabled = control.Enabled;
                control.Enabled = false;
                control.Text = value;
                control.Enabled = enabled;
            }

            return value;
        }

        #endregion

        #region " SEARCH AND SELECT "

        /// <summary> Searches the combo box and selects a located item. </summary>
        /// <remarks> Use this method to search and select combo box index. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="combo"> Specifies an instance of a ComboBox. </param>
        /// <param name="e">     Specifies an instance of the
        /// <see cref="System.Windows.Forms.KeyPressEventArgs">event arguments</see>,
        /// which specify the key pressed. </param>
        /// <returns> The found and select. </returns>
        public static int SearchAndSelect( this ComboBox combo, KeyPressEventArgs e )
        {
            if ( combo is null )
            {
                throw new ArgumentNullException( nameof( combo ) );
            }

            if ( e is object && char.IsControl( e.KeyChar ) )
            {
                return combo.SelectedIndex;
            }
            else
            {
                int cursorPosition = combo.SelectionStart;
                int selectionLength = combo.SelectionLength;
                int itemNumber = combo.FindString( combo.Text );
                if ( itemNumber >= 0 )
                {
                    // if we have a match, select the current item and reposition the cursor
                    combo.SelectedIndex = itemNumber;
                    combo.SelectionStart = cursorPosition;
                    combo.SelectionLength = selectionLength;
                    return itemNumber;
                }
                else
                {
                    return combo.SelectedIndex;
                }
            }
        }

        /// <summary> Searches the combo box and selects a located item upon releasing a key. </summary>
        /// <remarks> Use this method to search and select combo box index. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="combo"> Specifies an instance of a ComboBox. </param>
        /// <param name="e">     Specifies an instance of the
        /// <see cref="System.Windows.Forms.KeyEventArgs">event arguments</see>,
        /// which specify the key released. </param>
        /// <returns> The found and select. </returns>
        public static int SearchAndSelect( this ComboBox combo, KeyEventArgs e )
        {
            if ( combo is null )
            {
                throw new ArgumentNullException( nameof( combo ) );
            }

            if ( e is null )
            {
                throw new ArgumentNullException( nameof( e ) );
            }

            if ( e.KeyCode.ToString().Length > 1 )
            {
                // if a control character, skip
                return combo.SelectedIndex;
            }
            else
            {
                int cursorPosition = combo.SelectionStart;
                int selectionLength = combo.SelectionLength;
                int itemNumber = combo.FindString( combo.Text );
                if ( itemNumber >= 0 )
                {
                    // if we have a match, select the current item and reposition the cursor
                    combo.SelectedIndex = itemNumber;
                    combo.SelectionStart = cursorPosition;
                    combo.SelectionLength = selectionLength;
                    return itemNumber;
                }
                else
                {
                    return combo.SelectedIndex;
                }
            }
        }

        #endregion

        #region " SELECT ITEM "

        /// <summary> Determines whether the specified control contains display value. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="control">      The control. </param>
        /// <param name="displayValue"> The display value. </param>
        /// <returns>
        /// <c>True</c> if the specified control contains display value; otherwise, <c>False</c>.
        /// </returns>
        public static bool ContainsDisplayValue( this ComboBox control, string displayValue )
        {
            return control is null
                ? throw new ArgumentNullException( nameof( control ) )
                : displayValue is null ? throw new ArgumentNullException( nameof( displayValue ) ) : 0 <= control.FindStringExact( displayValue );
        }

        /// <summary>
        /// Gets the selected or default <paramref name="defaultValue">key value</paramref>.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control">      The control. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns> ``0. </returns>
        public static T GetSelectedItemKey<T>( this ComboBox control, T defaultValue )
        {
            if ( control is null || control.SelectedValue is null )
            {
                return defaultValue;
            }
            else
            {
                KeyValuePair<T, string> v = ( KeyValuePair<T, string> ) control.SelectedItem;
                return v.Key;
            }
        }

        /// <summary> Selects an item. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control">      The control. </param>
        /// <param name="displayValue"> The display value. </param>
        public static void SelectItem( this ComboBox control, string displayValue )
        {
            if ( control is object )
            {
                if ( control.SelectedValue is null || string.IsNullOrWhiteSpace( control.SelectedText ) || !control.Text.Equals( displayValue ) )
                {
                    if ( string.IsNullOrWhiteSpace( displayValue ) )
                    {
                        if ( control.SelectedIndex != -1 )
                        {
                            control.SelectedIndex = -1;
                        }
                    }
                    else
                    {
                        int i = control.FindStringExact( displayValue );
                        if ( control.SelectedIndex != i )
                        {
                            control.SelectedIndex = i;
                            if ( control.SelectedIndex == -1 && control.DropDownStyle == ComboBoxStyle.DropDown )
                            {
                                control.Text = displayValue;
                            }
                        }
                    }
                }
            }
        }

        /// <summary> Selects an item in a 'Silent' (control is disabled) way. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control">      The control. </param>
        /// <param name="displayValue"> The display value. </param>
        public static void SilentSelectItem( this ComboBox control, string displayValue )
        {
            if ( control is object )
            {
                if ( control.SelectedValue is null || string.IsNullOrWhiteSpace( control.SelectedText ) || !control.Text.Equals( displayValue ) )
                {
                    bool enabled = control.Enabled;
                    control.Enabled = false;
                    control.SelectItem( displayValue );
                    control.Enabled = enabled;
                }
            }
        }

        /// <summary>
        /// Selects the <see cref="System.Windows.Forms.ComboBox">combo box</see> item by setting the
        /// selected item to the <see cref="System.Collections.Generic.KeyValuePair{TKey, TValue}">key value
        /// pair</see>. Thhis setter disables the control before altering the checked state allowing the
        /// control code to use the enabled state for preventing the execution of the control checked
        /// change actions.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> Combo box control. </param>
        /// <param name="value">   The selected item value. </param>
        /// <returns> value. </returns>
        public static object SilentSelectItem( this ComboBox control, object value )
        {
            if ( control is object )
            {
                bool wasEnabled = control.Enabled;
                control.Enabled = false;
                control.SelectedItem = value;
                control.Enabled = wasEnabled;
            }

            return value;
        }

        /// <summary>
        /// Selects the <see Cref="System.Windows.Forms.ComboBox">combo box</see> item by setting the
        /// selected item to the <see cref="System.Collections.Generic.KeyValuePair{TKey, TValue}">key value
        /// pair</see>. This setter disables the control before altering the checked state allowing the
        /// control code to use the enabled state for preventing the execution of the control checked
        /// change actions.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> Combo box control. </param>
        /// <param name="key">     The selected item key. </param>
        /// <param name="value">   The selected item value. </param>
        /// <returns> value. </returns>
        public static object SilentSelectItem( this ComboBox control, Enum key, string value )
        {
            return control.SilentSelectItem( new KeyValuePair<Enum, string>( key, value ) );
        }

        #endregion

        #region " SELECT Value "

        /// <summary>
        /// Selects the <see cref="System.Windows.Forms.ComboBox">combo box</see> Value by setting the
        /// selected Value to the <see cref="System.Collections.Generic.KeyValuePair{TKey, TValue}">key value
        /// pair</see>. This setter disables the control before altering the checked state allowing the
        /// control code to use the enabled state for preventing the execution of the control checked
        /// change actions.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> Combo box control. </param>
        /// <param name="value">   The selected Value. </param>
        /// <returns> value. </returns>
        public static object SilentSelectValue( this ComboBox control, object value )
        {
            if ( control is object )
            {
                bool wasEnabled = control.Enabled;
                control.Enabled = false;
                control.SelectedValue = value;
                control.Enabled = wasEnabled;
            }

            return value;
        }

        #endregion

        #endregion

        #region " NUMERIC UP DOWN "

        #region " RANGE "

        /// <summary> Range setter. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="control"> The numeric up down control. </param>
        /// <param name="minimum"> The minimum. </param>
        /// <param name="maximum"> The maximum. </param>
        /// <returns> A Tuple(Of Decimal, Decimal) </returns>
        public static (decimal Min, decimal Max) RangeSetter( this NumericUpDown control, decimal minimum, decimal maximum )
        {
            if ( control is null )
            {
                throw new ArgumentNullException( nameof( control ) );
            }

            if ( control.Value > maximum )
            {
                control.Value = maximum;
            }

            if ( control.Value < minimum )
            {
                control.Value = minimum;
            }

            control.Maximum = maximum;
            control.Minimum = minimum;
            return ( control.Minimum, control.Maximum );
        }

        /// <summary> Range setter. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="control"> The numeric up down control. </param>
        /// <param name="minimum"> The minimum. </param>
        /// <param name="maximum"> The maximum. </param>
        /// <returns> A Tuple(Of Decimal, Decimal) </returns>
        public static (decimal Min, decimal Max) RangeSetter( this NumericUpDown control, double minimum, double maximum )
        {
            return control is null
                ? throw new ArgumentNullException( nameof( control ) )
                : control.RangeSetter( minimum < ( double ) decimal.MinValue ? decimal.MinValue : ( decimal ) minimum, maximum > ( double ) decimal.MaxValue ? decimal.MaxValue : ( decimal ) maximum );
        }

        #endregion

        #region " MAXIMUM "

        /// <summary>
        /// Sets the <see cref="NumericUpDown">control</see>
        /// <see cref=" NumericUpDown.Maximum">Maximum</see>.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The numeric up down control. </param>
        /// <param name="value">   The candidate Maximum. </param>
        /// <returns> The limited Maximum. </returns>
        public static decimal MaximumSetter( this NumericUpDown control, decimal value )
        {
            if ( control is object )
            {
                if ( control.Value > value )
                {
                    control.Value = value;
                }

                control.Maximum = value;
            }

            return value;
        }

        /// <summary>
        /// Sets the <see cref="NumericUpDown">control</see>
        /// <see cref=" NumericUpDown.Maximum">Maximum</see>.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The numeric up down control. </param>
        /// <param name="value">   The candidate Maximum. </param>
        /// <returns> A Decimal. </returns>
        public static decimal MaximumSetter( this NumericUpDown control, double value )
        {
            return control.MaximumSetter( value > ( double ) decimal.MaxValue ? decimal.MaxValue : ( decimal ) value );
        }

        #endregion

        #region " MINIMUM "

        /// <summary>
        /// Sets the <see cref="NumericUpDown">control</see>
        /// <see cref=" NumericUpDown.Minimum">Minimum</see>.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The numeric up down control. </param>
        /// <param name="value">   The candidate Minimum. </param>
        /// <returns> The limited Minimum. </returns>
        public static decimal MinimumSetter( this NumericUpDown control, decimal value )
        {
            if ( control is object )
            {
                if ( control.Value < value )
                {
                    control.Value = value;
                }

                control.Minimum = value;
            }

            return value;
        }

        /// <summary>
        /// Sets the <see cref="NumericUpDown">control</see>
        /// <see cref=" NumericUpDown.Minimum">Minimum</see>.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The numeric up down control. </param>
        /// <param name="value">   The candidate Maximum. </param>
        /// <returns> The limited Minimum. </returns>
        public static decimal MinimumSetter( this NumericUpDown control, double value )
        {
            return control.MinimumSetter( value < ( double ) decimal.MinValue ? decimal.MinValue : ( decimal ) value );
        }


        #endregion

        #region " VALUE "

        /// <summary>
        /// Sets the <see cref="NumericUpDown">control</see> value to the
        /// <paramref name="value">value</paramref> scaled by the
        /// <paramref name="scalar">scalar</paramref> and limited by the control range.
        /// Returns limited value (control value divided by the scalar).
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The numeric up down control. </param>
        /// <param name="scalar">  The scalar. </param>
        /// <param name="value">   The candidate value. </param>
        /// <returns> The control value divided by the scalar. </returns>
        public static decimal ValueSetter( this NumericUpDown control, decimal scalar, decimal value )
        {
            return control.ValueSetter( scalar * value ) / scalar;
        }

        /// <summary>
        /// Sets the <see cref="NumericUpDown">control</see> value to the
        /// <paramref name="value">value</paramref> scaled by the
        /// <paramref name="scalar">scalar</paramref> and limited by the control range.
        /// Returns limited value (value within the control range divided by the scalar). The setter
        /// disables the control before altering the checked state allowing the control code to use the
        /// enabled state for preventing the execution of the control checked change actions.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The numeric up down control. </param>
        /// <param name="scalar">  The scalar. </param>
        /// <param name="value">   The candidate value. </param>
        /// <returns> The control value divided by the scalar. </returns>
        public static decimal SilentValueSetter( this NumericUpDown control, decimal scalar, decimal value )
        {
            return control.SilentValueSetter( scalar * value ) / scalar;
        }

        /// <summary>
        /// Sets the <see cref="NumericUpDown">control</see> value to the
        /// <paramref name="value">value</paramref> limited by the control range.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The numeric up down control. </param>
        /// <param name="value">   The candidate value. </param>
        /// <returns> The limited value. </returns>
        public static decimal ValueSetter( this NumericUpDown control, decimal value )
        {
            if ( control is object )
            {
                value = value < control.Minimum ? control.Minimum : value > control.Maximum ? control.Maximum : value;
                if ( value != control.Value )
                {
                    control.Value = value;
                }
            }

            return value;
        }

        /// <summary>
        /// Sets the <see cref="NumericUpDown">control</see> value to the
        /// <paramref name="value">value</paramref> limited by the control range.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The numeric up down control. </param>
        /// <param name="value">   The candidate value. </param>
        /// <returns> The limited value. </returns>
        public static decimal? ValueSetter( this NumericUpDown control, decimal? value )
        {
            if ( control is object )
            {
                if ( value.HasValue )
                {
                    control.Value = value.Value < control.Minimum ? control.Minimum : value.Value > control.Maximum ? control.Maximum : value.Value;
                }
                else
                {
                    control.Text = string.Empty;
                }

                return control.Value;
            }
            else
            {
                return value;
            }
        }

        /// <summary>
        /// Sets the <see cref="NumericUpDown">control</see> value to the
        /// <paramref name="value">value</paramref> limited by the control range.
        /// This setter disables the control before altering the checked state allowing the control code
        /// to use the enabled state for preventing the execution of the control checked change actions.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The numeric up down control. </param>
        /// <param name="value">   The candidate value. </param>
        /// <returns> The limited value. </returns>
        public static decimal SilentValueSetter( this NumericUpDown control, decimal value )
        {
            if ( control is object )
            {
                bool wasEnabled = control.Enabled;
                control.Enabled = false;
                _ = control.ValueSetter( value );
                control.Enabled = wasEnabled;
                return control.Value;
            }
            else
            {
                return value;
            }
        }

        /// <summary>
        /// Sets the <see cref="NumericUpDown">control</see> value to the
        /// <paramref name="value">value</paramref> limited by the control range.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The numeric up down control. </param>
        /// <param name="value">   The candidate value. </param>
        /// <returns> The limited value. </returns>
        public static double ValueSetter( this NumericUpDown control, double value )
        {
            return ( double ) control.ValueSetter( ( decimal ) value );
        }

        /// <summary>
        /// Sets the <see cref="NumericUpDown">control</see> value to the
        /// <paramref name="value">value</paramref> limited by the control range.
        /// This setter disables the control before altering the checked state allowing the control code
        /// to use the enabled state for preventing the execution of the control checked change actions.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The numeric up down control. </param>
        /// <param name="value">   The candidate value. </param>
        /// <returns> The limited value. </returns>
        public static decimal SilentValueSetter( this NumericUpDown control, double value )
        {
            if ( control is object )
            {
                bool wasEnabled = control.Enabled;
                control.Enabled = false;
                _ = control.ValueSetter( ( decimal ) value );
                control.Enabled = wasEnabled;
                return control.Value;
            }

            return ( decimal ) value;
        }

        #endregion

        #endregion

        #region " RADIO BUTTON "

        /// <summary>
        /// Sets the <see cref="System.Windows.Forms.RadioButton">radio button</see> checked value to the
        /// <paramref name="value">value</paramref>.
        /// This setter disables the control before altering the checked state allowing the control code
        /// to use the enabled state for preventing the execution of the control checked change actions.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> Check box control. </param>
        /// <param name="value">   The value. </param>
        /// <returns> value. </returns>
        public static bool SilentCheckedSetter( this RadioButton control, bool value )
        {
            if ( control is object )
            {
                bool wasEnabled = control.Enabled;
                control.Enabled = false;
                control.Checked = value;
                control.Enabled = wasEnabled;
            }

            return value;
        }

        #endregion

        #region " TOOL STRIP "

        #region " TOOL STRIP ITEM "

        /// <summary>
        /// Sets the <see cref=" ToolStripMenuItem">Control</see> checked value to the The setter
        /// disables the control before altering the checked state allowing the control code to use the
        /// enabled state for preventing the execution of the control checked change actions.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> Check box control. </param>
        /// <param name="value">   The value. </param>
        /// <returns> value. </returns>
        public static CheckState SilentCheckStateSetter( this ToolStripMenuItem control, CheckState value )
        {
            if ( control is object )
            {
                bool wasEnabled = control.Enabled;
                control.Enabled = false;
                control.CheckState = value;
                control.Enabled = wasEnabled;
            }

            return value;
        }

        /// <summary>
        /// Sets the <see cref=" ToolStripMenuItem">Control</see> checked value to the The setter
        /// disables the control before altering the checked state allowing the control code to use the
        /// enabled state for preventing the execution of the control checked change actions.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> Check box control. </param>
        /// <param name="value">   The value. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool SilentCheckedSetter( this ToolStripMenuItem control, bool value )
        {
            if ( control is object )
            {
                bool wasEnabled = control.Enabled;
                control.Enabled = false;
                control.Checked = value;
                control.Enabled = wasEnabled;
            }

            return value;
        }

        #endregion

        #region " TOOL STRIP DROP DOWN ITEM "

        /// <summary> Maximum drop down item width. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="control"> The tool strip progress bar control. </param>
        /// <returns> An Integer. </returns>
        public static int MaxDropDownItemWidth( this ToolStripDropDownItem control )
        {
            return control is null ? throw new ArgumentNullException( nameof( control ) ) : control.DropDownItems.MaxItemWidth();
        }

        #endregion

        #region " TOOL STRIP ITEM COLLECTION "

        /// <summary> Maximum item width. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="items"> The items. </param>
        /// <returns> An Integer. </returns>
        public static int MaxItemWidth( this ToolStripItemCollection items )
        {
            int width = 0;
            if ( items is object )
            {
                foreach ( ToolStripItem item in items )
                {
                    width = width < item.Width ? item.Width : width;
                }
            }

            return width;
        }

        #endregion

        #region " TOOL STRIP PROGRESS BAR "

        /// <summary>
        /// Sets the <see cref="ProgressBar">control</see> value to the
        /// <paramref name="value">value</paramref> limited by the control range.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The tool strip progress bar control. </param>
        /// <param name="value">   The candidate value. </param>
        /// <returns> The value limited within the range of the progress bar. </returns>
        public static int ValueSetter( this ToolStripProgressBar control, int value )
        {
            if ( control is object )
            {
                value = value < control.Minimum ? control.Minimum : value > control.Maximum ? control.Maximum : value;
                if ( control.Value != value )
                {
                    control.Value = value;
                }

                return control.Value;
            }

            return value;
        }

        /// <summary>
        /// Sets the <see cref="ProgressBar">
        /// control</see>
        /// value to the.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The tool strip progress bar control. </param>
        /// <param name="value">   The candidate value. </param>
        /// <returns> The value limited within the range of the progress bar. </returns>
        public static int ValueSetter( this ToolStripProgressBar control, double value )
        {
            return control.ValueSetter( ( int ) value );
        }

        #endregion


        #endregion

        #region " PRODUCT PROCESS CAPTION "

        /// <summary>   Prefix process name to the product name. </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="assembly"> Represents an assembly, which is a reusable, versionable, and self-
        ///                         describing building block of a common language runtime application. </param>
        /// <returns>   A String. </returns>
        public static string PrefixProcessName( this Assembly assembly )
        {
            if ( assembly is null )
            {
                throw new ArgumentNullException( nameof( assembly ) );
            }

            var productName = assembly
                .GetCustomAttributes( typeof( AssemblyProductAttribute ) )
                .OfType<AssemblyProductAttribute>()
                .FirstOrDefault().Product;
            string processName = System.Diagnostics.Process.GetCurrentProcess().ProcessName;
            if ( !productName.StartsWith( processName, StringComparison.OrdinalIgnoreCase ) )
            {
                productName = $"{processName}.{productName}";
            }
            return productName;
        }

        /// <summary>
        /// Builds time caption using <see cref="DateTimeOffset"/> showing local time and offset.
        /// </summary>
        /// <remarks>
        /// <list type="bullet">Use the following format options: <item>
        /// u - UTC - 2019-09-10 19:27:04Z</item><item>
        /// r - GMT - Tue, 10 May 2019 19:26:42 GMT</item><item>
        /// o - ISO - 2019-09-10T12:12:29.7552627-07:00</item><item>
        /// s - ISO - 2019-09-10T12:24:47</item><item>
        /// empty   - 2019-09-10 16:57:24 -07:00</item><item>
        /// s + zzz - 2019-09-10T12:24:47-07:00</item></list>
        /// </remarks>
        /// <param name="timeCaptionFormat">    The time caption format. </param>
        /// <param name="kindFormat">           The kind format. </param>
        /// <returns>   A string. </returns>
        public static string BuildLocalTimeCaption( string timeCaptionFormat, string kindFormat )
        {
            string result = string.IsNullOrWhiteSpace( timeCaptionFormat )
                ? $"{DateTimeOffset.Now}"
                : string.IsNullOrWhiteSpace( kindFormat )
                    ? $"{DateTimeOffset.Now.ToString( timeCaptionFormat )}"
                    : $"{DateTimeOffset.Now.ToString( timeCaptionFormat )}{DateTimeOffset.Now.ToString( kindFormat )}";
            return result;
        }

        /// <summary>   Builds product time caption. </summary>
        /// <remarks>
        /// <list type="bullet">Use the following format options: <item>
        /// u - UTC - 2019-09-10 19:27:04Z</item><item>
        /// r - GMT - Tue, 10 May 2019 19:26:42 GMT</item><item>
        /// o - ISO - 2019-09-10T12:12:29.7552627-07:00</item><item>
        /// s - ISO - 2019-09-10T12:24:47</item><item>
        /// empty   - 2019-09-10 16:57:24 -07:00</item><item>
        /// s + zzz - 2019-09-10T12:24:47-07:00</item></list>
        /// </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="assembly">             Represents an assembly, which is a reusable, versionable,
        ///                                     and self-describing building block of a common language
        ///                                     runtime application. </param>
        /// <param name="versionElements">      The version elements. </param>
        /// <param name="timeCaptionFormat">    The time caption format. </param>
        /// <param name="kindFormat">           The kind format. </param>
        /// <returns>   A String. </returns>
        public static string BuildProductTimeCaption( this Assembly assembly, int versionElements, string timeCaptionFormat, string kindFormat )
        {
            return assembly is null
                ? throw new ArgumentNullException( nameof( assembly ) )
                : $"{PrefixProcessName( assembly )}.r.{assembly.GetName().Version.ToString( versionElements )} {BuildLocalTimeCaption( timeCaptionFormat, kindFormat )}";
        }

        /// <summary>
        /// Builds product time caption using full version and local time plus kind format.
        /// </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="assembly"> Represents an assembly, which is a reusable, versionable, and self-
        ///                         describing building block of a common language runtime application. </param>
        /// <returns>   A String. </returns>
        public static string BuildProductTimeCaption( this Assembly assembly )
        {
            return assembly.BuildProductTimeCaption( 4, string.Empty, string.Empty );
        }

        #endregion

    }
}
