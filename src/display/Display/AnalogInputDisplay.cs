using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

using isr.IO.OL.ExceptionExtensions;

namespace isr.IO.OL.Display
{
    /// <summary> The new chart control based on the Open Layers
    /// <see cref="OpenLayers.Controls.Display">display</see>
    /// class. </summary>
    /// <remarks> (c) 2007 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para></remarks>
    [Description( "Analog Input Display Control" )]
    [ToolboxBitmap( typeof( AnalogInputDisplay ), "AnalogInputDisplay.bmp" )]
    public partial class AnalogInputDisplay : UserControl, INotifyPropertyChanged
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="AnalogInputDisplay" /> class. </summary>
        public AnalogInputDisplay() : base()
        {

            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.StopTimeout = TimeSpan.FromMilliseconds( 1000d );
            this.ConfigureSuspensionQueue = new Queue<bool>();
            this.BroadcastLevel = TraceEventType.Warning;
            _ = this._RefreshRateNumeric.MaximumSetter( 100m );
            _ = this._RefreshRateNumeric.MinimumSetter( 1m );
            _ = this._BuffersCountNumeric.MaximumSetter( 1000m );
            _ = this._BuffersCountNumeric.MinimumSetter( 2m );
            _ = this._MovingAverageLengthNumeric.MinimumSetter( 2m );
            _ = this._SampleSizeNumeric.MinimumSetter( 2m );
            _ = this._SampleSizeNumeric.ValueSetter( 1000m );
            _ = this._SignalBufferSizeNumeric.MinimumSetter( 2m );
            this.UsbBufferSize = 256;
            this._VoltsReadingsStringFormat = "{0:0.000 V}";
            this._VoltsReadingFormat = "0.000";
            this.CalculatePeriodCount = 10;
            this.UpdatePeriodCount = 10;
            this._RefreshStopwatch = new Stopwatch();

            // allow single selections
            this._SignalsListView.MultiSelect = false;

            // Set the view to show details.
            this._SignalsListView.View = View.Details;

            // Do not Allow the user to edit item text.
            this._SignalsListView.LabelEdit = false;

            // Do not Allow the user to rearrange columns.
            this._SignalsListView.AllowColumnReorder = false;

            // Do Not Display check boxes.
            this._SignalsListView.CheckBoxes = false;

            // Select the item and sub items when selection is made.
            this._SignalsListView.FullRowSelect = true;

            // Display grid lines.
            this._SignalsListView.GridLines = true;

            // Do not Sort the items in the list in ascending order.
            this._SignalsListView.Sorting = SortOrder.None;

            // Create columns for the items and sub items.
            this._SignalsListView.Columns.Clear();
            _ = this._SignalsListView.Columns.Add( "#", -2, HorizontalAlignment.Left );
            _ = this._SignalsListView.Columns.Add( "Name", -2, HorizontalAlignment.Left );
            _ = this._SignalsListView.Columns.Add( "Range", -2, HorizontalAlignment.Left );
            this._SignalsListView.Invalidate();

            // allow single selections
            this._ChannelsListView.MultiSelect = false;

            // Set the view to show details.
            this._ChannelsListView.View = View.Details;

            // Do not Allow the user to edit item text.
            this._ChannelsListView.LabelEdit = false;

            // Do not Allow the user to rearrange columns.
            this._ChannelsListView.AllowColumnReorder = false;

            // Do Not Display check boxes.
            this._ChannelsListView.CheckBoxes = false;

            // Select the item and sub items when selection is made.
            this._ChannelsListView.FullRowSelect = true;

            // Display grid lines.
            this._ChannelsListView.GridLines = true;

            // Do not Sort the items in the list in ascending order.
            this._ChannelsListView.Sorting = SortOrder.None;

            // Create columns for the items and sub items.
            this._ChannelsListView.Columns.Clear();
            _ = this._ChannelsListView.Columns.Add( "#", -2, HorizontalAlignment.Left );
            _ = this._ChannelsListView.Columns.Add( "Name", -2, HorizontalAlignment.Left );
            _ = this._ChannelsListView.Columns.Add( "Range", -2, HorizontalAlignment.Left );
            this._ChannelsListView.Invalidate();

            // enable selecting a device.
            this._DeviceNameChooser.Enabled = !this.IsOpen;

            // Me.BuffersCount = Me.BuffersCount

            // disable controls.
            this.EnableChartControls();
            this.EnableSignalsControls();
            this.EnableChannelsControls();
        }

        /// <summary> Creates a new AnalogInputDisplay. </summary>
        /// <returns> An AnalogInputDisplay. </returns>
        public static AnalogInputDisplay Create()
        {
            AnalogInputDisplay result = null;
            try
            {
                result = new AnalogInputDisplay();
            }
            catch
            {
                if ( result is object )
                    result.Dispose();
                throw;
            }

            return result;
        }

        /// <summary> Cleans up managed components. </summary>
        /// <remarks> Use this method to reclaim managed resources used by this class. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void OnDisposeManagedResources()
        {
            if ( AcquisitionStarted is object )
            {
                foreach ( Delegate d in AcquisitionStarted.GetInvocationList() )
                    AcquisitionStarted -= ( EventHandler<EventArgs> ) d;
            }

            if ( AcquisitionStopped is object )
            {
                foreach ( Delegate d in AcquisitionStopped.GetInvocationList() )
                    AcquisitionStopped -= ( EventHandler<EventArgs> ) d;
            }

            if ( BoardsLocated is object )
            {
                foreach ( Delegate d in BoardsLocated.GetInvocationList() )
                    BoardsLocated -= ( EventHandler<EventArgs> ) d;
            }

            if ( BufferDone is object )
            {
                foreach ( Delegate d in BufferDone.GetInvocationList() )
                    BufferDone -= ( EventHandler<EventArgs> ) d;
            }

            if ( Disconnected is object )
            {
                foreach ( Delegate d in Disconnected.GetInvocationList() )
                    Disconnected -= ( EventHandler<EventArgs> ) d;
            }

            if ( UpdatePeriodDone is object )
            {
                foreach ( Delegate d in UpdatePeriodDone.GetInvocationList() )
                    UpdatePeriodDone -= ( EventHandler<EventArgs> ) d;
            }

            if ( DriverError is object )
            {
                foreach ( Delegate d in DriverError.GetInvocationList() )
                    DriverError -= ( EventHandler<System.Threading.ThreadExceptionEventArgs> ) d;
            }

            // stop data acquisition and close the device.
            try
            {
                if ( this.AnalogInput is object )
                {
                    this.TryReleaseAnalogInput();
                    this.AnalogInput.Dispose();
                    this.AnalogInput = null;
                }
            }
            catch
            {
            }

            try
            {
                if ( this._BufferQueue is object )
                {
                    this._BufferQueue.Clear();
                    this._BufferQueue = null;
                }
            }
            catch
            {
            }

            try
            {
                if ( this.Device is object )
                {
                    this.Device.Dispose();
                    this.Device = null;
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the isr.Core.Agnostic.PropertyNotifyControlBase and
        /// optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        ///                                                   release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {
                        this.components?.Dispose();
                        this.OnDisposeManagedResources();
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLoad( EventArgs e )
        {
            base.OnLoad( e );
            if ( string.IsNullOrWhiteSpace( this.ChartTitle ) )
            {
                this.ChartTitle = "Analog Input Time Series";
            }
            else
            {
                // set default values.  For some reason the text values do not get set upon starting.
                this.ChartTitle = this.ChartTitle;
            }

            this.ChartFooter = this.ChartFooter;
        }

        #endregion

        #region " I NOTIFY PROPERTY CHAGNED "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Synchronously notify property changed described by propertyName. </summary>
        /// <remarks>   David, 2021-02-25. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        #endregion

        #region " STATE TRANSITION CONTROL  "

        /// <summary> Gets the stop or abort timeout. </summary>
        /// <value> The timeout. </value>
        public TimeSpan StopTimeout { get; set; }

        /// <summary> Enumerates the state transition commands. </summary>
        private enum TransitionCommand
        {
            /// <summary> An Enum constant representing the none option. </summary>
            None,
            /// <summary> An Enum constant representing the open] option. </summary>
            Open,
            /// <summary> An Enum constant representing the configure option. </summary>
            Configure,
            /// <summary> An Enum constant representing the start] option. </summary>
            Start,
            /// <summary> An Enum constant representing the stop] option. </summary>
            Stop,
            /// <summary> An Enum constant representing the abort option. </summary>
            Abort,
            /// <summary> An Enum constant representing the close option. </summary>
            Close
        }

        /// <summary> Returns the analog input subsystem state. Returns the
        /// <see cref="OpenLayers.Base.SubsystemBase.States.ConfiguredForSingleValue">state </see>
        /// as a dummy output to indicate that the analog input is not defined. </summary>
        /// <value> The analog input state. </value>
        public OpenLayers.Base.SubsystemBase.States AnalogInputState => this.AnalogInput is null ? OpenLayers.Base.SubsystemBase.States.ConfiguredForSingleValue : this.AnalogInput.State;

        /// <summary> Report an illegal transition. </summary>
        /// <param name="transitionCommand"> The transition command. </param>
        /// <param name="state">             The state. </param>
        private void OnIllegalTransition( TransitionCommand transitionCommand, OpenLayers.Base.SubsystemBase.States state )
        {
            this.OnHandlerException( $"Illegal transition command {transitionCommand} issued when in the {state} state--Ignored;. " );
        }

        /// <summary> Report an illegal state. </summary>
        /// <param name="state"> The state. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private void OnIllegalState( OpenLayers.Base.SubsystemBase.States state )
        {
            this.OnHandlerException( $"Illegal state {state};. " );
        }

        /// <summary> Report an unhandled transition request. </summary>
        /// <param name="transitionCommand"> The transition command. </param>
        /// <param name="state">             The state. </param>
        private void OnUnhandledTransition( TransitionCommand transitionCommand, OpenLayers.Base.SubsystemBase.States state )
        {
            this.OnHandlerException( $"Unhandled transition command {transitionCommand} issued when in the {state} state--ignored;. " );
        }

        /// <summary> Report an Unnecessary transition. </summary>
        /// <param name="transitionCommand"> The transition command. </param>
        /// <param name="state">             The state. </param>
        private void OnUnnecessaryTransition( TransitionCommand transitionCommand, OpenLayers.Base.SubsystemBase.States state )
        {
            this.OnHandlerException( $"Unnecessary transition command {transitionCommand};. issued when in the {state} state--ignored." );
        }

        /// <summary> Returns true if the analog input is in continuous operation running state. </summary>
        /// <value> The continuous operation running. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public bool ContinuousOperationRunning => this.IsOpen && this.AnalogInput.ContinuousOperationRunning;

        /// <summary> Returns true if the analog input is in continuous operation. </summary>
        /// <value> The continuous operation active. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public bool ContinuousOperationActive => this.IsOpen && this.AnalogInput.ContinuousOperationActive;

        /// <summary> Stops a continuous operation on the subsystem immediately without waiting for the
        /// current buffer to be filled. </summary>
        public void Abort()
        {
            var command = TransitionCommand.Abort;
            var state = this.AnalogInputState;
            switch ( state )
            {
                case OpenLayers.Base.SubsystemBase.States.Aborting:
                    {
                        this.OnUnnecessaryTransition( command, state );
                        break;
                    }

                case OpenLayers.Base.SubsystemBase.States.ConfiguredForContinuous:
                    {

                        // this is the stopped state.
                        this.OnUnnecessaryTransition( command, state );
                        break;
                    }

                case OpenLayers.Base.SubsystemBase.States.ConfiguredForSingleValue:
                    {

                        // this is the closed state
                        this.OnIllegalTransition( command, state );
                        break;
                    }

                case OpenLayers.Base.SubsystemBase.States.Initialized:
                    {

                        // the board was not stated
                        this.OnUnnecessaryTransition( command, state );
                        break;
                    }

                case OpenLayers.Base.SubsystemBase.States.IoComplete:
                case OpenLayers.Base.SubsystemBase.States.PreStarted:
                case OpenLayers.Base.SubsystemBase.States.Running:
                    {

                        // The final analog output sample has been written from the FIFO on the device. 
                        // The subsystem was pre-started for a continuous operation. 
                        this.TryStopContinuousInput( this.StopTimeout, false );
                        break;
                    }

                case OpenLayers.Base.SubsystemBase.States.Stopping:
                    {

                        // already aborting.
                        this.OnUnnecessaryTransition( command, state );
                        break;
                    }

                default:
                    {
                        this.OnUnhandledTransition( command, state );
                        break;
                    }
            }
        }

        /// <summary> Sends a signal to close the analog input and release the selected board. </summary>
        public void Close()
        {
            var command = TransitionCommand.Close;
            var state = this.AnalogInputState;
            switch ( state )
            {
                case OpenLayers.Base.SubsystemBase.States.Aborting:
                    {

                        // this will await for aborting and then close.
                        this.TryCloseAnalogInput();
                        break;
                    }

                case OpenLayers.Base.SubsystemBase.States.ConfiguredForContinuous:
                    {

                        // this is the stopped state.
                        this.TryCloseAnalogInput();
                        break;
                    }

                case OpenLayers.Base.SubsystemBase.States.ConfiguredForSingleValue:
                    {

                        // this is the closed state.
                        this.OnUnnecessaryTransition( command, state );
                        break;
                    }

                case OpenLayers.Base.SubsystemBase.States.Initialized:
                    {

                        // the board is connected but not yet configured.
                        this.TryCloseAnalogInput();
                        break;
                    }

                case OpenLayers.Base.SubsystemBase.States.IoComplete:
                case OpenLayers.Base.SubsystemBase.States.PreStarted:
                case OpenLayers.Base.SubsystemBase.States.Running:
                    {

                        // The final analog output sample has been written from the FIFO on the device. 
                        // The subsystem was pre-started for a continuous operation. 

                        // stop running.
                        this.Stop();

                        // close
                        this.TryCloseAnalogInput();
                        break;
                    }

                case OpenLayers.Base.SubsystemBase.States.Stopping:
                    {

                        // this will await for stopping and then close.
                        this.TryCloseAnalogInput();
                        break;
                    }

                default:
                    {
                        this.OnUnhandledTransition( command, state );
                        break;
                    }
            }
        }

        /// <summary> Configures the board for data collection. </summary>
        /// <param name="details"> [in,out] A non-empty validation outcome string containing the reason
        /// configuration failed. </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        public bool TryConfigure( ref string details )
        {
            var command = TransitionCommand.Configure;
            var state = this.AnalogInputState;
            switch ( state )
            {
                case OpenLayers.Base.SubsystemBase.States.Aborting:
                    {

                        // must wait for aborting to complete.
                        this.OnIllegalTransition( command, state );
                        details = "Illegal Transition";
                        return false;
                    }

                case OpenLayers.Base.SubsystemBase.States.ConfiguredForContinuous:
                    {

                        // the analog input can be reconfigured.
                        this.StartStopEnabled = this.TryConfigureThis( ref details );
                        return this.StartStopEnabled;
                    }

                case OpenLayers.Base.SubsystemBase.States.ConfiguredForSingleValue:
                    {

                        // this is the closed state. need to open first.
                        this.OnIllegalTransition( command, state );
                        details = "Illegal Transition";
                        return false;
                    }

                case OpenLayers.Base.SubsystemBase.States.Initialized:
                    {

                        // this is the first configuration after opening the board.
                        this.StartStopEnabled = this.TryConfigureThis( ref details );
                        return this.StartStopEnabled;
                    }

                case OpenLayers.Base.SubsystemBase.States.IoComplete:
                case OpenLayers.Base.SubsystemBase.States.PreStarted:
                case OpenLayers.Base.SubsystemBase.States.Running:
                    {

                        // The final analog output sample has been written from the FIFO on the device. 
                        // The subsystem was pre-started for a continuous operation. 
                        this.OnIllegalTransition( command, state );
                        details = "Illegal Transition";
                        return false;
                    }

                case OpenLayers.Base.SubsystemBase.States.Stopping:
                    {

                        // must wait for stopping to complete.
                        this.OnIllegalTransition( command, state );
                        details = "Illegal Transition";
                        return false;
                    }

                default:
                    {
                        this.OnUnhandledTransition( command, state );
                        details = "Unhandled Transition";
                        return false;
                    }
            }
        }

        /// <summary> Sends a signal to select a board and open the analog input. </summary>
        /// <param name="deviceName"> Name of the device. </param>
        public void Open( string deviceName )
        {
            this._CandidateDeviceName = deviceName;
            var command = TransitionCommand.Open;
            var state = this.AnalogInputState;
            switch ( state )
            {
                case OpenLayers.Base.SubsystemBase.States.Aborting:
                    {

                        // Already open
                        this.OnIllegalTransition( command, state );
                        break;
                    }

                case OpenLayers.Base.SubsystemBase.States.ConfiguredForContinuous:
                    {

                        // this is the stopped state. Already open
                        this.OnIllegalTransition( command, state );
                        break;
                    }

                case OpenLayers.Base.SubsystemBase.States.ConfiguredForSingleValue:
                    {

                        // this is the closed state
                        this.TryOpenAnalogInput();
                        break;
                    }

                case OpenLayers.Base.SubsystemBase.States.Initialized:
                    {

                        // board is already open.
                        this.OnUnnecessaryTransition( command, state );
                        break;
                    }

                case OpenLayers.Base.SubsystemBase.States.IoComplete:
                case OpenLayers.Base.SubsystemBase.States.PreStarted:
                case OpenLayers.Base.SubsystemBase.States.Running:
                    {

                        // The final analog output sample has been written from the FIFO on the device. 
                        // The subsystem was pre-started for a continuous operation. 
                        // Already open
                        this.OnIllegalTransition( command, state );
                        break;
                    }

                case OpenLayers.Base.SubsystemBase.States.Stopping:
                    {

                        // Already open
                        this.OnIllegalTransition( command, state );
                        break;
                    }

                default:
                    {
                        this.OnUnhandledTransition( command, state );
                        break;
                    }
            }
        }

        /// <summary> Starts data collection. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is
        /// invalid. </exception>
        public void Start()
        {
            if ( this.ConfigurationRequired )
            {
                throw new InvalidOperationException( "Configuration required" );
            }
            else
            {
                var command = TransitionCommand.Start;
                var state = this.AnalogInputState;
                switch ( state )
                {
                    case OpenLayers.Base.SubsystemBase.States.Aborting:
                        {
                            this.TryStartContinuousInput();
                            break;
                        }

                    case OpenLayers.Base.SubsystemBase.States.ConfiguredForContinuous:
                        {

                            // this is the stopped state.
                            this.TryStartContinuousInput();
                            break;
                        }

                    case OpenLayers.Base.SubsystemBase.States.ConfiguredForSingleValue:
                        {

                            // this is the closed state. need to open first.
                            this.OnIllegalTransition( command, state );
                            break;
                        }

                    case OpenLayers.Base.SubsystemBase.States.Initialized:
                        {

                            // illegal -- board not configured yet.
                            this.OnIllegalTransition( command, state );
                            break;
                        }

                    case OpenLayers.Base.SubsystemBase.States.IoComplete:
                    case OpenLayers.Base.SubsystemBase.States.PreStarted:
                    case OpenLayers.Base.SubsystemBase.States.Running:
                        {

                            // The final analog output sample has been written from the FIFO on the device. 
                            // The subsystem was pre-started for a continuous operation. 
                            this.OnUnnecessaryTransition( command, state );
                            break;
                        }

                    case OpenLayers.Base.SubsystemBase.States.Stopping:
                        {
                            this.TryStartContinuousInput();
                            break;
                        }

                    default:
                        {
                            this.OnUnhandledTransition( command, state );
                            break;
                        }
                }
            }
        }

        /// <summary> Gets or sets the started status.  This is reflected in the status of the Start/Stop
        /// control. </summary>
        /// <value> The started. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public bool Started
        {
            get => this._StartStopToolStripMenuItem.Checked;

            set {
                this.StartStopEnabled = false;
                this._StartStopToolStripMenuItem.Checked = value;
                this._StartStopToolStripMenuItem.Invalidate();
                // enable or disable all chart controls as necessary
                this.EnableChartControls();
            }
        }

        /// <summary> Gets or sets the start stop enable state. </summary>
        /// <value> The start stop enabled. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public bool StartStopEnabled
        {
            get => this._StartStopToolStripMenuItem.Enabled;

            set => this._StartStopToolStripMenuItem.Enabled = value;
        }

        /// <summary> Stops Running. </summary>
        public void Stop()
        {
            var command = TransitionCommand.Stop;
            var state = this.AnalogInputState;
            switch ( state )
            {
                case OpenLayers.Base.SubsystemBase.States.Aborting:
                    {

                        // already stopping.
                        this.OnUnnecessaryTransition( command, state );
                        break;
                    }

                case OpenLayers.Base.SubsystemBase.States.ConfiguredForContinuous:
                    {

                        // this is the stopped state. already stopped.
                        this.OnUnnecessaryTransition( command, state );
                        break;
                    }

                case OpenLayers.Base.SubsystemBase.States.ConfiguredForSingleValue:
                    {

                        // this is the closed state. Not open.
                        this.OnIllegalTransition( command, state );
                        break;
                    }

                case OpenLayers.Base.SubsystemBase.States.Initialized:
                    {

                        // the board was not started.
                        this.OnUnnecessaryTransition( command, state );
                        break;
                    }

                case OpenLayers.Base.SubsystemBase.States.IoComplete:
                case OpenLayers.Base.SubsystemBase.States.PreStarted:
                case OpenLayers.Base.SubsystemBase.States.Running:
                    {

                        // The final analog output sample has been written from the FIFO on the device. 
                        // The subsystem was pre-started for a continuous operation. 
                        this.TryStopContinuousInput( this.StopTimeout, false );
                        break;
                    }

                case OpenLayers.Base.SubsystemBase.States.Stopping:
                    {

                        // already stopping.
                        this.OnUnnecessaryTransition( command, state );
                        break;
                    }

                default:
                    {
                        this.OnUnhandledTransition( command, state );
                        break;
                    }
            }
        }

        #endregion

        #region " CONFIGURATION/OPERATION COMMANDS "

        /// <summary> Restores default settings. </summary>
        public void RestoreDefaults()
        {
            this.SuspendConfig();
            if ( this.ChartMode == ChartModeId.Scope )
            {
                this.BuffersCount = 5;
                this.SampleSize = 1000;
                this.SamplingRate = 1000d;
            }
            else if ( this.ChartMode == ChartModeId.StripChart )
            {
                this.BuffersCount = 20;
                this.SampleSize = 2;
                this.SamplingRate = 240d;
                this.SignalBufferSize = 240;
            }

            this.CalculateEnabled = false;
            this.CalculatePeriodCount = 10;
            this.ChartingEnabled = true;
            this.DisplayAverageEnabled = false;
            this.DisplayVoltageEnabled = false;
            this.FiniteBuffering = false;
            this.MovingAverageLength = 10;
            this.RefreshRate = 24d;
            this.UpdateEventEnabled = false;
            this.UpdatePeriodCount = 10;
            this.UsingSmartBuffering = false;
            this.ResumeConfig();
        }

        /// <summary> Reset to Opened and not running conditions. </summary>
        /// <exception cref="TimeoutException"> Abort timeout--system still running after
        /// abort. </exception>
        public void Reset()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                // abort if running.
                this.TryStopContinuousInput( this.StopTimeout, true );

                // clear the configuration suspension Queue.
                this.ConfigureSuspensionQueue = new Queue<bool>();
                if ( this.AnalogInput is null )
                {
                    // clear the display.
                    this.ClearChannelsList();
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " FUNCTIONAL PROPERTIES "

        /// <summary> Gets the
        /// <see cref="System.Diagnostics.TraceEventType">broadcast level</see>. </summary>
        /// <value> The broadcast level. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TraceEventType BroadcastLevel { get; set; }

        #endregion

        #region " DEVICE "

        /// <summary> Releases the analog input event handlers. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void TryReleaseAnalogInput()
        {
            if ( this.AnalogInput is object )
            {

                // add event handlers for the events we're interested in
                try
                {
                    this.AnalogInput.GeneralFailureEvent -= this.HandleGeneralFailure;
                }
                catch
                {
                }

                try
                {
                    this.AnalogInput.BufferDoneEvent -= this.HandleBufferDone;
                }
                catch
                {
                }

                try
                {
                    this.AnalogInput.QueueDoneEvent -= this.HandleQueueDone;
                }
                catch
                {
                }

                try
                {
                    this.AnalogInput.QueueStoppedEvent -= this.HandleQueueStopped;
                }
                catch
                {
                }

                try
                {
                    this.AnalogInput.DeviceRemovedEvent -= this.HandleDeviceRemoved;
                }
                catch
                {
                }

                try
                {
                    this.AnalogInput.DriverRunTimeErrorEvent -= this.HandleDriverRuntimeError;
                }
                catch
                {
                }
            }
        }

        /// <summary> Stops acquisition, if necessary, and closes the analog input board. </summary>
        /// <remarks> Use this method to close the instance.  The method returns true if success or false
        /// if it failed closing the instance. </remarks>
        /// <exception cref="InvalidOperationException"> Operation timed out occurred attempting to close
        /// the analog input
        /// <innerException cref="System.TimeoutException">Abort timeout--system still running after
        /// abort</innerException>
        /// or Open layers exception occurred attempting to close the analog input
        /// <innerException cref="OpenLayers.Base.OlException">open layers
        /// exception.</innerException> </exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void CloseAnalogInput()
        {
            try
            {
                this.StatusMessage  = "Closing...;. ";

                // clear the channel list before closing the channel
                this.ClearChannelsList();

                // stop if still running
                this.TryStopContinuousInput( this.StopTimeout, false );

                // removes event handlers for the events we're interested in
                this.TryReleaseAnalogInput();
                if ( this.AnalogInput is object )
                {
                    this.AnalogInput.Dispose();
                    this.AnalogInput = null;
                }

                // clear the buffer queue
                if ( this._BufferQueue is object )
                {
                    this._BufferQueue.Clear();
                    this._BufferQueue = null;
                }

                if ( this.Device is object )
                {
                    this.Device.Dispose();
                    this.Device = null;
                }

                // clear the board name.
                this._CandidateDeviceName = string.Empty;
            }
            catch ( TimeoutException ex )
            {
                throw new InvalidOperationException( "Operation timed out attempting to close the analog input", ex );
            }
            catch ( OpenLayers.Base.OlException ex )
            {
                throw new InvalidOperationException( "Open layers exception occurred attempting to close the analog input", ex );
            }
            finally
            {
                try
                {
                    if ( this.AnalogInput is object )
                    {
                        this.AnalogInput.Dispose();
                        this.AnalogInput = null;
                    }
                }
                catch
                {
                }

                try
                {
                    if ( this._BufferQueue is object )
                    {
                        this._BufferQueue.Clear();
                        this._BufferQueue = null;
                    }
                }
                catch
                {
                }

                try
                {
                    if ( this.Device is object )
                    {
                        this.Device.Dispose();
                        this.Device = null;
                    }
                }
                catch
                {
                }


                // force turning off of the start button
                this.Started = false;

                // update the status of the open check box.
                _ = this._OpenCloseCheckBox.SilentCheckedSetter( this.IsOpen );
                this._DeviceNameChooser.Enabled = !this.IsOpen;

                // disable user interface
                this.EnableChartControls();
                this.EnableSignalsControls();
                this.EnableChannelsControls();
                if ( this.IsOpen )
                {
                    // if still open, display message and leave in the closing state. 
                    this.StatusMessage = "Failed closing;. ";
                }
                else
                {

                    // set the closed prompt
                    this.StatusMessage = "Closed;. ";
                }
            }
        }

        /// <summary> Tries to close the analog input. </summary>
        private void TryCloseAnalogInput()
        {
            try
            {
                this.CloseAnalogInput();
            }
            catch ( InvalidOperationException ex )
            {
                _ = OL.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                this.StatusMessage = ex.InnerException is OpenLayers.Base.OlException
                    ? $"Open Layers Driver exception occurred closing;. {ex}"
                    : ex.InnerException is TimeoutException
                        ? $"Timeout exception occurred closing;. {ex}"
                        : $"Unexpected exception occurred closing;. {ex}";
            }
            catch
            {
                throw;
            }
        }

        /// <summary> Disconnected an already disconnected board. </summary>
        /// <remarks> Use this method to close the instance.  The method returns true if success or false
        /// if it failed closing the instance. </remarks>
        /// <exception cref="InvalidOperationException"> Operation timed out occurred attempting to close
        /// the analog input
        /// <innerException cref="System.TimeoutException">Abort timeout--system still running after
        /// abort</innerException>
        /// or Open layers exception occurred attempting to close the analog input
        /// <innerException cref="OpenLayers.Base.OlException">open layers
        /// exception.</innerException> </exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void DisconnectAnalogInput()
        {
            try
            {
                this.StatusMessage = "Disconnecting...;. ";

                // removes event handlers for the events we're interested in
                this.TryReleaseAnalogInput();

                // clear the board name.
                this._CandidateDeviceName = string.Empty;
            }
            catch ( TimeoutException ex )
            {
                throw new InvalidOperationException( "Operation timed out attempting to close the analog input", ex );
            }
            catch ( OpenLayers.Base.OlException ex )
            {
                throw new InvalidOperationException( "Open layers exception occurred attempting to close the analog input", ex );
            }
            finally
            {
                try
                {
                    if ( this.AnalogInput is object )
                    {
                        // Me.AnalogInput.Dispose()
                        this.AnalogInput = null;
                    }
                }
                catch
                {
                }

                try
                {
                    if ( this._BufferQueue is object )
                    {
                        // Me._bufferQueue.Clear()
                        this._BufferQueue = null;
                    }
                }
                catch
                {
                }

                try
                {
                    if ( this.Device is object )
                    {
                        // Me._Device.Dispose()
                        this.Device = null;
                    }
                }
                catch
                {
                }

                try
                {
                    // clear the channel list -- this clears the displays.
                    this.ClearChannelsList();
                }
                catch
                {
                }

                // force turning off of the start button
                this.Started = false;

                // update the status of the open check box.
                _ = this._OpenCloseCheckBox.SilentCheckedSetter( this.IsOpen );
                this._DeviceNameChooser.Enabled = true;
                this._OpenCloseCheckBox.Enabled = false;
                try
                {
                    // clear the list of devices.
                    this._DeviceNameChooser.RefreshDeviceNamesList();
                }
                catch
                {
                }

                // disable user interface
                this.EnableChartControls();
                this.EnableSignalsControls();
                this.EnableChannelsControls();
                if ( this.IsOpen )
                {

                    // if still open, display message and leave in the closing state. 
                    this.StatusMessage = "Failed disconnecting;. ";
                }
                else
                {

                    // set the closed prompt
                    this.StatusMessage = "Disconnected;. ";
                }
            }
        }

        /// <summary> Name of the candidate device. </summary>
        private string _CandidateDeviceName;

        /// <summary> Gets the board name. </summary>
        /// <value> The name of the device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string DeviceName => this.Device is null ? "n/a" : this.Device.DeviceName;

        /// <summary> Gets the default device. </summary>
        /// <value> The device. </value>
        private OpenLayers.Base.Device Device { get; set; }

        /// <summary> Determines whether the specified range equals the analog input voltage range. Used as
        /// a predicate in the array search for an index. </summary>
        /// <param name="range"> The range. </param>
        /// <returns> <c>True</c> if [is equal range] [the specified range]; otherwise, <c>False</c>. </returns>
        private bool IsEqualRange( OpenLayers.Base.Range range )
        {
            return range is object && Equals( this.AnalogInput.VoltageRange, 0.0001d );
        }

        /// <summary> Gets or sets a value indicating whether the board was disconnected. </summary>
        /// <remarks> Allows to immediately address the disconnection of the device. </remarks>
        /// <value> <c>True</c> if disconnected; otherwise, <c>False</c>. </value>
        public bool BoardDisconnected { get; private set; }

        /// <summary> Gets the Open status flag. </summary>
        /// <remarks> Use this property to get the open status of this instance, which is True if the
        /// instance was opened. </remarks>
        /// <value> <c>IsOpen</c> is a Boolean property that can be read from (read only). </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public bool IsOpen => !(this.BoardDisconnected || this.AnalogInput is null);

        /// <summary> Opens the analog input board and instantiates the analog input subsystem. Requires
        /// setting of the candidate device name. </summary>
        /// <remarks> Use this method to open the instance.  The method returns true if success or false if
        /// it failed opening the instance. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <exception cref="InvalidOperationException">  Thrown when operation failed to execute. </exception>
        private void OpenAnalogInput()
        {
            if ( !OpenLayers.Base.DeviceMgr.Get().HardwareAvailable() )
            {
                // Me.onHardwareDisconnected(New Global.OpenLayers.Base.GeneralEventArgs)
                var eventHandler = Disconnected;
                if ( eventHandler is object )
                    eventHandler.Invoke( this, new EventArgs() );
                throw new InvalidOperationException( "Hardware not available." );
            }

            if ( string.IsNullOrWhiteSpace( this._CandidateDeviceName ) )
            {
                this.StatusMessage = "Attempted opening board with an empty board name";
                throw new InvalidOperationException( this.StatusMessage );
            }

            try
            {

                // reset to open state conditions.
                this.Reset();

                // Opens the subsystem
                this.StatusMessage = $"Opening board '{this._CandidateDeviceName}';. ";

                // get a board 
                this.Device = OpenLayers.Base.DeviceMgr.Get().SelectDevice( this._CandidateDeviceName );
                if ( this.Device is null )
                {
                    this.StatusMessage = $"Failed opening board '{this._CandidateDeviceName}';. ";
                    throw new InvalidOperationException( this.StatusMessage );
                }
                else
                {
                    this.StatusMessage = $"Opened Device {this.Device.DeviceName} Board #{this.Device.GetHardwareInfo().BoardId};. ";

                    // update the board name.
                    this._CandidateDeviceName = this.Device.DeviceName;

                    // instantiate the open layer analog input sub system
                    this.AnalogInput = new AnalogInputSubsystem( this.Device, 0 );

                    // clear the selected channel and signal
                    _ = this.SelectChannel( -1 );
                    _ = this.SelectSignal( -1 );

                    // set the channel type
                    this.AnalogInput.ChannelType = this.AnalogInput.ChannelType == OpenLayers.Base.ChannelType.SingleEnded ? this.AnalogInput.SupportsSingleEndedInput ? OpenLayers.Base.ChannelType.SingleEnded : OpenLayers.Base.ChannelType.Differential : this.AnalogInput.SupportsDifferentialInput ? OpenLayers.Base.ChannelType.Differential : OpenLayers.Base.ChannelType.SingleEnded;

                    // set the board voltage range selection list
                    this._BoardInputRangesComboBox.DataSource = this.AnalogInput.AvailableBoardRanges( "({0},{1}) V" );
                    int i = Array.FindIndex( this.AnalogInput.SupportedVoltageRanges, new Predicate<OpenLayers.Base.Range>( this.IsEqualRange ) );
                    this._BoardInputRangesComboBox.SelectedIndex = i;

                    // set the channel selection list.
                    this._ChannelComboBox.DataSource = this.AnalogInput.AvailableChannels( "{0}" );

                    // set the ranges combo
                    this._ChannelRangesComboBox.DataSource = this.AnalogInput.AvailableRanges( "({0},{1}) V" );

                    // set some properties
                    this.AnalogInput.DataFlow = OpenLayers.Base.DataFlow.Continuous;
                    string toolTipText = "Channel samples per second between {0} and {1}";
                    toolTipText = string.Format( System.Globalization.CultureInfo.CurrentCulture, toolTipText, this.AnalogInput.Clock.MinFrequency, this.AnalogInput.Clock.MaxFrequency );
                    this._ToolTip.SetToolTip( this._SamplingRateNumeric, toolTipText );

                    // add event handlers for the events we're interested in
                    this.AnalogInput.GeneralFailureEvent += this.HandleGeneralFailure;
                    this.AnalogInput.BufferDoneEvent += this.HandleBufferDone;
                    this.AnalogInput.QueueDoneEvent += this.HandleQueueDone;
                    this.AnalogInput.QueueStoppedEvent += this.HandleQueueStopped;
                    this.AnalogInput.DeviceRemovedEvent += this.HandleDeviceRemoved;
                    this.AnalogInput.DriverRunTimeErrorEvent += this.HandleDriverRuntimeError;
                    this.StatusMessage = $"Initialized analog input on '{this.DeviceName }';. ";

                    // reset if board was disconnected.
                    if ( this.BoardDisconnected )
                    {
                        this.Reset();
                    }

                    this.BoardDisconnected = false;
                }
            }
            catch
            {

                // close to meet strong guarantees
                try
                {
                    this.Close();
                }
                finally
                {
                }

                // throw an exception
                throw;
            }
            finally
            {

                // force turning off of the start button
                this.Started = false;

                // enable user interface
                this.EnableChartControls();
                this.EnableSignalsControls();
                this.EnableChannelsControls();

                // update the status of the open check box.
                _ = this._OpenCloseCheckBox.SilentCheckedSetter( this.IsOpen );
                this._DeviceNameChooser.Enabled = !this.IsOpen;
                this._AnalogInputConfigurationRequired = true;
                this._ChartConfigurationRequired = true;
            }
        }


        /// <summary> Tries to open the analog input. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void TryOpenAnalogInput()
        {
            try
            {
                this.OpenAnalogInput();
            }
            catch ( Exception ex ) 
            {
                this.OnHandlerException( ex );
            }
        }

        #endregion

        #region " ANALOG INPUT CONFIGURATION "

        /// <summary> The analog input. </summary>

        /// <summary> Gets reference to the analog input. </summary>
        /// <value> The analog input. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public AnalogInputSubsystem AnalogInput { get; private set; }

        #region " CHANNELS "

        /// <summary> Adds a channel to the channel list. </summary>
        /// <param name="channelNumber"> . </param>
        /// <param name="gain">          . </param>
        /// <param name="name">          The name. </param>
        public void AddChannel( int channelNumber, double gain, string name )
        {

            // add the new channel to the channel list.
            _ = this.AnalogInput.AddChannel( channelNumber, gain, name );
            _ = this.SelectChannel( this.AnalogInput.SelectedChannelLogicalNumber );

            // update the display
            this.UpdateChannelList();
            this.EnableChartControls();
            this.EnableSignalsControls();
            this.EnableChannelsControls();

            // request configuration of analog input.
            this.AnalogInputConfigurationRequired = true;
        }

        /// <summary> Adds a channel to the channel list. </summary>
        /// <param name="channelNumber"> . </param>
        /// <param name="gainIndex">     . </param>
        /// <param name="name">          The name. </param>
        public void AddChannel( int channelNumber, int gainIndex, string name )
        {

            // add the new channel to the channel list.
            _ = this.AnalogInput.AddChannel( channelNumber, gainIndex, name );
            _ = this.SelectChannel( this.AnalogInput.SelectedChannelLogicalNumber );

            // update the display
            this.UpdateChannelList();
            this.EnableChartControls();
            this.EnableSignalsControls();
            this.EnableChannelsControls();

            // request configuration of analog input.
            this.AnalogInputConfigurationRequired = true;
        }

        /// <summary> Clears the channel and signal lists. </summary>
        public void ClearChannelsList()
        {

            // Clear the selected signal and list
            this.ClearSignalsList();

            // Clear the selected channel and list
            _ = this.SelectChannel( -1 );

            // the list must have at least one item.
            if ( this.AnalogInput is object && !this.ContinuousOperationActive )
            {
                this.AnalogInput.ChannelList.Clear();
            }
            else if ( this.AnalogInput is object && this.ContinuousOperationActive )
            {
                this.StatusMessage = $"Attempt to clear the channel list was ignored because the subsystem is running;. ";
            }

            if ( this.AnalogInput is null || this.AnalogInput.ChannelsCount == 0 )
            {

                // remove all items from the list view
                this._ChannelsListView.Items.Clear();
                this._ChannelsListView.Invalidate();

                // clear the channel list.
                this._SignalChannelComboBox.Items.Clear();
                this._SignalChannelComboBox.Invalidate();
            }

            // update user interface to reflect the removal of the channels.
            this.EnableChartControls();
            this.EnableSignalsControls();
            this.EnableChannelsControls();
        }

        /// <summary> Returns a channel list view item. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="channel"> The channel. </param>
        /// <returns> The new channel list view item. </returns>
        private ListViewItem CreateChannelListViewItem( OpenLayers.Base.ChannelListEntry channel )
        {
            if ( channel is null )
            {
                throw new ArgumentNullException( nameof( channel ) );
            }
            // Create the channel item
            var channelItem = new ListViewItem( channel.PhysicalChannelNumber.ToString( System.Globalization.CultureInfo.CurrentCulture ) );
            _ = channelItem.SubItems.Add( channel.Name );
            string formatValue = "({0},{1}) V";
            _ = channelItem.SubItems.Add( string.Format( System.Globalization.CultureInfo.CurrentCulture, formatValue, this.AnalogInput.MinVoltage(), this.AnalogInput.MaxVoltage() ) );
            return channelItem;
        }

        /// <summary> Returns a default channel name. </summary>
        /// <param name="channelNumber"> . </param>
        /// <returns> The default channel name. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private static string DefaultChannelName( int channelNumber )
        {
            return string.Format( System.Globalization.CultureInfo.CurrentCulture, "Ch. {0}", channelNumber );
        }

        /// <summary> Removes the specified channel from the list of channels. </summary>
        /// <param name="channelNumber"> . </param>
        public void RemoveChannel( int channelNumber )
        {
            int physicalChannelNumber = channelNumber;
            if ( this.AnalogInput.PhysicalChannelExists( physicalChannelNumber ) )
            {
                var channel = this.AnalogInput.SelectPhysicalChannel( physicalChannelNumber );
                this.RemoveSignal( channel );
                this.AnalogInput.ChannelList.Remove( channel );
                this.UpdateChannelList();
                if ( this.SelectedChannel is null || !this.AnalogInput.ChannelList.Contains( this.SelectedChannel ) )
                {
                    if ( this.AnalogInput.HasChannels )
                    {
                        _ = this.SelectChannel( 0 );
                    }
                }

                // request configuration of analog input and chart.
                this.AnalogInputConfigurationRequired = true;
                this.ChartConfigurationRequired = true;
            }
        }

        /// <summary> Removes the specified channel from the list of channels. </summary>
        /// <param name="channel"> The channel. </param>
        public void RemoveChannel( OpenLayers.Base.ChannelListEntry channel )
        {
            if ( channel is object && this.AnalogInput.ChannelList.Contains( channel ) )
            {
                this.RemoveSignal( channel );
                this.AnalogInput.ChannelList.Remove( channel );
                this.UpdateChannelList();
                if ( this.SelectedChannel is null || !this.AnalogInput.ChannelList.Contains( this.SelectedChannel ) )
                {
                    if ( this.AnalogInput.HasChannels )
                    {
                        _ = this.SelectChannel( 0 );
                    }
                }

                if ( this.SelectedSignal is null || !this.Chart.Signals.Contains( this.SelectedSignal ) )
                {
                    if ( this.Chart.Signals.Count > 0 )
                    {
                        _ = this.SelectSignal( 0 );
                    }
                }
            }
        }

        /// <summary> Selects a channel. </summary>
        /// <param name="channelIndex"> The channel logical number = channel index. </param>
        /// <returns> The selected channel. </returns>
        private OpenLayers.Base.ChannelListEntry SelectChannel( int channelIndex )
        {
            this.SelectedChannelIndex = channelIndex;
            if ( this.AnalogInput is object && channelIndex >= 0 && channelIndex < this.AnalogInput.ChannelList.Count )
            {
                this.SelectedChannel = this.AnalogInput.ChannelList[this.SelectedChannelIndex];
            }
            else
            {
                this.SelectedChannelIndex = -1;
                this.SelectedChannel = null;
            }

            return this.SelectedChannel;
        }

        /// <summary> Gets or sets the index of the selected channel in the channel list. This is required
        /// because some Open Layer methods requires the index rather than the channel. </summary>
        /// <value> The selected channel index. </value>
        private int SelectedChannelIndex { get; set; }

        /// <summary> Gets or sets the selected channel. </summary>
        /// <value> The selected channel. </value>
        private OpenLayers.Base.ChannelListEntry SelectedChannel { get; set; }

        /// <summary> Updates a channel in the channel list. </summary>
        /// <param name="channelNumber"> . </param>
        /// <param name="gainIndex">     . </param>
        /// <param name="name">          The name. </param>
        public void UpdateChannel( int channelNumber, int gainIndex, string name )
        {
            if ( string.IsNullOrWhiteSpace( name ) )
            {
                name = string.Empty;
            }

            _ = this.AnalogInput.UpdateChannel( channelNumber, gainIndex, name );

            // add the new channel to the channel list.
            _ = this.SelectChannel( this.AnalogInput.SelectedChannelLogicalNumber );

            // update the display
            this.UpdateChannelList();
            this.EnableChartControls();
            this.EnableSignalsControls();
            this.EnableChannelsControls();

            // request configuration of analog input.
            this.AnalogInputConfigurationRequired = true;
        }

        /// <summary> Updates the channel list display. </summary>
        private void UpdateChannelList()
        {
            int selectedIndex = -1;
            if ( this._ChannelsListView.SelectedIndices.Count > 0 )
            {
                selectedIndex = this._ChannelsListView.SelectedIndices[0];
            }

            int selectedSignalChannelIndex = -1;
            if ( this._SignalChannelComboBox.Items.Count > 0 )
            {
                selectedSignalChannelIndex = this._SignalChannelComboBox.SelectedIndex;
            }

            // update the list of available channels for signals.
            this._SignalChannelComboBox.Items.Clear();
            this._ChannelsListView.Items.Clear();
            foreach ( OpenLayers.Base.ChannelListEntry channel in this.AnalogInput.ChannelList )
            {

                // get a channel item,
                var channelItem = this.CreateChannelListViewItem( channel );
                _ = this._ChannelsListView.Items.Add( channelItem );

                // add the channel to the list of signal channels
                _ = this._SignalChannelComboBox.Items.Add( channel.Name );
            }

            this._ChannelsListView.Invalidate();
            if ( this._ChannelsListView.Items.Count > 0 )
            {
                selectedIndex = Math.Min( Math.Max( selectedIndex, 0 ), this._ChannelsListView.Items.Count - 1 );
                this._ChannelsListView.Items[selectedIndex].Selected = true;
            }

            if ( this._SignalChannelComboBox.Items.Count > 0 )
            {
                selectedSignalChannelIndex = Math.Min( Math.Max( selectedSignalChannelIndex, 0 ), this._SignalChannelComboBox.Items.Count - 1 );
                this._SignalChannelComboBox.SelectedIndex = selectedSignalChannelIndex;
            }
        }

        #endregion

        #region " SIGNALS "

        /// <summary> Adds a signal to the display. </summary>
        /// <param name="channelIndex"> The channel index. </param>
        public void AddSignal( int channelIndex )
        {
            this.AddSignal( this.AnalogInput.ChannelList[channelIndex], ( double ) this._SignalMinNumericUpDown.Value, ( double ) this._SignalMaxNumericUpDown.Value, this._DefaultSignalColors[this.Chart.Signals.Count] );
        }

        /// <summary> Adds a signal to the display. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="channel">    The channel. </param>
        /// <param name="minVoltage"> The minimum voltage. </param>
        /// <param name="maxVoltage"> The maximum voltage. </param>
        /// <param name="color">      The color. </param>
        public void AddSignal( OpenLayers.Base.ChannelListEntry channel, double minVoltage, double maxVoltage, Color color )
        {
            if ( channel is null )
            {
                throw new ArgumentNullException( nameof( channel ) );
            }

            // update the selected channel
            _ = this.SelectChannel( this.AnalogInput.ChannelList.LogicalChannelNumber( channel ) );

            // select the specified channel
            _ = this.AnalogInput.SelectPhysicalChannel( channel.PhysicalChannelNumber );

            // add signal 
            var signal = new OpenLayers.Signals.MemorySignal() {
                Name = channel.Name,
                RangeMax = this.AnalogInput.MaxVoltage(),
                RangeMin = this.AnalogInput.MinVoltage(),
                CurrentRangeMax = maxVoltage,
                CurrentRangeMin = minVoltage,
                Unit = "Volts"
            };
            _ = this.Chart.DisableRendering();

            // add signal to the chart
            _ = this.Chart.Signals.Add( signal );

            // set color.
            this.Chart.SetCurveColor( this.Chart.Signals.Count - 1, color );

            // configure the chart
            this.Chart.SignalListUpdate();
            _ = this.Chart.EnableRendering();

            // update the list of signals.
            this.UpdateSignalList();

            // select this signal
            _ = this.SelectSignal( this.Chart.Signals.Count - 1 );
            this.EnableChartControls();
            this.EnableSignalsControls();
            this.EnableChannelsControls();

            // request configuration of analog input and chart.
            this.AnalogInputConfigurationRequired = true;
            this.ChartConfigurationRequired = true;
        }

        /// <summary> Clears the channel and signal lists. </summary>
        public void ClearSignalsList()
        {

            // Clear the selected signal and list
            _ = this.SelectSignal( -1 );
            this.Chart.Signals.Clear();

            // remove all items from the list view
            this._SignalsListView.Items.Clear();
            this._SignalsListView.Invalidate();

            // update user interface to reflect the removal of the channels.
            this.EnableChartControls();
            this.EnableSignalsControls();
            this.EnableChannelsControls();

            // request configuration of analog input and chart.
            this.AnalogInputConfigurationRequired = true;
            this.ChartConfigurationRequired = true;
        }

        /// <summary> Returns a channel list view item. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="channel"> The channel. </param>
        /// <param name="signal">  . </param>
        /// <returns> The new signal list view item. </returns>
        private static ListViewItem CreateSignalListViewItem( OpenLayers.Base.ChannelListEntry channel, OpenLayers.Signals.MemorySignal signal )
        {
            if ( channel is null )
            {
                throw new ArgumentNullException( nameof( channel ) );
            }

            if ( signal is null )
            {
                throw new ArgumentNullException( nameof( signal ) );
            }
            // Create the signal item
            var signalItem = new ListViewItem( channel.PhysicalChannelNumber.ToString( System.Globalization.CultureInfo.CurrentCulture ) );
            _ = signalItem.SubItems.Add( signal.Name );
            string formatValue = "({0},{1}) V";
            _ = signalItem.SubItems.Add( string.Format( System.Globalization.CultureInfo.CurrentCulture, formatValue, signal.CurrentRangeMin, signal.CurrentRangeMax ) );
            return signalItem;
        }

        /// <summary> Removes the signal associated with the specified channel. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="channel"> The channel. </param>
        public void RemoveSignal( OpenLayers.Base.ChannelListEntry channel )
        {
            if ( channel is null )
                throw new ArgumentNullException( nameof( channel ) );
            if ( this.Chart.Signals.Contains( channel.Name ) )
            {
                this.RemoveSignal( this.Chart.Signals.SelectNamedSignal( channel.Name ) );
            }
        }

        /// <summary> Removes the signal associated with the specified channel. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="signal"> . </param>
        public void RemoveSignal( OpenLayers.Signals.MemorySignal signal )
        {
            if ( signal is null )
            {
                throw new ArgumentNullException( nameof( signal ) );
            }

            if ( this.Chart.Signals.Contains( signal ) )
            {
                _ = this.Chart.DisableRendering();

                // remove the signal
                this.Chart.Signals.Remove( signal );

                // update the chart
                this.Chart.SignalListUpdate();
                _ = this.Chart.EnableRendering();

                // update the list of signals.
                this.UpdateSignalList();
                if ( this.SelectedSignal is null || !this.Chart.Signals.Contains( this.SelectedSignal ) )
                {
                    if ( this.Chart.Signals.Count > 0 )
                    {
                        _ = this.SelectSignal( 0 );
                    }
                }
            }
        }

        /// <summary> Selects a signal. </summary>
        /// <param name="signalIndex"> . </param>
        /// <returns> The selected signal. </returns>
        private OpenLayers.Signals.MemorySignal SelectSignal( int signalIndex )
        {
            this.SelectedSignalIndex = signalIndex;
            if ( signalIndex >= 0 && signalIndex < this.Chart.Signals.Count )
            {
                this.SelectedSignal = this.Chart.Signals[this.SelectedSignalIndex];

                // update tool tips for displays
                this.DisplayAverageEnabled = this.DisplayAverageEnabled;
                this.DisplayVoltageEnabled = this.DisplayVoltageEnabled;

                // update selected signal display
                if ( this.SelectedSignal.CurrentRangeMin < ( double ) this._SignalMinNumericUpDown.Maximum && this.SelectedSignal.CurrentRangeMin > ( double ) this._SignalMinNumericUpDown.Minimum )
                {
                    this._SignalMinNumericUpDown.Value = ( decimal ) this.SelectedSignal.CurrentRangeMin;
                }

                if ( this.SelectedSignal.CurrentRangeMax < ( double ) this._SignalMaxNumericUpDown.Maximum && this.SelectedSignal.CurrentRangeMax > ( double ) this._SignalMaxNumericUpDown.Minimum )
                {
                    this._SignalMaxNumericUpDown.Value = ( decimal ) this.SelectedSignal.CurrentRangeMax;
                }

                if ( this.SelectedSignalIndex >= 0 && this.SelectedSignalIndex < this._SignalsListView.Items.Count )
                {
                    if ( this._SignalsListView.SelectedIndices.Count >= 1 && this._SignalsListView.SelectedIndices[0] != signalIndex )
                    {
                        bool wasEnabled = this._SignalsListView.Enabled;
                        this._SignalsListView.Enabled = false;
                        this._SignalsListView.SelectedIndices.Clear();
                        _ = this._SignalsListView.SelectedIndices.Add( this.SelectedSignalIndex );
                        this._SignalsListView.Enabled = wasEnabled;
                    }
                }
            }
            else
            {
                this.SelectedSignalIndex = -1;
                this.SelectedSignal = null;
            }

            return this.SelectedSignal;
        }

        /// <summary> Gets or sets the index of the selected signal in the signal list. This is required
        /// because some Open Layer methods requires the index rather than the signal. </summary>
        /// <value> The selected signal index. </value>
        private int SelectedSignalIndex { get; set; }

        /// <summary> Gets or sets the selected signal. </summary>
        /// <value> The selected signal. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        private OpenLayers.Signals.MemorySignal SelectedSignal { get; set; }

        /// <summary>
        /// Gets or sets the logical channel numbers for each signal.
        /// </summary>
        private int[] _SignalLogicalChannelNumbers;

        /// <summary> Updates the list of signals. </summary>
        private void UpdateSignalList()
        {
            int selectedIndex = -1;
            if ( this._SignalsListView.SelectedIndices.Count > 0 )
            {
                selectedIndex = this._SignalsListView.SelectedIndices[0];
            }

            if ( this.Chart.Signals.Count > 0 )
            {
                this._SignalLogicalChannelNumbers = new int[this.Chart.Signals.Count];
                Array.Clear( this._SignalLogicalChannelNumbers, 0, this._SignalLogicalChannelNumbers.Length );
            }

            _ = new OpenLayers.Signals.MemorySignal();
            this._SignalsListView.Items.Clear();
            foreach ( OpenLayers.Signals.MemorySignal signal in this.Chart.Signals )
            {

                // get the channel index (logical number)
                int channelIndex = this.AnalogInput.ChannelList.LogicalChannelNumber( signal.Name );

                // save the channel index for selecting the buffer to display for the signal.
                this._SignalLogicalChannelNumbers[this._SignalsListView.Items.Count] = channelIndex;

                // get a signal item,
                var signalItem = CreateSignalListViewItem( this.AnalogInput.ChannelList[channelIndex], signal );

                // get the item color from the signal color.
                signalItem.ForeColor = this.Chart.GetCurveColor( this._SignalsListView.Items.Count );

                // add the entry.
                _ = this._SignalsListView.Items.Add( signalItem );
            }

            this._SignalsListView.Invalidate();

            // update the signals control box.
            this.EnableSignalsControls();
            if ( this._SignalsListView.Items.Count > 0 )
            {
            }

            if ( this._SignalsListView.Items.Count > selectedIndex + 1 )
            {
                if ( selectedIndex > 0 )
                {
                    this._SignalsListView.Items[selectedIndex].Selected = true;
                }
            }
        }


        #endregion

        #region " BUFFERS COUNT "

        /// <summary> Gets or sets the maximum Buffers Count. </summary>
        /// <value> The max Buffers Count. </value>
        [Description( "The display maximum Buffers Count." )]
        [Category( "Data" )]
        [DefaultValue( 1000 )]
        public int MaximumBuffersCount
        {
            get => ( int ) Math.Round( this._BuffersCountNumeric.Maximum );

            set => _ = this._BuffersCountNumeric.MaximumSetter( ( decimal ) value );
        }

        /// <summary> Gets or sets the Minimum Buffers Count. </summary>
        /// <value> The Min Buffers Count. </value>
        [Description( "The display minimum Buffers Count." )]
        [Category( "Data" )]
        [DefaultValue( 2 )]
        public int MinimumBuffersCount
        {
            get => ( int ) Math.Round( this._BuffersCountNumeric.Minimum );

            set => _ = this._BuffersCountNumeric.MinimumSetter( ( decimal ) value );
        }

        /// <summary> Gets or sets the number of data collection buffers. </summary>
        /// <value> The number of buffers. </value>
        [Description( "Gets or sets the number of data collection buffers." )]
        [Category( "Data" )]
        [DefaultValue( 5 )]
        public int BuffersCount
        {
            get => ( int ) Math.Round( this._BuffersCountNumeric.Value );

            set {
                if ( value != this.BuffersCount )
                {
                    _ = this._BuffersCountNumeric.ValueSetter( ( decimal ) value );
                    // request configuration of analog input and chart.
                    this.AnalogInputConfigurationRequired = true;
                    this.ChartConfigurationRequired = true;
                }
            }
        }

        #endregion

        #region " CHANNEL BUFFER SIZE "

        /// <summary> Size of the channel buffer. </summary>

        /// <summary> Gets or sets the number of samples per channel. In scope mode this also equals the
        /// signal size. In strip chart mode, the board collects small buffers of 2 or more points. The
        /// signal is updated to display these new data when the buffers are filled. With
        /// <see cref="UsingSmartBuffering">smart buffering</see> this is larger than the
        /// <see cref="UsbBufferSize">USB buffer size</see>. </summary>
        /// <value> The size of the channel buffer. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public int ChannelBufferSize { get; private set; }

        /// <summary> Gets or sets total number of samples in the analog input buffer.  This is read only
        /// because the control exposes the buffer size per channel and configure this value based on the
        /// number of channels. Equals the channel buffer size times the number of channels. </summary>
        /// <value> The size of the sample buffer. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public int SampleBufferSize { get; private set; }

        /// <summary> Gets or sets the maximum Sample Size. </summary>
        /// <value> The max Sample Size. </value>
        [Description( "The maximum Sample Size." )]
        [Category( "Data" )]
        [DefaultValue( 1000 )]
        public int MaximumSampleSize
        {
            get => ( int ) Math.Round( this._SampleSizeNumeric.Maximum );

            set => _ = this._SampleSizeNumeric.MaximumSetter( ( decimal ) value );
        }

        /// <summary> Gets or sets the Minimum Sample Size. </summary>
        /// <value> The Min Sample Size. </value>
        [Description( "The minimum Sample Size." )]
        [Category( "Data" )]
        [DefaultValue( 2 )]
        public int MinimumSampleSize
        {
            get => ( int ) Math.Round( this._SampleSizeNumeric.Minimum );

            set => _ = this._SampleSizeNumeric.MinimumSetter( ( decimal ) value );
        }

        /// <summary> Gets or sets the number of samples acquired per channel buffer. In strip chart mode
        /// smart buffering this could be smaller than the signal buffer size. ????which is allocated
        /// based on the USB buffer size of 256 samples. </summary>
        /// <value> The size of the sample. </value>
        [Description( "Gets or sets the number of samples to collect per channel buffer." )]
        [Category( "Data" )]
        [DefaultValue( 1000.0d )]
        public int SampleSize
        {
            get => ( int ) Math.Round( this._SampleSizeNumeric.Value );

            set {
                if ( value != this.SampleSize )
                {
                    _ = this._SampleSizeNumeric.ValueSetter( ( decimal ) value );
                    // request configuration of analog input and chart.
                    this.AnalogInputConfigurationRequired = true;
                    this.ChartConfigurationRequired = true;
                }
            }
        }

        #endregion

        #region " MOVING AVERAGE "

        /// <summary> Gets or sets the maximum Moving Average Length. </summary>
        /// <value> The max Moving Average Length. </value>
        [Description( "The display maximum Moving Average Length." )]
        [Category( "Data" )]
        [DefaultValue( 100 )]
        public int MaximumMovingAverageLength
        {
            get => ( int ) Math.Round( this._MovingAverageLengthNumeric.Maximum );

            set => _ = this._MovingAverageLengthNumeric.MaximumSetter( ( decimal ) value );
        }

        /// <summary> Gets or sets the Minimum Moving Average Length. </summary>
        /// <value> The Min Moving Average Length. </value>
        [Description( "The display minimum Moving Average Length." )]
        [Category( "Data" )]
        [DefaultValue( 1 )]
        public int MinimumMovingAverageLength
        {
            get => ( int ) Math.Round( this._MovingAverageLengthNumeric.Minimum );

            set => _ = this._MovingAverageLengthNumeric.MinimumSetter( ( decimal ) value );
        }

        /// <summary> Gets or sets the average length of the moving. </summary>
        /// <value> The average length of the moving. </value>
        [Description( "The number of point to use when calculating the moving average for the status bar." )]
        [Category( "Data" )]
        [DefaultValue( 10 )]
        public int MovingAverageLength
        {
            get => ( int ) Math.Round( this._MovingAverageLengthNumeric.Value );

            set {
                if ( value != this.MovingAverageLength )
                {
                    _ = this._MovingAverageLengthNumeric.ValueSetter( ( decimal ) value );
                    // request configuration of analog input and chart.
                    this.AnalogInputConfigurationRequired = true;
                    this.ChartConfigurationRequired = true;
                }
            }
        }

        #endregion

        #region " REFRESH RATE "

        /// <summary> Gets or sets the maximum refresh rate. </summary>
        /// <value> The max refresh rate. </value>
        [Description( "The display maximum refresh rate per second." )]
        [Category( "Appearance" )]
        [DefaultValue( 100 )]
        public decimal MaximumRefreshRate
        {
            get => this._RefreshRateNumeric.Maximum;

            set => _ = this._RefreshRateNumeric.MaximumSetter( value );
        }

        /// <summary> Gets or sets the Minimum refresh rate. </summary>
        /// <value> The Min refresh rate. </value>
        [Description( "The display minimum refresh rate per second." )]
        [Category( "Appearance" )]
        [DefaultValue( 1.0d )]
        public decimal MinimumRefreshRate
        {
            get => this._RefreshRateNumeric.Minimum;

            set => _ = this._RefreshRateNumeric.MinimumSetter( value );
        }

        /// <summary> The next refresh time. </summary>
        private readonly Stopwatch _RefreshStopwatch;

        /// <summary> The refresh time span. </summary>
        private TimeSpan _RefreshTimespan;

        /// <summary> Gets or sets the display refresh rate. </summary>
        /// <value> The refresh rate. </value>
        [Description( "The display refresh rate per second." )]
        [Category( "Appearance" )]
        [DefaultValue( 24.0d )]
        public double RefreshRate
        {
            get => ( double ) this._RefreshRateNumeric.Value;

            set {
                if ( value != this.RefreshRate )
                {
                    _ = this._RefreshRateNumeric.ValueSetter( value );
                    // request configuration of chart.
                    this.ChartConfigurationRequired = true;
                }
            }
        }

        #endregion

        #region " SAMPLING RATE "

        /// <summary> Gets or sets the sampling period. This is the inverse of the
        /// <see cref="ClockRate">clock frequency</see>. </summary>
        /// <value> The sampling period. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public double SamplingPeriod
        {
            get => this.IsOpen ? 1.0d / this.AnalogInput.Clock.Frequency : 0d;

            set {
                if ( value > 0d )
                {
                    this.ClockRate = 1.0d / value;
                }
            }
        }

        /// <summary> Gets or sets the actual analog input board rate.  
        /// The clock differs from the <see cref="SamplingRate">sampling rate</see> in cases where a
        /// sampling rate slower then the minimum clock rate is desired or in strip chart
        /// <see cref="UsingSmartBuffering">smart buffering mode</see>. </summary>
        /// <value> The clock rate. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public double ClockRate
        {
            get => this.IsOpen ? this.AnalogInput.Clock.Frequency : 0d;

            set {
                if ( this.IsOpen )
                {
                    this.AnalogInput.Clock.Frequency = value;
                }
            }
        }

        /// <summary> Gets or sets the maximum Sampling rate. </summary>
        /// <value> The max Sampling rate. </value>
        [Description( "The display maximum Sampling rate per second." )]
        [Category( "Appearance" )]
        [DefaultValue( 100000 )]
        public decimal MaximumSamplingRate
        {
            get => this._SamplingRateNumeric.Maximum;

            set => _ = this._SamplingRateNumeric.MaximumSetter( value );
        }

        /// <summary> Gets or sets the Minimum Sampling rate. </summary>
        /// <value> The Min Sampling rate. </value>
        [Description( "The display minimum Sampling rate per second." )]
        [Category( "Appearance" )]
        [DefaultValue( 60 )]
        public decimal MinimumSamplingRate
        {
            get => this._SamplingRateNumeric.Minimum;

            set => _ = this._SamplingRateNumeric.MinimumSetter( value );
        }

        /// <summary> Gets or sets the sample rate in samples per second. </summary>
        /// <value> The sampling rate. </value>
        [Description( "Gets or sets the sample rate in samples per second." )]
        [Category( "Data" )]
        [DefaultValue( 1000.0d )]
        public double SamplingRate
        {
            get => ( double ) this._SamplingRateNumeric.Value;

            set {
                if ( Math.Abs( this.SamplingRate - value ) > 0.001d )
                {
                    _ = this._SamplingRateNumeric.ValueSetter( ( decimal ) value );
                    // request configuration of analog input and chart.
                    this.AnalogInputConfigurationRequired = true;
                    this.ChartConfigurationRequired = true;
                }

                if ( this.IsOpen )
                {
                    this._ToolTip.SetToolTip( this._SamplingRateNumeric, string.Format( System.Globalization.CultureInfo.CurrentCulture, "Number of samples per second to collect. Board Clock Rate is {0}Hz", this.ClockRate.ToString( "{0:0.00}", System.Globalization.CultureInfo.CurrentCulture ) ) );
                }
                else
                {
                    this._ToolTip.SetToolTip( this._SamplingRateNumeric, "Number of samples per second to collect." );
                }
            }
        }

        #endregion

        #region " SIGNAL BUFFER SIZE "

        /// <summary> Gets or sets the maximum Signal Buffer Size. </summary>
        /// <value> The max Signal Buffer Size. </value>
        [Description( "The maximum Signal Buffer Size." )]
        [Category( "Data" )]
        [DefaultValue( 10000 )]
        public int MaximumSignalBufferSize
        {
            get => ( int ) Math.Round( this._SignalBufferSizeNumeric.Maximum );

            set => _ = this._SignalBufferSizeNumeric.MaximumSetter( ( decimal ) value );
        }

        /// <summary> Gets or sets the Minimum Signal Buffer Size. </summary>
        /// <value> The Min Signal Buffer Size. </value>
        [Description( "The minimum Signal Buffer Size." )]
        [Category( "Data" )]
        [DefaultValue( 2 )]
        public int MinimumSignalBufferSize
        {
            get => ( int ) Math.Round( this._SignalBufferSizeNumeric.Minimum );

            set => _ = this._SignalBufferSizeNumeric.MinimumSetter( ( decimal ) value );
        }

        /// <summary> Gets or sets the number of samples per signal. This is the same as the maximum
        /// visible samples on the chart. In scope mode, this is the same as the channel buffer size.  In
        /// strip chart a signal includes the samples for many small sequential buffers that are
        /// retrieved from the voltage buffer. </summary>
        /// <value> The size of the signal buffer. </value>
        [Description( "Gets or sets the number of samples per signal." )]
        [Category( "Data" )]
        [DefaultValue( 1000.0d )]
        public int SignalBufferSize
        {
            get => ( int ) Math.Round( this._SignalBufferSizeNumeric.Value );

            set {
                if ( value != this.SignalBufferSize )
                {
                    _ = this._SignalBufferSizeNumeric.ValueSetter( ( decimal ) value );
                    // request configuration of analog input and chart.
                    this.AnalogInputConfigurationRequired = true;
                    this.ChartConfigurationRequired = true;
                }
            }
        }

        #endregion

        #region " SIGNAL MEMORY LENGTH "

        /// <summary> Gets or sets the maximum Signal Memory Length. </summary>
        /// <value> The max Signal Memory Length. </value>
        [Description( "The maximum Signal Memory Length." )]
        [Category( "Data" )]
        [DefaultValue( 100000000 )]
        public int MaximumSignalMemoryLength
        {
            get => ( int ) Math.Round( this._SignalMemoryLengthNumeric.Maximum );

            set => _ = this._SignalMemoryLengthNumeric.MaximumSetter( ( decimal ) value );
        }

        /// <summary> Gets or sets the Minimum Signal Memory Length. </summary>
        /// <value> The Min Signal Memory Length. </value>
        [Description( "The minimum Signal Memory Length." )]
        [Category( "Data" )]
        [DefaultValue( 2 )]
        public int MinimumSignalMemoryLength
        {
            get => ( int ) Math.Round( this._SignalMemoryLengthNumeric.Minimum );

            set => _ = this._SignalMemoryLengthNumeric.MinimumSetter( ( decimal ) value );
        }

        /// <summary> Gets or sets the signal memory length. </summary>
        /// <value> The length of the signal memory. </value>
        [Description( "The signal memory length" )]
        [Category( "Data" )]
        [DefaultValue( 10000.0d )]
        public int SignalMemoryLength
        {
            get => ( int ) Math.Round( this._SignalMemoryLengthNumeric.Value );

            set {
                if ( value != this.SignalMemoryLength )
                {
                    _ = this._SignalMemoryLengthNumeric.ValueSetter( ( decimal ) value );
                    // request configuration of analog input and chart.
                    this.AnalogInputConfigurationRequired = true;
                    this.ChartConfigurationRequired = true;
                }
            }
        }

        #endregion

        #region " BUFFERING "

        /// <summary> Gets or sets the finite buffering mode.  When true, the system collects the specified
        /// <see cref="BuffersCount">number of buffers</see> and stops with a buffer done event. </summary>
        /// <value> The finite buffering. </value>
        [Description( "Toggles the continuous (infinite) buffering mode." )]
        [Category( "Appearance" )]
        [DefaultValue( false )]
        public bool FiniteBuffering
        {
            get => !this._ContinuousBufferingCheckBox.Checked;

            set {
                if ( value != this.FiniteBuffering )
                {
                    this._ContinuousBufferingCheckBox.Checked = !value;
                    // request configuration of analog input and chart.
                    this.AnalogInputConfigurationRequired = true;
                    this.ChartConfigurationRequired = true;
                }
            }
        }

        /// <summary> Gets the number of samples allocated per USB buffer. Data collection is accomplished
        /// in USB buffer sizes.  Thus, the buffer done event is invoked only upon completion of the last
        /// USB buffer required to collect the sample buffer size.  If the sample buffer size is smaller
        /// than a USB buffer, the board collects as many sample buffers as would fit into the USB buffer
        /// size and then issues as many buffer done events as buffers collected. </summary>
        /// <value> The size of the USB buffer. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public int UsbBufferSize { get; private set; }

        /// <summary> Gets or set the smart buffering option.  With smart buffering a buffer size of
        /// multiples of the USB buffer size is allocated.  In strip chart smart buffering mode, a
        /// channel buffer size of at least <see cref="UsbBufferSize">USB buffer size</see>
        /// is allocated. </summary>
        /// <value> The using smart buffering. </value>
        [Description( "Gets or set the smart buffering option." )]
        [Category( "Data" )]
        [DefaultValue( true )]
        public bool UsingSmartBuffering
        {
            get => this._SmartBufferingCheckBox.Checked;

            set {
                if ( value != this.UsingSmartBuffering )
                {
                    this._SmartBufferingCheckBox.Checked = value;
                    // request configuration of analog input and chart.
                    this.AnalogInputConfigurationRequired = true;
                    this.ChartConfigurationRequired = true;
                }
            }
        }

        #endregion

        #endregion

        #region " ANALOG INPUT ACQUISITION "

        /// <summary> The sample counter. </summary>

        /// <summary> Returns the total number of counts since the chart started. </summary>
        /// <value> The sample counter. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public long SampleCounter { get; private set; }

        #endregion

        #region " CALCULATION CONFIGURATION "

        /// <summary> Gets or sets the option for calculating the average voltage. </summary>
        /// <value> The calculate enabled. </value>
        [Description( "Toggles the status for calculating averages." )]
        [Category( "Data" )]
        [DefaultValue( false )]
        public bool CalculateEnabled
        {
            get => this._CalculateAverageCheckBox.Checked;

            set {
                if ( value != this.CalculateEnabled )
                {
                    this._CalculateAverageCheckBox.Checked = value;
                }
            }
        }

        #endregion

        #region " CALCULATION OPERATION "

        /// <summary> Gets or sets the calculation period down counts. </summary>
        /// <value> The calculate period down counts. </value>
        private long CalculatePeriodDownCounts { get; set; }

        /// <summary> Gets or sets the number of samples between calculate events. </summary>
        /// <value> The number of calculate periods. </value>
        [Description( "Gets or sets the number of samples between calculation updates." )]
        [Category( "Data" )]
        [DefaultValue( 10.0d )]
        public int CalculatePeriodCount { get; set; }

        /// <summary> The last averages. </summary>
        private double[] _LastAverages;

        /// <summary> Returns the last average saved for each channel. </summary>
        /// <returns> The last average saved for each channel. </returns>
        public double[] LastAverages()
        {
            return this._LastAverages;
        }

        /// <summary> The last voltages. </summary>
        private double[] _LastVoltages;

        /// <summary> Returns the last voltages saved for each channel. </summary>
        /// <returns> Te last voltages saved for each channel.  </returns>
        public double[] LastVoltages()
        {
            return this._LastVoltages;
        }

        #endregion

        #region " CHART CONFIGURATION "

        /// <summary> Gets or sets the chart axes color. </summary>
        /// <value> The color of the axes. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public Color AxesColor
        {
            get => this.Chart.AxesColor;

            set {
                _ = this.Chart.DisableRendering();
                this.Chart.AxesColor = value;
                _ = this.Chart.EnableRendering();
                this.Chart.SignalUpdate();
            }
        }

        /// <summary> Gets or sets reference to the chart control. </summary>
        /// <value> The chart. </value>
        public OpenLayers.Controls.Display Chart { get; private set; }

        /// <summary> Gets or sets the <see cref="OpenLayers.Controls.BandMode">band mode</see>.  In a
        /// single band all channels are displayed on a single graph. In multi bank, each channel is
        /// allocated its own graph. </summary>
        /// <value> The is multi-band mode. </value>
        [Description( "Gets or sets the chart band mode.  Multi Band means separate chart for each signal" )]
        [Category( "Appearance" )]
        [DefaultValue( false )]
        public bool IsMultibandMode
        {
            get => this.Chart.BandMode == OpenLayers.Controls.BandMode.MultiBand;

            set => this.ChartBandMode = value ? OpenLayers.Controls.BandMode.MultiBand : OpenLayers.Controls.BandMode.SingleBand;
        }

        /// <summary> Gets or sets the <see cref="OpenLayers.Controls.BandMode">band mode</see>.  In a
        /// single band all channels are displayed on a single graph. In multi bank, each channel is
        /// allocated its own graph.  This property is not exposed directly so as not to require the
        /// hosting assembly to directly reference the <see cref="OpenLayers.Controls">controls</see>
        /// assembly. </summary>
        /// <value> The chart band mode. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0052:Remove unread private members", Justification = "<Pending>" )]
        private OpenLayers.Controls.BandMode ChartBandMode
        {
            get => this.Chart.BandMode;

            set {
                _ = this.Chart.DisableRendering();
                this.Chart.BandMode = value;
                _ = this.Chart.EnableRendering();
                this.Chart.SignalUpdate();
                if ( this._SingleRadioButton.Checked ^ this.Chart.BandMode == OpenLayers.Controls.BandMode.SingleBand )
                {
                    _ = this._SingleRadioButton.SilentCheckedSetter( this.Chart.BandMode == OpenLayers.Controls.BandMode.SingleBand );
                }
            }
        }

        /// <summary> Gets or sets the chart footer. </summary>
        /// <value> The chart footer. </value>
        [Description( "Gets or sets the chart footer." )]
        [Category( "Appearance" )]
        [DefaultValue( "" )]
        public string ChartFooter
        {
            get => this._ChartFooterTextBox.Text;

            set {
                if ( (value ?? "") != (this.ChartFooter ?? "") )
                {
                    this._ChartFooterTextBox.Text = value;
                }

                if ( this.Chart is object )
                {
                    this.Chart.Footer = value;
                }

                if ( string.IsNullOrWhiteSpace( value ) )
                {
                    this._ToolTip.SetToolTip( this._ChartFooterTextBox, "Chart Footer: Empty" );
                }
                else
                {
                    this._ToolTip.SetToolTip( this._ChartFooterTextBox, value );
                }
            }
        }

        /// <summary> Gets or sets the <see cref="ChartModeId">scope or strip chart modes</see>. </summary>
        /// <value> The chart mode. </value>
        [Description( "Gets or sets the chart mode" )]
        [Category( "Appearance" )]
        [DefaultValue( ChartModeId.Scope )]
        public ChartModeId ChartMode
        {
            get => this._ScopeRadioButton.Checked ? ChartModeId.Scope : ChartModeId.StripChart;

            set {
                if ( value != this.ChartMode )
                {
                    _ = this._ScopeRadioButton.SilentCheckedSetter( value == ChartModeId.Scope );
                    // request configuration of analog input and chart.
                    this.AnalogInputConfigurationRequired = true;
                    this.ChartConfigurationRequired = true;
                }

                this.EnableChannelsControls();
                this.EnableSignalsControls();
            }
        }

        /// <summary> Gets or sets the chart title. </summary>
        /// <value> The chart title. </value>
        [Description( "Gets or sets the chart title." )]
        [Category( "Appearance" )]
        [DefaultValue( "Analog Input Time Series" )]
        public string ChartTitle
        {
            get => this._ChartTitleTextBox.Text;

            set {
                if ( (value ?? "") != (this.ChartTitle ?? "") )
                {
                    this._ChartTitleTextBox.Text = value;
                }

                if ( this.Chart is object )
                {
                    this.Chart.Title = value;
                }

                if ( string.IsNullOrWhiteSpace( value ) )
                {
                    this._ToolTip.SetToolTip( this._ChartTitleTextBox, "Chart Title: Empty" );
                }
                else
                {
                    this._ToolTip.SetToolTip( this._ChartTitleTextBox, value );
                }
            }
        }

        /// <summary> Gets or sets or set the charting enabled status. </summary>
        /// <value> The charting enabled. </value>
        [Description( "Toggles the charting of sampled data." )]
        [Category( "Appearance" )]
        [DefaultValue( true )]
        public bool ChartingEnabled
        {
            get => this._EnableChartingCheckBox.Checked;

            set {
                if ( value != this.ChartingEnabled )
                {
                    this._EnableChartingCheckBox.Checked = value;
                }
            }
        }

        /// <summary>Gets or sets the available signal colors.</summary>
        private readonly Color[] _DefaultSignalColors = new[] { Color.Red, Color.Blue, Color.Magenta, Color.Lime, Color.Green, Color.Orange };

        /// <summary>   Default signal color getter. </summary>
        /// <param name="index">    Zero-based index of the. </param>
        /// <returns>   The default signal color. </returns>
        public Color DefaultSignalColorGetter( int index )
        {
            index = Math.Max( 0, Math.Min( index, this._DefaultSignalColors.Length - 1 ) );
            return this._DefaultSignalColors[index];
        }

        /// <summary>   Default signal color setter. </summary>
        /// <remarks>   David, 2022-01-11. </remarks>
        /// <param name="index">    Zero-based index of the. </param>
        /// <param name="value">    The value. </param>
        public void DefaultSignalColorSetter( int index, Color value )
        {
            index = Math.Max( 0, Math.Min( index, this._DefaultSignalColors.Length - 1 ) );
            this._DefaultSignalColors[index] = value;
        }

        /// <summary> Gets or sets the option for displaying the average voltage reading. </summary>
        /// <value> The display average enabled. </value>
        [Description( "Toggles the average voltage status display." )]
        [Category( "Appearance" )]
        [DefaultValue( false )]
        public bool DisplayAverageEnabled
        {
            get => this._DisplayAverageCheckBox.Checked;

            set {
                if ( value != this.DisplayAverageEnabled )
                {
                    this._DisplayAverageCheckBox.Checked = value;
                }

                if ( value )
                {
                    this._AverageVoltageToolStripLabel.ToolTipText = this.SelectedSignal is null ? "Average" : string.Format( System.Globalization.CultureInfo.CurrentCulture, "{0} Average", this.SelectedSignal.Name );
                }
                else
                {
                    this._AverageVoltageToolStripLabel.Text = string.Empty;
                    this._AverageVoltageToolStripLabel.ToolTipText = "Average: Off";
                }
            }
        }

        /// <summary> Gets or sets the option for displaying the voltage reading. </summary>
        /// <value> The display voltage enabled. </value>
        [Description( "Toggles the voltage reading display." )]
        [Category( "Appearance" )]
        [DefaultValue( false )]
        public bool DisplayVoltageEnabled
        {
            get => this._DisplayVoltageCheckBox.Checked;

            set {
                if ( value != this._DisplayVoltageCheckBox.Checked )
                {
                    this._DisplayVoltageCheckBox.Checked = value;
                }

                if ( value )
                {
                    this._CurrentVoltageToolStripLabel.ToolTipText = this.SelectedSignal is null ? "Current Voltage" : string.Format( System.Globalization.CultureInfo.CurrentCulture, "{0} voltage", this.SelectedSignal.Name );
                }
                else
                {
                    this._CurrentVoltageToolStripLabel.Text = string.Empty;
                    this._CurrentVoltageToolStripLabel.ToolTipText = "Current Voltage: Off";
                }
            }
        }

        /// <summary> Gets or sets the chart grid color. </summary>
        /// <value> The color of the grid. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public Color GridColor
        {
            get => this.Chart.GridColor;

            set {
                _ = this.Chart.DisableRendering();
                this.Chart.GridColor = value;
                _ = this.Chart.EnableRendering();
                this.Chart.SignalUpdate();
            }
        }

        /// <summary> Set the color of the selected signal. </summary>
        /// <value> The color of the selected signal. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public Color SelectedSignalColor
        {
            get => this.SelectedSignal is object ? this.Chart.GetCurveColor( this.SelectedSignalIndex ) : Color.Blue;

            set {
                if ( this.SelectedSignalIndex >= 0 )
                {
                    _ = this.Chart.DisableRendering();

                    // set the color
                    this.Chart.SetCurveColor( this.SelectedSignalIndex, value );

                    // update the chart
                    this.Chart.SignalListUpdate();
                    _ = this.Chart.EnableRendering();

                    // update the list of signals.
                    this.UpdateSignalList();
                }
            }
        }

        /// <summary> The update period down counter. </summary>
        private long _UpdatePeriodDownCounter;

        /// <summary> Gets or sets the number of samples between update events. </summary>
        /// <value> The number of update periods. </value>
        [Description( "Gets or sets the number of samples between update events." )]
        [Category( "Data" )]
        [DefaultValue( 10.0d )]
        public int UpdatePeriodCount { get; set; }

        /// <summary> Gets or sets the option for calculating the average voltage. </summary>
        /// <value> The update event enabled. </value>
        [Description( "Toggles the status for update events." )]
        [Category( "Data" )]
        [DefaultValue( false )]
        public bool UpdateEventEnabled { get; set; }

        /// <summary> The volts readings string format. </summary>
        private string _VoltsReadingsStringFormat;
        /// <summary> The volts reading format. </summary>
        private string _VoltsReadingFormat;

        /// <summary> Gets or sets the format for displaying the voltage. </summary>
        /// <value> The volts reading format. </value>
        [Description( "Formats the voltage reading display." )]
        [Category( "Appearance" )]
        [DefaultValue( "0.000" )]
        public string VoltsReadingFormat
        {
            get => this._VoltsReadingFormat;

            set {
                this._VoltsReadingFormat = value;
                this._VoltsReadingsStringFormat = "{0:" + value + " v}";
            }
        }

        #endregion

        #region " CHART DISPLAY "

        /// <summary> Gets or sets the time axis period in seconds or milliseconds. </summary>
        /// <value> The time axis period. </value>
        private double TimeAxisPeriod { get; set; }

        /// <summary> Gets or sets the time axis duration in seconds or milliseconds. </summary>
        /// <value> The time axis duration. </value>
        private double TimeAxisDuration { get; set; }

        /// <summary> Scroll the time frame of the chart. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private void ScrollTimeFrame()
        {
            ulong newScrollValue = ( ulong ) (this.SampleCounter - this.SignalBufferSize);
            if ( newScrollValue < 0m )
            {
                newScrollValue = 0UL;
            }

            this.ScrollTimeFrame( newScrollValue );
        }

        /// <summary> Scroll the time frame of the chart. </summary>
        /// <param name="newScrollValue"> . </param>
        private void ScrollTimeFrame( ulong newScrollValue )
        {
            double startTime = newScrollValue * this.TimeAxisPeriod;
            double endTime = startTime + this.TimeAxisDuration;

            // setup range of x-axis
            this.Chart.XDataCurrentRangeMin = startTime;
            this.Chart.XDataCurrentRangeMax = endTime;
            this.Chart.XDataRangeMin = startTime;
            this.Chart.XDataRangeMax = endTime;
        }

        #endregion

        #region " VALIDATE AND CONFIGURE "

        /// <summary> <c>True</c> if analog input configuration required. </summary>
        private bool _AnalogInputConfigurationRequired;

        /// <summary> Gets or sets a value indicating whether [analog input configuration required].
        /// Disables start when set True. </summary>
        /// <value> <c>True</c> if [analog input configuration required]; otherwise, <c>False</c>. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public bool AnalogInputConfigurationRequired
        {
            get => this._AnalogInputConfigurationRequired || OpenLayers.Base.SubsystemBase.States.ConfiguredForContinuous != this.AnalogInputState;

            private set {
                this._AnalogInputConfigurationRequired = value;
                if ( value )
                {
                    this.StartStopEnabled = false;
                }
            }
        }

        /// <summary> <c>True</c> if chart configuration required. </summary>
        private bool _ChartConfigurationRequired;

        /// <summary> Gets or sets a value indicating whether [chart configuration required]. Disables
        /// start when set True. </summary>
        /// <value> <c>True</c> if [chart configuration required]; otherwise, <c>False</c>. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public bool ChartConfigurationRequired
        {
            get => this._ChartConfigurationRequired;

            set {
                this._ChartConfigurationRequired = value;
                if ( value )
                {
                    this.StartStopEnabled = false;
                }
            }
        }

        /// <summary> Gets or sets a value indicating whether [configuration required]. </summary>
        /// <value> <c>True</c> if [configuration required]; otherwise, <c>False</c>. </value>
        public bool ConfigurationRequired => this.AnalogInputConfigurationRequired || this.ChartConfigurationRequired;

        /// <summary> Validates the configuration. </summary>
        /// <param name="details"> [in,out] A non-empty validation outcome string containing the reason
        /// validation failed. </param>
        /// <returns> <c>True</c> if validated; otherwise, <c>False</c>. </returns>
        public bool ValidateConfiguration( ref string details )
        {
            var outcomeBuilder = new System.Text.StringBuilder();
            if ( !this.IsOpen )
            {
                _ = outcomeBuilder.Append( "Board not initialized (not 'open')" );
            }
            else if ( !this.AnalogInput.HasChannels )
            {
                // configure only if we have defined channels.
                _ = outcomeBuilder.Append( "The list of analog input channels is empty." );
            }
            else
            {
                if ( this.SamplingRate > this.AnalogInput.Clock.MaxFrequency )
                {
                    this.Enunciate( this._SamplingRateNumeric, "Requested sampling rate {0} is larger than the maximum {1}", this.SamplingRate, this.AnalogInput.Clock.MaxFrequency );
                    _ = outcomeBuilder.AppendLine( $"Requested sampling rate {this.SamplingRate} is larger than the maximum {this.AnalogInput.Clock.MaxFrequency}" );
                }

                if ( this.BuffersCount < this.MinimumBuffersCount )
                {
                    this.Enunciate( this._BuffersCountNumeric, "Requested number of channel buffers {0} is lower than the minimum {1}", this.BuffersCount, this.MinimumBuffersCount );
                    _ = outcomeBuilder.AppendLine( $"Requested number of channel buffers {this.BuffersCount} is lower than the minimum {this.MinimumBuffersCount}" );
                }
                else if ( this.BuffersCount > this.MaximumBuffersCount )
                {
                    this.Enunciate( this._BuffersCountNumeric, "Requested number of channel buffers {0} is greater than the maximum {1}", this.BuffersCount, this.MaximumBuffersCount );
                    _ = outcomeBuilder.AppendLine( $"Requested number of channel buffers {this.BuffersCount} is greater than the maximum {this.MaximumBuffersCount}" );
                }

                if ( this.MovingAverageLength < this.MinimumMovingAverageLength )
                {
                    this.Enunciate( this._MovingAverageLengthNumeric, "Requested length of moving average {0} is lower than the minimum {1}", this.MovingAverageLength, this.MinimumMovingAverageLength );
                    _ = outcomeBuilder.AppendLine( $"Requested length of moving average {this.MovingAverageLength} is lower than the minimum {this.MinimumMovingAverageLength}" );
                }
                else if ( this.SignalBufferSize > 0 && this.MovingAverageLength > this.SignalBufferSize )
                {
                    this.Enunciate( this._MovingAverageLengthNumeric, "Requested length of moving average {0} is greater than the signal buffer length {1}", this.MovingAverageLength, this.SignalBufferSize );
                    _ = outcomeBuilder.AppendLine( $"Requested length of moving average {this.MovingAverageLength} is greater than the signal buffer length {this.SignalBufferSize}" );
                }

                if ( this.SampleSize < this.MinimumSampleSize )
                {
                    this.Enunciate( this._SampleSizeNumeric, "Requested size of sample buffer {0} is lower than the minimum {1}", this.SampleSize, this.MinimumSampleSize );
                    _ = outcomeBuilder.AppendLine( $"Requested size of sample buffer {this.SampleSize} is lower than the minimum {this.MinimumSampleSize}" );
                }

                if ( this.SamplingRate > this.AnalogInput.Clock.MaxFrequency )
                {
                    this.Enunciate( this._SamplingRateNumeric, "Requested sampling rate {0} is larger than the minimum {1}", this.SamplingRate, this.AnalogInput.Clock.MaxFrequency );
                    _ = outcomeBuilder.AppendLine( $"Requested sampling rate {this.SamplingRate} is larger than the minimum {this.AnalogInput.Clock.MaxFrequency}" );
                }

                if ( this.SignalBufferSize < this.MinimumSignalBufferSize )
                {
                    this.Enunciate( this._SignalBufferSizeNumeric, "Signal buffer size {0} must be larger than {1}", this.SignalBufferSize, this.MinimumSignalBufferSize );
                    _ = outcomeBuilder.AppendLine( $"Signal buffer size {this.SignalBufferSize} must be larger than {this.MinimumSignalBufferSize}" );
                }

                if ( this.SignalMemoryLength < this.SignalBufferSize )
                {
                    this.Enunciate( this._SignalMemoryLengthNumeric, "Signal memory length {0} must be larger than signal buffer length {1}", this.SignalMemoryLength, this.SignalBufferSize );
                    _ = outcomeBuilder.AppendLine( $"Signal memory length {this.SignalMemoryLength} must be larger than signal buffer length {this.SignalBufferSize}" );
                }

                if ( this.RefreshRate > ( double ) this.MaximumRefreshRate )
                {
                    this.Enunciate( this._RefreshRateNumeric, "Refresh rate {0} exceeds maximum of {1}", this.RefreshRate, this.MaximumRefreshRate );
                }
                else if ( this.RefreshRate < ( double ) this.MinimumRefreshRate )
                {
                    this.Enunciate( this._RefreshRateNumeric, "Refresh rate {0} is below the minimum of {1}", this.RefreshRate, this.MinimumRefreshRate );
                }
            }

            if ( outcomeBuilder.Length > 0 )
            {
                details = outcomeBuilder.ToString();
                this._ErrorProvider.SetError( this._ConfigureButton, details );
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary> Configures analog inputs and signals. Disables start if all clear. </summary>
        /// <param name="details"> [in,out] A non-empty validation outcome string containing the reason
        /// configuration failed. </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        private bool TryConfigureThis( ref string details )
        {
            if ( this.ConfigurationRequired )
            {
                if ( !this.ValidateConfiguration( ref details ) )
                {
                    this._ErrorProvider.SetError( this._ConfigureButton, details );
                    this.StatusMessage = $"Validation failed;. Details: {details}";
                    return false;
                }
            }

            this._ErrorProvider.Clear();
            if ( this.AnalogInputConfigurationRequired )
            {
                if ( !this.TryConfigureAnalogInput( ref details ) )
                {
                    this._ErrorProvider.SetError( this._ConfigureButton, details );
                    this.StatusMessage = $"Analog input configuration failed;. Details: {details}";
                    return false;
                }
            }

            if ( this.ChartConfigurationRequired )
            {
                if ( !this.TryConfigureChart( ref details ) )
                {
                    this._ErrorProvider.SetError( this._ConfigureButton, details );
                    this.StatusMessage = $"Chart configuration failed;. Details: {details}";
                    return false;
                }
            }

            return true;
        }

        /// <summary> Configures the analog input. </summary>
        /// <param name="details"> [in,out] A non-empty validation outcome string containing the reason
        /// configuration failed. </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public bool TryConfigureAnalogInput( ref string details )
        {
            var outcomeBuilder = new System.Text.StringBuilder();
            if ( this.ConfigureSuspended )
            {
                _ = outcomeBuilder.Append( "Configuration suspended" );
            }
            else if ( !this.IsOpen )
            {
                _ = outcomeBuilder.Append( "Board not initialized (not 'open')" );
            }
            else if ( !this.AnalogInput.HasChannels )
            {
                // configure only if we have defined channels.
                _ = outcomeBuilder.Append( "The list of analog input channels is empty." );
            }

            // turn off the configuration required sentinel.
            this.AnalogInputConfigurationRequired = false;
            this._LastAverages = new double[this.AnalogInput.ChannelList.Count];
            Array.Clear( this._LastAverages, 0, this._LastAverages.Length );
            this._LastVoltages = new double[this.AnalogInput.ChannelList.Count];
            Array.Clear( this._LastAverages, 0, this._LastAverages.Length );
            try
            {
                this.SuspendConfig();
                double desiredClockFrequency = this.SamplingRate;
                this.DecimationRatio = 1;
                if ( this.SamplingRate > this.AnalogInput.Clock.MaxFrequency )
                {
                    this.Enunciate( this._SamplingRateNumeric, "Requested sampling rate {0} is larger than the maximum {1}.", this.SamplingRate, this.AnalogInput.Clock.MaxFrequency );
                    _ = outcomeBuilder.AppendLine( $"Requested sampling rate {this.SamplingRate} is larger than the maximum {this.AnalogInput.Clock.MaxFrequency}" );
                }

                // set channel buffer size to sample size unless we have some special conditions.
                this.ChannelBufferSize = this.SampleSize;

                // set the sample buffer size based on the number of channels
                this.SampleBufferSize = this.ChannelBufferSize * this.AnalogInput.ChannelList.Count;
                if ( this.ChartMode == ChartModeId.StripChart )
                {
                    if ( this.UsingSmartBuffering )
                    {

                        // with smart buffering each sample buffer must be at least 256 points
                        if ( this.SampleBufferSize < this.UsbBufferSize )
                        {

                            // set channel buffer size to the use buffer at least minimum sampling rate.
                            this.DecimationRatio = ( int ) Math.Round( Math.Max( Math.Ceiling( this.UsbBufferSize / ( double ) this.SampleBufferSize ), Math.Ceiling( this.AnalogInput.Clock.MinFrequency / this.SamplingRate ) ) );

                            // set the sampling rate to reflect the increase in channel buffer size
                            desiredClockFrequency = this.DecimationRatio * this.SamplingRate;
                            this.ChannelBufferSize = this.DecimationRatio * this.SampleSize;
                            if ( desiredClockFrequency > this.AnalogInput.Clock.MaxFrequency )
                            {
                                this.Enunciate( this._SampleSizeNumeric, "Decimated sampling rate {0} is larger than the maximum {1}. Either use simple buffering or increase the sample size.", this.SamplingRate, this.AnalogInput.Clock.MaxFrequency );
                                _ = outcomeBuilder.AppendLine( $"Decimated sampling rate {this.SamplingRate} is larger than the maximum {this.AnalogInput.Clock.MaxFrequency}. Either use simple buffering or increase the sample size." );
                            }
                        }
                        else if ( desiredClockFrequency < this.AnalogInput.Clock.MinFrequency )
                        {
                            // if sampling rate lower than the minimum clock frequency then we need to decimate.
                            this.DecimationRatio = ( int ) Math.Round( Math.Ceiling( (this.AnalogInput.Clock.MinFrequency + 1d) / this.SamplingRate ) );
                            desiredClockFrequency = this.DecimationRatio * this.SamplingRate;
                            this.ChannelBufferSize = this.DecimationRatio * this.SampleSize;
                        }
                    }

                    this.SignalBufferSize = Math.Max( this.SampleSize, this.SignalBufferSize );

                    // the signal memory length must be longer than the buffer size.
                    this.SignalMemoryLength = Math.Max( this.SignalMemoryLength, this.SignalBufferSize );
                }
                else
                {
                    if ( this.SamplingRate < this.AnalogInput.Clock.MinFrequency )
                    {

                        // if sampling rate lower than the minimum clock frequency then we need to decimate.
                        this.DecimationRatio = ( int ) Math.Round( Math.Ceiling( (this.AnalogInput.Clock.MinFrequency + 1d) / this.SamplingRate ) );
                        desiredClockFrequency = this.DecimationRatio * this.SamplingRate;
                        this.ChannelBufferSize = this.DecimationRatio * this.SampleSize;
                    }

                    // for scope we force the signal buffer size to equal the sample size.
                    this.SignalBufferSize = this.SampleSize;

                    // the signal memory length is the same as the buffer length.
                    this.SignalMemoryLength = this.SignalBufferSize;
                }

                // turn off the configuration required sentinel.
                this.AnalogInputConfigurationRequired = false;

                // update the sample buffer size based on the number of channels
                this.SampleBufferSize = this.ChannelBufferSize * this.AnalogInput.ChannelList.Count;

                // reallocate other value based on these calculations
                this.MovingAverageLength = Math.Min( this.MovingAverageLength, this.SignalBufferSize );

                // set the clock frequency
                this.ClockRate = desiredClockFrequency;

                // configure the analog input
                this.AnalogInput.Config();

                // adjust the actual sampling rate based on the A/D clock frequency.
                this.SamplingRate = this.ClockRate / this.DecimationRatio;

                // free then allocate buffers
                this.AnalogInput.AllocateBuffers( this.BuffersCount, this.SampleBufferSize );

                // Me._bufferPeriod = Me.samplingPeriod * Me.ChannelBufferSize

                // allocate buffers.
                this.AnalogInput.AllocateBuffers( this.BuffersCount, this.SampleBufferSize );

                // check if the configuration sentinel was turned on indicating the some values are inconsistent with the user required values
                if ( this.AnalogInputConfigurationRequired )
                {
                    _ = outcomeBuilder.AppendLine( "Analog input configuration altered user set value and needs to be repeated." );
                }

                this.StatusMessage = outcomeBuilder.Length > 0
                    ? $"Analog Input configuration failed;. Details: {outcomeBuilder}" 
                    : $"Analog Input Configured;. ";
            }
            catch ( Exception ex )
            {
                _ = outcomeBuilder.AppendLine( $"Exception occurred;. {ex}" );
            }
            finally
            {
                this.ResumeConfig();
            }

            if ( outcomeBuilder.Length > 0 )
            {
                details = outcomeBuilder.ToString();
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary> Configures the analog input. </summary>
        /// <param name="details"> [in,out] A non-empty validation outcome string containing the reason
        /// configuration failed. </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public bool TryConfigureChart( ref string details )
        {
            var outcomeBuilder = new System.Text.StringBuilder();
            if ( this.ConfigureSuspended )
            {
                _ = outcomeBuilder.Append( "Configuration suspended" );
            }
            else if ( this.Chart.Signals.Count == 0 )
            {

                // configure only if we have defined channels.
                _ = outcomeBuilder.Append( "The chart has no signal defined." );
            }
            else
            {
                // turn off the sentinel
                this._ChartConfigurationRequired = false;
                try
                {
                    this._RefreshTimespan = TimeSpan.FromSeconds( 1d / this.RefreshRate );

                    // stop the scope display.
                    _ = this.Chart.DisableRendering();

                    // suspend configuration
                    this.SuspendConfig();

                    // set the chart duration to display one whole signal.
                    double chartDuration = this.SignalBufferSize / this.SamplingRate;

                    // set chart for seconds or milliseconds
                    if ( chartDuration - 1d < 0.1d )
                    {
                        this.TimeAxisDuration = chartDuration * 1000d;
                        this.TimeAxisPeriod = this.SamplingPeriod * 1000d;
                        this.Chart.XDataUnit = "Milliseconds";
                    }
                    else
                    {
                        this.TimeAxisDuration = chartDuration;
                        this.TimeAxisPeriod = this.SamplingPeriod;
                        this.Chart.XDataUnit = "Seconds";
                    }

                    // allocate signal size for the chart.
                    this.Chart.SignalBufferLength = this.SignalBufferSize;

                    // setup time axis title
                    this.Chart.XDataName = "Time";

                    // setup range of x-axis
                    this.ScrollTimeFrame( 0UL );

                    // check if the configuration sentinel was turned on indicating the some values are inconsistent with the user required values
                    if ( this.ChartConfigurationRequired )
                    {
                        _ = outcomeBuilder.AppendLine( "Chart configuration altered user set value and needs to be repeated." );
                    }

                    this.StatusMessage = outcomeBuilder.Length > 0
                        ? $"Chart configuration failed;. Details: {outcomeBuilder}"
                        : "Chart Configured;. "; 
                }
                catch ( Exception ex )
                {
                    _ = outcomeBuilder.AppendLine( $"Exception occurred;. {ex}" );
                }
                finally
                {
                    this.ResumeConfig();

                    // resume the display.
                    _ = this.Chart.EnableRendering();

                    // notify chart that data is available
                    this.Chart.SignalUpdate();
                }
            }

            if ( outcomeBuilder.Length > 0 )
            {
                details = outcomeBuilder.ToString();
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary> Shows or hides the configuration panel. </summary>
        /// <value> The configuration panel visible. </value>
        [Description( "Shows or hides the configuration panel" )]
        [Category( "Appearance" )]
        [DefaultValue( true )]
        public bool ConfigurationPanelVisible
        {
            get => this._Tabs.Visible;

            set {
                this._Tabs.Visible = value;
                _ = this._ConfigureToolStripMenuItem.SilentCheckedSetter( value );
            }
        }

        /// <summary> Gets or set the status of configuration suspension. </summary>
        /// <value> A Queue of configure suspensions. </value>
        private Queue<bool> ConfigureSuspensionQueue { get; set; }

        /// <summary> Returns true if configuration is suspended. </summary>
        /// <value> The configure suspended. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public bool ConfigureSuspended => this.ConfigureSuspensionQueue.Count > 0;

        /// <summary> Resumes configuration. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        public void ResumeConfig()
        {
            _ = this.ConfigureSuspended
                ? this.ConfigureSuspensionQueue.Dequeue()
                : throw new InvalidOperationException( "Attempted to resume configuration that was not suspended." );
        }

        /// <summary> Suspends configuration to allow setting a few properties without reconfiguring. </summary>
        public void SuspendConfig()
        {
            this.ConfigureSuspensionQueue.Enqueue( true );
        }

        /// <summary> Resumes configuration without an exception. </summary>
        /// <returns> <c>True</c> if resumed, <c>False</c> if not suspended. </returns>
        public bool TryResumeConfig()
        {
            if ( this.ConfigureSuspended )
            {
                this.ResumeConfig();
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region " GUI "

        /// <summary> Shows or hides the action menu. </summary>
        /// <value> The action menu visible. </value>
        [Description( "Shows or hides the actions menu" )]
        [Category( "Appearance" )]
        [DefaultValue( true )]
        public bool ActionMenuVisible
        {
            get => this._ActionsToolStripDropDownButton.Visible;

            set => this._ActionsToolStripDropDownButton.Visible = value;
        }

        /// <summary> Enunciates the specified control. </summary>
        /// <param name="control"> The control. </param>
        /// <param name="format">  The format. </param>
        /// <param name="args">    The arguments. </param>
        private void Enunciate( Control control, string format, params object[] args )
        {
            this._ErrorProvider.SetError( control, string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
        }

        #endregion

        #region " DATA METHODS AND PROPERTIES  "

        /// <summary> Gets or sets the pointer to the most recent (current) data point acquired.
        /// For a scope display this is the last data point of the signal.
        /// For a strip chart this is the last data point that was added to the signal. </summary>
        private int _LoopPointer;

        /// <summary> Allocates storage for voltages. </summary>
        /// <param name="channels">         Number of channels. </param>
        /// <param name="pointsPerChannel"> Number of samples per channel. </param>
        /// <returns> The Voltages </returns>
        public double[][] AllocateVoltages( int channels, int pointsPerChannel )
        {
            this._Voltages = new double[channels][];
            for ( int i = 0, loopTo = channels - 1; i <= loopTo; i++ )
                this._Voltages[i] = new double[pointsPerChannel];
            return this._Voltages;
        }

        /// <summary> The voltages. </summary>
        private double[][] _Voltages;

        /// <summary> Returns a read only reference to the voltage array. </summary>
        /// <returns> The Voltages </returns>
        public double[][] Voltages()
        {
            return this._Voltages;
        }

        /// <summary> Returns a read only reference to the voltage array. </summary>
        /// <param name="channelIndex"> Specifies the channel index. </param>
        /// <returns> The Voltages </returns>
        public double[] Voltages( int channelIndex )
        {
            return this._Voltages[channelIndex];
        }

        /// <summary> Returns the current voltage of the specifies channel. </summary>
        /// <param name="channelIndex"> Specifies the channel index. </param>
        /// <returns> The current voltage of the specifies channel </returns>
        public double CurrentVoltage( int channelIndex )
        {
            return this._LoopPointer >= 0 ? this._Voltages[channelIndex][this._LoopPointer] : 0d;
        }

        /// <summary> Return a voltages array from the specified voltages. </summary>
        /// <param name="channelIndex"> . </param>
        /// <param name="pointCount">   Specifies the number of data points to return. </param>
        /// <returns> Voltages array from the specified voltages. </returns>
        public double[] DataEpoch( int channelIndex, int pointCount )
        {
            return AnalogInputSubsystem.DataEpoch( this.Voltages( channelIndex ), this._LoopPointer, pointCount, this.SampleCounter );
        }

        /// <summary> Return a voltages array from the specified voltages. </summary>
        /// <param name="dataArray">  . </param>
        /// <param name="pointCount"> Specifies the number of data points to return. </param>
        /// <returns> Voltages array from the specified voltages. </returns>
        public double[] DataEpoch( double[] dataArray, int pointCount )
        {
            if ( pointCount <= 0 || dataArray is null || dataArray.Length == 0 )
            {
                var nullData = Array.Empty<double>();
                return nullData;
            }

            return AnalogInputSubsystem.DataEpoch( dataArray, this._LoopPointer, pointCount, this.SampleCounter );
        }

        #endregion

        #region " PROCESS BUFFER METHODS "

        /// <summary>
        /// Gets or sets a queue of <see cref="OpenLayers.Base.OlBuffer">Open Layers buffers</see>.
        /// </summary>
        private Queue<OpenLayers.Base.OlBuffer> _BufferQueue;

        /// <summary> Calculate and save the averages. </summary>
        private void CalculateAverages()
        {
            for ( int channelIndex = 0, loopTo = this.AnalogInput.ChannelsCount - 1; channelIndex <= loopTo; channelIndex++ )

                // calculate the average of the selected data epoch.
                this._LastAverages[channelIndex] = AnalogInputSubsystem.Average( this.DataEpoch( channelIndex, this.MovingAverageLength ) );
        }

        /// <summary> The chart volts locker. </summary>
        private readonly object _ChartVoltsLocker = new();

        /// <summary> Charts the saved voltages. </summary>
        private void ChartVoltageTimeSeries()
        {
            if ( this.ChartingEnabled && this.SampleCounter > 0L )
            {
                try
                {
                    _ = this.Chart.DisableRendering();
                    if ( this.ChartMode == ChartModeId.Scope )
                    {
                        int signalIndex = 0;
                        foreach ( OpenLayers.Signals.MemorySignal signal in this.Chart.Signals )
                        {
                            signal.Data = this.Voltages( this._SignalLogicalChannelNumbers[signalIndex] );
                            signalIndex += 1;
                        }
                    }
                    else if ( this.ChartMode == ChartModeId.StripChart )
                    {
                        for ( int signalIndex = 0, loopTo = this.Chart.Signals.Count - 1; signalIndex <= loopTo; signalIndex++ )
                            this.Chart.Signals[signalIndex].Data = AnalogInputSubsystem.DataEpoch( this.Voltages( this._SignalLogicalChannelNumbers[signalIndex] ), this._LoopPointer, this.SignalBufferSize, this.SampleCounter );

                        // This slows down operation big time.  So it is not done. 
                        // update the time frame
                        // Me.scrollTimeFrame()

                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    _ = this.Chart.EnableRendering();
                    this.Chart.SignalUpdate();

                    // start count-down to next display time.
                    this._RefreshStopwatch.Restart();
                }
            }
        }

        /// <summary> Gets the relative number of samples collected per sample selected. </summary>
        /// <value> The decimation ratio. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public int DecimationRatio { get; private set; }

        /// <summary> Display values for the current channel. </summary>
        private void DisplayVoltages()
        {
            if ( this.DisplayAverageEnabled && this.CalculateEnabled )
            {
                this._AverageVoltageToolStripLabel.Text = string.Format( System.Globalization.CultureInfo.CurrentCulture, this._VoltsReadingsStringFormat, this._LastAverages[this.SelectedChannelIndex] );
            }

            if ( this.DisplayVoltageEnabled )
            {
                this._CurrentVoltageToolStripLabel.Text = string.Format( System.Globalization.CultureInfo.CurrentCulture, this._VoltsReadingsStringFormat, this._LastVoltages[this.SelectedChannelIndex] );
            }
        }

        /// <summary> Processes the buffer queue. </summary>
        /// <remarks> Test results for scope display: 5 KHz, 1000 Samples, 5 buffers, 10 points average.
        /// Storage   CALC           Display        Chart         Total 0.1388ms  off: 0.0156ms  off:
        /// 0.0134ms  on:  0.045ms  0.2128ms 0.1449ms  off: 0.0162ms  v:   0.8381ms; on:  0.0411ms
        /// 1.0403ms 0.1435ms  on:  0.0185ms  v:   0.0316ms; on:  0.0463ms 0.2399ms 0.1519ms  on:
        /// 0.0185ms  v,a: 1.5622ms; on:  0.0441ms 1.7767ms 0.1385ms  on:  0.0187ms  v,a: 1.4083ms; off:
        /// 0.0159ms 1.5814ms Strip Chart 200 Hz, 10 samples. Storage   CALC           Display
        /// Chart         Total 0.1209ms  off:0.0165ms   off: 0.0137ms  on:  0.0841ms 0.2352ms 0.0385ms
        /// 0.0154ms        0.0131ms       5.7326ms 5.7996ms. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ProcessBufferQueue()
        {
            if ( this._BufferQueue.Count <= 0 )
            {
                // this allows simple repeating without having to worry about 
                // an empty queue
                return;
            }

            var buffer = this._BufferQueue.Dequeue();
            try
            {
                if ( buffer.ValidSamples >= this.SampleBufferSize )
                {

                    // save buffered data
                    this.CopyBuffer( buffer );
                    if ( this.CalculatePeriodDownCounts <= 0L )
                    {
                        // reset the count
                        this.CalculatePeriodDownCounts = this.CalculatePeriodCount;
                        // calculate averages as necessary
                        this.CalculateAverages();
                    }

                    if ( this._UpdatePeriodDownCounter <= 0L )
                    {
                        this._UpdatePeriodDownCounter = this.UpdatePeriodCount;
                        this.OnUpdatePeriodDone( new EventArgs() );
                    }
                }
                else
                {
                    Debug.WriteLine( "Missing points=" + buffer.ValidSamples );
                }
            }
            catch ( OpenLayers.Base.OlException ex )
            {
                _ = ExceptionExtensionMethods.AddExceptionData( ex );
                this.OnDisplayDriverError( $"Failed processing buffer;. Failure occurred on sample {this.SampleCounter} loop pointer {this._LoopPointer};. {ex}" ) ;
            }
            catch ( Exception ex )
            {
                _ = ExceptionExtensionMethods.AddExceptionData( ex );
                this.OnDisplayDriverError( $"Failed processing buffer;. Failure occurred on sample {this.SampleCounter} loop pointer {this._LoopPointer};. {ex}" );
            }
            finally
            {

                // release the buffer to the queue
                if ( !this.FiniteBuffering )
                {
                    // if infinite buffering, re-queue the buffer so that it can be filled again.
                    this.AnalogInput.BufferQueue.QueueBuffer( buffer );
                }
            }

            if ( this._BufferQueue.Count <= 0 )
            {

                // if done processing, check if time to update the display
                if ( this._RefreshStopwatch.Elapsed > this._RefreshTimespan )
                {
                    lock ( this._ChartVoltsLocker )
                    {

                        // display 
                        this.DisplayVoltages();

                        // chart if required.
                        this.ChartVoltageTimeSeries();
                    }
                }
            }
            else
            {
                // repeat until the queue is processed.
                this.ProcessBufferQueue();
            }
        }

        /// <summary> Copy buffer data to the voltages array. </summary>
        /// <param name="buffer"> . </param>
        private void CopyBuffer( OpenLayers.Base.OlBuffer buffer )
        {
            int channelIndex;
            var loopPointer = default( int );
            if ( this.ChartMode == ChartModeId.Scope && this.DecimationRatio == 1 )
            {

                // if we are saving the entire buffer size, just go ahead.

                // set the loop pointer to the last data point in the channel buffer.
                loopPointer = this.ChannelBufferSize - 1;

                // if saving sub buffers, decimate starting with the last loop pointer or zero for scope.
                var loopTo = this.AnalogInput.ChannelsCount - 1;
                for ( channelIndex = 0; channelIndex <= loopTo; channelIndex++ )
                {

                    // update the voltage buffer.
                    this._Voltages[channelIndex] = buffer.GetDataAsVolts( this.AnalogInput.ChannelList[channelIndex] );

                    // save the last voltage
                    this._LastVoltages[channelIndex] = this._Voltages[channelIndex][loopPointer];
                }
            }
            else
            {
                int initialLoopPoint = -1;
                if ( this.ChartMode == ChartModeId.StripChart )
                {
                    initialLoopPoint = this._LoopPointer;
                }

                // if saving sub buffers, decimate starting with the last loop pointer or zero for scope.
                var loopTo1 = this.AnalogInput.ChannelsCount - 1;
                for ( channelIndex = 0; channelIndex <= loopTo1; channelIndex++ )
                {

                    // get the voltages for this channel
                    var channelVolts = buffer.GetDataAsVolts( this.AnalogInput.ChannelList[channelIndex] );
                    loopPointer = initialLoopPoint;
                    var voltage = default( double );
                    for ( int i = 0, loopTo2 = channelVolts.Length - 1; this.DecimationRatio >= 0 ? i <= loopTo2 : i >= loopTo2; i += this.DecimationRatio )
                    {
                        voltage = channelVolts[i];
                        loopPointer += 1;
                        if ( loopPointer >= this.SignalMemoryLength )
                        {
                            loopPointer = 0;
                        }

                        this._Voltages[channelIndex][loopPointer] = voltage;
                    }

                    // save the last voltage
                    this._LastVoltages[channelIndex] = voltage;
                }
            }

            // update loop pointer
            // Me._loopPointer += Me._channelBufferSize
            this._LoopPointer = loopPointer;

            // update the sample counters
            this.SampleCounter += this.SampleSize;
            if ( this.CalculateEnabled )
            {
                this.CalculatePeriodDownCounts -= this.SampleSize;
            }

            if ( this.UpdateEventEnabled )
            {
                this._UpdatePeriodDownCounter -= this.SampleSize;
            }
        }

        #endregion

        #region " PRIVATE GUI METHODS AND PROPERTIES "

        /// <summary> Updates the enabled status of the add signal control. </summary>
        private void EnableAddSignalControl()
        {
            this._AddSignalButton.Enabled = this.IsOpen && this._SignalChannelComboBox.SelectedIndex >= 0 && !this.ContinuousOperationActive;
        }

        /// <summary> Updates the enabled status of the channels controls. </summary>
        private void EnableChannelsControls()
        {
            this._AddChannelButton.Enabled = this.IsOpen;
            this._ChannelsGroupBox.Enabled = this.IsOpen;
            this._SamplingGroupBox.Enabled = this.IsOpen;
            this._RemoveChannelButton.Enabled = this.SelectedChannel is object && !this.ContinuousOperationActive;
            this._BoardGroupBox.Enabled = this.IsOpen;
            this._ProcessingGroupBox.Enabled = true;
            this._SmartBufferingCheckBox.Enabled = this.ChartMode == ChartModeId.StripChart;
        }

        /// <summary> Updates the enabled status of the chart controls. </summary>
        private void EnableChartControls()
        {
            this._ChartEnableGroupBox.Enabled = true;
            this._ChartTypeGroupBox.Enabled = !this.ContinuousOperationActive;
            this._BandModeGroupBox.Enabled = true;
            this._ChartColorsGroupBox.Enabled = true;
            this.StartStopEnabled = this.IsOpen && this.AnalogInput.HasChannels;
            this._ChartTitlesGroupBox.Enabled = true;
        }

        /// <summary> Updates the enabled status of the signals controls. </summary>
        private void EnableSignalsControls()
        {
            this.EnableAddSignalControl();
            this._SignalChannelGroupBox.Enabled = this.IsOpen && this.AnalogInput.HasChannels && !this.ContinuousOperationActive;
            this._SignalSamplingGroupBox.Enabled = !this.ContinuousOperationActive;
            this._UpdateSignalButton.Enabled = this.SelectedSignal is object;
            this._ColorSignalButton.Enabled = this.SelectedSignal is object;
            this._SignalsListView.Enabled = true;
            this._RemoveSignalButton.Enabled = this.SelectedSignal is object && !this.ContinuousOperationActive;
            this._SignalBufferSizeNumeric.Enabled = this.ChartMode == ChartModeId.StripChart;
            this._SignalMemoryLengthNumeric.Enabled = this.ChartMode == ChartModeId.StripChart;
        }

        #endregion

        #region " EVENTS AND ON EVENTS "

        #region " ACQUISITION STATED "

        /// <summary>Defines the event for announcing that acquisition started.</summary>
        public event EventHandler<EventArgs> AcquisitionStarted;

        /// <summary> Raises the Acquisition Started event. </summary>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnAcquisitionStarted( EventArgs e )
        {
            AcquisitionStarted?.Invoke( this, e );
        }

        /// <summary> Starts data acquisition and display. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <exception cref="InvalidOperationException">  Failed to start. </exception>
        private void StartContinuousInput()
        {
            if ( !OpenLayers.Base.DeviceMgr.Get().HardwareAvailable() )
            {
                // Me.onHardwareDisconnected(New Global.OpenLayers.Base.GeneralEventArgs)
                var eventHandler = Disconnected;
                if ( eventHandler is object )
                    eventHandler.Invoke( this, new EventArgs() );
                throw new InvalidOperationException( "Hardware not available." );
            }

            if ( this.ChartMode == ChartModeId.Scope )
            {
            }
            else if ( this.ChartMode == ChartModeId.StripChart )
            {
                // Set time of start
                this.ScrollTimeFrame( 0UL );
            }
            else
            {
                throw new InvalidOperationException( "Strip chart mode not set." );
            }

            // instantiate the buffer queue
            this._BufferQueue = new Queue<OpenLayers.Base.OlBuffer>();

            // clear the voltage storage
            _ = this.AllocateVoltages( this.AnalogInput.ChannelList.Count, this.SignalMemoryLength );

            // initialize sample count
            this.SampleCounter = 0L;

            // initialize the update period counter.
            this._UpdatePeriodDownCounter = this.UpdatePeriodCount;

            // initialize the calculate period counter.
            this.CalculatePeriodDownCounts = this.CalculatePeriodCount;
            if ( this.ChartMode == ChartModeId.Scope )
            {
                this._LoopPointer = 0;
            }
            else if ( this.ChartMode == ChartModeId.StripChart )
            {
                this._LoopPointer = -1;
            }

            // Disable user interface until the chart is stopped.
            this.EnableChartControls();
            this.EnableSignalsControls();
            this.EnableChannelsControls();
            this._DeviceNameChooser.Enabled = !this.IsOpen;

            // enable selection of signals.
            this._SignalsListView.Enabled = true;

            // select the channel
            if ( this.SelectedSignal is null )
            {
                _ = this.SelectSignal( 0 );
            }

            _ = this.AnalogInput.SelectNamedChannel( this.SelectedSignal.Name );
            _ = this.SelectChannel( this.AnalogInput.SelectedChannelLogicalNumber );

            // Set the status of the start/stop control making sure we can stop
            this.Started = true;

            // clear the displays.
            this._CurrentVoltageToolStripLabel.Text = string.Empty;
            this._AverageVoltageToolStripLabel.Text = string.Empty;

            // start data acquisition
            this.AnalogInput.Start();
            this.StatusMessage = this.ChartingEnabled
                ? "Charting...;. "
                : "Running...;. ";

            // notify display that new data is available
            this.Chart.SignalUpdate();

            // start refresh count-down
            this._RefreshStopwatch.Restart();

            // raise the acquisition started event
            this.OnAcquisitionStarted( EventArgs.Empty );
            if ( !this.Started )
            {
                throw new InvalidOperationException( "Failed to start" );
            }
        }

        /// <summary> Tries to start continuous input. </summary>
        private void TryStartContinuousInput()
        {
            try
            {
                this.StartContinuousInput();
            }
            catch ( OpenLayers.Base.OlException ex )
            {
                this.StatusMessage = $"Open Layers Driver exception occurred starting;. {ex}";
                this.OnHandlerException( ex );
            }
            catch ( InvalidOperationException ex )
            {
                this.StatusMessage = $"Invalid operation occurred starting;. {ex}";
                this.OnHandlerException( ex );
            }
            catch ( TimeoutException ex )
            {
                this.StatusMessage = $"Timeout exception occurred starting;. {ex}";
                this.OnHandlerException( ex );
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region " ACQUISITION STOPPED "

        /// <summary>Defines the event for announcing that acquisition stopped.</summary>
        public event EventHandler<EventArgs> AcquisitionStopped;

        /// <summary> Raises the Acquisition Stopped event. </summary>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnAcquisitionStopped( EventArgs e )
        {
            AcquisitionStopped?.Invoke( this, e );

            // force abort
            this.TryStopContinuousInput( this.StopTimeout, true );
        }

        /// <summary> Raises the acquisition Stopped message. </summary>
        private void OnDisplayAcquisitionStopped()
        {
            this.OnAcquisitionStopped( EventArgs.Empty );
        }

        /// <summary> Stops or aborts continuous input. This also stops charting. </summary>
        /// <param name="timeout"> The timeout. </param>
        /// <param name="abort">   if set to <c>True</c> [abort]. </param>
        /// <exception cref="TimeoutException"> Abort timeout--system still running after abort. </exception>
        private void StopContinuousInput( TimeSpan timeout, bool abort )
        {
            try
            {
                if ( this.ContinuousOperationActive )
                {
                    // stop data acquisition if running.
                    this.StatusMessage = "Aborting data acquisition;. ";
                    if ( abort )
                    {
                        this.AnalogInput.Abort( timeout );
                    }
                    else
                    {
                        this.AnalogInput.Stop( timeout );
                    }
                }
            }
            catch
            {
                this.StatusMessage = "Failed aborting data acquisition;. ";
                throw;
            }
            finally
            {

                // make sure we can start
                // Set the status of the start/stop control making sure we can start
                this.Started = this.ContinuousOperationActive;

                // enable GUI
                this.EnableChartControls();
                this.EnableSignalsControls();
                this.EnableChannelsControls();

                // update the status of the open check box.
                _ = this._OpenCloseCheckBox.SilentCheckedSetter( this.IsOpen );
                this._DeviceNameChooser.Enabled = !this.IsOpen;
            }
        }

        /// <summary> Tries the stop continuous input. </summary>
        /// <param name="timeout"> The timeout. </param>
        /// <param name="abort">   if set to <c>True</c> [abort]. </param>
        private void TryStopContinuousInput( TimeSpan timeout, bool abort )
        {
            try
            {
                this.StopContinuousInput( timeout, abort );
            }
            catch ( OpenLayers.Base.OlException ex )
            {
                this.StatusMessage = $"Open Layers Driver exception occurred stopping;. {ex}";
            }
            catch ( TimeoutException ex )
            {
                this.StatusMessage = $"Timeout exception occurred stopping;. {ex}";
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region " BOARDS LOCATED "

        /// <summary>Defines the event handler for locating the boards.</summary>
        public event EventHandler<EventArgs> BoardsLocated;

        /// <summary> Raises the Boards Located event. </summary>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnBoardsLocated( EventArgs e )
        {
            this.StatusMessage = "Data Acquisition board(s) located;. ";
            BoardsLocated?.Invoke( this, e );
        }

        #endregion

        #region " BUFFER DONE "

        /// <summary>Defines the buffer done event.</summary>
        public event EventHandler<EventArgs> BufferDone;

        #endregion

        #region " DISCONNECTED "

        /// <summary>Occurs when the subsystem detects that the hardware is no longer
        ///   connected.
        /// </summary>
        public event EventHandler<EventArgs> Disconnected;

        #endregion

        #region " UPDATE PERIOD DONE "

        /// <summary>Notifies of new data.</summary>
        public event EventHandler<EventArgs> UpdatePeriodDone;

        /// <summary> Raises the update period event. </summary>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnUpdatePeriodDone( EventArgs e )
        {
            UpdatePeriodDone?.Invoke( this, e );
        }

        #endregion

        #region " DRIVER ERROR "

        /// <summary>Occurs when the driver had a general error.
        /// </summary>
        public event EventHandler<System.Threading.ThreadExceptionEventArgs> DriverError;

        /// <summary> Raises the driver general error event. </summary>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnDriverError( System.Threading.ThreadExceptionEventArgs e )
        {

            // force stop without raising exceptions.
            this.TryStopContinuousInput( this.StopTimeout, true );

            // invoke the device error event
            DriverError?.Invoke( this, e );
        }

        /// <summary> Displays and raises the driver error message. </summary>
        private void OnDisplayDriverError( string details )
        {
            this.OnDriverError( new System.Threading.ThreadExceptionEventArgs( new InvalidOperationException( details ) ) );
        }

        #endregion

        #endregion

        #region " OPEN LAYERS HANDLERS "

        /// <summary> Updates the acquired data and displays signals. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Buffer done event information. </param>
        private void HandleBufferDone( object sender, OpenLayers.Base.BufferDoneEventArgs e )
        {
            if ( this.InvokeRequired )
            {
                _ = this.Invoke( new OpenLayers.Base.BufferDoneHandler( this.HandleBufferDone ), new object[] { sender, e } );
            }
            else
            {
                this.OnBufferDone( e );
            }
        }

        /// <summary> The process buffer queue locker. </summary>
        private readonly object _ProcessBufferQueueLocker = new();

        /// <summary> Handles the buffer done event from the board driver. </summary>
        /// <remarks> Test results for scope display: 5 KHz, 1000 Samples, 5 buffers, 10 points average.
        /// Storage   CALC           Display        Chart         Total 0.1388ms  off: 0.0156ms  off:
        /// 0.0134ms  on:  0.045ms  0.2128ms 0.1449ms  off: 0.0162ms  v:   0.8381ms; on:  0.0411ms
        /// 1.0403ms 0.1435ms  on:  0.0185ms  v:   0.0316ms; on:  0.0463ms 0.2399ms 0.1519ms  on:
        /// 0.0185ms  v,a: 1.5622ms; on:  0.0441ms 1.7767ms 0.1385ms  on:  0.0187ms  v,a: 1.4083ms; off:
        /// 0.0159ms 1.5814ms. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected virtual void OnBufferDone( OpenLayers.Base.BufferDoneEventArgs e )
        {
            if ( e is object && this._BufferQueue is object )
            {
                try
                {
                    if ( e.OlBuffer is null )
                    {
                        this.StatusMessage = $"Unexpected null reference to Open Layer buffer occurred @{System.Reflection.MethodBase.GetCurrentMethod().Name };. ";
                    }
                    else
                    {
                        // Add the buffer to the queue separating the buffer that is queued from the
                        // one that is processed.
                        this._BufferQueue.Enqueue( e.OlBuffer );
                        lock ( this._ProcessBufferQueueLocker )
                            // process the buffer queue
                            this.ProcessBufferQueue();
                    }
                }
                catch ( Exception ex )
                {
                    this.StatusMessage = $"Exception occurred handling buffer done;. {ex}";
                }
            }
        }

        /// <summary> Handles the hardware disconnected event. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      General event information. </param>
        private void HandleDeviceRemoved( object sender, OpenLayers.Base.GeneralEventArgs e )
        {
            if ( this.InvokeRequired )
            {
                _ = this.Invoke( new OpenLayers.Base.DeviceRemovedHandler( this.HandleDeviceRemoved ), new object[] { sender, e } );
            }
            else
            {
                this.OnHardwareDisconnected( e );
            }
        }

        /// <summary> Close and raise the hardware disconnected event. </summary>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected virtual void OnHardwareDisconnected( OpenLayers.Base.GeneralEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.BoardDisconnected = true;
                activity = $"driver disconnected on subsystem {e.Subsystem} at {e.DateTime:T}";
                this.DisconnectAnalogInput();
                if ( e is object )
                {
                    this.StatusMessage = $"{activity};. ";
                }
            }
            catch ( InvalidOperationException )
            {
            }
            // ignore operation failure as this is expected at this point.
            catch ( Exception ex )
            {
                _ = ExceptionExtensionMethods.AddExceptionData( ex );
                this.StatusMessage = $"Exception occurred disconnecting hardware";
                this.OnHandlerException( ex );
            }
            finally
            {
                var eventHandler = Disconnected;
                if ( eventHandler is object )
                    eventHandler.Invoke( this, e );
            }
        }

        /// <summary> Reports driver runtime errors. </summary>
        /// <remarks> The Driver runtime error occurs when the device driver detects one of the following
        /// error conditions during runtime: FifoOverflow, FifoUnderflow, DeviceOverClocked, TriggerError,
        /// or DeviceError. See OpenLayerError for more information. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Driver run time error event information. </param>
        private void HandleDriverRuntimeError( object sender, OpenLayers.Base.DriverRunTimeErrorEventArgs e )
        {
            if ( this.InvokeRequired )
            {
                _ = this.Invoke( new OpenLayers.Base.DriverRunTimeErrorEventHandler( this.HandleDriverRuntimeError ), new object[] { sender, e } );
            }
            else
            {
                this.OnDriverRuntimeError( e );
            }
        }

        /// <summary> Displays a message and raises the driver error event. </summary>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnDriverRuntimeError( OpenLayers.Base.DriverRunTimeErrorEventArgs e )
        {
            if ( e is object )
            {
                this.OnDisplayDriverError( $"Driver Runtime Failure #{e.ErrorCode}: {e.Message}. occurred on subsystem {e.Subsystem} at {e.DateTime:T}" );
                switch ( e.ErrorCode )
                {
                    case OpenLayers.Base.ErrorCode.AccessDenied:
                    case OpenLayers.Base.ErrorCode.CannotOpenDriver:
                    case OpenLayers.Base.ErrorCode.DeviceError:
                    case OpenLayers.Base.ErrorCode.GeneralFailure:
                        {
                            this.TryStopContinuousInput( this.StopTimeout, true );
                            break;
                        }
                    // do nothing on bad data?!
                    case OpenLayers.Base.ErrorCode.FifoOverflow:
                    case OpenLayers.Base.ErrorCode.FifoUnderflow:
                        {
                            break;
                        }

                    default:
                        {
                            break;
                        }
                        // do nothing
                }
            }
        }

        /// <summary> Handles General Failure events.  Displays a message. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      General event information. </param>
        private void HandleGeneralFailure( object sender, OpenLayers.Base.GeneralEventArgs e )
        {
            if ( this.InvokeRequired )
            {
                _ = this.Invoke( new OpenLayers.Base.GeneralFailureHandler( this.HandleGeneralFailure ), new object[] { sender, e } );
            }
            else
            {
                this.OnGeneralFailure( e );
            }
        } // HandleOverrunError

        /// <summary> Displays a message and raises the driver error event. </summary>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnGeneralFailure( OpenLayers.Base.GeneralEventArgs e )
        {
            if ( e is object )
            {
                this.StatusMessage = $"General Failure occurred on subsystem {e.Subsystem} at {e.DateTime:T}.";
                this.OnDisplayDriverError( this.StatusMessage );
                this.TryStopContinuousInput( this.StopTimeout, true );
            }
        }

        /// <summary> Handles the Queue Done events.  Displays a message. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      General event information. </param>
        private void HandleQueueDone( object sender, OpenLayers.Base.GeneralEventArgs e )
        {
            if ( this.InvokeRequired )
            {
                _ = this.Invoke( new OpenLayers.Base.QueueDoneHandler( this.HandleQueueDone ), new object[] { sender, e } );
            }
            else
            {
                this.OnQueueDone( e );
            }
        } // HandleQueueDone

        /// <summary> Handles the normal end of the queue. </summary>
        /// <remarks> Rev. 2.0.2773. Finite buffering Queue tested successfully with suspend and recover. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnQueueDone( OpenLayers.Base.GeneralEventArgs e )
        {

            // check if we have a finite or infinite buffering mode
            if ( this.FiniteBuffering )
            {
                if ( e is object )
                {
                    this.StatusMessage = $"Queue done on subsystem {e.Subsystem} at {e.DateTime:T}";
                    this.OnDisplayAcquisitionStopped();
                }
            }
            else if ( e is object )
            {
                // if this occurred on infinite buffering this represents a buffer overrun.
                this.OnDisplayDriverError( $"Buffer overrun Failure occurred on subsystem {this.AnalogInput} at {DateTimeOffset.Now:T}. Queue count is {this.AnalogInput.BufferQueue.QueuedCount}." );
            }
        }

        /// <summary> Handles the Queue Stopped events.  Displays a message. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      General event information. </param>
        private void HandleQueueStopped( object sender, OpenLayers.Base.GeneralEventArgs e )
        {
            if ( this.InvokeRequired )
            {
                _ = this.Invoke( new OpenLayers.Base.QueueStoppedHandler( this.HandleQueueStopped ), new object[] { sender, e } );
            }
            else
            {
                this.OnQueueStopped( e );
            }
        }

        /// <summary> Handles a stopped queue. </summary>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnQueueStopped( OpenLayers.Base.GeneralEventArgs e )
        {
            if ( e is object )
            {
                this.StatusMessage = $"Queue stopped on subsystem {e.Subsystem} at {e.DateTime:T};. ";
                this.OnDisplayAcquisitionStopped();
            }
        }

        #endregion

        #region " BOARD CONTROLS HANDLERS "

        /// <summary> Enables the initialization button. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void DeviceNameChooser_DeviceSelected( object sender, EventArgs e )
        {
            this._OpenCloseCheckBox.Enabled = this._DeviceNameChooser.DeviceName.Length > 0;
        }

        /// <summary> Handles the CheckedChanged event of the _openCloseCheckBox control. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        private void OpenCloseCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this._OpenCloseCheckBox.Text = this._OpenCloseCheckBox.Checked ? "Cl&ose" : "&Open";
        }

        /// <summary> Opens of closes the strip chart. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void OpenCloseCheckBox_Click( object sender, EventArgs e )
        {
            if ( this._OpenCloseCheckBox.Enabled )
            {
                if ( this._OpenCloseCheckBox.Checked )
                {
                    try
                    {
                        this.Open( this._DeviceNameChooser.DeviceName );
                    }
                    catch ( OpenLayers.Base.OlException ex )
                    {
                        this.StatusMessage = $"Exception occurred opening;. {ex}";
                    }
                }
                else
                {
                    try
                    {
                        this.Close();
                    }
                    catch ( OpenLayers.Base.OlException ex )
                    {
                        this.StatusMessage = $"Exception occurred closing;. {ex}";
                    }
                }
            }
        }

        #endregion

        #region " CHART CONTROLS HANDLERS "

        /// <summary> Event handler. Called by _AbortToolStripMenuItem for click events. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void AbortToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.IsOpen )
            {
                try
                {
                    this.Abort();
                }
                catch ( OpenLayers.Base.OlException ex )
                {
                    _ = ExceptionExtensionMethods.AddExceptionData( ex );
                    this.StatusMessage = $"Exception occurred aborting;. {ex}";
                    this.OnHandlerException( ex );
                }
            }
        }

        /// <summary> Handles the Validating event of the ChartTitleTextBox control. Updates the chart
        /// title. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.ComponentModel.CancelEventArgs" /> instance
        /// containing the event data. </param>
        private void ChartTitleTextBox_Validating( object sender, CancelEventArgs e )
        {
            try
            {
                this.ChartTitle = this.ChartTitle;
            }
            catch ( OpenLayers.Base.OlException ex )
            {
                _ = ExceptionExtensionMethods.AddExceptionData( ex );
                this.StatusMessage = $"Exception occurred setting chart;. {ex}";
                this.OnHandlerException( ex );
            }
        }

        /// <summary> Handles the Validating event of the ChartFooterTextBox control. Updates the chart
        /// footer. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.ComponentModel.CancelEventArgs" /> instance
        /// containing the event data. </param>
        private void ChartFooterTextBox_Validating( object sender, CancelEventArgs e )
        {
            try
            {
                this.ChartFooter = this.ChartFooter;
            }
            catch ( OpenLayers.Base.OlException ex )
            {
                _ = ExceptionExtensionMethods.AddExceptionData( ex );
                this.StatusMessage = $"Exception occurred setting chart footer;. {ex}";
                this.OnHandlerException( ex );
            }
        }

        /// <summary> Handles the Click event of the colorAxesButton control. Selects the color. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="EventArgs" /> instance containing the event data. </param>
        private void ColorAxesButton_Click( object sender, EventArgs e )
        {
            try
            {
                using var dialog = new ColorDialog();
                dialog.Color = this.AxesColor;
                if ( dialog.ShowDialog() == DialogResult.OK )
                {
                    this.AxesColor = dialog.Color;
                }
            }
            catch ( OpenLayers.Base.OlException ex )
            {
                _ = ExceptionExtensionMethods.AddExceptionData( ex );
                this.StatusMessage = $"Exception occurred setting axis color;. {ex}";
                this.OnHandlerException( ex );
            }
        }

        /// <summary> Event handler. Called by _colorGridsButton for click events. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ColorGridsButton_Click( object sender, EventArgs e )
        {
            try
            {
                using var dialog = new ColorDialog();
                dialog.Color = this.GridColor;
                if ( dialog.ShowDialog() == DialogResult.OK )
                {
                    this.GridColor = dialog.Color;
                }
            }
            catch ( OpenLayers.Base.OlException ex )
            {
                _ = ExceptionExtensionMethods.AddExceptionData( ex );
                this.StatusMessage = $"Exception occurred setting grid color;. {ex}";
                this.OnHandlerException( ex );
            }
        }

        /// <summary> Event handler. Called by _DisplayVoltageCheckBox for checked changed events. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void DisplayVoltageCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            if ( this._DisplayVoltageCheckBox.Enabled )
            {
                this.DisplayVoltageEnabled = this._DisplayVoltageCheckBox.Checked;
            }
        }

        /// <summary> Event handler. Called by _DisplayAverageCheckBox for checked changed events. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void DisplayAverageCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            if ( this._DisplayAverageCheckBox.Enabled )
            {
                this.DisplayAverageEnabled = this._DisplayAverageCheckBox.Checked;
            }
        }

        /// <summary> Event handler. Called by _EnableChartingCheckBox for checked changed events. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void EnableChartingCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            if ( this._EnableChartingCheckBox.Enabled )
            {
                this.ChartingEnabled = this._EnableChartingCheckBox.Checked;
            }
        }

        /// <summary> Event handler. Called by _RefreshRateNumeric for value changed events. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void RefreshRateNumeric_ValueChanged( object sender, EventArgs e )
        {
            // request configuration of chart.
            this.ChartConfigurationRequired = true;
        }

        /// <summary> Selects the chart mode as scope or strip chart. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ScopeRadioButton_CheckedChanged( object sender, EventArgs e )
        {
            if ( this._ChartTypeGroupBox.Enabled && this._ScopeRadioButton.Enabled )
            {
                try
                {
                    this.ChartMode = this._ScopeRadioButton.Checked ? ChartModeId.Scope : ChartModeId.StripChart;
                    // request configuration of analog input and chart.
                    this.AnalogInputConfigurationRequired = true;
                    this.ChartConfigurationRequired = true;
                }
                catch ( OpenLayers.Base.OlException ex )
                {
                    _ = ExceptionExtensionMethods.AddExceptionData( ex );
                    this.StatusMessage = $"Exception occurred setting chart mode;. {ex}";
                    this.OnHandlerException( ex );
                }
            }
        }

        /// <summary> Change BandMode. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void SingleRadioButton_CheckedChanged( object sender, EventArgs e )
        {
            if ( this._SingleRadioButton.Enabled )
            {
                try
                {
                    _ = this.Chart.DisableRendering();
                    this.ChartBandMode = this._SingleRadioButton.Checked ? OpenLayers.Controls.BandMode.SingleBand : OpenLayers.Controls.BandMode.MultiBand;
                    _ = this.Chart.EnableRendering();
                    this.Chart.SignalUpdate();
                }
                catch ( OpenLayers.Base.OlException ex )
                {
                    _ = ExceptionExtensionMethods.AddExceptionData( ex );
                    this.StatusMessage = $"Exception occurred setting chart band mode;. {ex}";
                    this.OnHandlerException( ex );
                }
            }
        }

        /// <summary> Handles the CheckedChanged event of the _StartStopToolStripMenuItem control. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        private void StartStopToolStripMenuItem_CheckedChanged( object sender, EventArgs e )
        {
            if ( this._StartStopToolStripMenuItem.Checked )
            {
                this._StartStopToolStripMenuItem.Text = "&STOP";
                this._StartStopToolStripMenuItem.ToolTipText = "Stops data collection and chart (if enabled)";
            }
            else
            {
                this._StartStopToolStripMenuItem.Text = "&START";
                this._StartStopToolStripMenuItem.ToolTipText = "Starts data collection and chart (if enabled)";
            }

            this._StartStopToolStripMenuItem.Invalidate();
        } // singleRadioButton_CheckedChanged

        /// <summary> Starts or stops Running. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void StartStopToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this._StartStopToolStripMenuItem.Enabled )
            {
                if ( this.Started )
                {
                    try
                    {
                        this.Stop();
                    }
                    catch ( OpenLayers.Base.OlException ex )
                    {
                        _ = ExceptionExtensionMethods.AddExceptionData( ex );
                        this.StatusMessage = $"Exception occurred stopping;. {ex}";
                        this.OnHandlerException( ex );
                    }
                }
                else
                {
                    try
                    {
                        this.Start();
                    }
                    catch ( InvalidOperationException ex )
                    {
                        _ = ExceptionExtensionMethods.AddExceptionData( ex );
                        this.StatusMessage = $"Invalid Operation Exception occurred starting;. {ex}";
                        this.OnHandlerException( ex );
                    }
                    catch ( OpenLayers.Base.OlException ex )
                    {
                        _ = ExceptionExtensionMethods.AddExceptionData( ex );
                        this.StatusMessage = $"Driver Exception occurred starting;. {ex}";
                        this.OnHandlerException( ex );
                    }
                }
            }
        }

        #endregion

        #region " GLOBAL CONTROLS HANDLERS "

        /// <summary> Event handler. Called by _ConfigureButton for click events. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ConfigureButton_Click( object sender, EventArgs e )
        {
            if ( this._ConfigureButton.Enabled )
            {
                try
                {
                    string details = string.Empty;
                    this.StartStopEnabled = this.TryConfigureThis( ref details );
                }
                catch ( OpenLayers.Base.OlException ex )
                {
                    _ = ExceptionExtensionMethods.AddExceptionData( ex );
                    this.StatusMessage = $"Exception occurred configuring analog input or chart;. {ex}";
                    this.OnHandlerException( ex );
                }
            }
        }

        /// <summary> Show or hide the configuration menu. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ConfigureToolStripMenuItem1_Checkchanged( object sender, EventArgs e )
        {
            if ( this._ConfigureToolStripMenuItem.Enabled )
            {
                if ( this.ConfigurationPanelVisible == this._ConfigureToolStripMenuItem.Checked )
                {
                    this._ConfigureToolStripMenuItem.Checked = !this._ConfigureToolStripMenuItem.Checked;
                }
            }

            this.ConfigurationPanelVisible = this._ConfigureToolStripMenuItem.Checked;
        }

        /// <summary> Event handler. Called by _ResetButton for click events. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ResetButton_Click( object sender, EventArgs e )
        {
            if ( this._ResetButton.Enabled )
            {
                try
                {
                    this.Reset();
                }
                catch ( OpenLayers.Base.OlException ex )
                {
                    _ = ExceptionExtensionMethods.AddExceptionData( ex );
                    this.StatusMessage = $"Exception occurred resetting to opened state values;. {ex}";
                    this.OnHandlerException( ex );
                }
            }
        }

        /// <summary> Restores default values. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void RestoreDefaultsButton_Click( object sender, EventArgs e )
        {
            this.RestoreDefaults();
        }

        #endregion

        #region " CHANNELS CONTROLS HANDLERS "

        /// <summary> Adds or updates a channel. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void AddChannelButton_Click( object sender, EventArgs e )
        {
            if ( this._AddChannelButton.Enabled && this._ChannelsGroupBox.Enabled && this._ChannelRangesComboBox.SelectedIndex >= 0 )
            {
                if ( this.IsOpen && int.TryParse( this._ChannelComboBox.Text, out int physicalChannelNumber ) )
                {
                    try
                    {
                        this.AddChannel( physicalChannelNumber, this._ChannelRangesComboBox.SelectedIndex, this._ChannelNameTextBox.Text );
                    }
                    catch ( OpenLayers.Base.OlException ex )
                    {
                        _ = ExceptionExtensionMethods.AddExceptionData( ex );
                        this.StatusMessage = $"Exception occurred adding channel { physicalChannelNumber};. {ex}";
                        this.OnHandlerException( ex );
                    }
                    finally
                    {
                        if ( this.AnalogInput.HasChannels )
                        {
                            if ( this.AnalogInput.PhysicalChannelExists( physicalChannelNumber ) )
                            {
                                this.UpdateChannel( physicalChannelNumber, this._ChannelRangesComboBox.SelectedIndex, this._ChannelNameTextBox.Text );
                            }
                            else
                            {
                                this.AddChannel( physicalChannelNumber, this._ChannelRangesComboBox.SelectedIndex, this._ChannelNameTextBox.Text );
                            }
                        }
                        else
                        {
                            this.AddChannel( physicalChannelNumber, this._ChannelRangesComboBox.SelectedIndex, this._ChannelNameTextBox.Text );
                        }
                    }
                }
            }
        }

        /// <summary> Selects the board input range. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void BoardInputRangesComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( this.IsOpen && this._BoardInputRangesComboBox.SelectedIndex >= 0 )
            {
                try
                {
                    this.AnalogInput.VoltageRange = this.AnalogInput.SupportedVoltageRanges[this._BoardInputRangesComboBox.SelectedIndex];
                }
                catch ( OpenLayers.Base.OlException ex )
                {
                    _ = ExceptionExtensionMethods.AddExceptionData( ex );
                    this.StatusMessage = $"Exception occurred setting voltage range;. {ex}";
                    this.OnHandlerException( ex );
                }

                decimal max;
                decimal min;
                max = Math.Min( this._SignalMinNumericUpDown.Maximum, ( decimal ) this.AnalogInput.VoltageRange.High );
                min = Math.Max( this._SignalMinNumericUpDown.Minimum, ( decimal ) this.AnalogInput.VoltageRange.Low );
                this._SignalMinNumericUpDown.Value = Math.Max( Math.Min( this._SignalMinNumericUpDown.Value, max ), min );
                this._SignalMinNumericUpDown.Maximum = ( decimal ) this.AnalogInput.VoltageRange.High;
                this._SignalMinNumericUpDown.Minimum = ( decimal ) this.AnalogInput.VoltageRange.Low;
                max = Math.Min( this._SignalMaxNumericUpDown.Maximum, ( decimal ) this.AnalogInput.VoltageRange.High );
                min = Math.Max( this._SignalMaxNumericUpDown.Minimum, ( decimal ) this.AnalogInput.VoltageRange.Low );
                this._SignalMaxNumericUpDown.Value = Math.Max( Math.Min( this._SignalMaxNumericUpDown.Value, max ), min );
                this._SignalMaxNumericUpDown.Maximum = ( decimal ) this.AnalogInput.VoltageRange.High;
                this._SignalMaxNumericUpDown.Minimum = ( decimal ) this.AnalogInput.VoltageRange.Low;

                // request configuration of analog input and chart.
                this.AnalogInputConfigurationRequired = true;
                this.ChartConfigurationRequired = true;
            }
        }

        /// <summary> Event handler. Called by _BuffersCountNumeric for value changed events. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void BuffersCountNumeric_ValueChanged( object sender, EventArgs e )
        {
            // request configuration of analog input and chart.
            this.AnalogInputConfigurationRequired = true;
            this.ChartConfigurationRequired = true;
        }

        /// <summary> Event handler. Called by _CalculateAverageCheckBox for checked changed events. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void CalculateAverageCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            if ( this._CalculateAverageCheckBox.Enabled )
            {
                // request configuration of analog input and chart.
                this.AnalogInputConfigurationRequired = true;
                this.ChartConfigurationRequired = true;
            }
        }

        /// <summary> selects a new channel or an existing channel. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ChannelComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( this._ChannelComboBox.SelectedIndex >= 0 )
            {
                short physicalChannelNumber = short.Parse( this._ChannelComboBox.Text, System.Globalization.CultureInfo.CurrentCulture );
                if ( this.IsOpen && this.AnalogInput.HasChannels )
                {
                    if ( this.AnalogInput.PhysicalChannelExists( physicalChannelNumber ) )
                    {
                        var channel = this.AnalogInput.ChannelList.SelectPhysicalChannel( physicalChannelNumber );
                        this._ChannelNameTextBox.Text = channel.Name;
                        this._ChannelRangesComboBox.SelectedIndex = this.AnalogInput.SupportedGainIndex( channel.Gain );
                        this._AddChannelButton.Text = "Update";
                    }
                    else
                    {
                        this._AddChannelButton.Text = "Add";
                        this._ChannelNameTextBox.Text = string.Format( System.Globalization.CultureInfo.CurrentCulture, "A in {0}", physicalChannelNumber );
                    }
                }
                else
                {
                    this._ChannelNameTextBox.Text = string.Format( System.Globalization.CultureInfo.CurrentCulture, "A in {0}", physicalChannelNumber );
                }
            }
        }

        /// <summary> Event handler. Called by _ChannelsListView for click events. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ChannelsListView_Click( object sender, EventArgs e )
        {
            if ( this.IsOpen && this._ChannelsListView.SelectedIndices.Count > 0 )
            {
                try
                {
                    _ = this.SelectChannel( this._ChannelsListView.SelectedIndices[0] );
                }
                catch ( OpenLayers.Base.OlException ex )
                {
                    _ = ExceptionExtensionMethods.AddExceptionData( ex );
                    this.StatusMessage = $"Exception occurred selecting channel;. {ex}";
                    this.OnHandlerException( ex );
                }
            }
        }

        /// <summary> Event handler. Called by _ContinuousBufferingCheckBox for checked changed events. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ContinuousBufferingCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            if ( this._ContinuousBufferingCheckBox.Enabled )
            {
                this.FiniteBuffering = !this._ContinuousBufferingCheckBox.Checked;
                // request configuration of analog input and chart.
                this.AnalogInputConfigurationRequired = true;
                this.ChartConfigurationRequired = true;
            }
        }

        /// <summary> Event handler. Called by _MovingAverageLengthNumeric for value changed events. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void MovingAverageLengthNumeric_ValueChanged( object sender, EventArgs e )
        {
            // request configuration of analog input and chart.
            this.AnalogInputConfigurationRequired = true;
            this.ChartConfigurationRequired = true;
        }

        /// <summary> Removes the selected channel and signal from the lists. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void RemoveChannelButton_Click( object sender, EventArgs e )
        {
            if ( this.SelectedChannel is object )
            {
                try
                {
                    this.RemoveChannel( this.SelectedChannel );
                }
                catch ( OpenLayers.Base.OlException ex )
                {
                    _ = ExceptionExtensionMethods.AddExceptionData( ex );
                    this.StatusMessage = $"Exception occurred removing the selected channel;. {ex}";
                    this.OnHandlerException( ex );
                }
            }
        }

        /// <summary> Event handler. Called by _SampleSizeNumeric for value changed events. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void SampleSizeNumeric_ValueChanged( object sender, EventArgs e )
        {
            // request configuration of analog input and chart.
            this.AnalogInputConfigurationRequired = true;
            this.ChartConfigurationRequired = true;
        }

        /// <summary> Event handler. Called by _SamplingRateNumeric for value changed events. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void SamplingRateNumeric_ValueChanged( object sender, EventArgs e )
        {
            // request configuration of analog input and chart.
            this.AnalogInputConfigurationRequired = true;
            this.ChartConfigurationRequired = true;
        }

        /// <summary> Event handler. Called by _SmartBufferingCheckBox for checked changed events. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void SmartBufferingCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            if ( this._SmartBufferingCheckBox.Enabled )
            {
                // request configuration of analog input and chart.
                this.AnalogInputConfigurationRequired = true;
                this.ChartConfigurationRequired = true;
            }
        }

        #endregion

        #region " SIGNALS CONTROLS HANDLERS "

        /// <summary> Adds a signals. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void AddSignalButton_Click( object sender, EventArgs e )
        {
            if ( this.AnalogInput is object && this._AddSignalButton.Enabled && this._SignalChannelComboBox.SelectedIndex >= 0 )
            {
                try
                {
                    this.AddSignal( this.AnalogInput.ChannelList[this._SignalChannelComboBox.SelectedIndex], ( double ) this._SignalMinNumericUpDown.Value, ( double ) this._SignalMaxNumericUpDown.Value, this._DefaultSignalColors[this.Chart.Signals.Count] );
                }
                catch ( OpenLayers.Base.OlException ex )
                {
                    _ = ExceptionExtensionMethods.AddExceptionData( ex );
                    this.StatusMessage = $"Exception occurred adding signal;. {ex}";
                    this.OnHandlerException( ex );
                }
            }
        }

        /// <summary> Update selected signal color. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ColorSignalButton_Click( object sender, EventArgs e )
        {
            if ( this.SelectedSignal is object )
            {
                try
                {
                    using var dialog = new ColorDialog();
                    dialog.Color = this.SelectedSignalColor;
                    if ( dialog.ShowDialog() == DialogResult.OK )
                    {
                        this.SelectedSignalColor = dialog.Color;
                    }
                }
                catch ( OpenLayers.Base.OlException ex )
                {
                    _ = ExceptionExtensionMethods.AddExceptionData( ex );
                    this.StatusMessage = $"Exception occurred setting signal color;. {ex}";
                    this.OnHandlerException( ex );
                }
            }
        }

        /// <summary> Event handler. Called by _RemoveSignalButton for click events. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void RemoveSignalButton_Click( object sender, EventArgs e )
        {
            try
            {
                this.RemoveSignal( this.SelectedSignal );
            }
            catch ( OpenLayers.Base.OlException ex )
            {
                _ = ExceptionExtensionMethods.AddExceptionData( ex );
                this.StatusMessage = $"Exception occurred removing signal;. {ex}";
                this.OnHandlerException( ex );
            }
        }

        /// <summary> Event handler. Called by _SignalBufferSizeNumeric for value changed events. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void SignalBufferSizeNumeric_ValueChanged( object sender, EventArgs e )
        {
            // request configuration of analog input and chart.
            this.AnalogInputConfigurationRequired = true;
            this.ChartConfigurationRequired = true;
        }

        /// <summary> Event handler. Called by _SignalMemoryLengthNumeric for value changed events. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void SignalMemoryLengthNumeric_ValueChanged( object sender, EventArgs e )
        {
            // request configuration of analog input and chart.
            this.AnalogInputConfigurationRequired = true;
            this.ChartConfigurationRequired = true;
        }

        /// <summary> Event handler. Called by _SignalChannelComboBox for selected value changed
        /// events. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void SignalChannelComboBox_SelectedValueChanged( object sender, EventArgs e )
        {
            // Update the signal ranges based on the channel
            this.EnableAddSignalControl();
            if ( this.IsOpen )
            {
                try
                {
                    _ = this.AnalogInput.SelectLogicalChannel( this._SignalChannelComboBox.SelectedIndex );
                    this._SignalMaxNumericUpDown.Value = decimal.Parse( this.AnalogInput.MaxVoltage().ToString( System.Globalization.CultureInfo.CurrentCulture ), System.Globalization.CultureInfo.CurrentCulture );
                    this._SignalMinNumericUpDown.Value = decimal.Parse( this.AnalogInput.MinVoltage().ToString( System.Globalization.CultureInfo.CurrentCulture ), System.Globalization.CultureInfo.CurrentCulture );
                }
                catch ( OpenLayers.Base.OlException ex )
                {
                    _ = ExceptionExtensionMethods.AddExceptionData( ex );
                    this.StatusMessage = $"Exception occurred selecting signal channel;. {ex}";
                    this.OnHandlerException( ex );
                }
            }
        }

        /// <summary> Removes a signal from the list. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void SignalsListView_Click( object sender, EventArgs e )
        {
            if ( this.Chart.Signals.Count > 0 && this._SignalsListView.SelectedIndices.Count > 0 )
            {
                try
                {
                    _ = this.SelectSignal( this._SignalsListView.SelectedIndices[0] );
                }
                catch ( OpenLayers.Base.OlException ex )
                {
                    _ = ExceptionExtensionMethods.AddExceptionData( ex );
                    this.StatusMessage = $"Exception occurred selecting signal;. {ex}";
                    this.OnHandlerException( ex );
                }
            }
        }

        /// <summary> Event handler. Called by _SignalMaxNumericUpDown for validating events. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Cancel event information. </param>
        private void SignalMaxNumericUpDown_Validating( object sender, CancelEventArgs e )
        {
            e.Cancel = this._SignalMaxNumericUpDown.Value <= this._SignalMinNumericUpDown.Value;
        }

        /// <summary> Event handler. Called by _SignalMinNumericUpDown for validating events. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Cancel event information. </param>
        private void SignalMinNumericUpDown_Validating( object sender, CancelEventArgs e )
        {
            e.Cancel = this._SignalMaxNumericUpDown.Value <= this._SignalMinNumericUpDown.Value;
        }

        /// <summary> Updates the selected signal. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void UpdateSignalButton_Click( object sender, EventArgs e )
        {
            if ( this.SelectedSignal is object )
            {
                if ( this._SignalMinNumericUpDown.Value < this._SignalMaxNumericUpDown.Value )
                {
                    try
                    {
                        _ = this.Chart.DisableRendering();
                        if ( this.SelectedSignal.RangeMin < ( double ) this._SignalMinNumericUpDown.Value && this.SelectedSignal.RangeMax > ( double ) this._SignalMinNumericUpDown.Value )
                        {
                            this.SelectedSignal.CurrentRangeMin = ( double ) this._SignalMinNumericUpDown.Value;
                        }

                        if ( this.SelectedSignal.RangeMin < ( double ) this._SignalMaxNumericUpDown.Value && this.SelectedSignal.RangeMax > ( double ) this._SignalMaxNumericUpDown.Value )
                        {
                            this.SelectedSignal.CurrentRangeMax = ( double ) this._SignalMaxNumericUpDown.Value;
                        }

                        this.Chart.SignalListUpdate();
                        _ = this.Chart.EnableRendering();
                        this.UpdateSignalList();
                    }
                    catch ( OpenLayers.Base.OlException ex )
                    {
                        _ = ExceptionExtensionMethods.AddExceptionData( ex );
                        this.StatusMessage = $"Exception occurred updating selected signal;. {ex}";
                        this.OnHandlerException( ex );
                    }
                }
            }
        }

        #endregion

        #region " STATUS MESSAGE "

        private string _StatusMessage;
        /// <summary>   Gets or sets a message describing the status. </summary>
        /// <value> A message describing the status. </value>
        public string StatusMessage
        {
            get => this._StatusMessage;
            set {
                if ( !string.Equals( value, this.StatusMessage ) )
                {
                    this._StatusMessage = value;
                    if ( string.IsNullOrWhiteSpace( value ) )
                    {
                        this._ChartToolStripStatusLabel.ToolTipText = string.Empty;
                        this._ChartToolStripStatusLabel.Text = string.Empty;
                    }
                    else
                    {
                        this._ChartToolStripStatusLabel.ToolTipText = value;
                        string[] separatingStrings = { ";." };
                        this._ChartToolStripStatusLabel.Text = value.Split( separatingStrings, System.StringSplitOptions.RemoveEmptyEntries )[0];
                    }
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " HANDLER EXCEPTION HANDLER "

        /// <summary> Notifies of the HandlerExceptionOccurred event. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="ex"> The ex. </param>
        protected virtual void OnHandlerException( Exception ex )
        {
            this.NotifyHandlerExceptionOccurred( new System.Threading.ThreadExceptionEventArgs( ex ) );
        }

        /// <summary> Notifies of the HandlerExceptionOccurred event. </summary>
        /// <remarks>   David, 2022-01-12. </remarks>
        /// <param name="details">  A non-empty validation outcome string containing the reason
        ///                         configuration failed. </param>
        protected virtual void OnHandlerException( string details )
        {
            this.OnHandlerException( new InvalidOperationException(  details )  );
        }

        /// <summary> Notifies of the HandlerExceptionOccurred event. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnHandlerExceptionOccurred( System.Threading.ThreadExceptionEventArgs e )
        {
            this.NotifyHandlerExceptionOccurred( e );
        }

        /// <summary> Event queue for all listeners interested in HandlerExceptionOccurred events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<System.Threading.ThreadExceptionEventArgs> HandlerExceptionOccurred;

        /// <summary>   Notifies a handler exception occurred. </summary>
        /// <remarks>   David, 2020-11-11. </remarks>
        /// <param name="e">    The <see cref="System.IO.ErrorEventArgs" /> instance containing the event
        ///                     data. </param>
        protected void NotifyHandlerExceptionOccurred( System.Threading.ThreadExceptionEventArgs e )
        {
            this.HandlerExceptionOccurred?.Invoke( this, e );
        }

        #endregion

    }
}
