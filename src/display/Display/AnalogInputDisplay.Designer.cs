using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace isr.IO.OL.Display
{
    public partial class AnalogInputDisplay
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(AnalogInputDisplay));
            _Tabs = new TabControl();
            _BoardTabPage = new TabPage();
            _BoardTableLayoutPanel = new TableLayoutPanel();
            _BoardPanel = new Panel();
            _BoardGroupBox = new GroupBox();
            _BoardInputRangesComboBox = new ComboBox();
            _BoardInputRangesComboBoxLabel = new Label();
            _OpenCloseCheckBox = new CheckBox();
            _DeviceNameChooser = new DeviceChooser.DeviceNameChooser();
            _DeviceNameLabel = new Label();
            _GlobalGroupBox = new GroupBox();
            _ConfigureButton = new Button();
            _ResetButton = new Button();
            _RestoreDefaultsButton = new Button();
            _ChartTabPage = new TabPage();
            _ChartTableLayoutPanel = new TableLayoutPanel();
            _ChartTypeTableLayoutPanel = new TableLayoutPanel();
            _BandModeGroupBox = new GroupBox();
            _MultipleRadioButton = new RadioButton();
            _SingleRadioButton = new RadioButton();
            _ChartTypeGroupBox = new GroupBox();
            _StripChartRadioButton = new RadioButton();
            _ScopeRadioButton = new RadioButton();
            _ChartColorsTableLayoutPanel = new TableLayoutPanel();
            _ChartColorsGroupBox = new GroupBox();
            _ColorGridsButton = new Button();
            _ColorAxesButton = new Button();
            _ChartTitlesTableLayoutPanel = new TableLayoutPanel();
            _ChartTitlesGroupBox = new GroupBox();
            _ChartFooterTextBox = new TextBox();
            _ChartFooterTextBoxLabel = new Label();
            _ChartTitleTextBox = new TextBox();
            _ChartTitleTextBoxLabel = new Label();
            _ChartEnableTableLayoutPanel = new TableLayoutPanel();
            _ChartEnableGroupBox = new GroupBox();
            _RefreshRateNumeric = new NumericUpDown();
            _RefreshRateNumericLabel = new Label();
            _DisplayAverageCheckBox = new CheckBox();
            _DisplayVoltageCheckBox = new CheckBox();
            _EnableChartingCheckBox = new CheckBox();
            _ChannelsTabPage = new TabPage();
            _ChannelsTableLayoutPanel = new TableLayoutPanel();
            _ChannelsGroupBox = new GroupBox();
            _ChannelNameTextBox = new TextBox();
            _ChannelNameTextBoxLabel = new Label();
            _AddChannelButton = new Button();
            _ChannelRangesComboBox = new ComboBox();
            _ChannelRangeComboBoxLabel = new Label();
            _ChannelComboBox = new ComboBox();
            _ChannelComboBoxLabel = new Label();
            _RemoveChannelButton = new Button();
            _ChannelsListView = new ListView();
            _ChannelChannelColumnHeader = new ColumnHeader();
            _ChannelRangeColumnHeader = new ColumnHeader();
            _SamplingGroupBox = new GroupBox();
            _SampleSizeNumeric = new NumericUpDown();
            _BuffersCountNumeric = new NumericUpDown();
            _SamplingRateNumeric = new NumericUpDown();
            _SmartBufferingCheckBox = new CheckBox();
            _ContinuousBufferingCheckBox = new CheckBox();
            _SamplingRateNumericLabel = new Label();
            _SampleSizeNumericLabel = new Label();
            _BuffersCountNumericLabel = new Label();
            _ProcessingGroupBox = new GroupBox();
            _MovingAverageLengthNumeric = new NumericUpDown();
            _MovingAverageLengthNumericLabel = new Label();
            _CalculateAverageCheckBox = new CheckBox();
            _SignalsTabPage = new TabPage();
            _SignalsTableLayoutPanel = new TableLayoutPanel();
            _SignalChannelGroupBox = new GroupBox();
            _AddSignalButton = new Button();
            _AddSignalButton.Click += new EventHandler(AddSignalButton_Click);
            _SignalChannelComboBox = new ComboBox();
            _SignalChannelComboBoxLabel = new Label();
            _SignalSamplingGroupBox = new GroupBox();
            _SignalMemoryLengthNumeric = new NumericUpDown();
            _SignalBufferSizeNumeric = new NumericUpDown();
            _SignalMemoryLengthNumericLabel = new Label();
            _SignalBufferSizeNumericLabel = new Label();
            _SignalsGroupBox = new GroupBox();
            _UpdateSignalButton = new Button();
            _SignalMinNumericUpDownLabel = new Label();
            _SignalMaxNumericUpDownLabel = new Label();
            _SignalMinNumericUpDown = new NumericUpDown();
            _SignalMaxNumericUpDown = new NumericUpDown();
            _ColorSignalButton = new Button();
            _RemoveSignalButton = new Button();
            _SignalsListView = new ListView();
            _SignalChannelColumnHeader = new ColumnHeader();
            _SignalRangeColumnHeader = new ColumnHeader();
            Chart = new OpenLayers.Controls.Display();
            _ToolTip = new ToolTip(components);
            _ErrorProvider = new ErrorProvider(components);
            _MainTableLayoutPanel = new TableLayoutPanel();
            _DisplayTableLayoutPanel = new TableLayoutPanel();
            _StatusStrip = new StatusStrip();
            _ChartToolStripStatusLabel = new ToolStripStatusLabel();
            _CurrentVoltageToolStripLabel = new ToolStripStatusLabel();
            _AverageVoltageToolStripLabel = new ToolStripStatusLabel();
            _ActionsToolStripDropDownButton = new ToolStripDropDownButton();
            _StartStopToolStripMenuItem = new ToolStripMenuItem();
            _AbortToolStripMenuItem = new ToolStripMenuItem();
            _ConfigureToolStripMenuItem = new ToolStripMenuItem();
            _InfoProvider = new ErrorProvider(components);
            _Tabs.SuspendLayout();
            _BoardTabPage.SuspendLayout();
            _BoardTableLayoutPanel.SuspendLayout();
            _BoardPanel.SuspendLayout();
            _BoardGroupBox.SuspendLayout();
            _GlobalGroupBox.SuspendLayout();
            _ChartTabPage.SuspendLayout();
            _ChartTableLayoutPanel.SuspendLayout();
            _ChartTypeTableLayoutPanel.SuspendLayout();
            _BandModeGroupBox.SuspendLayout();
            _ChartTypeGroupBox.SuspendLayout();
            _ChartColorsTableLayoutPanel.SuspendLayout();
            _ChartColorsGroupBox.SuspendLayout();
            _ChartTitlesTableLayoutPanel.SuspendLayout();
            _ChartTitlesGroupBox.SuspendLayout();
            _ChartEnableTableLayoutPanel.SuspendLayout();
            _ChartEnableGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_RefreshRateNumeric).BeginInit();
            _ChannelsTabPage.SuspendLayout();
            _ChannelsTableLayoutPanel.SuspendLayout();
            _ChannelsGroupBox.SuspendLayout();
            _SamplingGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_SampleSizeNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_BuffersCountNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_SamplingRateNumeric).BeginInit();
            _ProcessingGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_MovingAverageLengthNumeric).BeginInit();
            _SignalsTabPage.SuspendLayout();
            _SignalsTableLayoutPanel.SuspendLayout();
            _SignalChannelGroupBox.SuspendLayout();
            _SignalSamplingGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_SignalMemoryLengthNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_SignalBufferSizeNumeric).BeginInit();
            _SignalsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_SignalMinNumericUpDown).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_SignalMaxNumericUpDown).BeginInit();
            ((System.ComponentModel.ISupportInitialize)Chart).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).BeginInit();
            _MainTableLayoutPanel.SuspendLayout();
            _DisplayTableLayoutPanel.SuspendLayout();
            _StatusStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_InfoProvider).BeginInit();
            SuspendLayout();
            // 
            // _Tabs
            // 
            _Tabs.Controls.Add(_BoardTabPage);
            _Tabs.Controls.Add(_ChartTabPage);
            _Tabs.Controls.Add(_ChannelsTabPage);
            _Tabs.Controls.Add(_SignalsTabPage);
            _Tabs.Location = new System.Drawing.Point(3, 3);
            _Tabs.Name = "_Tabs";
            _Tabs.Padding = new System.Drawing.Point(5, 3);
            _Tabs.SelectedIndex = 0;
            _Tabs.Size = new System.Drawing.Size(224, 550);
            _Tabs.TabIndex = 0;
            // 
            // _BoardTabPage
            // 
            _BoardTabPage.Controls.Add(_BoardTableLayoutPanel);
            _BoardTabPage.Location = new System.Drawing.Point(4, 26);
            _BoardTabPage.Name = "_BoardTabPage";
            _BoardTabPage.Size = new System.Drawing.Size(216, 520);
            _BoardTabPage.TabIndex = 0;
            _BoardTabPage.Text = "Board";
            _BoardTabPage.UseVisualStyleBackColor = true;
            // 
            // _BoardTableLayoutPanel
            // 
            _BoardTableLayoutPanel.ColumnCount = 3;
            _BoardTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _BoardTableLayoutPanel.ColumnStyles.Add(new ColumnStyle());
            _BoardTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _BoardTableLayoutPanel.Controls.Add(_BoardPanel, 1, 1);
            _BoardTableLayoutPanel.Controls.Add(_GlobalGroupBox, 1, 3);
            _BoardTableLayoutPanel.Dock = DockStyle.Fill;
            _BoardTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            _BoardTableLayoutPanel.Name = "_BoardTableLayoutPanel";
            _BoardTableLayoutPanel.RowCount = 5;
            _BoardTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 33.33333f));
            _BoardTableLayoutPanel.RowStyles.Add(new RowStyle());
            _BoardTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 9.0f));
            _BoardTableLayoutPanel.RowStyles.Add(new RowStyle());
            _BoardTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 66.66666f));
            _BoardTableLayoutPanel.Size = new System.Drawing.Size(216, 520);
            _BoardTableLayoutPanel.TabIndex = 0;
            // 
            // _BoardPanel
            // 
            _BoardPanel.Controls.Add(_BoardGroupBox);
            _BoardPanel.Controls.Add(_OpenCloseCheckBox);
            _BoardPanel.Controls.Add(_DeviceNameChooser);
            _BoardPanel.Controls.Add(_DeviceNameLabel);
            _BoardPanel.Location = new System.Drawing.Point(8, 74);
            _BoardPanel.Name = "_BoardPanel";
            _BoardPanel.Size = new System.Drawing.Size(200, 161);
            _BoardPanel.TabIndex = 1;
            // 
            // _BoardGroupBox
            // 
            _BoardGroupBox.Controls.Add(_BoardInputRangesComboBox);
            _BoardGroupBox.Controls.Add(_BoardInputRangesComboBoxLabel);
            _BoardGroupBox.Location = new System.Drawing.Point(10, 97);
            _BoardGroupBox.Name = "_BoardGroupBox";
            _BoardGroupBox.Size = new System.Drawing.Size(181, 50);
            _BoardGroupBox.TabIndex = 3;
            _BoardGroupBox.TabStop = false;
            _BoardGroupBox.Text = "Board";
            // 
            // _BoardInputRangesComboBox
            // 
            _BoardInputRangesComboBox.BackColor = System.Drawing.SystemColors.Window;
            _BoardInputRangesComboBox.Cursor = Cursors.Default;
            _BoardInputRangesComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            _BoardInputRangesComboBox.ForeColor = System.Drawing.SystemColors.WindowText;
            _BoardInputRangesComboBox.Location = new System.Drawing.Point(89, 16);
            _BoardInputRangesComboBox.Name = "_BoardInputRangesComboBox";
            _BoardInputRangesComboBox.RightToLeft = RightToLeft.No;
            _BoardInputRangesComboBox.Size = new System.Drawing.Size(83, 25);
            _BoardInputRangesComboBox.TabIndex = 1;
            _BoardInputRangesComboBox.SelectedIndexChanged += new EventHandler( BoardInputRangesComboBox_SelectedIndexChanged );
            // 
            // _BoardInputRangesComboBoxLabel
            // 
            _BoardInputRangesComboBoxLabel.AutoSize = true;
            _BoardInputRangesComboBoxLabel.BackColor = System.Drawing.Color.Transparent;
            _BoardInputRangesComboBoxLabel.Cursor = Cursors.Default;
            _BoardInputRangesComboBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            _BoardInputRangesComboBoxLabel.Location = new System.Drawing.Point(6, 20);
            _BoardInputRangesComboBoxLabel.Name = "_BoardInputRangesComboBoxLabel";
            _BoardInputRangesComboBoxLabel.RightToLeft = RightToLeft.No;
            _BoardInputRangesComboBoxLabel.Size = new System.Drawing.Size(81, 17);
            _BoardInputRangesComboBoxLabel.TabIndex = 0;
            _BoardInputRangesComboBoxLabel.Text = "Input Range:";
            _BoardInputRangesComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _OpenCloseCheckBox
            // 
            _OpenCloseCheckBox.Appearance = Appearance.Button;
            _OpenCloseCheckBox.Enabled = false;
            _OpenCloseCheckBox.Location = new System.Drawing.Point(11, 64);
            _OpenCloseCheckBox.Name = "_OpenCloseCheckBox";
            _OpenCloseCheckBox.Size = new System.Drawing.Size(180, 28);
            _OpenCloseCheckBox.TabIndex = 2;
            _OpenCloseCheckBox.Text = "&Open";
            _OpenCloseCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            _OpenCloseCheckBox.CheckedChanged += new EventHandler( OpenCloseCheckBox_CheckedChanged );
            _OpenCloseCheckBox.Click += new EventHandler( OpenCloseCheckBox_Click );
            // 
            // _DeviceNameChooser
            // 
            _DeviceNameChooser.Location = new System.Drawing.Point(11, 27);
            _DeviceNameChooser.Margin = new Padding(4, 3, 4, 3);
            _DeviceNameChooser.Name = "_DeviceNameChooser";
            _DeviceNameChooser.Size = new System.Drawing.Size(180, 28);
            _DeviceNameChooser.TabIndex = 1;
            _DeviceNameChooser.DeviceSelected += new EventHandler<EventArgs>( DeviceNameChooser_DeviceSelected );
            // 
            // _DeviceNameLabel
            // 
            _DeviceNameLabel.AutoSize = true;
            _DeviceNameLabel.Location = new System.Drawing.Point(11, 9);
            _DeviceNameLabel.Margin = new Padding(4, 0, 4, 0);
            _DeviceNameLabel.Name = "_DeviceNameLabel";
            _DeviceNameLabel.Size = new System.Drawing.Size(88, 17);
            _DeviceNameLabel.TabIndex = 0;
            _DeviceNameLabel.Text = "Device Name:";
            _DeviceNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _GlobalGroupBox
            // 
            _GlobalGroupBox.Controls.Add(_ConfigureButton);
            _GlobalGroupBox.Controls.Add(_ResetButton);
            _GlobalGroupBox.Controls.Add(_RestoreDefaultsButton);
            _GlobalGroupBox.Location = new System.Drawing.Point(8, 250);
            _GlobalGroupBox.Name = "_GlobalGroupBox";
            _GlobalGroupBox.Size = new System.Drawing.Size(200, 124);
            _GlobalGroupBox.TabIndex = 0;
            _GlobalGroupBox.TabStop = false;
            _GlobalGroupBox.Text = "Global";
            // 
            // _ConfigureButton
            // 
            _ConfigureButton.Location = new System.Drawing.Point(10, 85);
            _ConfigureButton.Name = "_ConfigureButton";
            _ConfigureButton.Size = new System.Drawing.Size(180, 28);
            _ConfigureButton.TabIndex = 2;
            _ConfigureButton.Text = "&Configure";
            _ToolTip.SetToolTip(_ConfigureButton, "Configure using current settings.");
            _ConfigureButton.UseVisualStyleBackColor = true;
            _ConfigureButton.Click += new EventHandler( ConfigureButton_Click );
            // 
            // _ResetButton
            // 
            _ResetButton.Location = new System.Drawing.Point(11, 52);
            _ResetButton.Name = "_ResetButton";
            _ResetButton.Size = new System.Drawing.Size(180, 28);
            _ResetButton.TabIndex = 1;
            _ResetButton.Text = "Re&set";
            _ToolTip.SetToolTip(_ResetButton, "Reset to Opened state conditions.");
            _ResetButton.UseVisualStyleBackColor = true;
            _ResetButton.Click += new EventHandler( ResetButton_Click );
            // 
            // _RestoreDefaultsButton
            // 
            _RestoreDefaultsButton.Location = new System.Drawing.Point(11, 19);
            _RestoreDefaultsButton.Name = "_RestoreDefaultsButton";
            _RestoreDefaultsButton.Size = new System.Drawing.Size(180, 28);
            _RestoreDefaultsButton.TabIndex = 0;
            _RestoreDefaultsButton.Text = "&Restore Defaults";
            _ToolTip.SetToolTip(_RestoreDefaultsButton, "Restore  to default settings");
            _RestoreDefaultsButton.UseVisualStyleBackColor = true;
            _RestoreDefaultsButton.Click += new EventHandler( RestoreDefaultsButton_Click );
            // 
            // _ChartTabPage
            // 
            _ChartTabPage.Controls.Add(_ChartTableLayoutPanel);
            _ChartTabPage.Location = new System.Drawing.Point(4, 26);
            _ChartTabPage.Name = "_ChartTabPage";
            _ChartTabPage.Size = new System.Drawing.Size(216, 520);
            _ChartTabPage.TabIndex = 1;
            _ChartTabPage.Text = "Chart";
            _ChartTabPage.UseVisualStyleBackColor = true;
            // 
            // _ChartTableLayoutPanel
            // 
            _ChartTableLayoutPanel.ColumnCount = 3;
            _ChartTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 1.0f));
            _ChartTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100.0f));
            _ChartTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 1.0f));
            _ChartTableLayoutPanel.Controls.Add(_ChartTypeTableLayoutPanel, 1, 2);
            _ChartTableLayoutPanel.Controls.Add(_ChartColorsTableLayoutPanel, 1, 3);
            _ChartTableLayoutPanel.Controls.Add(_ChartTitlesTableLayoutPanel, 1, 4);
            _ChartTableLayoutPanel.Controls.Add(_ChartEnableTableLayoutPanel, 1, 1);
            _ChartTableLayoutPanel.Dock = DockStyle.Fill;
            _ChartTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            _ChartTableLayoutPanel.Name = "_ChartTableLayoutPanel";
            _ChartTableLayoutPanel.RowCount = 6;
            _ChartTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _ChartTableLayoutPanel.RowStyles.Add(new RowStyle());
            _ChartTableLayoutPanel.RowStyles.Add(new RowStyle());
            _ChartTableLayoutPanel.RowStyles.Add(new RowStyle());
            _ChartTableLayoutPanel.RowStyles.Add(new RowStyle());
            _ChartTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _ChartTableLayoutPanel.Size = new System.Drawing.Size(216, 520);
            _ChartTableLayoutPanel.TabIndex = 0;
            // 
            // _ChartTypeTableLayoutPanel
            // 
            _ChartTypeTableLayoutPanel.ColumnCount = 6;
            _ChartTypeTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25.0f));
            _ChartTypeTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25.0f));
            _ChartTypeTableLayoutPanel.ColumnStyles.Add(new ColumnStyle());
            _ChartTypeTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25.0f));
            _ChartTypeTableLayoutPanel.ColumnStyles.Add(new ColumnStyle());
            _ChartTypeTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25.0f));
            _ChartTypeTableLayoutPanel.Controls.Add(_BandModeGroupBox, 4, 1);
            _ChartTypeTableLayoutPanel.Controls.Add(_ChartTypeGroupBox, 2, 1);
            _ChartTypeTableLayoutPanel.Dock = DockStyle.Fill;
            _ChartTypeTableLayoutPanel.Location = new System.Drawing.Point(4, 159);
            _ChartTypeTableLayoutPanel.Name = "_ChartTypeTableLayoutPanel";
            _ChartTypeTableLayoutPanel.RowCount = 3;
            _ChartTypeTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _ChartTypeTableLayoutPanel.RowStyles.Add(new RowStyle());
            _ChartTypeTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _ChartTypeTableLayoutPanel.Size = new System.Drawing.Size(208, 99);
            _ChartTypeTableLayoutPanel.TabIndex = 1;
            // 
            // _BandModeGroupBox
            // 
            _BandModeGroupBox.Controls.Add(_MultipleRadioButton);
            _BandModeGroupBox.Controls.Add(_SingleRadioButton);
            _BandModeGroupBox.Location = new System.Drawing.Point(109, 10);
            _BandModeGroupBox.Margin = new Padding(4, 3, 4, 3);
            _BandModeGroupBox.Name = "_BandModeGroupBox";
            _BandModeGroupBox.Padding = new Padding(4, 3, 4, 3);
            _BandModeGroupBox.Size = new System.Drawing.Size(90, 79);
            _BandModeGroupBox.TabIndex = 1;
            _BandModeGroupBox.TabStop = false;
            _BandModeGroupBox.Text = "Band mode";
            // 
            // _MultipleRadioButton
            // 
            _MultipleRadioButton.Location = new System.Drawing.Point(8, 47);
            _MultipleRadioButton.Margin = new Padding(4, 3, 4, 3);
            _MultipleRadioButton.Name = "_MultipleRadioButton";
            _MultipleRadioButton.Size = new System.Drawing.Size(76, 24);
            _MultipleRadioButton.TabIndex = 1;
            _MultipleRadioButton.Text = "Multiple";
            // 
            // _SingleRadioButton
            // 
            _SingleRadioButton.Checked = true;
            _SingleRadioButton.Location = new System.Drawing.Point(8, 19);
            _SingleRadioButton.Margin = new Padding(4, 3, 4, 3);
            _SingleRadioButton.Name = "_SingleRadioButton";
            _SingleRadioButton.Size = new System.Drawing.Size(66, 24);
            _SingleRadioButton.TabIndex = 0;
            _SingleRadioButton.TabStop = true;
            _SingleRadioButton.Text = "Single";
            _SingleRadioButton.CheckedChanged += new EventHandler( SingleRadioButton_CheckedChanged );
            // 
            // _ChartTypeGroupBox
            // 
            _ChartTypeGroupBox.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _ChartTypeGroupBox.Controls.Add(_StripChartRadioButton);
            _ChartTypeGroupBox.Controls.Add(_ScopeRadioButton);
            _ChartTypeGroupBox.Location = new System.Drawing.Point(8, 10);
            _ChartTypeGroupBox.Margin = new Padding(4, 3, 4, 3);
            _ChartTypeGroupBox.Name = "_ChartTypeGroupBox";
            _ChartTypeGroupBox.Padding = new Padding(4, 3, 4, 3);
            _ChartTypeGroupBox.Size = new System.Drawing.Size(91, 79);
            _ChartTypeGroupBox.TabIndex = 0;
            _ChartTypeGroupBox.TabStop = false;
            _ChartTypeGroupBox.Text = "Chart Type";
            // 
            // _StripChartRadioButton
            // 
            _StripChartRadioButton.Location = new System.Drawing.Point(8, 47);
            _StripChartRadioButton.Margin = new Padding(4, 3, 4, 3);
            _StripChartRadioButton.Name = "_StripChartRadioButton";
            _StripChartRadioButton.Size = new System.Drawing.Size(77, 24);
            _StripChartRadioButton.TabIndex = 1;
            _StripChartRadioButton.Text = "Strip Chart";
            // 
            // _ScopeRadioButton
            // 
            _ScopeRadioButton.Checked = true;
            _ScopeRadioButton.Location = new System.Drawing.Point(8, 19);
            _ScopeRadioButton.Margin = new Padding(4, 3, 4, 3);
            _ScopeRadioButton.Name = "_ScopeRadioButton";
            _ScopeRadioButton.Size = new System.Drawing.Size(67, 24);
            _ScopeRadioButton.TabIndex = 0;
            _ScopeRadioButton.TabStop = true;
            _ScopeRadioButton.Text = "Scope";
            _ScopeRadioButton.CheckedChanged += new EventHandler( ScopeRadioButton_CheckedChanged );
            // 
            // _ChartColorsTableLayoutPanel
            // 
            _ChartColorsTableLayoutPanel.ColumnCount = 3;
            _ChartColorsTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _ChartColorsTableLayoutPanel.ColumnStyles.Add(new ColumnStyle());
            _ChartColorsTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _ChartColorsTableLayoutPanel.Controls.Add(_ChartColorsGroupBox, 1, 1);
            _ChartColorsTableLayoutPanel.Dock = DockStyle.Fill;
            _ChartColorsTableLayoutPanel.Location = new System.Drawing.Point(4, 264);
            _ChartColorsTableLayoutPanel.Name = "_ChartColorsTableLayoutPanel";
            _ChartColorsTableLayoutPanel.RowCount = 3;
            _ChartColorsTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _ChartColorsTableLayoutPanel.RowStyles.Add(new RowStyle());
            _ChartColorsTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _ChartColorsTableLayoutPanel.Size = new System.Drawing.Size(208, 115);
            _ChartColorsTableLayoutPanel.TabIndex = 2;
            // 
            // _ChartColorsGroupBox
            // 
            _ChartColorsGroupBox.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _ChartColorsGroupBox.Controls.Add(_ColorGridsButton);
            _ChartColorsGroupBox.Controls.Add(_ColorAxesButton);
            _ChartColorsGroupBox.Location = new System.Drawing.Point(41, 11);
            _ChartColorsGroupBox.Margin = new Padding(4, 3, 4, 3);
            _ChartColorsGroupBox.Name = "_ChartColorsGroupBox";
            _ChartColorsGroupBox.Padding = new Padding(4, 3, 4, 3);
            _ChartColorsGroupBox.Size = new System.Drawing.Size(125, 92);
            _ChartColorsGroupBox.TabIndex = 0;
            _ChartColorsGroupBox.TabStop = false;
            _ChartColorsGroupBox.Text = "Chart Colors";
            // 
            // _ColorGridsButton
            // 
            _ColorGridsButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _ColorGridsButton.Location = new System.Drawing.Point(16, 57);
            _ColorGridsButton.Margin = new Padding(4, 3, 4, 3);
            _ColorGridsButton.Name = "_ColorGridsButton";
            _ColorGridsButton.Size = new System.Drawing.Size(95, 23);
            _ColorGridsButton.TabIndex = 1;
            _ColorGridsButton.Text = "Grids color";
            _ColorGridsButton.Click += new EventHandler( ColorGridsButton_Click );
            // 
            // _ColorAxesButton
            // 
            _ColorAxesButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _ColorAxesButton.Location = new System.Drawing.Point(16, 25);
            _ColorAxesButton.Margin = new Padding(4, 3, 4, 3);
            _ColorAxesButton.Name = "_ColorAxesButton";
            _ColorAxesButton.Size = new System.Drawing.Size(95, 23);
            _ColorAxesButton.TabIndex = 0;
            _ColorAxesButton.Text = "Axes color";
            _ColorAxesButton.Click += new EventHandler( ColorAxesButton_Click );
            // 
            // _ChartTitlesTableLayoutPanel
            // 
            _ChartTitlesTableLayoutPanel.ColumnCount = 3;
            _ChartTitlesTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _ChartTitlesTableLayoutPanel.ColumnStyles.Add(new ColumnStyle());
            _ChartTitlesTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _ChartTitlesTableLayoutPanel.Controls.Add(_ChartTitlesGroupBox, 1, 1);
            _ChartTitlesTableLayoutPanel.Dock = DockStyle.Fill;
            _ChartTitlesTableLayoutPanel.Location = new System.Drawing.Point(4, 385);
            _ChartTitlesTableLayoutPanel.Name = "_ChartTitlesTableLayoutPanel";
            _ChartTitlesTableLayoutPanel.RowCount = 3;
            _ChartTitlesTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _ChartTitlesTableLayoutPanel.RowStyles.Add(new RowStyle());
            _ChartTitlesTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _ChartTitlesTableLayoutPanel.Size = new System.Drawing.Size(208, 121);
            _ChartTitlesTableLayoutPanel.TabIndex = 3;
            // 
            // _ChartTitlesGroupBox
            // 
            _ChartTitlesGroupBox.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _ChartTitlesGroupBox.Controls.Add(_ChartFooterTextBox);
            _ChartTitlesGroupBox.Controls.Add(_ChartFooterTextBoxLabel);
            _ChartTitlesGroupBox.Controls.Add(_ChartTitleTextBox);
            _ChartTitlesGroupBox.Controls.Add(_ChartTitleTextBoxLabel);
            _ChartTitlesGroupBox.Location = new System.Drawing.Point(15, 5);
            _ChartTitlesGroupBox.Margin = new Padding(4, 3, 4, 3);
            _ChartTitlesGroupBox.Name = "_ChartTitlesGroupBox";
            _ChartTitlesGroupBox.Padding = new Padding(4, 3, 4, 3);
            _ChartTitlesGroupBox.Size = new System.Drawing.Size(177, 110);
            _ChartTitlesGroupBox.TabIndex = 0;
            _ChartTitlesGroupBox.TabStop = false;
            _ChartTitlesGroupBox.Text = "Titles";
            // 
            // _ChartFooterTextBox
            // 
            _ChartFooterTextBox.Location = new System.Drawing.Point(8, 80);
            _ChartFooterTextBox.Margin = new Padding(4, 3, 4, 3);
            _ChartFooterTextBox.Name = "_ChartFooterTextBox";
            _ChartFooterTextBox.Size = new System.Drawing.Size(163, 25);
            _ChartFooterTextBox.TabIndex = 3;
            _ToolTip.SetToolTip(_ChartFooterTextBox, "Chart Footer");
            _ChartFooterTextBox.Validating += new System.ComponentModel.CancelEventHandler( ChartFooterTextBox_Validating );
            // 
            // _ChartFooterTextBoxLabel
            // 
            _ChartFooterTextBoxLabel.AutoSize = true;
            _ChartFooterTextBoxLabel.Location = new System.Drawing.Point(5, 61);
            _ChartFooterTextBoxLabel.Margin = new Padding(4, 0, 4, 0);
            _ChartFooterTextBoxLabel.Name = "_ChartFooterTextBoxLabel";
            _ChartFooterTextBoxLabel.Size = new System.Drawing.Size(53, 17);
            _ChartFooterTextBoxLabel.TabIndex = 2;
            _ChartFooterTextBoxLabel.Text = "Footer: ";
            _ChartFooterTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _ChartTitleTextBox
            // 
            _ChartTitleTextBox.Location = new System.Drawing.Point(8, 36);
            _ChartTitleTextBox.Margin = new Padding(4, 3, 4, 3);
            _ChartTitleTextBox.Name = "_ChartTitleTextBox";
            _ChartTitleTextBox.Size = new System.Drawing.Size(163, 25);
            _ChartTitleTextBox.TabIndex = 1;
            _ToolTip.SetToolTip(_ChartTitleTextBox, "Chart Title");
            _ChartTitleTextBox.Validating += new System.ComponentModel.CancelEventHandler( ChartTitleTextBox_Validating );
            // 
            // _ChartTitleTextBoxLabel
            // 
            _ChartTitleTextBoxLabel.AutoSize = true;
            _ChartTitleTextBoxLabel.Location = new System.Drawing.Point(5, 17);
            _ChartTitleTextBoxLabel.Margin = new Padding(4, 0, 4, 0);
            _ChartTitleTextBoxLabel.Name = "_ChartTitleTextBoxLabel";
            _ChartTitleTextBoxLabel.Size = new System.Drawing.Size(39, 17);
            _ChartTitleTextBoxLabel.TabIndex = 0;
            _ChartTitleTextBoxLabel.Text = "Title: ";
            _ChartTitleTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _ChartEnableTableLayoutPanel
            // 
            _ChartEnableTableLayoutPanel.ColumnCount = 3;
            _ChartEnableTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _ChartEnableTableLayoutPanel.ColumnStyles.Add(new ColumnStyle());
            _ChartEnableTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _ChartEnableTableLayoutPanel.Controls.Add(_ChartEnableGroupBox, 1, 1);
            _ChartEnableTableLayoutPanel.Dock = DockStyle.Fill;
            _ChartEnableTableLayoutPanel.Location = new System.Drawing.Point(4, 14);
            _ChartEnableTableLayoutPanel.Name = "_ChartEnableTableLayoutPanel";
            _ChartEnableTableLayoutPanel.RowCount = 3;
            _ChartEnableTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _ChartEnableTableLayoutPanel.RowStyles.Add(new RowStyle());
            _ChartEnableTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _ChartEnableTableLayoutPanel.Size = new System.Drawing.Size(208, 139);
            _ChartEnableTableLayoutPanel.TabIndex = 0;
            // 
            // _ChartEnableGroupBox
            // 
            _ChartEnableGroupBox.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _ChartEnableGroupBox.Controls.Add(_RefreshRateNumeric);
            _ChartEnableGroupBox.Controls.Add(_RefreshRateNumericLabel);
            _ChartEnableGroupBox.Controls.Add(_DisplayAverageCheckBox);
            _ChartEnableGroupBox.Controls.Add(_DisplayVoltageCheckBox);
            _ChartEnableGroupBox.Controls.Add(_EnableChartingCheckBox);
            _ChartEnableGroupBox.Location = new System.Drawing.Point(23, 11);
            _ChartEnableGroupBox.Margin = new Padding(4, 3, 4, 3);
            _ChartEnableGroupBox.Name = "_ChartEnableGroupBox";
            _ChartEnableGroupBox.Padding = new Padding(4, 3, 4, 3);
            _ChartEnableGroupBox.Size = new System.Drawing.Size(162, 117);
            _ChartEnableGroupBox.TabIndex = 0;
            _ChartEnableGroupBox.TabStop = false;
            _ChartEnableGroupBox.Text = "Chart";
            // 
            // _RefreshRateNumeric
            // 
            _RefreshRateNumeric.Location = new System.Drawing.Point(101, 87);
            _RefreshRateNumeric.Name = "_RefreshRateNumeric";
            _RefreshRateNumeric.Size = new System.Drawing.Size(51, 25);
            _RefreshRateNumeric.TabIndex = 4;
            _ToolTip.SetToolTip(_RefreshRateNumeric, "Display Refresh Rate Per Second");
            _RefreshRateNumeric.Value = new decimal(new int[] { 24, 0, 0, 0 });
            _RefreshRateNumeric.ValueChanged += new EventHandler( RefreshRateNumeric_ValueChanged );
            // 
            // _RefreshRateNumericLabel
            // 
            _RefreshRateNumericLabel.AutoSize = true;
            _RefreshRateNumericLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            _RefreshRateNumericLabel.Location = new System.Drawing.Point(14, 91);
            _RefreshRateNumericLabel.Margin = new Padding(4, 0, 4, 0);
            _RefreshRateNumericLabel.Name = "_RefreshRateNumericLabel";
            _RefreshRateNumericLabel.Size = new System.Drawing.Size(85, 17);
            _RefreshRateNumericLabel.TabIndex = 3;
            _RefreshRateNumericLabel.Text = "Refresh Rate:";
            _RefreshRateNumericLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _DisplayAverageCheckBox
            // 
            _DisplayAverageCheckBox.AutoSize = true;
            _DisplayAverageCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            _DisplayAverageCheckBox.Location = new System.Drawing.Point(13, 64);
            _DisplayAverageCheckBox.Name = "_DisplayAverageCheckBox";
            _DisplayAverageCheckBox.Size = new System.Drawing.Size(121, 21);
            _DisplayAverageCheckBox.TabIndex = 2;
            _DisplayAverageCheckBox.Text = "Display Average";
            _ToolTip.SetToolTip(_DisplayAverageCheckBox, "Check to display average voltage");
            _DisplayAverageCheckBox.UseVisualStyleBackColor = true;
            _DisplayAverageCheckBox.CheckedChanged += new EventHandler( DisplayAverageCheckBox_CheckedChanged );
            // 
            // _DisplayVoltageCheckBox
            // 
            _DisplayVoltageCheckBox.AutoSize = true;
            _DisplayVoltageCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            _DisplayVoltageCheckBox.Location = new System.Drawing.Point(17, 41);
            _DisplayVoltageCheckBox.Name = "_DisplayVoltageCheckBox";
            _DisplayVoltageCheckBox.Size = new System.Drawing.Size(118, 21);
            _DisplayVoltageCheckBox.TabIndex = 1;
            _DisplayVoltageCheckBox.Text = "Display Voltage";
            _ToolTip.SetToolTip(_DisplayVoltageCheckBox, "Check to display current voltage");
            _DisplayVoltageCheckBox.UseVisualStyleBackColor = true;
            _DisplayVoltageCheckBox.CheckedChanged += new EventHandler( DisplayVoltageCheckBox_CheckedChanged );
            // 
            // _EnableChartingCheckBox
            // 
            _EnableChartingCheckBox.AutoSize = true;
            _EnableChartingCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            _EnableChartingCheckBox.Checked = true;
            _EnableChartingCheckBox.CheckState = CheckState.Checked;
            _EnableChartingCheckBox.Location = new System.Drawing.Point(15, 18);
            _EnableChartingCheckBox.Name = "_EnableChartingCheckBox";
            _EnableChartingCheckBox.Size = new System.Drawing.Size(119, 21);
            _EnableChartingCheckBox.TabIndex = 0;
            _EnableChartingCheckBox.Text = "Enable Charting";
            _ToolTip.SetToolTip(_EnableChartingCheckBox, "Check to enable charting");
            _EnableChartingCheckBox.UseVisualStyleBackColor = true;
            _EnableChartingCheckBox.CheckedChanged += new EventHandler( EnableChartingCheckBox_CheckedChanged );
            // 
            // _channelsTabPage
            // 
            _ChannelsTabPage.Controls.Add(_ChannelsTableLayoutPanel);
            _ChannelsTabPage.Location = new System.Drawing.Point(4, 26);
            _ChannelsTabPage.Name = "_channelsTabPage";
            _ChannelsTabPage.Size = new System.Drawing.Size(216, 520);
            _ChannelsTabPage.TabIndex = 3;
            _ChannelsTabPage.Text = "Channels";
            _ChannelsTabPage.UseVisualStyleBackColor = true;
            // 
            // _ChannelsTableLayoutPanel
            // 
            _ChannelsTableLayoutPanel.ColumnCount = 3;
            _ChannelsTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _ChannelsTableLayoutPanel.ColumnStyles.Add(new ColumnStyle());
            _ChannelsTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _ChannelsTableLayoutPanel.Controls.Add(_ChannelsGroupBox, 1, 1);
            _ChannelsTableLayoutPanel.Controls.Add(_SamplingGroupBox, 1, 3);
            _ChannelsTableLayoutPanel.Controls.Add(_ProcessingGroupBox, 1, 5);
            _ChannelsTableLayoutPanel.Dock = DockStyle.Fill;
            _ChannelsTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            _ChannelsTableLayoutPanel.Name = "_ChannelsTableLayoutPanel";
            _ChannelsTableLayoutPanel.RowCount = 7;
            _ChannelsTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _ChannelsTableLayoutPanel.RowStyles.Add(new RowStyle());
            _ChannelsTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 8.0f));
            _ChannelsTableLayoutPanel.RowStyles.Add(new RowStyle());
            _ChannelsTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 8.0f));
            _ChannelsTableLayoutPanel.RowStyles.Add(new RowStyle());
            _ChannelsTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _ChannelsTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 20.0f));
            _ChannelsTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 20.0f));
            _ChannelsTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 20.0f));
            _ChannelsTableLayoutPanel.Size = new System.Drawing.Size(216, 520);
            _ChannelsTableLayoutPanel.TabIndex = 0;
            // 
            // _ChannelsGroupBox
            // 
            _ChannelsGroupBox.Controls.Add(_ChannelNameTextBox);
            _ChannelsGroupBox.Controls.Add(_ChannelNameTextBoxLabel);
            _ChannelsGroupBox.Controls.Add(_AddChannelButton);
            _ChannelsGroupBox.Controls.Add(_ChannelRangesComboBox);
            _ChannelsGroupBox.Controls.Add(_ChannelRangeComboBoxLabel);
            _ChannelsGroupBox.Controls.Add(_ChannelComboBox);
            _ChannelsGroupBox.Controls.Add(_ChannelComboBoxLabel);
            _ChannelsGroupBox.Controls.Add(_RemoveChannelButton);
            _ChannelsGroupBox.Controls.Add(_ChannelsListView);
            _ChannelsGroupBox.Location = new System.Drawing.Point(8, 12);
            _ChannelsGroupBox.Name = "_ChannelsGroupBox";
            _ChannelsGroupBox.Size = new System.Drawing.Size(200, 229);
            _ChannelsGroupBox.TabIndex = 0;
            _ChannelsGroupBox.TabStop = false;
            _ChannelsGroupBox.Text = "Acquired Channels";
            _ToolTip.SetToolTip(_ChannelsGroupBox, "List of acquired channels.");
            // 
            // _ChannelNameTextBox
            // 
            _ChannelNameTextBox.Location = new System.Drawing.Point(63, 75);
            _ChannelNameTextBox.Margin = new Padding(4, 3, 4, 3);
            _ChannelNameTextBox.Name = "_ChannelNameTextBox";
            _ChannelNameTextBox.Size = new System.Drawing.Size(124, 25);
            _ChannelNameTextBox.TabIndex = 5;
            _ToolTip.SetToolTip(_ChannelNameTextBox, "Channel Name");
            // 
            // _ChannelNameTextBoxLabel
            // 
            _ChannelNameTextBoxLabel.AutoSize = true;
            _ChannelNameTextBoxLabel.BackColor = System.Drawing.Color.Transparent;
            _ChannelNameTextBoxLabel.Cursor = Cursors.Default;
            _ChannelNameTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            _ChannelNameTextBoxLabel.Location = new System.Drawing.Point(15, 79);
            _ChannelNameTextBoxLabel.Name = "_ChannelNameTextBoxLabel";
            _ChannelNameTextBoxLabel.RightToLeft = RightToLeft.No;
            _ChannelNameTextBoxLabel.Size = new System.Drawing.Size(46, 17);
            _ChannelNameTextBoxLabel.TabIndex = 4;
            _ChannelNameTextBoxLabel.Text = "Name:";
            _ChannelNameTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _AddChannelButton
            // 
            _AddChannelButton.BackColor = System.Drawing.SystemColors.Control;
            _AddChannelButton.Cursor = Cursors.Default;
            _AddChannelButton.Enabled = false;
            _AddChannelButton.ForeColor = System.Drawing.SystemColors.ControlText;
            _AddChannelButton.Location = new System.Drawing.Point(14, 104);
            _AddChannelButton.Name = "_AddChannelButton";
            _AddChannelButton.RightToLeft = RightToLeft.No;
            _AddChannelButton.Size = new System.Drawing.Size(70, 23);
            _AddChannelButton.TabIndex = 6;
            _AddChannelButton.Text = "Add";
            _ToolTip.SetToolTip(_AddChannelButton, "Add channel as a signal");
            _AddChannelButton.UseVisualStyleBackColor = false;
            _AddChannelButton.Click += new EventHandler( AddChannelButton_Click );
            // 
            // _ChannelRangesComboBox
            // 
            _ChannelRangesComboBox.BackColor = System.Drawing.SystemColors.Window;
            _ChannelRangesComboBox.Cursor = Cursors.Default;
            _ChannelRangesComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            _ChannelRangesComboBox.ForeColor = System.Drawing.SystemColors.WindowText;
            _ChannelRangesComboBox.Location = new System.Drawing.Point(63, 47);
            _ChannelRangesComboBox.Name = "_ChannelRangesComboBox";
            _ChannelRangesComboBox.RightToLeft = RightToLeft.No;
            _ChannelRangesComboBox.Size = new System.Drawing.Size(87, 25);
            _ChannelRangesComboBox.TabIndex = 3;
            // 
            // _ChannelRangeComboBoxLabel
            // 
            _ChannelRangeComboBoxLabel.AutoSize = true;
            _ChannelRangeComboBoxLabel.BackColor = System.Drawing.Color.Transparent;
            _ChannelRangeComboBoxLabel.Cursor = Cursors.Default;
            _ChannelRangeComboBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            _ChannelRangeComboBoxLabel.Location = new System.Drawing.Point(13, 51);
            _ChannelRangeComboBoxLabel.Name = "_ChannelRangeComboBoxLabel";
            _ChannelRangeComboBoxLabel.RightToLeft = RightToLeft.No;
            _ChannelRangeComboBoxLabel.Size = new System.Drawing.Size(48, 17);
            _ChannelRangeComboBoxLabel.TabIndex = 2;
            _ChannelRangeComboBoxLabel.Text = "Range:";
            _ChannelRangeComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _ChannelComboBox
            // 
            _ChannelComboBox.BackColor = System.Drawing.SystemColors.Window;
            _ChannelComboBox.Cursor = Cursors.Default;
            _ChannelComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            _ChannelComboBox.ForeColor = System.Drawing.SystemColors.WindowText;
            _ChannelComboBox.Location = new System.Drawing.Point(63, 19);
            _ChannelComboBox.Name = "_ChannelComboBox";
            _ChannelComboBox.RightToLeft = RightToLeft.No;
            _ChannelComboBox.Size = new System.Drawing.Size(65, 25);
            _ChannelComboBox.TabIndex = 1;
            _ChannelComboBox.SelectedIndexChanged += new EventHandler( ChannelComboBox_SelectedIndexChanged );
            // 
            // _ChannelComboBoxLabel
            // 
            _ChannelComboBoxLabel.AutoSize = true;
            _ChannelComboBoxLabel.BackColor = System.Drawing.Color.Transparent;
            _ChannelComboBoxLabel.Cursor = Cursors.Default;
            _ChannelComboBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            _ChannelComboBoxLabel.Location = new System.Drawing.Point(0, 23);
            _ChannelComboBoxLabel.Name = "_ChannelComboBoxLabel";
            _ChannelComboBoxLabel.RightToLeft = RightToLeft.No;
            _ChannelComboBoxLabel.Size = new System.Drawing.Size(61, 17);
            _ChannelComboBoxLabel.TabIndex = 0;
            _ChannelComboBoxLabel.Text = "Channel: ";
            _ChannelComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _RemoveChannelButton
            // 
            _RemoveChannelButton.BackColor = System.Drawing.SystemColors.Control;
            _RemoveChannelButton.Cursor = Cursors.Default;
            _RemoveChannelButton.Enabled = false;
            _RemoveChannelButton.ForeColor = System.Drawing.SystemColors.ControlText;
            _RemoveChannelButton.Location = new System.Drawing.Point(116, 104);
            _RemoveChannelButton.Name = "_RemoveChannelButton";
            _RemoveChannelButton.RightToLeft = RightToLeft.No;
            _RemoveChannelButton.Size = new System.Drawing.Size(70, 23);
            _RemoveChannelButton.TabIndex = 7;
            _RemoveChannelButton.Text = "Remove";
            _ToolTip.SetToolTip(_RemoveChannelButton, "Remove this channel");
            _RemoveChannelButton.UseVisualStyleBackColor = false;
            _RemoveChannelButton.Click += new EventHandler( RemoveChannelButton_Click );
            // 
            // _ChannelsListView
            // 
            _ChannelsListView.Columns.AddRange(new ColumnHeader[] { _ChannelChannelColumnHeader, _ChannelRangeColumnHeader });
            _ChannelsListView.GridLines = true;
            _ChannelsListView.Location = new System.Drawing.Point(13, 130);
            _ChannelsListView.MultiSelect = false;
            _ChannelsListView.Name = "_ChannelsListView";
            _ChannelsListView.Size = new System.Drawing.Size(174, 89);
            _ChannelsListView.TabIndex = 8;
            _ChannelsListView.UseCompatibleStateImageBehavior = false;
            _ChannelsListView.Click += new EventHandler( ChannelsListView_Click );
            // 
            // _ChannelChannelColumnHeader
            // 
            _ChannelChannelColumnHeader.Text = "Channel";
            // 
            // _ChannelRangeColumnHeader
            // 
            _ChannelRangeColumnHeader.Text = "Range";
            // 
            // _SamplingGroupBox
            // 
            _SamplingGroupBox.Controls.Add(_SampleSizeNumeric);
            _SamplingGroupBox.Controls.Add(_BuffersCountNumeric);
            _SamplingGroupBox.Controls.Add(_SamplingRateNumeric);
            _SamplingGroupBox.Controls.Add(_SmartBufferingCheckBox);
            _SamplingGroupBox.Controls.Add(_ContinuousBufferingCheckBox);
            _SamplingGroupBox.Controls.Add(_SamplingRateNumericLabel);
            _SamplingGroupBox.Controls.Add(_SampleSizeNumericLabel);
            _SamplingGroupBox.Controls.Add(_BuffersCountNumericLabel);
            _SamplingGroupBox.Location = new System.Drawing.Point(8, 255);
            _SamplingGroupBox.Name = "_SamplingGroupBox";
            _SamplingGroupBox.Size = new System.Drawing.Size(200, 157);
            _SamplingGroupBox.TabIndex = 1;
            _SamplingGroupBox.TabStop = false;
            _SamplingGroupBox.Text = "Sampling";
            // 
            // _SampleSizeNumeric
            // 
            _SampleSizeNumeric.Location = new System.Drawing.Point(130, 71);
            _SampleSizeNumeric.Maximum = new decimal(new int[] { 1000000, 0, 0, 0 });
            _SampleSizeNumeric.Name = "_SampleSizeNumeric";
            _SampleSizeNumeric.Size = new System.Drawing.Size(59, 25);
            _SampleSizeNumeric.TabIndex = 5;
            _ToolTip.SetToolTip(_SampleSizeNumeric, "The number of samples per buffer. This is the number of samples processed each ti" + "me a channel buffer is collected.");
            _SampleSizeNumeric.Value = new decimal(new int[] { 1000, 0, 0, 0 });
            _SampleSizeNumeric.ValueChanged += new EventHandler( SampleSizeNumeric_ValueChanged );
            // 
            // _BuffersCountNumeric
            // 
            _BuffersCountNumeric.Location = new System.Drawing.Point(130, 43);
            _BuffersCountNumeric.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            _BuffersCountNumeric.Name = "_BuffersCountNumeric";
            _BuffersCountNumeric.Size = new System.Drawing.Size(59, 25);
            _BuffersCountNumeric.TabIndex = 3;
            _ToolTip.SetToolTip(_BuffersCountNumeric, "The number of signal buffers to queue");
            _BuffersCountNumeric.Value = new decimal(new int[] { 5, 0, 0, 0 });
            _BuffersCountNumeric.ValueChanged += new EventHandler( BuffersCountNumeric_ValueChanged );
            // 
            // _SamplingRateNumeric
            // 
            _SamplingRateNumeric.Location = new System.Drawing.Point(130, 15);
            _SamplingRateNumeric.Maximum = new decimal(new int[] { 100000000, 0, 0, 0 });
            _SamplingRateNumeric.Name = "_SamplingRateNumeric";
            _SamplingRateNumeric.Size = new System.Drawing.Size(59, 25);
            _SamplingRateNumeric.TabIndex = 1;
            _ToolTip.SetToolTip(_SamplingRateNumeric, "The number of samples to collect per second.");
            _SamplingRateNumeric.Value = new decimal(new int[] { 1000, 0, 0, 0 });
            _SamplingRateNumeric.ValueChanged += new EventHandler( SamplingRateNumeric_ValueChanged );
            // 
            // _SmartBufferingCheckBox
            // 
            _SmartBufferingCheckBox.AutoSize = true;
            _SmartBufferingCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            _SmartBufferingCheckBox.Checked = true;
            _SmartBufferingCheckBox.CheckState = CheckState.Checked;
            _SmartBufferingCheckBox.Location = new System.Drawing.Point(31, 129);
            _SmartBufferingCheckBox.Name = "_SmartBufferingCheckBox";
            _SmartBufferingCheckBox.Size = new System.Drawing.Size(124, 21);
            _SmartBufferingCheckBox.TabIndex = 7;
            _SmartBufferingCheckBox.Text = "Smart Buffering: ";
            _ToolTip.SetToolTip(_SmartBufferingCheckBox, "Allocates at least 256 samples for each channel buffer in Strip Chart or slow sam" + "pling modes.");
            _SmartBufferingCheckBox.UseVisualStyleBackColor = true;
            _SmartBufferingCheckBox.CheckedChanged += new EventHandler( SmartBufferingCheckBox_CheckedChanged );
            // 
            // _ContinuousBufferingCheckBox
            // 
            _ContinuousBufferingCheckBox.AutoSize = true;
            _ContinuousBufferingCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            _ContinuousBufferingCheckBox.Checked = true;
            _ContinuousBufferingCheckBox.CheckState = CheckState.Checked;
            _ContinuousBufferingCheckBox.Location = new System.Drawing.Point(1, 103);
            _ContinuousBufferingCheckBox.Name = "_ContinuousBufferingCheckBox";
            _ContinuousBufferingCheckBox.Size = new System.Drawing.Size(155, 21);
            _ContinuousBufferingCheckBox.TabIndex = 6;
            _ContinuousBufferingCheckBox.Text = "Continuous Buffering: ";
            _ToolTip.SetToolTip(_ContinuousBufferingCheckBox, "Check to collect repeated buffering");
            _ContinuousBufferingCheckBox.UseVisualStyleBackColor = true;
            _ContinuousBufferingCheckBox.CheckedChanged += new EventHandler( ContinuousBufferingCheckBox_CheckedChanged );
            // 
            // _SamplingRateNumericLabel
            // 
            _SamplingRateNumericLabel.AutoSize = true;
            _SamplingRateNumericLabel.Location = new System.Drawing.Point(20, 19);
            _SamplingRateNumericLabel.Margin = new Padding(4, 0, 4, 0);
            _SamplingRateNumericLabel.Name = "_SamplingRateNumericLabel";
            _SamplingRateNumericLabel.Size = new System.Drawing.Size(108, 17);
            _SamplingRateNumericLabel.TabIndex = 0;
            _SamplingRateNumericLabel.Text = "Samples/Second:";
            _SamplingRateNumericLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _SampleSizeNumericLabel
            // 
            _SampleSizeNumericLabel.AutoSize = true;
            _SampleSizeNumericLabel.Location = new System.Drawing.Point(47, 75);
            _SampleSizeNumericLabel.Margin = new Padding(4, 0, 4, 0);
            _SampleSizeNumericLabel.Name = "_SampleSizeNumericLabel";
            _SampleSizeNumericLabel.Size = new System.Drawing.Size(81, 17);
            _SampleSizeNumericLabel.TabIndex = 4;
            _SampleSizeNumericLabel.Text = "Sample Size:";
            _SampleSizeNumericLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _BuffersCountNumericLabel
            // 
            _BuffersCountNumericLabel.AutoSize = true;
            _BuffersCountNumericLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            _BuffersCountNumericLabel.Location = new System.Drawing.Point(9, 47);
            _BuffersCountNumericLabel.Margin = new Padding(4, 0, 4, 0);
            _BuffersCountNumericLabel.Name = "_BuffersCountNumericLabel";
            _BuffersCountNumericLabel.Size = new System.Drawing.Size(119, 17);
            _BuffersCountNumericLabel.TabIndex = 2;
            _BuffersCountNumericLabel.Text = "Number of Buffers:";
            _BuffersCountNumericLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _ProcessingGroupBox
            // 
            _ProcessingGroupBox.Controls.Add(_MovingAverageLengthNumeric);
            _ProcessingGroupBox.Controls.Add(_MovingAverageLengthNumericLabel);
            _ProcessingGroupBox.Controls.Add(_CalculateAverageCheckBox);
            _ProcessingGroupBox.Location = new System.Drawing.Point(8, 426);
            _ProcessingGroupBox.Name = "_ProcessingGroupBox";
            _ProcessingGroupBox.Size = new System.Drawing.Size(200, 82);
            _ProcessingGroupBox.TabIndex = 2;
            _ProcessingGroupBox.TabStop = false;
            _ProcessingGroupBox.Text = "Processing";
            // 
            // _MovingAverageLengthNumeric
            // 
            _MovingAverageLengthNumeric.Location = new System.Drawing.Point(137, 48);
            _MovingAverageLengthNumeric.Maximum = new decimal(new int[] { 1000, 0, 0, 0 });
            _MovingAverageLengthNumeric.Name = "_MovingAverageLengthNumeric";
            _MovingAverageLengthNumeric.Size = new System.Drawing.Size(48, 25);
            _MovingAverageLengthNumeric.TabIndex = 2;
            _ToolTip.SetToolTip(_MovingAverageLengthNumeric, "Number of samples in the moving average.");
            _MovingAverageLengthNumeric.Value = new decimal(new int[] { 10, 0, 0, 0 });
            _MovingAverageLengthNumeric.ValueChanged += new EventHandler( MovingAverageLengthNumeric_ValueChanged );
            // 
            // _MovingAverageLengthNumericLabel
            // 
            _MovingAverageLengthNumericLabel.AutoSize = true;
            _MovingAverageLengthNumericLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            _MovingAverageLengthNumericLabel.Location = new System.Drawing.Point(5, 52);
            _MovingAverageLengthNumericLabel.Margin = new Padding(4, 0, 4, 0);
            _MovingAverageLengthNumericLabel.Name = "_MovingAverageLengthNumericLabel";
            _MovingAverageLengthNumericLabel.Size = new System.Drawing.Size(127, 17);
            _MovingAverageLengthNumericLabel.TabIndex = 1;
            _MovingAverageLengthNumericLabel.Text = "Moving Avg. Length:";
            _MovingAverageLengthNumericLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            _ToolTip.SetToolTip(_MovingAverageLengthNumericLabel, "Number of samples in the moving average.");
            // 
            // _CalculateAverageCheckBox
            // 
            _CalculateAverageCheckBox.AutoSize = true;
            _CalculateAverageCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            _CalculateAverageCheckBox.Location = new System.Drawing.Point(16, 21);
            _CalculateAverageCheckBox.Name = "_CalculateAverageCheckBox";
            _CalculateAverageCheckBox.Size = new System.Drawing.Size(134, 21);
            _CalculateAverageCheckBox.TabIndex = 0;
            _CalculateAverageCheckBox.Text = "Calculate Average:";
            _ToolTip.SetToolTip(_CalculateAverageCheckBox, "Check to calculate average voltage");
            _CalculateAverageCheckBox.UseVisualStyleBackColor = true;
            _CalculateAverageCheckBox.CheckedChanged += new EventHandler( CalculateAverageCheckBox_CheckedChanged );
            // 
            // _SignalsTabPage
            // 
            _SignalsTabPage.Controls.Add(_SignalsTableLayoutPanel);
            _SignalsTabPage.Location = new System.Drawing.Point(4, 26);
            _SignalsTabPage.Name = "_SignalsTabPage";
            _SignalsTabPage.Size = new System.Drawing.Size(216, 520);
            _SignalsTabPage.TabIndex = 2;
            _SignalsTabPage.Text = "Signals";
            _SignalsTabPage.UseVisualStyleBackColor = true;
            // 
            // _SignalsTableLayoutPanel
            // 
            _SignalsTableLayoutPanel.ColumnCount = 3;
            _SignalsTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _SignalsTableLayoutPanel.ColumnStyles.Add(new ColumnStyle());
            _SignalsTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _SignalsTableLayoutPanel.Controls.Add(_SignalChannelGroupBox, 1, 1);
            _SignalsTableLayoutPanel.Controls.Add(_SignalSamplingGroupBox, 1, 5);
            _SignalsTableLayoutPanel.Controls.Add(_SignalsGroupBox, 1, 3);
            _SignalsTableLayoutPanel.Dock = DockStyle.Fill;
            _SignalsTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            _SignalsTableLayoutPanel.Name = "_SignalsTableLayoutPanel";
            _SignalsTableLayoutPanel.RowCount = 7;
            _SignalsTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _SignalsTableLayoutPanel.RowStyles.Add(new RowStyle());
            _SignalsTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 8.0f));
            _SignalsTableLayoutPanel.RowStyles.Add(new RowStyle());
            _SignalsTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 8.0f));
            _SignalsTableLayoutPanel.RowStyles.Add(new RowStyle());
            _SignalsTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _SignalsTableLayoutPanel.Size = new System.Drawing.Size(216, 520);
            _SignalsTableLayoutPanel.TabIndex = 0;
            // 
            // _SignalChannelGroupBox
            // 
            _SignalChannelGroupBox.Controls.Add(_AddSignalButton);
            _SignalChannelGroupBox.Controls.Add(_SignalChannelComboBox);
            _SignalChannelGroupBox.Controls.Add(_SignalChannelComboBoxLabel);
            _SignalChannelGroupBox.Location = new System.Drawing.Point(8, 46);
            _SignalChannelGroupBox.Name = "_SignalChannelGroupBox";
            _SignalChannelGroupBox.Size = new System.Drawing.Size(199, 83);
            _SignalChannelGroupBox.TabIndex = 0;
            _SignalChannelGroupBox.TabStop = false;
            _SignalChannelGroupBox.Text = "Signal: ";
            _ToolTip.SetToolTip(_SignalChannelGroupBox, "Select a channel to add as a signal");
            // 
            // _AddSignalButton
            // 
            _AddSignalButton.BackColor = System.Drawing.SystemColors.Control;
            _AddSignalButton.Cursor = Cursors.Default;
            _AddSignalButton.Enabled = false;
            _AddSignalButton.ForeColor = System.Drawing.SystemColors.ControlText;
            _AddSignalButton.Location = new System.Drawing.Point(110, 19);
            _AddSignalButton.Name = "_AddSignalButton";
            _AddSignalButton.RightToLeft = RightToLeft.No;
            _AddSignalButton.Size = new System.Drawing.Size(68, 23);
            _AddSignalButton.TabIndex = 0;
            _AddSignalButton.Text = "Add";
            _ToolTip.SetToolTip(_AddSignalButton, "Add channel as a signal");
            _AddSignalButton.UseVisualStyleBackColor = false;
            // 
            // _SignalChannelComboBox
            // 
            _SignalChannelComboBox.BackColor = System.Drawing.SystemColors.Window;
            _SignalChannelComboBox.Cursor = Cursors.Default;
            _SignalChannelComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            _SignalChannelComboBox.ForeColor = System.Drawing.SystemColors.WindowText;
            _SignalChannelComboBox.Location = new System.Drawing.Point(20, 47);
            _SignalChannelComboBox.Name = "_SignalChannelComboBox";
            _SignalChannelComboBox.RightToLeft = RightToLeft.No;
            _SignalChannelComboBox.Size = new System.Drawing.Size(156, 25);
            _SignalChannelComboBox.TabIndex = 2;
            _SignalChannelComboBox.SelectedValueChanged += new EventHandler( SignalChannelComboBox_SelectedValueChanged );
            // 
            // _SignalChannelComboBoxLabel
            // 
            _SignalChannelComboBoxLabel.AutoSize = true;
            _SignalChannelComboBoxLabel.BackColor = System.Drawing.Color.Transparent;
            _SignalChannelComboBoxLabel.Cursor = Cursors.Default;
            _SignalChannelComboBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            _SignalChannelComboBoxLabel.Location = new System.Drawing.Point(20, 28);
            _SignalChannelComboBoxLabel.Name = "_SignalChannelComboBoxLabel";
            _SignalChannelComboBoxLabel.RightToLeft = RightToLeft.No;
            _SignalChannelComboBoxLabel.Size = new System.Drawing.Size(61, 17);
            _SignalChannelComboBoxLabel.TabIndex = 1;
            _SignalChannelComboBoxLabel.Text = "Channel: ";
            _SignalChannelComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _SignalSamplingGroupBox
            // 
            _SignalSamplingGroupBox.Controls.Add(_SignalMemoryLengthNumeric);
            _SignalSamplingGroupBox.Controls.Add(_SignalBufferSizeNumeric);
            _SignalSamplingGroupBox.Controls.Add(_SignalMemoryLengthNumericLabel);
            _SignalSamplingGroupBox.Controls.Add(_SignalBufferSizeNumericLabel);
            _SignalSamplingGroupBox.Location = new System.Drawing.Point(8, 395);
            _SignalSamplingGroupBox.Name = "_SignalSamplingGroupBox";
            _SignalSamplingGroupBox.Size = new System.Drawing.Size(200, 78);
            _SignalSamplingGroupBox.TabIndex = 2;
            _SignalSamplingGroupBox.TabStop = false;
            _SignalSamplingGroupBox.Text = "Sampling";
            // 
            // _SignalMemoryLengthNumeric
            // 
            _SignalMemoryLengthNumeric.Location = new System.Drawing.Point(114, 48);
            _SignalMemoryLengthNumeric.Maximum = new decimal(new int[] { 100000000, 0, 0, 0 });
            _SignalMemoryLengthNumeric.Name = "_SignalMemoryLengthNumeric";
            _SignalMemoryLengthNumeric.Size = new System.Drawing.Size(69, 25);
            _SignalMemoryLengthNumeric.TabIndex = 3;
            _ToolTip.SetToolTip(_SignalMemoryLengthNumeric, "Number of elements to store for the entire strip chart.");
            _SignalMemoryLengthNumeric.ValueChanged += new EventHandler( SignalMemoryLengthNumeric_ValueChanged );
            // 
            // _SignalBufferSizeNumeric
            // 
            _SignalBufferSizeNumeric.Location = new System.Drawing.Point(114, 16);
            _SignalBufferSizeNumeric.Maximum = new decimal(new int[] { 1000000, 0, 0, 0 });
            _SignalBufferSizeNumeric.Name = "_SignalBufferSizeNumeric";
            _SignalBufferSizeNumeric.Size = new System.Drawing.Size(69, 25);
            _SignalBufferSizeNumeric.TabIndex = 1;
            _ToolTip.SetToolTip(_SignalBufferSizeNumeric, "Number of elements to display for each signal.");
            _SignalBufferSizeNumeric.ValueChanged += new EventHandler( SignalBufferSizeNumeric_ValueChanged );
            // 
            // _SignalMemoryLengthNumericLabel
            // 
            _SignalMemoryLengthNumericLabel.AutoSize = true;
            _SignalMemoryLengthNumericLabel.Location = new System.Drawing.Point(9, 52);
            _SignalMemoryLengthNumericLabel.Margin = new Padding(4, 0, 4, 0);
            _SignalMemoryLengthNumericLabel.Name = "_SignalMemoryLengthNumericLabel";
            _SignalMemoryLengthNumericLabel.Size = new System.Drawing.Size(103, 17);
            _SignalMemoryLengthNumericLabel.TabIndex = 2;
            _SignalMemoryLengthNumericLabel.Text = "Memory Length:";
            _SignalMemoryLengthNumericLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            _ToolTip.SetToolTip(_SignalMemoryLengthNumericLabel, "The signal memory length");
            // 
            // _SignalBufferSizeNumericLabel
            // 
            _SignalBufferSizeNumericLabel.AutoSize = true;
            _SignalBufferSizeNumericLabel.Location = new System.Drawing.Point(40, 20);
            _SignalBufferSizeNumericLabel.Margin = new Padding(4, 0, 4, 0);
            _SignalBufferSizeNumericLabel.Name = "_SignalBufferSizeNumericLabel";
            _SignalBufferSizeNumericLabel.Size = new System.Drawing.Size(72, 17);
            _SignalBufferSizeNumericLabel.TabIndex = 0;
            _SignalBufferSizeNumericLabel.Text = "Buffer Size:";
            _SignalBufferSizeNumericLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            _ToolTip.SetToolTip(_SignalBufferSizeNumericLabel, "The signal buffer size.");
            // 
            // _SignalsGroupBox
            // 
            _SignalsGroupBox.Controls.Add(_UpdateSignalButton);
            _SignalsGroupBox.Controls.Add(_SignalMinNumericUpDownLabel);
            _SignalsGroupBox.Controls.Add(_SignalMaxNumericUpDownLabel);
            _SignalsGroupBox.Controls.Add(_SignalMinNumericUpDown);
            _SignalsGroupBox.Controls.Add(_SignalMaxNumericUpDown);
            _SignalsGroupBox.Controls.Add(_ColorSignalButton);
            _SignalsGroupBox.Controls.Add(_RemoveSignalButton);
            _SignalsGroupBox.Controls.Add(_SignalsListView);
            _SignalsGroupBox.Location = new System.Drawing.Point(8, 143);
            _SignalsGroupBox.Name = "_SignalsGroupBox";
            _SignalsGroupBox.Size = new System.Drawing.Size(200, 238);
            _SignalsGroupBox.TabIndex = 1;
            _SignalsGroupBox.TabStop = false;
            _SignalsGroupBox.Text = "Charted Signals";
            // 
            // _UpdateSignalButton
            // 
            _UpdateSignalButton.BackColor = System.Drawing.SystemColors.Control;
            _UpdateSignalButton.Location = new System.Drawing.Point(31, 198);
            _UpdateSignalButton.Margin = new Padding(4, 3, 4, 3);
            _UpdateSignalButton.Name = "_UpdateSignalButton";
            _UpdateSignalButton.Size = new System.Drawing.Size(138, 27);
            _UpdateSignalButton.TabIndex = 7;
            _UpdateSignalButton.Text = "Update";
            _ToolTip.SetToolTip(_UpdateSignalButton, "Update the Signal Range");
            _UpdateSignalButton.UseVisualStyleBackColor = false;
            _UpdateSignalButton.Click += new EventHandler( UpdateSignalButton_Click );
            // 
            // _SignalMinNumericUpDownLabel
            // 
            _SignalMinNumericUpDownLabel.AutoSize = true;
            _SignalMinNumericUpDownLabel.BackColor = System.Drawing.Color.Transparent;
            _SignalMinNumericUpDownLabel.Cursor = Cursors.Default;
            _SignalMinNumericUpDownLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            _SignalMinNumericUpDownLabel.Location = new System.Drawing.Point(31, 152);
            _SignalMinNumericUpDownLabel.Name = "_SignalMinNumericUpDownLabel";
            _SignalMinNumericUpDownLabel.RightToLeft = RightToLeft.No;
            _SignalMinNumericUpDownLabel.Size = new System.Drawing.Size(69, 17);
            _SignalMinNumericUpDownLabel.TabIndex = 3;
            _SignalMinNumericUpDownLabel.Text = "Minimum: ";
            _SignalMinNumericUpDownLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            _SignalMinNumericUpDown.Validating += new System.ComponentModel.CancelEventHandler( SignalMinNumericUpDown_Validating );
            // 
            // _SignalMaxNumericUpDownLabel
            // 
            _SignalMaxNumericUpDownLabel.AutoSize = true;
            _SignalMaxNumericUpDownLabel.BackColor = System.Drawing.Color.Transparent;
            _SignalMaxNumericUpDownLabel.Cursor = Cursors.Default;
            _SignalMaxNumericUpDownLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            _SignalMaxNumericUpDownLabel.Location = new System.Drawing.Point(106, 152);
            _SignalMaxNumericUpDownLabel.Name = "_SignalMaxNumericUpDownLabel";
            _SignalMaxNumericUpDownLabel.RightToLeft = RightToLeft.No;
            _SignalMaxNumericUpDownLabel.Size = new System.Drawing.Size(72, 17);
            _SignalMaxNumericUpDownLabel.TabIndex = 4;
            _SignalMaxNumericUpDownLabel.Text = "Maximum: ";
            _SignalMaxNumericUpDownLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            _SignalMaxNumericUpDown.Validating += new System.ComponentModel.CancelEventHandler( SignalMaxNumericUpDown_Validating );
            // 
            // _SignalMinNumericUpDown
            // 
            _SignalMinNumericUpDown.DecimalPlaces = 2;
            _SignalMinNumericUpDown.Increment = new decimal(new int[] { 1, 0, 0, 65536 });
            _SignalMinNumericUpDown.Location = new System.Drawing.Point(31, 170);
            _SignalMinNumericUpDown.Maximum = new decimal(new int[] { 10, 0, 0, 0 });
            _SignalMinNumericUpDown.Minimum = new decimal(new int[] { 10, 0, 0, (int)-2147483648L });
            _SignalMinNumericUpDown.Name = "_SignalMinNumericUpDown";
            _SignalMinNumericUpDown.Size = new System.Drawing.Size(63, 25);
            _SignalMinNumericUpDown.TabIndex = 5;
            _SignalMinNumericUpDown.TextAlign = HorizontalAlignment.Center;
            // 
            // _SignalMaxNumericUpDown
            // 
            _SignalMaxNumericUpDown.DecimalPlaces = 2;
            _SignalMaxNumericUpDown.Increment = new decimal(new int[] { 1, 0, 0, 65536 });
            _SignalMaxNumericUpDown.Location = new System.Drawing.Point(107, 170);
            _SignalMaxNumericUpDown.Maximum = new decimal(new int[] { 10, 0, 0, 0 });
            _SignalMaxNumericUpDown.Minimum = new decimal(new int[] { 10, 0, 0, (int)-2147483648L });
            _SignalMaxNumericUpDown.Name = "_SignalMaxNumericUpDown";
            _SignalMaxNumericUpDown.Size = new System.Drawing.Size(63, 25);
            _SignalMaxNumericUpDown.TabIndex = 6;
            _SignalMaxNumericUpDown.TextAlign = HorizontalAlignment.Center;
            // 
            // _ColorSignalButton
            // 
            _ColorSignalButton.BackColor = System.Drawing.SystemColors.Control;
            _ColorSignalButton.Location = new System.Drawing.Point(107, 126);
            _ColorSignalButton.Margin = new Padding(4, 3, 4, 3);
            _ColorSignalButton.Name = "_ColorSignalButton";
            _ColorSignalButton.Size = new System.Drawing.Size(63, 23);
            _ColorSignalButton.TabIndex = 2;
            _ColorSignalButton.Text = "Color";
            _ToolTip.SetToolTip(_ColorSignalButton, "Select a new color for this signal");
            _ColorSignalButton.UseVisualStyleBackColor = false;
            _ColorSignalButton.Click += new EventHandler( ColorSignalButton_Click );
            // 
            // _RemoveSignalButton
            // 
            _RemoveSignalButton.BackColor = System.Drawing.SystemColors.Control;
            _RemoveSignalButton.Cursor = Cursors.Default;
            _RemoveSignalButton.Enabled = false;
            _RemoveSignalButton.ForeColor = System.Drawing.SystemColors.ControlText;
            _RemoveSignalButton.Location = new System.Drawing.Point(31, 126);
            _RemoveSignalButton.Name = "_RemoveSignalButton";
            _RemoveSignalButton.RightToLeft = RightToLeft.No;
            _RemoveSignalButton.Size = new System.Drawing.Size(63, 23);
            _RemoveSignalButton.TabIndex = 1;
            _RemoveSignalButton.Text = "Remove";
            _ToolTip.SetToolTip(_RemoveSignalButton, "Remove this signal");
            _RemoveSignalButton.UseVisualStyleBackColor = false;
            _RemoveSignalButton.Click += new EventHandler( RemoveSignalButton_Click );
            // 
            // _SignalsListView
            // 
            _SignalsListView.Columns.AddRange(new ColumnHeader[] { _SignalChannelColumnHeader, _SignalRangeColumnHeader });
            _SignalsListView.GridLines = true;
            _SignalsListView.Location = new System.Drawing.Point(19, 19);
            _SignalsListView.MultiSelect = false;
            _SignalsListView.Name = "_SignalsListView";
            _SignalsListView.Size = new System.Drawing.Size(162, 103);
            _SignalsListView.TabIndex = 0;
            _SignalsListView.UseCompatibleStateImageBehavior = false;
            _SignalsListView.Click += new EventHandler( SignalsListView_Click );
            // 
            // _SignalChannelColumnHeader
            // 
            _SignalChannelColumnHeader.Text = "Channel";
            // 
            // _SignalRangeColumnHeader
            // 
            _SignalRangeColumnHeader.Text = "Range";
            // 
            // _Chart
            // 
            Chart.AutoScale = false;
            Chart.AxesColor = System.Drawing.Color.Blue;
            Chart.BackGradientAngle = 0.0f;
            Chart.BackGradientColor = System.Drawing.SystemColors.Control;
            Chart.BackgroundStyle = OpenLayers.Chart.Layers.BackgroundStyle.Color;
            Chart.BandMode = OpenLayers.Controls.BandMode.SingleBand;
            Chart.BorderStyle = BorderStyle.Fixed3D;
            Chart.Dock = DockStyle.Fill;
            Chart.Footer = "Footer";
            Chart.FooterFont = new System.Drawing.Font(Font.FontFamily, 8.0f);
            Chart.ForeColor = System.Drawing.Color.Blue;
            Chart.GridColor = System.Drawing.Color.Blue;
            Chart.Location = new System.Drawing.Point(3, 3);
            Chart.Name = "_Chart";
            Chart.SignalBufferLength = 0;
            Chart.Size = new System.Drawing.Size(641, 520);
            Chart.TabIndex = 0;
            Chart.Title = "Analog Input Voltage Time Series";
            Chart.TitleFont = new System.Drawing.Font(Font.FontFamily, 8.25f, System.Drawing.FontStyle.Bold);
            Chart.XDataCurrentRangeMax = 1000.0d;
            Chart.XDataCurrentRangeMin = 0.0d;
            Chart.XDataName = "Time";
            Chart.XDataRangeMax = 1000.0d;
            Chart.XDataRangeMin = 0.0d;
            Chart.XDataUnit = "sec";
            // 
            // _errorProvider
            // 
            _ErrorProvider.ContainerControl = this;
            // 
            // _MainTableLayoutPanel
            // 
            _MainTableLayoutPanel.ColumnCount = 2;
            _MainTableLayoutPanel.ColumnStyles.Add(new ColumnStyle());
            _MainTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100.0f));
            _MainTableLayoutPanel.Controls.Add(_Tabs, 0, 0);
            _MainTableLayoutPanel.Controls.Add(_DisplayTableLayoutPanel, 1, 0);
            _MainTableLayoutPanel.Dock = DockStyle.Fill;
            _MainTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            _MainTableLayoutPanel.Name = "_MainTableLayoutPanel";
            _MainTableLayoutPanel.RowCount = 1;
            _MainTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 100.0f));
            _MainTableLayoutPanel.Size = new System.Drawing.Size(887, 556);
            _MainTableLayoutPanel.TabIndex = 1;
            // 
            // _DisplayTableLayoutPanel
            // 
            _DisplayTableLayoutPanel.ColumnCount = 1;
            _DisplayTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _DisplayTableLayoutPanel.Controls.Add(Chart, 0, 0);
            _DisplayTableLayoutPanel.Controls.Add(_StatusStrip, 0, 1);
            _DisplayTableLayoutPanel.Dock = DockStyle.Fill;
            _DisplayTableLayoutPanel.Location = new System.Drawing.Point(233, 3);
            _DisplayTableLayoutPanel.Name = "_DisplayTableLayoutPanel";
            _DisplayTableLayoutPanel.RowCount = 2;
            _DisplayTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 53.46756f));
            _DisplayTableLayoutPanel.RowStyles.Add(new RowStyle());
            _DisplayTableLayoutPanel.Size = new System.Drawing.Size(651, 550);
            _DisplayTableLayoutPanel.TabIndex = 3;
            // 
            // _StatusStrip
            // 
            _StatusStrip.Items.AddRange(new ToolStripItem[] { _ChartToolStripStatusLabel, _CurrentVoltageToolStripLabel, _AverageVoltageToolStripLabel, _ActionsToolStripDropDownButton });
            _StatusStrip.Location = new System.Drawing.Point(0, 526);
            _StatusStrip.Name = "_StatusStrip";
            _StatusStrip.ShowItemToolTips = true;
            _StatusStrip.Size = new System.Drawing.Size(647, 24);
            _StatusStrip.TabIndex = 3;
            _StatusStrip.Text = "<ready>";
            // 
            // _ChartToolStripStatusLabel
            // 
            _ChartToolStripStatusLabel.BorderSides = ToolStripStatusLabelBorderSides.Left | ToolStripStatusLabelBorderSides.Top | ToolStripStatusLabelBorderSides.Right | ToolStripStatusLabelBorderSides.Bottom;

            _ChartToolStripStatusLabel.BorderStyle = Border3DStyle.Sunken;
            _ChartToolStripStatusLabel.Name = "_ChartToolStripStatusLabel";
            _ChartToolStripStatusLabel.Size = new System.Drawing.Size(456, 19);
            _ChartToolStripStatusLabel.Spring = true;
            _ChartToolStripStatusLabel.Text = "Status: N/A";
            _ChartToolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            _ChartToolStripStatusLabel.ToolTipText = "Status";
            // 
            // _CurrentVoltageToolStripLabel
            // 
            _CurrentVoltageToolStripLabel.BorderSides = ToolStripStatusLabelBorderSides.Left | ToolStripStatusLabelBorderSides.Top | ToolStripStatusLabelBorderSides.Right | ToolStripStatusLabelBorderSides.Bottom;

            _CurrentVoltageToolStripLabel.BorderStyle = Border3DStyle.Sunken;
            _CurrentVoltageToolStripLabel.Name = "_CurrentVoltageToolStripLabel";
            _CurrentVoltageToolStripLabel.Size = new System.Drawing.Size(51, 19);
            _CurrentVoltageToolStripLabel.Text = "Voltage";
            _CurrentVoltageToolStripLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            _CurrentVoltageToolStripLabel.ToolTipText = " Voltage";
            // 
            // _AverageVoltageToolStripLabel
            // 
            _AverageVoltageToolStripLabel.BorderSides = ToolStripStatusLabelBorderSides.Left | ToolStripStatusLabelBorderSides.Top | ToolStripStatusLabelBorderSides.Right | ToolStripStatusLabelBorderSides.Bottom;

            _AverageVoltageToolStripLabel.BorderStyle = Border3DStyle.Sunken;
            _AverageVoltageToolStripLabel.DoubleClickEnabled = true;
            _AverageVoltageToolStripLabel.Name = "_AverageVoltageToolStripLabel";
            _AverageVoltageToolStripLabel.Size = new System.Drawing.Size(54, 19);
            _AverageVoltageToolStripLabel.Text = "Average";
            _AverageVoltageToolStripLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            _AverageVoltageToolStripLabel.ToolTipText = "Mean";
            // 
            // _ActionsToolStripDropDownButton
            // 
            _ActionsToolStripDropDownButton.DropDownItems.AddRange(new ToolStripItem[] { _StartStopToolStripMenuItem, _AbortToolStripMenuItem, _ConfigureToolStripMenuItem });
            _ActionsToolStripDropDownButton.Image = (System.Drawing.Image)resources.GetObject("_ActionsToolStripDropDownButton.Image");
            _ActionsToolStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _ActionsToolStripDropDownButton.Name = "_ActionsToolStripDropDownButton";
            _ActionsToolStripDropDownButton.Size = new System.Drawing.Size(71, 22);
            _ActionsToolStripDropDownButton.Text = "Action";
            _ActionsToolStripDropDownButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // _StartStopToolStripMenuItem
            // 
            _StartStopToolStripMenuItem.Name = "_StartStopToolStripMenuItem";
            _StartStopToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            _StartStopToolStripMenuItem.Text = "START";
            _StartStopToolStripMenuItem.ToolTipText = "Starts data collection and chart (if enabled)";
            _StartStopToolStripMenuItem.CheckedChanged += new EventHandler( StartStopToolStripMenuItem_CheckedChanged );
            _StartStopToolStripMenuItem.Click += new EventHandler( StartStopToolStripMenuItem_Click );
            // 
            // _AbortToolStripMenuItem
            // 
            _AbortToolStripMenuItem.Name = "_AbortToolStripMenuItem";
            _AbortToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            _AbortToolStripMenuItem.Text = "Abort";
            _AbortToolStripMenuItem.Click += new EventHandler( AbortToolStripMenuItem_Click );
            // 
            // _ConfigureToolStripMenuItem
            // 
            _ConfigureToolStripMenuItem.Checked = true;
            _ConfigureToolStripMenuItem.CheckOnClick = true;
            _ConfigureToolStripMenuItem.CheckState = CheckState.Checked;
            _ConfigureToolStripMenuItem.Name = "_ConfigureToolStripMenuItem";
            _ConfigureToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            _ConfigureToolStripMenuItem.Text = "Configure";
            _ConfigureToolStripMenuItem.CheckedChanged += new EventHandler( ConfigureToolStripMenuItem1_Checkchanged );
            // 
            // _InfoProvider
            // 
            _InfoProvider.ContainerControl = this;
            _InfoProvider.Icon = (System.Drawing.Icon)resources.GetObject("_InfoProvider.Icon");
            // 
            // AnalogInputDisplay
            // 
            Controls.Add(_MainTableLayoutPanel);
            Name = "AnalogInputDisplay";
            Size = new System.Drawing.Size(887, 556);
            _Tabs.ResumeLayout(false);
            _BoardTabPage.ResumeLayout(false);
            _BoardTableLayoutPanel.ResumeLayout(false);
            _BoardPanel.ResumeLayout(false);
            _BoardPanel.PerformLayout();
            _BoardGroupBox.ResumeLayout(false);
            _BoardGroupBox.PerformLayout();
            _GlobalGroupBox.ResumeLayout(false);
            _ChartTabPage.ResumeLayout(false);
            _ChartTableLayoutPanel.ResumeLayout(false);
            _ChartTypeTableLayoutPanel.ResumeLayout(false);
            _BandModeGroupBox.ResumeLayout(false);
            _ChartTypeGroupBox.ResumeLayout(false);
            _ChartColorsTableLayoutPanel.ResumeLayout(false);
            _ChartColorsGroupBox.ResumeLayout(false);
            _ChartTitlesTableLayoutPanel.ResumeLayout(false);
            _ChartTitlesGroupBox.ResumeLayout(false);
            _ChartTitlesGroupBox.PerformLayout();
            _ChartEnableTableLayoutPanel.ResumeLayout(false);
            _ChartEnableGroupBox.ResumeLayout(false);
            _ChartEnableGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_RefreshRateNumeric).EndInit();
            _ChannelsTabPage.ResumeLayout(false);
            _ChannelsTableLayoutPanel.ResumeLayout(false);
            _ChannelsGroupBox.ResumeLayout(false);
            _ChannelsGroupBox.PerformLayout();
            _SamplingGroupBox.ResumeLayout(false);
            _SamplingGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_SampleSizeNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_BuffersCountNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_SamplingRateNumeric).EndInit();
            _ProcessingGroupBox.ResumeLayout(false);
            _ProcessingGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_MovingAverageLengthNumeric).EndInit();
            _SignalsTabPage.ResumeLayout(false);
            _SignalsTableLayoutPanel.ResumeLayout(false);
            _SignalChannelGroupBox.ResumeLayout(false);
            _SignalChannelGroupBox.PerformLayout();
            _SignalSamplingGroupBox.ResumeLayout(false);
            _SignalSamplingGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_SignalMemoryLengthNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_SignalBufferSizeNumeric).EndInit();
            _SignalsGroupBox.ResumeLayout(false);
            _SignalsGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_SignalMinNumericUpDown).EndInit();
            ((System.ComponentModel.ISupportInitialize)_SignalMaxNumericUpDown).EndInit();
            ((System.ComponentModel.ISupportInitialize)Chart).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).EndInit();
            _MainTableLayoutPanel.ResumeLayout(false);
            _DisplayTableLayoutPanel.ResumeLayout(false);
            _DisplayTableLayoutPanel.PerformLayout();
            _StatusStrip.ResumeLayout(false);
            _StatusStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_InfoProvider).EndInit();
            ResumeLayout(false);
        }

        private TabControl _Tabs;
        private TabPage _BoardTabPage;
        private TabPage _ChartTabPage;
        private TabPage _SignalsTabPage;
        private TableLayoutPanel _BoardTableLayoutPanel;
        private DeviceChooser.DeviceNameChooser _DeviceNameChooser;
        private CheckBox _OpenCloseCheckBox;
        private TableLayoutPanel _ChartTableLayoutPanel;
        private Label _DeviceNameLabel;
        private TableLayoutPanel _SignalsTableLayoutPanel;
        private GroupBox _SignalChannelGroupBox;
        private ComboBox _SignalChannelComboBox;
        private Label _SignalChannelComboBoxLabel;
        private GroupBox _SignalsGroupBox;
        private Button _AddSignalButton;
        private Button _RemoveSignalButton;
        private ListView _SignalsListView;
        private ColumnHeader _SignalChannelColumnHeader;
        private ColumnHeader _SignalRangeColumnHeader;
        private Button _ColorSignalButton;
        private ToolTip _ToolTip;
        private GroupBox _SignalSamplingGroupBox;
        private Label _SignalBufferSizeNumericLabel;
        private GroupBox _GlobalGroupBox;
        private Button _RestoreDefaultsButton;
        private GroupBox _BandModeGroupBox;
        private RadioButton _MultipleRadioButton;
        private RadioButton _SingleRadioButton;
        private TableLayoutPanel _ChartTypeTableLayoutPanel;
        private GroupBox _ChartTypeGroupBox;
        private RadioButton _StripChartRadioButton;
        private RadioButton _ScopeRadioButton;
        private TableLayoutPanel _ChartColorsTableLayoutPanel;
        private GroupBox _ChartColorsGroupBox;
        private Button _ColorGridsButton;
        private Button _ColorAxesButton;
        private Label _SignalMaxNumericUpDownLabel;
        private NumericUpDown _SignalMaxNumericUpDown;
        private Label _SignalMinNumericUpDownLabel;
        private NumericUpDown _SignalMinNumericUpDown;
        private ErrorProvider _ErrorProvider;
        private TabPage _ChannelsTabPage;
        private TableLayoutPanel _ChannelsTableLayoutPanel;
        private GroupBox _SamplingGroupBox;
        private Label _SamplingRateNumericLabel;
        private Label _SampleSizeNumericLabel;
        private Label _BuffersCountNumericLabel;
        private GroupBox _ChannelsGroupBox;
        private Button _RemoveChannelButton;
        private ListView _ChannelsListView;
        private ColumnHeader _ChannelChannelColumnHeader;
        private ColumnHeader _ChannelRangeColumnHeader;
        private TableLayoutPanel _MainTableLayoutPanel;
        private TableLayoutPanel _DisplayTableLayoutPanel;
        private StatusStrip _StatusStrip;
        private ToolStripStatusLabel _ChartToolStripStatusLabel;
        private ToolStripStatusLabel _CurrentVoltageToolStripLabel;
        private ToolStripStatusLabel _AverageVoltageToolStripLabel;
        private ToolStripDropDownButton _ActionsToolStripDropDownButton;
        private ToolStripMenuItem _StartStopToolStripMenuItem;
        private ToolStripMenuItem _AbortToolStripMenuItem;
        private ToolStripMenuItem _ConfigureToolStripMenuItem;
        private Panel _BoardPanel;
        private TableLayoutPanel _ChartTitlesTableLayoutPanel;
        private GroupBox _ChartTitlesGroupBox;
        private TextBox _ChartTitleTextBox;
        private Label _ChartTitleTextBoxLabel;
        private TextBox _ChartFooterTextBox;
        private Label _ChartFooterTextBoxLabel;
        private Button _UpdateSignalButton;
        private Label _SignalMemoryLengthNumericLabel;
        private TableLayoutPanel _ChartEnableTableLayoutPanel;
        private GroupBox _ChartEnableGroupBox;
        private CheckBox _EnableChartingCheckBox;
        private CheckBox _DisplayAverageCheckBox;
        private CheckBox _DisplayVoltageCheckBox;
        private GroupBox _BoardGroupBox;
        private ComboBox _BoardInputRangesComboBox;
        private Label _BoardInputRangesComboBoxLabel;
        private TextBox _ChannelNameTextBox;
        private Label _ChannelNameTextBoxLabel;
        private Button _AddChannelButton;
        private ComboBox _ChannelRangesComboBox;
        private Label _ChannelRangeComboBoxLabel;
        private ComboBox _ChannelComboBox;
        private Label _ChannelComboBoxLabel;
        private GroupBox _ProcessingGroupBox;
        private Label _MovingAverageLengthNumericLabel;
        private CheckBox _CalculateAverageCheckBox;
        private Button _ConfigureButton;
        private Button _ResetButton;
        private CheckBox _ContinuousBufferingCheckBox;
        private CheckBox _SmartBufferingCheckBox;
        private ErrorProvider _InfoProvider;
        private Label _RefreshRateNumericLabel;
        private NumericUpDown _SamplingRateNumeric;
        private NumericUpDown _SampleSizeNumeric;
        private NumericUpDown _BuffersCountNumeric;
        private NumericUpDown _MovingAverageLengthNumeric;
        private NumericUpDown _SignalMemoryLengthNumeric;
        private NumericUpDown _SignalBufferSizeNumeric;
        private NumericUpDown _RefreshRateNumeric;
    }
}
