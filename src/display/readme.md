# About

isr.IO.OL.Display is a .Net library encapsulating the open layers display control.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

isr.IO.OL.Display is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Open Layers Repository].

[Open Layers Repository]: https://bitbucket.org/davidhary/dn.openlayers

